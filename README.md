# AIKit - AI algorithms library

AIKit is a library of various automated planning ang game theory algorithms.

## Planning algorithms and frameworks
- PDDL parser, generator, modification utils
- FDR parser, generator, modification utils
- External PDDL/FDR planners interfaces

## Game Theory algorithms
- Double Oracle

## Experiments management
- experiment resuming and results caching
- generator of experiments parameters
- results data proccessing

## Algorithms from the following articles

1. [Using Classical Planning in Adversarial Problems](https://doi.org/10.1109/ictai.2019.00185)

2. [Acting in Dynamic Environments: Models of Agent-Environment Interaction](https://icaps20subpages.icaps-conference.org/wp-content/uploads/2020/10/KEPS-2020_paper_22.pdf)

3. [Towards Generating Effective Plans in Zero-sum Games by Estimating Strategy of an Adversary](https://plansig2020.files.wordpress.com/2020/12/plansig_2020_paper_5.pdf)

4. [Planning Against Adversary in Zero-Sum Games: Heuristics for Selecting and Ordering Critical Actions](https://aaai.org/ocs/index.php/SOCS/SOCS20/paper/view/18514)

5. [Adversary Strategy Sampling for Effective Plan Generation](https://ojs.aaai.org/index.php/SOCS/article/view/18571)

6. [Competing for Resources: Estimating Adversary Strategy for Effective Plan Generation]
    * The main algorithm is implemented in [Sources/AIKit/SamplingPlanner/SamplingAdversaryActionPlanner.swift](Sources/AIKit/SamplingPlanner/SamplingAdversaryActionPlanner.swift)



