(define (problem taxi-5)
	(:domain taxi)
	(:objects 
		player0 player1 - player
		car0 car1 - unit
		person0 person1 person2 person3 - person
		n0 n1 n2 n3 n4 n5 n6 n7 n8 n9 n10 n11 n12 n13 n14 - location
		)
	(:init 
		(is-enemy player0 player1)
		(has-unit player0 car0)
		(is-enemy player1 player0)
		(has-unit player1 car1)
		
		(at-unit car0 n0)
		(empty car0)
		(at-unit car1 n5)
		(empty car1)
		
		(at-person person0 n4)
		(destination person0 n6)
		(= (payment person0) 60)
		(at-person person1 n13)
		(destination person1 n3)
		(= (payment person1) 90)
		(at-person person2 n2)
		(destination person2 n0)
		(= (payment person2) 80)
		(at-person person3 n9)
		(destination person3 n14)
		(= (payment person3) 40)
		
		(connected n7 n11)
		(connected n11 n7)
		(= (distance n7 n11) 1)
		(= (distance n11 n7) 1)
		(connected n11 n13)
		(connected n13 n11)
		(= (distance n11 n13) 1)
		(= (distance n13 n11) 1)
		(connected n11 n9)
		(connected n9 n11)
		(= (distance n11 n9) 2)
		(= (distance n9 n11) 2)
		(connected n13 n9)
		(connected n9 n13)
		(= (distance n13 n9) 1)
		(= (distance n9 n13) 1)
		(connected n13 n14)
		(connected n14 n13)
		(= (distance n13 n14) 2)
		(= (distance n14 n13) 2)
		(connected n9 n14)
		(connected n14 n9)
		(= (distance n9 n14) 1)
		(= (distance n14 n9) 1)
		(connected n9 n2)
		(connected n2 n9)
		(= (distance n9 n2) 3)
		(= (distance n2 n9) 3)
		(connected n14 n2)
		(connected n2 n14)
		(= (distance n14 n2) 3)
		(= (distance n2 n14) 3)
		(connected n14 n5)
		(connected n5 n14)
		(= (distance n14 n5) 2)
		(= (distance n5 n14) 2)
		(connected n2 n5)
		(connected n5 n2)
		(= (distance n2 n5) 2)
		(= (distance n5 n2) 2)
		(connected n5 n0)
		(connected n0 n5)
		(= (distance n5 n0) 3)
		(= (distance n0 n5) 3)
		(connected n5 n6)
		(connected n6 n5)
		(= (distance n5 n6) 1)
		(= (distance n6 n5) 1)
		(connected n0 n6)
		(connected n6 n0)
		(= (distance n0 n6) 3)
		(= (distance n6 n0) 3)
		(connected n0 n3)
		(connected n3 n0)
		(= (distance n0 n3) 4)
		(= (distance n3 n0) 4)
		(connected n6 n3)
		(connected n3 n6)
		(= (distance n6 n3) 1)
		(= (distance n3 n6) 1)
		(connected n6 n4)
		(connected n4 n6)
		(= (distance n6 n4) 3)
		(= (distance n4 n6) 3)
		(connected n3 n4)
		(connected n4 n3)
		(= (distance n3 n4) 3)
		(= (distance n4 n3) 3)
		(connected n3 n10)
		(connected n10 n3)
		(= (distance n3 n10) 2)
		(= (distance n10 n3) 2)
		(connected n4 n10)
		(connected n10 n4)
		(= (distance n4 n10) 2)
		(= (distance n10 n4) 2)
		(connected n10 n12)
		(connected n12 n10)
		(= (distance n10 n12) 1)
		(= (distance n12 n10) 1)
		(connected n10 n1)
		(connected n1 n10)
		(= (distance n10 n1) 2)
		(= (distance n1 n10) 2)
		(connected n12 n1)
		(connected n1 n12)
		(= (distance n12 n1) 2)
		(= (distance n1 n12) 2)
		(connected n12 n8)
		(connected n8 n12)
		(= (distance n12 n8) 1)
		(= (distance n8 n12) 1)
		(connected n1 n8)
		(connected n8 n1)
		(= (distance n1 n8) 1)
		(= (distance n8 n1) 1)
		(connected n8 n7)
		(connected n7 n8)
		(= (distance n8 n7) 3)
		(= (distance n7 n8) 3)
		(connected n8 n11)
		(connected n11 n8)
		(= (distance n8 n11) 3)
		(= (distance n11 n8) 3)

		(= (load-cost) 1)
		(= (unload-cost) 1)
		
		)
	(:goal (and 
		(preference delivered-player0-person0 (is-delivered person0 player0))
		(preference delivered-player0-person1 (is-delivered person1 player0))
		(preference delivered-player0-person2 (is-delivered person2 player0))
		(preference delivered-player0-person3 (is-delivered person3 player0))))

	(:metric minimize (+ 
		(* 60 (is-violated delivered-player0-person0))
		(* 90 (is-violated delivered-player0-person1))
		(* 80 (is-violated delivered-player0-person2))
		(* 40 (is-violated delivered-player0-person3))))

	(:goal (and 
		(preference delivered-player1-person0 (is-delivered person0 player1))
		(preference delivered-player1-person1 (is-delivered person1 player1))
		(preference delivered-player1-person2 (is-delivered person2 player1))
		(preference delivered-player1-person3 (is-delivered person3 player1))))

	(:metric minimize (+ 
		(* 60 (is-violated delivered-player1-person0))
		(* 90 (is-violated delivered-player1-person1))
		(* 80 (is-violated delivered-player1-person2))
		(* 40 (is-violated delivered-player1-person3))))

	
)