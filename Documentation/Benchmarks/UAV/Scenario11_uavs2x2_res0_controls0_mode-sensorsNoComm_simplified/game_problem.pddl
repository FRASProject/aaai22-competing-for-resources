(define (problem grid-5)
(:domain resource-hunting-classical)
(:objects 
loc-21
loc-28
loc-34
loc-41
loc-42
loc-49
loc-55
loc-62
 - location
p1
p2
 - player
r5
r6
r7
r8
 - resource
s1
s2
 - sensor
uav1
uav2
uav3
uav4
 - unit
)
(:init
(at-resource r5 loc-21)
(at-resource r6 loc-34)
(at-resource r7 loc-49)
(at-resource r8 loc-62)

(at-unit uav1 loc-28)
(at-unit uav2 loc-42)
(at-unit uav3 loc-41)
(at-unit uav4 loc-55)

(available r5)
(available r6)
(available r7)
(available r8)

(connected loc-21 loc-28)
(connected loc-21 loc-34)
(connected loc-21 loc-41)
(connected loc-21 loc-42)
(connected loc-21 loc-49)
(connected loc-21 loc-55)
(connected loc-21 loc-62)
(connected loc-28 loc-21)
(connected loc-28 loc-34)
(connected loc-28 loc-41)
(connected loc-28 loc-42)
(connected loc-28 loc-49)
(connected loc-28 loc-55)
(connected loc-28 loc-62)
(connected loc-34 loc-21)
(connected loc-34 loc-28)
(connected loc-34 loc-41)
(connected loc-34 loc-42)
(connected loc-34 loc-49)
(connected loc-34 loc-55)
(connected loc-34 loc-62)
(connected loc-41 loc-21)
(connected loc-41 loc-28)
(connected loc-41 loc-34)
(connected loc-41 loc-42)
(connected loc-41 loc-49)
(connected loc-41 loc-55)
(connected loc-41 loc-62)
(connected loc-42 loc-21)
(connected loc-42 loc-28)
(connected loc-42 loc-34)
(connected loc-42 loc-41)
(connected loc-42 loc-49)
(connected loc-42 loc-55)
(connected loc-42 loc-62)
(connected loc-49 loc-21)
(connected loc-49 loc-28)
(connected loc-49 loc-34)
(connected loc-49 loc-41)
(connected loc-49 loc-42)
(connected loc-49 loc-55)
(connected loc-49 loc-62)
(connected loc-55 loc-21)
(connected loc-55 loc-28)
(connected loc-55 loc-34)
(connected loc-55 loc-41)
(connected loc-55 loc-42)
(connected loc-55 loc-49)
(connected loc-55 loc-62)
(connected loc-62 loc-21)
(connected loc-62 loc-28)
(connected loc-62 loc-34)
(connected loc-62 loc-41)
(connected loc-62 loc-42)
(connected loc-62 loc-49)
(connected loc-62 loc-55)

(free uav1)
(free uav2)
(free uav3)
(free uav4)

(has-sensor uav1 s1)
(has-sensor uav1 s2)
(has-sensor uav2 s1)
(has-sensor uav3 s1)
(has-sensor uav3 s2)
(has-sensor uav4 s1)

(has-unit uav1 p1)
(has-unit uav2 p1)
(has-unit uav3 p2)
(has-unit uav4 p2)

(required-sensor r6 s2)
(required-sensor r7 s1)

(required-two-sensors r5 s1 s2)
(required-two-sensors r8 s1 s2)

(= (move-cost loc-28 loc-42) 1)
(= (move-cost loc-41 loc-55) 1)
(= (move-cost loc-42 loc-28) 1)
(= (move-cost loc-55 loc-41) 1)
(= (move-cost loc-21 loc-34) 2)
(= (move-cost loc-21 loc-49) 2)
(= (move-cost loc-34 loc-21) 2)
(= (move-cost loc-34 loc-49) 2)
(= (move-cost loc-34 loc-62) 2)
(= (move-cost loc-49 loc-21) 2)
(= (move-cost loc-49 loc-34) 2)
(= (move-cost loc-49 loc-62) 2)
(= (move-cost loc-62 loc-34) 2)
(= (move-cost loc-62 loc-49) 2)
(= (move-cost loc-21 loc-62) 4)
(= (move-cost loc-62 loc-21) 4)
(= (move-cost loc-21 loc-41) 7)
(= (move-cost loc-41 loc-21) 7)
(= (move-cost loc-42 loc-62) 7)
(= (move-cost loc-62 loc-42) 7)
(= (move-cost loc-21 loc-28) 8)
(= (move-cost loc-21 loc-55) 8)
(= (move-cost loc-28 loc-21) 8)
(= (move-cost loc-28 loc-34) 8)
(= (move-cost loc-28 loc-62) 8)
(= (move-cost loc-34 loc-28) 8)
(= (move-cost loc-49 loc-55) 8)
(= (move-cost loc-55 loc-21) 8)
(= (move-cost loc-55 loc-49) 8)
(= (move-cost loc-55 loc-62) 8)
(= (move-cost loc-62 loc-28) 8)
(= (move-cost loc-62 loc-55) 8)
(= (move-cost loc-21 loc-42) 9)
(= (move-cost loc-34 loc-41) 9)
(= (move-cost loc-34 loc-42) 9)
(= (move-cost loc-41 loc-34) 9)
(= (move-cost loc-41 loc-49) 9)
(= (move-cost loc-41 loc-62) 9)
(= (move-cost loc-42 loc-21) 9)
(= (move-cost loc-42 loc-34) 9)
(= (move-cost loc-42 loc-49) 9)
(= (move-cost loc-49 loc-41) 9)
(= (move-cost loc-49 loc-42) 9)
(= (move-cost loc-62 loc-41) 9)
(= (move-cost loc-28 loc-49) 10)
(= (move-cost loc-34 loc-55) 10)
(= (move-cost loc-49 loc-28) 10)
(= (move-cost loc-55 loc-34) 10)
(= (move-cost loc-28 loc-41) 15)
(= (move-cost loc-41 loc-28) 15)
(= (move-cost loc-42 loc-55) 15)
(= (move-cost loc-55 loc-42) 15)
(= (move-cost loc-28 loc-55) 16)
(= (move-cost loc-41 loc-42) 16)
(= (move-cost loc-42 loc-41) 16)
(= (move-cost loc-55 loc-28) 16)

(= (sample-cost r5) 1)
(= (sample-cost r6) 1)
(= (sample-cost r7) 1)
(= (sample-cost r8) 1)

(= (sample-two-cost r5) 1)
(= (sample-two-cost r6) 1)
(= (sample-two-cost r7) 1)
(= (sample-two-cost r8) 1)

)
 ; Player 1
(:goal
(and 
(preference p1-r5-collected (collected r5 p1))
(preference p1-r6-collected (collected r6 p1))
(preference p1-r7-collected (collected r7 p1))
(preference p1-r8-collected (collected r8 p1))

))
(:metric minimize (+
(* 100 (is-violated p1-r5-collected))
(* 100 (is-violated p1-r6-collected))
(* 100 (is-violated p1-r7-collected))
(* 100 (is-violated p1-r8-collected))
)

)
 ; Player 2
(:goal
(and 
(preference p2-r5-collected (collected r5 p2))
(preference p2-r6-collected (collected r6 p2))
(preference p2-r7-collected (collected r7 p2))
(preference p2-r8-collected (collected r8 p2))

))
(:metric minimize (+
(* 100 (is-violated p2-r5-collected))
(* 100 (is-violated p2-r6-collected))
(* 100 (is-violated p2-r7-collected))
(* 100 (is-violated p2-r8-collected))
)

)
)
