//
//  ChartUtils.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 9/20/20.
//

import Foundation


public enum ChartUtils {
  public enum Series: Codable {
    public init(from decoder: Decoder) throws {
      fatalError("Implement me")
    }
    
    case double([Double])
    case int([Int])
    case string([String])
    public init(_ a: [Double]) {
      self = .double(a)
    }
    public init(_ a: [Int]) {
      self = .int(a)
    }
    public init(_ s: [String]) {
      self = .string(s)
    }
    public func encode(to encoder: Encoder) throws {
      var container = encoder.singleValueContainer()
      switch self {
      case let .double(array):
        try container.encode(array)
      case let .int(array):
        try container.encode(array)
      case let .string(array):
        try container.encode(array)
      }
    }
    public var doubles: [Double]? {
      guard case let .double(array) = self else {
        return nil
      }
      return array
    }
  }
  public enum ChartType: String, Codable {
    case errorbar, line, bar, xvline, scatter, annotate
  }
  public struct Dataset: Codable {
    public let name: String
    public let type: ChartType
    public let data: [String: Series]
    public let marker: String?
    public init(name: String, type: ChartType, marker: String? = nil, data: [String: Series]) {
      self.name = name
      self.type = type
      self.data = data
      self.marker = marker
    }
  }
  public struct ChartParametersData: ExperimentData, ExperimentDataResultDictionary {
    public var asDictionary: [String : AIKitData] {
      fatalError()
    }
    
    public let plotTitle: String
    public let datasets: [Dataset]
    public let parameters: [String: String]
    public let state: ExperimentDataItemState
    public init(plotTitle: String, datasets: [Dataset], parameters: [String: String]) {
      self.plotTitle = plotTitle
      self.datasets = datasets
      self.parameters = parameters
      self.state = .finished
    }
  }
  public struct FigureParametersData: Encodable {
    public let figureTitle: String
    public let plots: [ChartParametersData]
    public let parameters: [String: String]
    public init(figureTitle: String, plots: [ChartParametersData], parameters: [String: String]) {
      self.figureTitle = figureTitle
      self.plots = plots
      self.parameters = parameters
    }
  }
  
  public static func datasetToExperimentParameters(dataset: Dataset) -> AIKitData {
    precondition(dataset.data.count >= 2)
    return .dictionary([
      "x": .array(dataset.data["x"]!.doubles!.map { AIKitData($0)}),
      "y": .array(dataset.data["y"]!.doubles!.map { AIKitData($0)})
    ])
  }
  
  public static func makeFigure(
    chart data: FigureParametersData,
    fileNames: [String],
    jsonFileName: String? = nil) throws
  {
    let visualizationCommand = ExperimentManager.defaultManager.systemConfig["graphGeneratorPath"]!.string!
    let data = try JSONEncoder().encode(data)
#if os(Linux)
    // On linux, for some reason, spawning more than one process in parallel causes strange bugs.
    ExperimentManager.defaultManager.semaphore1.wait()
    defer {
      ExperimentManager.defaultManager.semaphore1.signal()
    }
#endif
    if let jsonFileName = jsonFileName {
      try data.write(to: URL(fileURLWithPath: jsonFileName))
    }
    let proc = Process()
    proc.executableURL = URL(fileURLWithPath: visualizationCommand)
    proc.arguments = fileNames
    let pipe = Pipe()
    proc.standardInput = pipe
    try proc.run()
    let writeHandle = pipe.fileHandleForWriting
    writeHandle.write(data)
    writeHandle.closeFile()
    proc.waitUntilExit()
    if proc.terminationStatus != 0 {
      throw AIKitError(message: "Python visualization command \(visualizationCommand) returned non-zero code: \(proc.terminationStatus)")
    }
  }
  
  public static func makeLineGraph(
    chart dataParameters: ChartParametersData,
    fileNames: [String],
    jsonFileName: String? = nil,
    figureParameters: [String: String] = [:]) throws
  {
    try makeFigure(
      chart: FigureParametersData(
        figureTitle: dataParameters.plotTitle,
        plots: [dataParameters],
        parameters: figureParameters),
      fileNames: fileNames,
      jsonFileName: jsonFileName)
  }
}
