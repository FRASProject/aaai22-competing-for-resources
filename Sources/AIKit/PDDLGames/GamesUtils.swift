//
//  File.swift
//  
//
//  Created by Pavel Rytir on 7/8/21.
//

import Foundation

public enum GamesUtils {
  public static func getGameStatistics(
    _ parameters: ExperimentParameters) throws -> AIKitTable<AIKitData>
  {
    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
    var result = parameters.indexDictionary
    result["p1Units"] = .integer(game.playersUnits[0].count)
    result["p2Units"] = .integer(game.playersUnits[1].count)
    result["p1Goals"] = .integer(game.softGoals[0].count)
    result["p2Goals"] = .integer(game.softGoals[1].count)
    result["p1MaxGameValue"] = .integer(game.goalsWeights(of: 0).sum)
    result["p2MaxGameValue"] = .integer(game.goalsWeights(of: 1).sum)
    if let gameDescription = ExperimentManager.defaultManager.findDescription(parameters.data) {
      result["domainDesc"] = .string(gameDescription.0)
      result["ProblemNo"] = .string(gameDescription.1)
      result["gameDesc"] = .string(gameDescription.2)
    }
    return AIKitTable(rows: [result])
  }
}
