//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/6/21.
//

import Foundation
import MathLib

public class PDDLGameFDRRepresentationAnalyticsLogic {
  public let gameDomainPDDLRaw: String
  public let gameProblemPDDLRaw: String
  public let groundingCacheDir: URL?
  public let game: PDDLGame
  public let players: [String]
  public let playersUnits: [[String]]
  public let softGoals: [[(name: String?, goal: PDDLAtomicTerm)]] // [player][goalnumber]  Allow more complicated goals?
  
  public init(
    gameDomainPDDLRaw: String,
    gameProblemPDDLRaw: String,
    groundingCacheDir: URL? = nil) throws
  {
    self.gameDomainPDDLRaw = gameDomainPDDLRaw
    self.gameProblemPDDLRaw = gameProblemPDDLRaw
    self.groundingCacheDir = groundingCacheDir
    let domain = try PDDLDomainProblemParser.parse(domain: gameDomainPDDLRaw)
    let problem = try PDDLDomainProblemParser.parse(gameProblem: gameProblemPDDLRaw)
    self.game = PDDLGame(domain: domain, problem: problem)
    self.players = game.players
    self.playersUnits = game.playersUnits
    self.softGoals = game.softGoals
  }
  
  public func filterActionsBy(unitIdx: Int, player: Int, actions: [PlanAction]) -> [PlanAction] {
    let unit = playersUnits[player][unitIdx]
    return actions.filter { $0.parameters.contains(where: {$0 == unit}) }
  }
  public lazy var groundedPlayersProblems: [TemporalProblemFDRCoding]! = {
    groundedPlayersProblemsWithComputationDuration.map {$0.0}
  }()
  public lazy var groundingComputationDuration: Double = {
    groundedPlayersProblemsWithComputationDuration.map {$0.1}.sum
  }()
  public lazy var groundedPlayersProblemsWithComputationDuration: [(TemporalProblemFDRCoding, Double)]! = {
    () -> [(TemporalProblemFDRCoding, Double)]? in
    let grounder = FastDownwardGrounder(temporal: true, runPreprocessing: true)
    let groundingProblems = self.game.createProblemsForGrounding()
    let groundingResults = try? groundingProblems.enumerated().map {
      (player, problem) -> (TemporalProblemFDRCoding, Double) in
      let cacheFile = groundingCacheDir?.appendingPathComponent("sas_repr_player\(player).json")
      let tfdOutput = try grounder.ground(
        problemPDDL: problem.problem.text,
        domainPDDL: problem.domain.text,
        fdrCache: cacheFile)!
      return (
        TemporalFastDownwardParser.parseSASNew(
          inputSas: tfdOutput.sas,
          inputVariables: tfdOutput.variables!),
        tfdOutput.computationDuration
      )
    }
    return groundingResults
  }()
  
  public lazy var groundedPlayersUnitsProblems: [[TemporalProblemFDRCoding]]! = {
    groundedPlayersUnitsProblemsWithComputatinDuration.map {$0.map {$0.0}}
  }()
  public lazy var groundingUnitsComputationDuration: Double = {
    groundedPlayersUnitsProblemsWithComputatinDuration.flatMap { $0.map {$0.1}}.sum
  }()
  public lazy var groundedPlayersUnitsProblemsWithComputatinDuration: [[(TemporalProblemFDRCoding, Double)]]! = {
    let grounder = FastDownwardGrounder(temporal: true, runPreprocessing: true)
    let subproblems = self.game.createProblemsForGrounding().map {$0.createSubProblemsForUnits()}
    let groundingResults = try? subproblems.enumerated().map {
      (player, unitsProblems) in
      try unitsProblems.enumerated().map {
        (unit, groundingProblem) -> (TemporalProblemFDRCoding, Double) in
        let cacheFile = groundingCacheDir?.appendingPathComponent("sas_repr_player\(player)_unit\(unit).json")
        let tfdOutput = try grounder.ground(
          problemPDDL: groundingProblem.problem.text,
          domainPDDL: groundingProblem.domain.text,
          fdrCache: cacheFile)!
        return (
          TemporalFastDownwardParser.parseSASNew(
            inputSas: tfdOutput.sas,
            inputVariables: tfdOutput.variables!),
          tfdOutput.computationDuration)
      }
    }
    return groundingResults
  }()
  
  
  public lazy var groundedActionsPlayersUnitsSet: [[Set<PlanAction>]]! = {
    groundedPlayersUnitsProblems.map {
      $0.map {Set($0.operators.map {PlanAction($0)})}}
  }()
  
  public lazy var actionInstancesPlayersUnits: [[[String: Set<PlanAction>]]]! = {
    groundedActionsPlayersUnitsSet.map {$0.map{
      Dictionary(grouping: $0, by: {$0.name}).mapValues {Set($0)}
    }}
  }()
  
  public lazy var serialGroundedActionsPlayersSet: [Set<PlanAction>]! = {
    groundedPlayersUnitsProblems.map {
      Set($0.flatMap {$0.operators.map {PlanAction($0)}})}
  }()
  
  public lazy var groundedActionsPlayersSet: [Set<PlanAction>]! = {
    groundedPlayersProblems.map {Set($0.operators.map {PlanAction($0)})}
  }()
  
  public lazy var groundedActionsSet: Set<PlanAction>! = {
    groundedActionsPlayersSet.reduce(Set(), {$0.union($1)})
  }()
  
  public lazy var serialPlayersFreeClusters: [[PDDLNameLiteral: CriticalCluster]]! = {
    var playersFreeClusters = [[PDDLNameLiteral: CriticalCluster]]()
    
    for idx in serialGroundedActionsPlayersSet.indices {
      let adversaryActions = serialGroundedActionsPlayersSet.enumerated().filter {
        $0.offset != idx
      }.flatMap {$0.element}
      playersFreeClusters.append(game.domain.restrictingNumberOfObjectsOf(
        type: PDDLDomain.unitType,
        to: 1)!.findAdversarialCriticalDurativeActions(
        playerGroundedActions: Array(serialGroundedActionsPlayersSet[idx]),
        adversariesGroundedActions: adversaryActions, initState: game.problem.initState))
    }
    return playersFreeClusters
  }()
  
  public lazy var playersUnitsFreeClusters: [[[PDDLNameLiteral: CriticalCluster]]]! = {
    var playersUnitsFreeClusters = [[[PDDLNameLiteral: CriticalCluster]]]()
    for playerIdx in groundedActionsPlayersUnitsSet.indices {
      let adversaryActions = groundedActionsPlayersUnitsSet.enumerated().filter {
        $0.offset != playerIdx
      }.flatMap {$0.element}.flatMap {$0}
      var unitsFreeClusters = [[PDDLNameLiteral: CriticalCluster]]()
      for unitIdx in groundedActionsPlayersUnitsSet[playerIdx].indices {
        unitsFreeClusters.append(game.domain.restrictingNumberOfObjectsOf(
          type: PDDLDomain.unitType,
          to: 1)!.findAdversarialCriticalDurativeActions(
          playerGroundedActions: Array(groundedActionsPlayersUnitsSet[playerIdx][unitIdx]),
          adversariesGroundedActions: adversaryActions,
          initState: game.problem.initState))
      }
      playersUnitsFreeClusters.append(unitsFreeClusters)
    }
    return playersUnitsFreeClusters
  }()
  
  public lazy var playersFreeClusters: [[PDDLNameLiteral: CriticalCluster]]! = {
    var playersFreeClusters = [[PDDLNameLiteral: CriticalCluster]]()
    
    for idx in groundedActionsPlayersSet.indices {
      let adversaryActions = groundedActionsPlayersSet.enumerated().filter {$0.offset != idx}.map {
        $0.element}.reduce([],+)
      playersFreeClusters.append(game.domain.findAdversarialCriticalDurativeActions(
        playerGroundedActions: Array(groundedActionsPlayersSet[idx]),
        adversariesGroundedActions: adversaryActions,
        initState: game.problem.initState))
    }
    return playersFreeClusters
  }()
  
  public lazy var freeClustersAllPlayers: [(key: PDDLNameLiteral, value: CriticalCluster)] = {
    playersFreeClusters.reduce([], +).sorted(by: {$0.key < $1.key})
  }()
  
  public lazy var serialFreeClustersAllPlayers: [(key: PDDLNameLiteral, value: CriticalCluster)] = {
    playersUnitsFreeClusters.flatMap { $0.flatMap {Array($0)} }.sorted(by: {$0.key < $1.key})
  }()
  
  
  private static func identifyGoalsClusters(
    problem: TemporalProblemFDRCoding,
    softGoals: [(name: String?, goal: PDDLAtomicTerm)],
    freeClusters: [CriticalCluster]
  ) -> [[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]]
  {
    var goalsCriticalAction = [[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]]()
    let coding = BitVectorCoding(
      from: BitVectorTemporalCoding(
        from: problem))
    for goal in softGoals {
      var goalCriticalActions = [(
        criticalAction: PlanAction,
        criticalFact: PDDLNameLiteral,
        adversarialActions: Set<PlanAction>
      )]()
      var goalCriticalActionsAdded = Set<Triple<PlanAction, PDDLNameLiteral, Set<PlanAction>>>()
      let disjointLandmarks = BackChaining.start(
        goal: SimpleExpression(
          positive: BitVectorState(
            fact: coding.findIndex(of: PDDLAtomicNameFormula(atomicTerm: goal.goal)!)!),
          negative: BitVectorState()),
        initState: coding.initState,
        operators: coding.operators).map {$0.map {PlanAction($0)}}
      
      let landmarks = disjointLandmarks.flatMap {$0}.sorted()
      
      for landmark in landmarks {
        for cluster in freeClusters where cluster.criticalActions.contains(landmark) {
          if !goalCriticalActionsAdded.contains(Triple(landmark, cluster.criticalFact, cluster.adversarialActions)) {
            goalCriticalActionsAdded.insert(Triple(landmark, cluster.criticalFact, cluster.adversarialActions))
            goalCriticalActions.append((
              criticalAction: landmark,
              criticalFact: cluster.criticalFact,
              adversarialActions: cluster.adversarialActions
            ))
          }
        }
      }
      goalsCriticalAction.append(goalCriticalActions)
    }
    return goalsCriticalAction
  }
  
  /// Indices: player, unit, softGoal, criticalActions.
  public lazy var softGoalsCriticalActionsUnits: [[[[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]]] = {
    var softGoalsCriticalActions = [[[[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]]]]()
    for (playerIdx, groundedUnitsProblems) in groundedPlayersUnitsProblems.enumerated() {
      var unitSoftGoalsCriticalActions = [[[(
        criticalAction: PlanAction,
        criticalFact: PDDLNameLiteral,
        adversarialActions: Set<PlanAction>
      )]]]()
      for (unitIdx, groundedProblem) in groundedUnitsProblems.enumerated() {
        let goalsCriticalAction = Self.identifyGoalsClusters(
          problem: groundedProblem,
          softGoals: softGoals[playerIdx],
          freeClusters: serialFreeClustersAllPlayers.map {$0.1})
        unitSoftGoalsCriticalActions.append(goalsCriticalAction)
      }
      softGoalsCriticalActions.append(unitSoftGoalsCriticalActions)
    }
    return softGoalsCriticalActions
  }()
  
  public lazy var serialSoftGoalsCriticalActions: [[[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]] = {
    var result = [[[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]]]()
    for unitSoftGoalsCriticalActions in softGoalsCriticalActionsUnits {
      var playerResult = unitSoftGoalsCriticalActions.first!
      for softGoalsCriticalActions in unitSoftGoalsCriticalActions.dropFirst() {
        for (goalIdx, goalCriticalActions) in softGoalsCriticalActions.enumerated() {
          playerResult[goalIdx].append(contentsOf: goalCriticalActions)
        }
      }
      result.append(playerResult)
    }
    return result
  }()
  
  public var playerAllCriticalAction: [[PlanAction]] {
    softGoalsCriticalActions.map {
      $0.flatMap {$0.map {$0.criticalAction}}
    }
  }
  
  public lazy var softGoalsCriticalActions: [[[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]] = {
    var softGoalsCriticalActions = [[[(
      criticalAction: PlanAction,
      criticalFact: PDDLNameLiteral,
      adversarialActions: Set<PlanAction>
    )]]]()
    for (i, groundingResult) in groundedPlayersProblems.enumerated() {
      let goalsCriticalAction = Self.identifyGoalsClusters(
        problem: groundingResult,
        softGoals: softGoals[i],
        freeClusters: freeClustersAllPlayers.map {$0.1})
      softGoalsCriticalActions.append(goalsCriticalAction)
    }
    return softGoalsCriticalActions
  }()
  
  public lazy var goalCriticalActionClustersUnits: [[[SoftGoalCriticalCluster]]]! = {
    var allClusters = [[[SoftGoalCriticalCluster]]]()
    for unitsSoftGoals in softGoalsCriticalActionsUnits {
      var playersSoftGoals = [[SoftGoalCriticalCluster]]()
      for softGoals in unitsSoftGoals {
        var unitsSoftGoals = [SoftGoalCriticalCluster]()
        for clusters in softGoals {
          var newCluster = [PDDLNameLiteral: [PlanAction: [PlanAction]]]()
          for cluster in clusters {
            newCluster[cluster.criticalFact, default: [:]][cluster.criticalAction, default: []].append(contentsOf: cluster.adversarialActions)
          }
          var softGoalCluster = SoftGoalCriticalCluster()
          for fact in Array(newCluster).sorted(by: {$0.key < $1.key}) {
            let critAdvActions = Array(fact.value).sorted(by: {$0.key < $1.key})
            softGoalCluster.addCriticalFact(
              fact.key,
              criticalActions: critAdvActions.map {$0.key},
              adversarialActions: critAdvActions.map {$0.value.sorted()})
          }
          unitsSoftGoals.append(softGoalCluster)
        }
        playersSoftGoals.append(unitsSoftGoals)
      }
      allClusters.append(playersSoftGoals)
    }
    return allClusters
  }()
  
  public lazy var goalCriticalActionClusters: [[SoftGoalCriticalCluster]]! =
    {
    var allSoftGoals = [[SoftGoalCriticalCluster]]()
    for softGoals in softGoalsCriticalActions {
      var playerSoftGoals = [SoftGoalCriticalCluster]()
      for clusters in softGoals {
        var newCluster = [PDDLNameLiteral: [PlanAction: [PlanAction]]]()
        for cluster in clusters {
          newCluster[cluster.criticalFact, default: [:]][cluster.criticalAction, default: []].append(contentsOf: cluster.adversarialActions)
        }
        var softGoalCluster = SoftGoalCriticalCluster()
        for fact in Array(newCluster).sorted(by: {$0.key < $1.key}) {
          let critAdvActions = Array(fact.value).sorted(by: {$0.key < $1.key})
          softGoalCluster.addCriticalFact(
            fact.key,
            criticalActions: critAdvActions.map {$0.key},
            adversarialActions: critAdvActions.map {$0.value.sorted()})
        }
        playerSoftGoals.append(softGoalCluster)
      }
      allSoftGoals.append(playerSoftGoals)
    }
    return allSoftGoals
  }()
  
  private lazy var adversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts:
    ([PlanAction: Set<PDDLNameLiteral>],
    [[PDDLNameLiteral: Set<PlanAction>]],
    [PlanAction: [PDDLNameLiteral]]) =
    {
      var adversarialActionCriticalFacts = [PlanAction: Set<PDDLNameLiteral>]()
      var actionCriticalFacts = [PlanAction: Set<PDDLNameLiteral>]()
      var playersCriticalFactCriticalActions = [[PDDLNameLiteral: Set<PlanAction>]]()
      for clusters in playersFreeClusters {
        var criticalFactCriticalActions = [PDDLNameLiteral: Set<PlanAction>]()
        for (_, cluster) in clusters {
          for adversarialAction in cluster.adversarialActions {
            adversarialActionCriticalFacts[adversarialAction, default: []].insert(cluster.criticalFact)
          }
          for criticalAction in cluster.criticalActions {
            criticalFactCriticalActions[cluster.criticalFact, default: []].insert((criticalAction))
            actionCriticalFacts[
              PlanAction(name: criticalAction.name, parameters: criticalAction.parameters),
              default: []].insert(cluster.criticalFact)
          }
        }
        playersCriticalFactCriticalActions.append(criticalFactCriticalActions)
      }
      return (adversarialActionCriticalFacts, playersCriticalFactCriticalActions, actionCriticalFacts.mapValues {Array($0)})
  }()
  
  private lazy var serialAdversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts:
    ([PlanAction: Set<PDDLNameLiteral>],
    [[PDDLNameLiteral: Set<PlanAction>]],
    [PlanAction: [PDDLNameLiteral]]) =
    {
      var adversarialActionCriticalFacts = [PlanAction: Set<PDDLNameLiteral>]()
      var actionCriticalFacts = [PlanAction: Set<PDDLNameLiteral>]()
      var playersCriticalFactCriticalActions = [[PDDLNameLiteral: Set<PlanAction>]]()
      for clusters in serialPlayersFreeClusters {
        var criticalFactCriticalActions = [PDDLNameLiteral: Set<PlanAction>]()
        for (_, cluster) in clusters {
          for adversarialAction in cluster.adversarialActions {
            adversarialActionCriticalFacts[adversarialAction, default: []].insert(cluster.criticalFact)
          }
          for criticalAction in cluster.criticalActions {
            criticalFactCriticalActions[cluster.criticalFact, default: []].insert((criticalAction))
            actionCriticalFacts[
              PlanAction(name: criticalAction.name, parameters: criticalAction.parameters),
              default: []].insert(cluster.criticalFact)
          }
        }
        playersCriticalFactCriticalActions.append(criticalFactCriticalActions)
      }
      return (adversarialActionCriticalFacts, playersCriticalFactCriticalActions, actionCriticalFacts.mapValues {Array($0)})
  }()
  
  public lazy var adversarialActionCriticalFacts: [PlanAction: Set<PDDLNameLiteral>] = {
    adversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.0
  }()
  public lazy var playersCriticalFactCriticalActions: [[PDDLNameLiteral: Set<PlanAction>]] = {
    adversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.1
  }()
  
  public lazy var playersCriticalActionCriticalFact: [[PlanAction: PDDLNameLiteral]] = {
    playersCriticalFactCriticalActions.map {
      var result = [PlanAction: PDDLNameLiteral]()
      for (criticalFact, criticalActions) in $0 {
        for criticalAction in criticalActions {
          result[criticalAction] = criticalFact
        }
      }
      return result
    }
  }()
  
  
  public lazy var actionWithoutUnitsCriticalFacts: [PlanAction: [PDDLNameLiteral]] = {
    let units = Set(playersUnits.flatMap {$0})
    return Dictionary(actionCriticalFacts.map {
      ($0.key.removing(parameters: units), $0.value)
    }, uniquingKeysWith: { $0 + $1})
  } ()
  
  public lazy var serialActionWithoutUnitsCriticalFacts: [PlanAction: [PDDLNameLiteral]] = {
    let units = Set(playersUnits.flatMap {$0})
    let retValueSets = Dictionary(serialActionCriticalFacts.map {
      ($0.key.removing(parameters: units), Set($0.value))
    }, uniquingKeysWith: { $0.union($1)})
    return retValueSets.mapValues {$0.sorted()}
  } ()
  
  public var actionCriticalFacts: [PlanAction: [PDDLNameLiteral]] {
    adversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.2
  }
  
  public var seriaAdversarialActionCriticalFacts: [PlanAction: Set<PDDLNameLiteral>] {
    serialAdversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.0
  }
  public var serialCriticalFactCriticalActions: [[PDDLNameLiteral: Set<PlanAction>]] {
    serialAdversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.1
  }
  public var serialActionCriticalFacts: [PlanAction: [PDDLNameLiteral]] {
    serialAdversarialActionCriticalFactsCriticalFactCriticalActionsActionCriticalFacts.2
  }
}

extension PDDLGameFDRRepresentationAnalyticsLogic {
  
  public func totalGoalWeights(of player: Int) -> Int {
    goalsWeights(of: player).sum
  }
  
  public func goalsWeights(of player: Int) -> [Int] {
    game.goalsWeights(of: player)
  }
  
  public func getWeight(of criticalFact: PDDLNameLiteral, for player: Int) -> Int {
    let weights = game.problem.metrics[player].softGoalsWeights
    let factWeight = getGoalNumber(for: criticalFact, player: player).map {
      weights[softGoals[player][$0].name!]!
    }.reduce(0, +)
    return factWeight
  }
  
  public func getGoalNumber(for criticalFact: PDDLNameLiteral, player: Int) -> [Int] {
    let playerGoals = self.softGoalsCriticalActions[player]
    var goalNumbers = [Int]()
    for (number, goal) in playerGoals.enumerated() {
      for fact in goal {
        if fact.criticalFact == criticalFact {
          goalNumbers.append(number)
        }
      }
    }
    return goalNumbers.removingDuplicates()
  }
  
  public func serialGetWeight(of criticalFact: PDDLNameLiteral, for player: Int) -> Int {
    let weights = game.problem.metrics[player].softGoalsWeights
    let factWeight = serialGetGoalNumber(for: criticalFact, player: player).map {
      weights[softGoals[player][$0].name!]!
    }.reduce(0, +)
    return factWeight
  }
  
  public func serialGetGoalNumber(for criticalFact: PDDLNameLiteral, player: Int) -> [Int] {
    let playerGoals = self.serialSoftGoalsCriticalActions[player]
    var goalNumbers = [Int]()
    for (number, goal) in playerGoals.enumerated() {
      for fact in goal {
        if fact.criticalFact == criticalFact {
          goalNumbers.append(number)
        }
      }
    }
    return goalNumbers.removingDuplicates()
  }
  
  @available(*, deprecated, message: "Use cost function in Temporal plan struct.")
  public func extractDeadlines(from plan: TemporalPlan, player: Int) -> [PDDLNameLiteral: Int] {
    var result = [PDDLNameLiteral: Int]()
    for action in plan.actions {
      for criticalFact in adversarialActionCriticalFacts[PlanAction(action)] ?? [] {
        result[criticalFact] = min(action.start, result[criticalFact] ?? action.start)
      }
    }
    return result
  }
  public var criticalFacts: Set<PDDLNameLiteral> {
    Set(adversarialActionCriticalFacts.values.flatMap {$0})
  }
  public var serialCriticalFacts: Set<PDDLNameLiteral> {
    Set(seriaAdversarialActionCriticalFacts.values.flatMap {$0})
  }
}

extension PDDLGameFDRRepresentationAnalyticsLogic {
  public static func getInstance(
    by config: AIKitData) throws -> PDDLGameFDRRepresentationAnalyticsLogic
  {
    return try ExperimentManager.defaultManager.getGame(config)
  }
  public convenience init(config: AIKitData) throws {
    let (gameDomainFile, gameInstanceFile, _, _, groundingCacheDir) =
      ExperimentManager.defaultManager.findGameDomainAndInstance(config)
    try self.init(
      gameDomainPDDLRaw: String(contentsOf: URL(fileURLWithPath: gameDomainFile)),
      gameProblemPDDLRaw: String(contentsOf: URL(fileURLWithPath: gameInstanceFile)),
      groundingCacheDir: URL(fileURLWithPath: groundingCacheDir, isDirectory: true)
    )
  }
}
