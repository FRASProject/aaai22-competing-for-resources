//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/29/21.
//

import Foundation
import MathLib
import Algorithms

public struct PlanMerger {
  public typealias Event = PlanSimulation.TemporalPlanEvent
  private let domain: PDDLDomain
  private let initState: PDDLInitState
  private var currentPredicateState: Set<PDDLAtomicNameFormula>
  private var currentFunctionState: [PDDLFunction: Int]
  private var inputPlansMaxTime: Int
  private var queue: PriorityQueue<Event>
  private var actionStatus: [PlanTemporalAction: ActionSimulationState]
  private var actionStatusDebug: [PlanTemporalAction: ActionSimulationState]
  private var conflictingEffects: [PDDLEffect: [PlanTemporalAction]]
  // Key: actionName, parameters without units parms. Value: Set of units.
  private let allUnits: Set<String>
  private var normalActions: [PlanTemporalAction]
  private var cooperativeActions: [PlanAction: [PlanTemporalAction]]
  private var cooperativeActionsNamesNumberOfUnits: [String: Int]
  private var processedCooperativeActions: Set<PlanAction>
  private var removedActions: [PlanTemporalAction]
  private init(initState: PDDLInitState, domain: PDDLDomain) {
    self.initState = initState
    self.domain = domain
    self.currentPredicateState = Set(initState.predicates.map {
      (l: PDDLNameLiteral)-> PDDLAtomicNameFormula in
      if case let .atomic(a) = l {
        return a
      } else {
        fatalError("Inconsistent init state")
      }
    })
    self.currentFunctionState = Dictionary(
      uniqueKeysWithValues: initState.functions.map {
        ($0.function, $0.value)
      })
    self.queue = PriorityQueue<Event>()
    self.actionStatus = [:]
    self.actionStatusDebug = [:]
    self.conflictingEffects = [:]
    self.allUnits = Set(initState.units)
    self.normalActions = []
    self.cooperativeActions = [:]
    self.cooperativeActionsNamesNumberOfUnits = [:]
    self.removedActions = []
    self.processedCooperativeActions = []
    self.inputPlansMaxTime = 0
  }
  
  private mutating func initQueue(plans: [TemporalPlan]) {
    self.normalActions = []
    self.cooperativeActions = [:]
    for plan in plans {
      for action in plan.actions {
        let domainAction = domain.getDurativeAction(by: action.name)!
        if domainAction.unitsParamaters.count <= 1 {
          normalActions.append(action)
        } else {
          let nonUnitParameters = action.parameters.filter { !allUnits.contains($0)}
          cooperativeActions[
            PlanAction(name: action.name, parameters: nonUnitParameters),
            default: []].append(action)
          cooperativeActionsNamesNumberOfUnits[action.name] = domainAction.unitsParamaters.count
        }
      }
    }
    for plan in plans {
      var unitEvents = Set<PlanSimulation.TemporalPlanEvent>()
      for action in plan.actions {
        queue.push(action.startEvent)
        queue.push(action.endEvent)
        inputPlansMaxTime = max(inputPlansMaxTime, action.endEvent.time)
        actionStatus[action] = .waiting
        unitEvents.insert(action.startEvent)
        unitEvents.insert(action.endEvent)
      }
    }
    actionStatusDebug = actionStatus
  }
  
  private mutating func merge(plans: [TemporalPlan]) {
    initQueue(plans: plans)
    while !queue.isEmpty {
      precondition(queue.peek()!.time < inputPlansMaxTime * plans.count)
      let (startEvents, endEvents) = sort(events: queue.popAllWithSamePriority())
      apply(events: groupCooperative(events: startEvents) + endEvents)
    }
  }
  
  private mutating func rebuildQueueByPostponingRemovedActions() {
    // Not optimal implementation, but it's not currently needed.
    let groupedRemovedActions = Dictionary(grouping: removedActions, by: {Set($0.containedParameters(allUnits))})
    let eventsToRemove = Set(removedActions.map {$0.endEvent})
    var queueElements = Array(queue).filter {!eventsToRemove.contains($0)}
    for (units, unitsActions) in groupedRemovedActions {
      guard let minStartTimeOfOtherUnits = queueElements.filter({
        units.intersection($0.action.parameters).isEmpty
      }).map({$0.time}).min() else { continue }
      let offset = minStartTimeOfOtherUnits - unitsActions.first!.start
      var eventsToAdd = [Event]()
      for unitAction in unitsActions {
        let action = unitAction.updating(time: minStartTimeOfOtherUnits)
        if let status = actionStatus.removeValue(forKey: unitAction) { actionStatus[action] = status }
        
//        print(action)
//        print(actionStatus[action])
        eventsToAdd.append(action.startEvent)
        eventsToAdd.append(action.endEvent)
      }
      var newQueueElements = eventsToAdd
      for queueEvent in queueElements {
        if !units.intersection(queueEvent.action.parameters).isEmpty {
          let updatedEvent = queueEvent.updating(time: queueEvent.time + offset)
          if let status = actionStatus.removeValue(forKey: queueEvent.action) {
            actionStatus[updatedEvent.action] = status }
//          print(updatedEvent.action)
//          print(actionStatus[updatedEvent.action])
          newQueueElements.append(queueEvent.updating(time: queueEvent.time + offset))
        } else {
          newQueueElements.append(queueEvent)
        }
      }
      queueElements = newQueueElements
    }
    removedActions.removeAll()
    queue = PriorityQueue()
    queueElements.forEach { queue.push($0) }
  }
  
  private mutating func mergeWithWaiting(plans: [TemporalPlan]) {
    initQueue(plans: plans)
    while !queue.isEmpty {
      precondition(queue.peek()!.time < 10_000, "")
      let (startEvents, endEvents) = sort(events: queue.popAllWithSamePriority())
      apply(events: groupCooperative(events: startEvents) + endEvents)
      rebuildQueueByPostponingRemovedActions()
    }
  }
  
  private mutating func groupCooperative(events: [Event]) -> [Event] {
    let cooperativeEvents = events.filter { cooperativeActionsNamesNumberOfUnits[$0.action.name] != nil}
    let normalEvents = events.filter { cooperativeActionsNamesNumberOfUnits[$0.action.name] == nil}
    var groupedCooperativeEvents = [Event]()
    let grouped = Dictionary(grouping: cooperativeEvents, by: { (event: Event) -> PlanAction in
      let nonUnitParameters = event.action.parameters.filter { !allUnits.contains($0)}
      return PlanAction(name: event.action.name, parameters: nonUnitParameters)
    }).filter {!processedCooperativeActions.contains($0.key)}
    for (groupAction, group) in grouped {
      let domainAction = self.domain.getDurativeAction(by: groupAction.name)!
      let unitsParameters = domainAction.unitsParamaters
      let groupUnits = group.flatMap { $0.action.parameters.filter {allUnits.contains($0)} }
      var groupActionAdded = false
      let numberOfUnits = cooperativeActionsNamesNumberOfUnits[groupAction.name]!
      for unitsPermutation in permutationsWithRepetitionNG(of: groupUnits, size: numberOfUnits)
      {
        let newParameters = PlanMerger.addUnitsParameter(
          unitsParametersOffsets: unitsParameters.map {$0.offset},
          unitParameters: unitsPermutation,
          nonUnitsParameters: groupAction.parameters)
        let candidateAction = PlanTemporalAction(
          start: group.first!.time,
          name: groupAction.name,
          parameters: newParameters,
          duration: group.first!.action.duration)
        let action = domainAction.substitute(candidateAction.parameters).substituted
        if action.isApplicable(in: currentPredicateState, time: .atStart) &&
            action.isApplicable(in: currentPredicateState, time: .overAll) {
          groupedCooperativeEvents.append(candidateAction.startEvent)
          self.queue.push(candidateAction.endEvent)
          self.actionStatus[candidateAction] = .waiting
          groupActionAdded = true
          processedCooperativeActions.insert(groupAction)
          break
        }
      }
      if !groupActionAdded {
        self.removedActions.append(contentsOf: group.map {$0.action})
      }
    }
    return normalEvents + groupedCooperativeEvents
  }
  
  private func sort(events: [Event]) -> (start: [Event], end: [Event]) {
    var start = [Event]()
    var end = [Event]()
    for event in events {
      switch event {
      case .start:
        start.append(event)
      case .end:
        end.append(event)
      }
    }
    return (start: start, end: end)
  }
  
  private func reconstructPlan() -> TemporalPlan {
    let sortedConflictingEffects = conflictingEffects.mapValues {$0.sorted()}
    let duplicitActionsToKeep = Set(sortedConflictingEffects.filter {
      $0.value.count > 1 }.map { $0.value.first! })
    let duplicitActionsToRemove = Set(sortedConflictingEffects.filter {
      $0.value.count > 1 }.flatMap { $0.value.dropFirst() })
    let intersection = duplicitActionsToKeep.intersection(duplicitActionsToRemove)
    precondition(intersection.isEmpty)
    return TemporalPlan(actions: actionStatus.filter {$0.value == .finished}.filter {
      !duplicitActionsToRemove.contains($0.key)
    }.map {$0.key}.sorted(by: { $0.start < $1.start }))
  }
  
  
  private mutating func apply(events: [PlanSimulation.TemporalPlanEvent]) {
    var effects = [PDDLEffect: [PlanTemporalAction]]()
    for event in events {
      if !(actionStatus[event.action]! == .waiting || actionStatus[event.action]! == .started) {
        continue
      }
      let time: PDDLTemporalCondition.TimeSpecifier
      switch event {
      case .start:
        time = .atStart
      case .end:
        time = .atEnd
        if actionStatus[event.action]! != .started {
          continue
        }
      }
      let action = self.domain.getDurativeAction(
        by: event.action.name)!.substitute(event.action.parameters).substituted
      if action.isApplicable(in: currentPredicateState, time: time) &&
          action.isApplicable(in: currentPredicateState, time: .overAll) {
        switch event {
        case .start:
          for effect in action.getEffects(time: .atStart).removingDuplicates() {
            effects[effect, default: []].append(event.action)
          }
          actionStatus[event.action] = .started
        case .end:
          for effect in action.getEffects(time: .atEnd).removingDuplicates() {
            effects[effect, default: []].append(event.action)
          }
          actionStatus[event.action] = .finished
        }
      } else {
        switch event {
        case .start:
          actionStatus[event.action] = .cannotBeStarted
        case .end:
          actionStatus[event.action] = .cannotBeFinished
        }
      }
    }
    for (effect, actions) in effects {
      if actions.count > 1 {
        precondition(conflictingEffects[effect] == nil, "Weird 74536536")
        conflictingEffects[effect] = actions
      }
      switch effect {
      case let .term(term):
        //precondition(!currentPredicateState.contains(term.asPDDLAtomicNameFormula!))
        currentPredicateState.insert(term.asPDDLAtomicNameFormula!)
      case let .negativeTerm(term):
        precondition(currentPredicateState.contains(term.asPDDLAtomicNameFormula!))
        currentPredicateState.remove(term.asPDDLAtomicNameFormula!)
      case .assignment:
        break //TODO: implement update of state of fluents.
      }
      
    }
    
  }
  private enum ActionSimulationState {
    case waiting
    case started
    case finished
    case merged
    case cannotBeStarted
    case cannotBeFinished
  }
  public static func addUnitsParameter(
    unitsParametersOffsets: [Int],
    unitParameters: [String],
    nonUnitsParameters: [String]) -> [String]
  {
    assert(unitParameters.count == unitsParametersOffsets.count)
    var result = [String]()
    var i = 0
    for otherParameter in nonUnitsParameters {
      while i < unitsParametersOffsets.count && unitsParametersOffsets[i] == result.count {
        result.append(unitParameters[i])
        i += 1
      }
      result.append(otherParameter)
    }
    return result
  }
  public static func merging(
    plans: [TemporalPlan],
    initState: PDDLInitState,
    in domain: PDDLDomain) -> (
      plan: TemporalPlan,
      removedActions: [PlanTemporalAction])
  {
    var merger = PlanMerger(initState: initState, domain: domain)
    merger.merge(plans: plans)
    let jointPlan = merger.reconstructPlan()
    let removedActions = merger.removedActions
    return (plan: jointPlan, removedActions: removedActions)
  }
  public static func mergingWithWaiting(
    plans: [TemporalPlan],
    initState: PDDLInitState,
    in domain: PDDLDomain) -> (
      plan: TemporalPlan,
      removedActions: [PlanTemporalAction])
  {
    var merger = PlanMerger(initState: initState, domain: domain)
    merger.mergeWithWaiting(plans: plans)
    let jointPlan = merger.reconstructPlan()
    let removedActions = merger.removedActions
    return (plan: jointPlan, removedActions: removedActions)
  }
}

