//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/22/21.
//

import Foundation
import MathLib

public struct PlanSimulation {
  public enum TemporalPlanEvent: Comparable, Hashable {
    case start(action: PlanTemporalAction)
    case end(action: PlanTemporalAction)
    public var action: PlanTemporalAction {
      switch self {
      case let .start(action: a):
        return a
      case let .end(action: a):
        return a
      }
    }
    public var time: Int {
      switch self {
      case let .end(action: action):
        return action.start + action.duration
      case let .start(action: action):
        return action.start
      }
    }
    public func updating(time: Int) -> TemporalPlanEvent {
      switch self {
      case let .start(action: a):
        return .start(action: a.updating(time: time))
      case let .end(action: a):
        return .end(action: a.updating(time: time - a.duration))
      }
    }
    public static func < (lhs: TemporalPlanEvent, rhs: TemporalPlanEvent) -> Bool {
      if lhs.time < rhs.time {
        return true
      } else if lhs.time == rhs.time {
        if case .end = lhs, case .start = rhs {
          return true
        }
      }
      return false
    }
  }
  public let game: PDDLGameFDRRepresentationAnalyticsLogic
  private var currentPredicateState: Set<PDDLAtomicNameFormula>
  private var currentFunctionState: [PDDLFunction: Int]
  private var queue: PriorityQueue<TemporalPlanEvent>
  private var actionStatus: [PlanTemporalAction:ActionSimulationState]
  private var conflictingEffects: [PDDLEffect: [PDDLDurativeAction]]
  public private(set) var currentValues: [Int: Double]
  public private(set) var notSatisfiedGoalsValue: [Double]
  public init(game: PDDLGameFDRRepresentationAnalyticsLogic) {
    self.game = game
    self.currentPredicateState = Set(game.game.problem.initState.predicates.map {
      (l: PDDLNameLiteral)-> PDDLAtomicNameFormula in
      if case let .atomic(a) = l {
        return a
      } else {
        fatalError("Inconsistent init state")
      }
    })
    self.currentFunctionState = Dictionary(
      uniqueKeysWithValues: game.game.problem.initState.functions.map {
        ($0.function, $0.value)
      })
    self.queue = PriorityQueue<TemporalPlanEvent>()
    self.conflictingEffects = [:]
    self.actionStatus = [PlanTemporalAction:ActionSimulationState]()
    self.currentValues = [:]
    self.notSatisfiedGoalsValue = game.players.map { _ in 0.0}
  }
  
  private mutating func apply(events: [TemporalPlanEvent]) {
    var effects = [PDDLEffect: [PDDLDurativeAction]]()
    for event in events {
      if !(actionStatus[event.action]! == .waiting || actionStatus[event.action]! == .started) {
        continue
      }
      let action = game.game.domain.getDurativeAction(
        by: event.action.name)!.substitute(event.action.parameters).substituted
      let time: PDDLTemporalCondition.TimeSpecifier
      switch event {
      case .start:
        time = .atStart
      case .end:
        time = .atEnd
      }
      if action.isApplicable(in: currentPredicateState, time: time) &&
          action.isApplicable(in: currentPredicateState, time: .overAll) {
        switch event {
        case .start:
          for effect in action.getEffects(time: .atStart).removingDuplicates() {
            effects[effect, default: []].append(action)
          }
          actionStatus[event.action] = .started
        case .end:
          for effect in action.getEffects(time: .atEnd).removingDuplicates() {
            effects[effect, default: []].append(action)
          }
          actionStatus[event.action] = .finished
        }
      } else {
        switch event {
        case .start:
          actionStatus[event.action] = .cannotBeStarted
        case .end:
          actionStatus[event.action] = .cannotBeFinished
        }
      }
    }
    for (effect, actions) in effects {
      if actions.count > 1 {
        precondition(conflictingEffects[effect] == nil, "Weird 74536536")
        conflictingEffects[effect] = actions
      }
      switch effect {
      case let .term(term):
        //precondition(!currentPredicateState.contains(term.asPDDLAtomicNameFormula!))
        currentPredicateState.insert(term.asPDDLAtomicNameFormula!)
      case let .negativeTerm(term):
        precondition(currentPredicateState.contains(term.asPDDLAtomicNameFormula!))
        currentPredicateState.remove(term.asPDDLAtomicNameFormula!)
      case .assignment:
        break //TODO: implement update of state of fluents.
      }
      
    }
    
  }
  
  public mutating func simulate(plans: [TemporalPlan]) {
    for plan in plans {
      for action in plan.actions {
        queue.push(action.startEvent)
        queue.push(action.endEvent)
        actionStatus[action] = .waiting
      }
    }
    while !queue.isEmpty {
      apply(events: queue.popAllWithSamePriority())
    }
    checkGoals()
  }
  private mutating func checkGoals() {
    var goalsFractions = [Int: [String: Int]]()
    for (player, goals) in game.softGoals.enumerated() {
      for (goalNumber, goal) in goals.enumerated() {
        if currentPredicateState.contains(goal.goal.asPDDLAtomicNameFormula!) {
          for cluster in game.softGoalsCriticalActions[player][goalNumber] {
            let fact = cluster.criticalFact.negation
            for (conflictEffect, actions) in conflictingEffects {
              switch (fact, conflictEffect) {
              case let (.not(f), .negativeTerm(fc)):
                if f == fc.asPDDLAtomicNameFormula {
                  precondition(goalsFractions[player, default: [:]][goal.name!] == nil ||
                    goalsFractions[player, default: [:]][goal.name!] == actions.count)
                  goalsFractions[player, default: [:]][goal.name!] = actions.count
                }
              case let (.atomic(f), .term(fc)):
                if f == fc.asPDDLAtomicNameFormula {
                  precondition(goalsFractions[player, default: [:]][goal.name!] == nil ||
                    goalsFractions[player, default: [:]][goal.name!] == actions.count)
                  goalsFractions[player, default: [:]][goal.name!] = actions.count
                }
              default:
                break
              }
            }
          }
        }
        let weight = game.game.problem.metrics[player].softGoalsWeights[goal.name!]!
        if currentPredicateState.contains(goal.goal.asPDDLAtomicNameFormula!) {
          currentValues[player, default: 0.0] += Double(weight) /
            Double(goalsFractions[player, default: [:]][goal.name!, default: 1])
        } else {
          self.notSatisfiedGoalsValue[player] += Double(weight)
        }
      }
    }
  }
  public enum ActionSimulationState {
    case waiting
    case started
    case finished
    case cannotBeStarted
    case cannotBeFinished
  }
  
}

extension PlanTemporalAction {
  public var startEvent: PlanSimulation.TemporalPlanEvent {
    return .start(action: self)
  }
  public var endEvent: PlanSimulation.TemporalPlanEvent {
    return .end(action: self)
  }
}
