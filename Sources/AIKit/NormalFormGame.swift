//
//  NormalFormGame.swift
//  FRASLib
//
//  Created by Pavel Rytir on 1/11/20.
//

import Foundation
import MathLib

public enum NormalFormGame {
  
  public static func evaluateStrats<E: PureStrategyEvaluator>(
    evaluator: E,
    player1Plans: [E.PSP1],
    player2Plans: [E.PSP2],
    player1PlansDistribution: [Double],
    player2PlansDistribution: [Double],
    outputFileName: String? = nil) throws -> (p1: Double, p2: Double)
  {
    
    let (player1UtilityMatrix, player2UtilityMatrix) = try UtilityMatrixUtils.constructUtilityMatrix(
      evaluator: evaluator,
      player1PureStrategies: player1Plans,
      player2PureStrategies: player2Plans
    )
    let (p1Value,p2Value) = evaluate(
      rowPlayer1Strat: player1PlansDistribution,
      columnPlayer2Strat: player2PlansDistribution,
      rowPlayer1UtilityMatrix: player1UtilityMatrix,
      columnPlayer2UtilityMatrix: player2UtilityMatrix)
    
    if let outputFileName = outputFileName {
      
      let result = ["p1Value": p1Value, "p2Value":p2Value] as [String : Any]
      let jsonData = try JSONSerialization.data(withJSONObject: result, options: [])
      if outputFileName == "/dev/stdout" {
        // Work around. For some reason URL does not work properly with /dev/stdout
        // So we just print it.
        let jsonString = String(decoding: jsonData, as: Unicode.ASCII.self)
        print(jsonString)
      } else {
        try jsonData.write(to: URL(fileURLWithPath: outputFileName))
      }
    }
    return (p1: p1Value, p2: p2Value)
  }
  
  
  /// Calculates value of players' strategies.
  /// - Parameters:
  ///   - rowPlayer1Strat: Player 1 strategy (distribution over rows).
  ///   - columnPlayer2Strat: Player 2 strategy (distribution over columns).
  ///   - rowPlayer1UtilityMatrix: Player 1 utility matrix.
  ///   - columnPlayer2UtilityMatrix: Player 2 utility matrix.
  /// - Returns: Expected utility of Player 1 and Player 2.
  static public func evaluate(
    rowPlayer1Strat: [Double],
    columnPlayer2Strat: [Double],
    rowPlayer1UtilityMatrix: Matrix,
    columnPlayer2UtilityMatrix: Matrix) -> (player1: Double, player2: Double)
  {
    assert(rowPlayer1UtilityMatrix.rows == columnPlayer2UtilityMatrix.rows &&
      rowPlayer1UtilityMatrix.columns == columnPlayer2UtilityMatrix.columns)
    
    var expectedUtilityPlayer1 = 0.0
    var expectedUtilityPlayer2 = 0.0
    
    for row in 0..<rowPlayer1UtilityMatrix.rows {
      for column in 0..<rowPlayer1UtilityMatrix.columns {
        let probability = rowPlayer1Strat[row] * columnPlayer2Strat[column]
        expectedUtilityPlayer1 += probability * rowPlayer1UtilityMatrix[row, column]
        expectedUtilityPlayer2 += probability * columnPlayer2UtilityMatrix[row, column]
      }
    }
    return (player1: expectedUtilityPlayer1, player2: expectedUtilityPlayer2)
  }
  
  static public func computeEquilibriumTwoPlayersConstantSum(
    player1Utility: Matrix,
    const: Double) -> ([Double],[Double],Double,Double)
  {
    let player2Utility = (-player1Utility).applyElementwise { $0 + const }
    
    let player1ZeroUtilityMatrix = player1Utility.applyElementwise { $0 + (-const / 2) }
    let player2ZeroUtilityMatrix = player2Utility.applyElementwise { $0 + (-const / 2) }
    let (eq1,eq2,val1,val2) = computeEquilibriumTwoPlayersZeroSum(
      player1Utility: player1ZeroUtilityMatrix,
      player2Utitility: player2ZeroUtilityMatrix)
    return (eq1,eq2,val1 + const / 2,val2 + const / 2)
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    player1Utility: Matrix) -> ([Double],[Double],Double,Double)
  {
    let player2Utility = -player1Utility
    return computeEquilibriumTwoPlayersZeroSum(
      utilityMatrixP1: player1Utility,
      utilityMatrixP2: player2Utility)
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    player1Utility: Matrix,
    player2Utitility: Matrix) -> ([Double],[Double],Double,Double) {
    
    let player2TransposedUtilityMatrix = player2Utitility.deepTranspose()
    
    let tempUtilityMatrix = (
      rows: player1Utility.rows,
      cols:player1Utility.columns,
      player1Utility: player1Utility.grid,
      player2UtilityTransposed: player2TransposedUtilityMatrix.grid)
    
    let equilibriumRaw = computeEquilibriumTwoPlayersZeroSum(matrix: tempUtilityMatrix)
    return equilibriumRaw
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    matrix: (rows:Int, cols:Int, player1Utility:[Double], player2UtilityTransposed:[Double])
  ) -> ([Double],[Double],Double,Double) {
    
    let player1Matrix = Matrix(
      rows: matrix.rows,
      columns: matrix.cols,
      grid: matrix.player1Utility)
    let player2Matrix = Matrix(
      rows: matrix.cols,
      columns: matrix.rows,
      grid: matrix.player2UtilityTransposed)
    
    let (player1Val, solutionPlayer1) = try! CPLEXSolver.findEquilibriumOfNFGame(
      utilityMatrix: player1Matrix)
    let (player2Val, solutionPlayer2) = try! CPLEXSolver.findEquilibriumOfNFGame(
      utilityMatrix: player2Matrix)
    return (solutionPlayer1,solutionPlayer2,player1Val,player2Val)
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    utilityMatrixP1: Matrix,
    utilityMatrixP2: Matrix) -> ([Double],[Double],Double,Double)
  {
    let (player1Val, solutionPlayer1) = try! CPLEXSolver.findEquilibriumOfNFGame(
      utilityMatrix: utilityMatrixP1)
    let utilityMatrixP2Transposed = utilityMatrixP2.deepTranspose()
    let (player2Val, solutionPlayer2) = try! CPLEXSolver.findEquilibriumOfNFGame(
      utilityMatrix: utilityMatrixP2Transposed)
    return (solutionPlayer1,solutionPlayer2,player1Val,player2Val)
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    rowPlayer1Utility: Matrix) -> (Double, [Double])
  {
    do {
      return try CPLEXSolver.findEquilibriumOfNFGame(utilityMatrix: rowPlayer1Utility)
    } catch {
      fatalError("Cplex error")
    }
  }
  
  static public func computeEquilibriumTwoPlayersZeroSum(
    columnPlayer2Utility: Matrix) -> (Double, [Double])
  {
    let transposedMatrix = columnPlayer2Utility.deepTranspose()
    do {
      return try CPLEXSolver.findEquilibriumOfNFGame(utilityMatrix: transposedMatrix)
    } catch {
      fatalError("Cplex error")
    }
  }
}
