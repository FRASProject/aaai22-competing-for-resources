//
//  TableUtils.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 10/1/20.
//

import Foundation

public struct AIKitTable<T> {
  public enum JoinType {
    case inner, left
  }
  public enum FileType {
    case csv, tex, screen
  }
  private var columns: [String: [T?]]
  public var rowCount: Int {
    columns.first?.value.count ?? 0
  }
  public var columnCount: Int {
    columns.count
  }
  public var columnsNames: [String] {
    Array(columns.keys).sorted()
  }
  public init(columns: [String: [T?]]) {
    self.columns = columns
  }
  public init(rows: [[String: T]]) {
    self.columns = TableUtils.convertRowsToTable(rows)
  }
  public init() {
    self.columns = [:]
  }
  
  public subscript(_ row: Int, _ column: String) -> T? {
    if let col = columns[column], row >= 0, row < col.count {
      return col[row]
    } else {
      return nil
    }
  }
  
  public init(rows: [AIKitTable<T>]) {
    var result = AIKitTable<T>()
    for row in rows {
      result = result.appendingBottom(row)
    }
    self = result
  }
  public func appendingBottom(_ other: AIKitTable<T>?) -> AIKitTable {
    if let other = other {
      var result = [String: [T?]]()
      for columnName in Set(columnsNames).subtracting(other.columnsNames) {
        result[columnName] = columns[columnName]! + [T?](repeating: nil, count: other.rowCount)
      }
      for columnName in Set(columnsNames).intersection(other.columnsNames) {
        result[columnName] = columns[columnName]! + other.columns[columnName]!
      }
      for columnName in Set(other.columnsNames).subtracting(columnsNames) {
        result[columnName] = [T?](repeating: nil, count: rowCount) + other.columns[columnName]!
      }
      
      return AIKitTable(columns: result)
    } else {
      return self
    }
  }
  public static func +(lhs: AIKitTable, rhs: AIKitTable) -> AIKitTable {
    lhs.appendingBottom(rhs)
  }
  public var asArray: (data: [[T?]], columnNames: [String]) {
    let columnNames = Array(columns.keys).sorted()
    var data = [[T?]]()
    for i in 0..<rowCount {
      var row = [T?]()
      for columName in columnsNames {
        row.append(columns[columName]![i])
      }
      data.append(row)
    }
    return (data: data, columnNames: columnNames)
  }
  public init(data: [[T?]], columnNames: [String]) {
    columns = [:]
    for (columnIndex, columnName) in columnNames.enumerated() {
      var column = [T?]()
      for i in data.indices {
        column.append(data[i][columnIndex])
      }
      columns[columnName] = column
    }
  }
  public func addingPrefixToColumnNames(prefix: String, exceptColumns: Set<String> = []) -> AIKitTable<T> {
    AIKitTable(columns: Dictionary(uniqueKeysWithValues: columns.map {
      (exceptColumns.contains($0.key) ? $0.key : prefix + $0.key, $0.value)
    }))
  }
  public func renamingColumns(newNames: [String: String]) -> AIKitTable<T> {
    AIKitTable(columns: Dictionary(uniqueKeysWithValues: columns.map {
      (newNames[$0.key] ?? $0.key, $0.value)
    }))
  }
  public func mapCells(
    cols: [String]? = nil,
    _ updateFunction: ((Self, Int, String, T?))->T?) -> AIKitTable
  {
    let cols = cols == nil ? Array(columns.keys) : cols!
    var newColumns = columns.filter {!cols.contains($0.key)}
    for (columnName, columnData) in columns.filter({cols.contains($0.key)}) {
      var newColumnData = [T?]()
      for (rowIdx, cell) in columnData.enumerated() {
        let newCell = updateFunction((self, rowIdx, columnName, cell))
        newColumnData.append(newCell)
      }
      newColumns[columnName] = newColumnData
    }
    return AIKitTable(columns: newColumns)
  }
  public func keepingColumns(_ newCols: [String]) -> Self {
    AIKitTable(columns: columns.filter {newCols.contains($0.key)})
  }
  public func computingNewColumn(
    name: String,
    _ computeFunction: (Self, Int)->T?) -> AIKitTable
  {
    var newColumnData = [T?]()
    for rowIdx in 0..<rowCount {
      let newCell = computeFunction(self, rowIdx)
      newColumnData.append(newCell)
    }
    var newColumns = columns
    newColumns[name] = newColumnData
    return AIKitTable(columns: newColumns)
  }
  public func combiningColums(_ columnsToCombine: [String], newColumnName: String, combiner: ([T?])->T?) -> AIKitTable {
    var newColumns = columns
    var newColumn = [T?]()
    for i in 0..<rowCount {
      let value = combiner(columnsToCombine.map { columns[$0]![i] } )
      newColumn.append(value)
    }
    columnsToCombine.forEach {
      newColumns.removeValue(forKey: $0)
    }
    newColumns[newColumnName] = newColumn
    return AIKitTable(columns: newColumns)
  }
}

extension AIKitTable where T : Comparable {
  public func sorted(by cols: [String]) -> AIKitTable {
    var (data, columnNames) = asArray
    let colsIndices = cols.compactMap {columnNames.firstIndex(of: $0)}
    data.sort { a, b in
      let projectedA = colsIndices.map {a[$0]}
      let projectedB = colsIndices.map {b[$0]}
      return projectedA.lexicographicallyPrecedes(projectedB) { e1, e2 in
        if let e1 = e1, let e2 = e2 {
          return e1 < e2
        } else if e1 == nil && e2 == nil {
          return false
        } else if e2 == nil {
          return true
        } else {
          return false
        }
      }
    }
    return AIKitTable(data: data, columnNames: columnNames)
  }
}

extension AIKitTable where T : Hashable {
  public func grouping(by groupingColumns: [String]) -> AIKitTableGrouping<T> {
    let grouping = Dictionary(grouping: (0..<rowCount)) { rowIdx in
      groupingColumns.map { columns[$0]?[rowIdx]} }
    return AIKitTableGrouping(table: self, groupingColumns: groupingColumns, grouping: grouping)
  }
}

public struct AIKitTableGrouping<T: Hashable> {
  let table: AIKitTable<T>
  let groupingColumns: [String]
  let grouping: [[T?] : [Int]]
  let columnsToAggregate: [String]
  public init(table: AIKitTable<T>, groupingColumns: [String], grouping: [[T?] : [Int]]) {
    self.table = table
    self.groupingColumns = groupingColumns
    self.grouping = grouping
    self.columnsToAggregate = table.columnsNames.filter {!groupingColumns.contains($0)}
  }
  public func aggregate(_ aggregator: (_ rowIndices: [Int], _ columnName: String, _ table: AIKitTable<T>) -> T) -> AIKitTable<T> {
    var newColumns = [String: [T?]]()
    for (rowCells, group) in grouping {
      for (colName, cell) in zip(groupingColumns, rowCells) {
        newColumns[colName, default: []].append(cell)
      }
      for colName in columnsToAggregate {
        let cell = aggregator(group, colName, table)
        newColumns[colName, default: []].append(cell)
      }
    }
    return AIKitTable(columns: newColumns)
  }
  public func subtables() -> [([(String, T?)],AIKitTable<T>)] {
    var tables = [([(String, T?)],AIKitTable<T>)]()
    for (rowCells, group) in grouping {
      let key = Array(zip(groupingColumns, rowCells))
      var subtableColumns = [String: [T?]]()
      for colName in columnsToAggregate {
        for row in group {
          subtableColumns[colName, default: []].append(table[row, colName])
        }
      }
      tables.append((key, AIKitTable(columns: subtableColumns)))
    }
    return tables
  }
}

extension AIKitTableGrouping where T == AIKitData {
  public func avg() -> AIKitTable<T> {
    aggregate { (rowIndices: [Int], columnName: String, table: AIKitTable<T>) in
      if let avg = rowIndices.compactMap({table[$0, columnName]?.double}).avg {
        return AIKitData(avg)
      } else {
        return .null
      }
    }
  }
}

extension AIKitTable where T : Equatable {
  
  public func select(key: [String: T]) -> AIKitTable {
    let selectedRows = search(key: key)
    let newColumns = columns.mapValues { column in
      selectedRows.map {column[$0]}
    }
    return AIKitTable(columns: newColumns)
  }
  
  public func search(key: [String: T]) -> [Int] {
    var result = [Int]()
    for rowNumber in 0..<rowCount {
      if key.allSatisfy({ columns[$0.key]![rowNumber] == $0.value }) {
        result.append(rowNumber)
      }
    }
    return result
  }
  
  public func joiningMaxLeft(with other: AIKitTable<T>?) -> AIKitTable {
    if let other = other {
      let commonColumns = Array(Set(columnsNames).intersection(other.columnsNames)).sorted()
      return joining(with: other, on: commonColumns, type: .left)
    } else {
      return self
    }
  }
  
  public func joining(
    with other: AIKitTable<T>,
    on cols: [String],
    type: JoinType = .inner,
    overlappingColumnsPrefix: String = "",
    overlappingColumnsSufix: String = "") -> AIKitTable
  {
    var result = [[T?]]()
    let cols = Set(columns.keys).intersection(other.columns.keys).intersection(cols)
    if !Set(columns.keys).isSuperset(of: cols) || !Set(other.columns.keys).isSuperset(of: cols) {
      print("AIKitTable warning. Joining on non-existing columns. Ignoring these columns.")
    }
    let currentTableCols = Array(columns.keys)
    let otherTableCols = Array(Set(other.columns.keys).subtracting(cols))
    let overlapingCols = Set(columns.keys).intersection(other.columns.keys).subtracting(cols)
    let allColumnsNames = Set(columns.keys).union(other.columns.keys)
    for currentRow in 0..<rowCount {
      var currentRowAdded = false
      for otherRow in 0..<other.rowCount {
        if cols.allSatisfy({
          self.columns[$0]![currentRow] == other.columns[$0]![otherRow]
        })
        {
          var newRow = [T?]()
          for colName in currentTableCols {
            newRow.append(columns[colName]![currentRow])
          }
          for colName in otherTableCols {
            newRow.append(other.columns[colName]![otherRow])
          }
          result.append(newRow)
          currentRowAdded = true
        }
      }
      if type == .left && !currentRowAdded {
        var newRow = [T?]()
        for colName in currentTableCols {
          newRow.append(columns[colName]![currentRow])
        }
        for _ in otherTableCols {
          newRow.append(nil)
        }
        result.append(newRow)
        currentRowAdded = true
      }
    }
    var resultColumns = [String: [T?]]()
    for (index, colName) in  currentTableCols.enumerated() {
      resultColumns[colName] = result.map {$0[index]}
    }
    for (index, colName) in  otherTableCols.enumerated() {
      var newColName: String
      if overlapingCols.contains(colName) {
        var newColSuffix = 1
        repeat {
          newColName = overlappingColumnsPrefix + colName +
          overlappingColumnsSufix +
          (newColSuffix > 1 ? String(newColSuffix) : "")
          newColSuffix += 1
        } while allColumnsNames.contains(newColName) || resultColumns[newColName] != nil
      } else {
        newColName = colName
      }
//      let newColName = overlapingCols.contains(colName) ? "\(colName)2" : colName
      resultColumns[newColName] = result.map {$0[index + currentTableCols.count]}
    }
    return AIKitTable(columns: resultColumns)
  }
}

extension AIKitTable where T == AIKitData {
  public func convertingCellsToString(
    format: [String: Int]? = nil,
    defaultFormat: Int? = nil,
    valuesInQuotationMarks: Bool = true) -> AIKitTable<String>
  {
    let newCols = TableUtils.convertCellsToString(
      columns,
      format: format,
      defaultFormat: defaultFormat,
      valuesInQuotationMarks: valuesInQuotationMarks)
    return AIKitTable<String>(columns: newCols)
  }
}

extension AIKitTable: Codable where T: Codable {}

extension AIKitTable where T == String {
  public func toText(
    type: FileType,
    cols: [String]? = nil,
    colsPadding: Bool = false) throws -> String
  {
    return try TableUtils.generate(columns, type: type, cols: cols, colsPadding: colsPadding)
  }
  public func combining(column1: String, column2: String) -> AIKitTable {
    combiningColums(
      [column1, column2],
      newColumnName: column1 + " " + column2,
      combiner: {
        ($0[0] ?? "") + " " + ($0[1] ?? "")
      })
  }
  public func transpose(keysColumn: String) -> AIKitTable {
    let columnsOrdering = columnsNames.filter {$0 != keysColumn}
    var transposedColumns = [String: [String?]]()
    transposedColumns[keysColumn] = columnsOrdering
    for rowIdx in columns[keysColumn]!.indices {
      var newColumn = [String?]()
      for column in columnsOrdering {
        newColumn.append(columns[column]![rowIdx])
      }
      transposedColumns[columns[keysColumn]![rowIdx]!] = newColumn
    }
    return AIKitTable(columns: transposedColumns)
  }
}

public enum TableUtils {
  
  public static func convertRowsToTable<T>(_ rows: [[String: T]]) -> [String: [T?]] {
    var result = [String: [T?]]()
    let mergedCols = Array(rows.map {
                            $0.keys }.reduce(Set<String>()) { $0.union($1) })
    for row in rows {
      for colName in mergedCols {
        let cellData = row[colName]
        var colData = result[colName, default: []]
        colData.append(cellData)
        result[colName] = colData
      }
    }
    return result
  }
  
  public static func convertCellsToString(
    _ table: [String: [AIKitData?]],
    format: [String: Int]? = nil,
    defaultFormat: Int? = nil,
    valuesInQuotationMarks: Bool = true) -> [String: [String]]
  {
    Dictionary(uniqueKeysWithValues: table.map { colName, col in
      (colName, col.map { cell in
      if let cell = cell {
        let cellValue =  cell.stringFormatedRepresentation(
          doubleDecimalDigits: format?[colName] ?? defaultFormat)
        return valuesInQuotationMarks ? "\"\(cellValue)\"" : cellValue
      } else {
        return "NA"
      }
    })
    })
  }
  
  public static func maxLeftJoin<T: Equatable>(
    _ table1: [[String: T]],
    _ table2: [[String: T]]
  ) -> [[String: T]]
  {
    let table1Keys = table1.map {Set($0.keys)}
    let commonColumns = (table1Keys +
                          table2.map {Set($0.keys)}).reduce(table1Keys.first!, {
                                                              $0.intersection($1) })
    return Self.leftJoin(table1, table2, on: commonColumns.sorted())
  }
  
  public static func leftJoin<T: Equatable>(
    _ table1: [[String: T]],
    _ table2: [[String: T]], on cols: [String]
  ) -> [[String: T]]
  {
    var resultTable = [[String: T]]()
    for row1 in table1 {
      var rowAdded = false
      for row2 in table2 {
        if cols.allSatisfy({
                            row1[$0] == row2[$0]})
        {
          var newRow = row1
          newRow.merge(row2, uniquingKeysWith: { a,b in a })
          resultTable.append(newRow)
          rowAdded = true
        }
      }
      if !rowAdded {
        resultTable.append(row1)
      }
    }
    return resultTable
  }
  
  public static func join<T: Equatable>(
    _ table1: [[String: T]],
    _ table2: [[String: T]], on cols: [String]
  ) -> [[String: T]]
  {
    var resultTable = [[String: T]]()
    for row1 in table1 {
      for row2 in table2 {
        if cols.allSatisfy({ row1[$0] != nil && row1[$0] == row2[$0]}) {
          var newRow = row1
          newRow.merge(row2, uniquingKeysWith: { a,b in a })
          resultTable.append(newRow)
        }
      }
    }
    return resultTable
  }
  
  public static func generate(
    _ table: [String: [String?]],
    type: AIKitTable<String>.FileType,
    cols: [String]? = nil,
    colsPadding: Bool = false) throws -> String
  {
    guard !table.isEmpty else {
      return ""
    }
    
    let separator: String
    let rowSeparator: String
    switch type {
    case .screen:
      separator = "|"
      rowSeparator = "\n"
    case .csv:
      separator = ","
      rowSeparator = "\n"
    case .tex:
      separator = " & "
      rowSeparator = "\\\\\n"
    }
    
    let colsMaxWidths = Dictionary(uniqueKeysWithValues: table.map {
      ($0.key, max($0.value.map { $0?.count ?? 0 }.max() ?? 0, $0.key.count))
    })

    var csvLines = [String]()
    let selectedColumns: [String]
    if cols != nil {
      selectedColumns = cols!.filter {table[$0] != nil}
    } else {
      selectedColumns = table.keys.sorted()
    }
    csvLines.append(selectedColumns.map {
      colsPadding ? $0.padding(toLength: colsMaxWidths[$0] ?? $0.count, withPad: " ", startingAt: 0) : $0
    }.joined(separator: separator))
    
    let numberOfRows = table.first!.value.count
    
    for rowNumber in 0..<numberOfRows {
      var cells = [String]()
      for columnName in selectedColumns {
        let cell: String
        if let column = table[columnName] {
          guard column.count == numberOfRows else {
            throw AIKitError(message: "Inconsistent table data.")
          }
          let rawCell = column[rowNumber] ?? "NA"
          cell = type == .tex ? rawCell.replacingOccurrences(of: "_", with: "\\_") : rawCell
        } else {
          print("Warning column \(columnName) not found.")
          //print(table.keys.filter { $0.contains("16384")})
          cell = "NA"
        }
        if colsPadding, let colWidth = colsMaxWidths[columnName] {
          cells.append(
            cell.padding(toLength: colWidth, withPad: " ", startingAt: 0))
        } else {
          cells.append(cell)
        }
      }
      csvLines.append(cells.joined(separator: separator))
    }
    
    let csvData = csvLines.joined(separator: rowSeparator)
    return csvData
  }
}
