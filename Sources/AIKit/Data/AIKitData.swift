//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/1/20.
//

import Foundation

public enum AIKitData: Hashable, Equatable {
  case integer(Int)
  case double(Double)
  case bool(Bool)
  case string(String)
  case null
  indirect case array(Array<AIKitData>)
  indirect case dictionary(Dictionary<String, AIKitData>)
  public init(withJSON data: Data) throws {
    let jsonObject = try JSONSerialization.jsonObject(
      with: data,
      options: [])
    self = AIKitData.convert(jsonObject)
  }
  
  public init(_ d: Double?) {
    if let d = d {
      self = .double(d)
    } else {
      self = .null
    }
  }
  
  public func getJSONData() throws -> Data {
    try JSONSerialization.data(withJSONObject: self.any, options: [])
  }
  
  public var array: [AIKitData]? {
    guard case let .array(a) = self else {
      return nil
    }
    return a
  }
  
  public var dictionary: [String: AIKitData]? {
    guard case let .dictionary(d) = self else {
      return nil
    }
    return d
  }
  
  /// Returns array of dicts. If this object is dict. It returns single element array with this dictionary.
  public var arrayOfDictionaries: [[String : AIKitData]]? {
    if let a = array {
      if !a.allSatisfy({$0.dictionary != nil}) {
        return nil
      }
      return a.map {$0.dictionary!}
    } else if let d = dictionary {
      return [d]
    } else {
      return nil
    }
  }
  
  public var string: String? {
    guard case let .string(s) = self else {
      return nil
    }
    return s
  }
  
  public var double: Double? {
    if case let .double(d) = self {
      return d
    } else if case let .integer(i) = self {
      return Double(i)
    } else {
      return nil
    }
  }
  
  public var integer: Int? {
    guard case let .integer(i) = self else {
      return nil
    }
    return i
  }
  
  public var bool: Bool? {
    guard case let .bool(b) = self else {
      return nil
    }
    return b
  }
  
  public subscript(_ key: String) -> AIKitData? {
    guard case let .dictionary(d) = self, let value = d[key] else {
      return nil
    }
    if case let .dictionary(subDictionary) = value {
      let newValue = subDictionary.merging(d, uniquingKeysWith: { e, _ in e })
      return .dictionary(newValue)
    } else {
      return value
    }
  }
  
  public var stringRepresentation: String {
    stringFormatedRepresentation()
  }
  
  public func stringFormatedRepresentation(doubleDecimalDigits: Int? = nil) -> String {
    switch self {
    case .null:
      return "null"
    case let .string(s):
      return s
    case let .double(d):
      if let doubleDecimalDigits = doubleDecimalDigits {
        let m = pow(10.0, Double(doubleDecimalDigits))
        let rounded = (m * d).rounded() / m
        return String(format: "%.\(doubleDecimalDigits)f", rounded)
      } else {
        return String(d)
      }
    case let .integer(i):
      return String(i)
    case let .bool(b):
      return String(b)
    case let .array(a):
      return "[" + a.map {
        $0.stringFormatedRepresentation(
          doubleDecimalDigits: doubleDecimalDigits) }.joined(separator: ", ") + " ]"
    case let .dictionary(d):
      var result = [String]()
      for (k, v) in d {
        result.append(
          "\(k): \(v.stringFormatedRepresentation(doubleDecimalDigits: doubleDecimalDigits))")
      }
      return "[" + result.joined(separator: ", ") + "]"
    }
  }
  
  public var any: Any {
    switch self {
    case .null:
      return NSNull()
    case let .integer(i):
      return i
    case let .double(d):
      if !d.isFinite {
        return String(d)
      }
      return d
    case let .bool(b):
      return b
    case let .string(s):
      return s
    case let .array(a):
      return a.map {$0.any}
    case let .dictionary(d):
      return d.mapValues { $0.any }
    }
  }
  
  private static func convert(_ json: Any) -> AIKitData {
    // Checking as? Bool incorectly catch 0/1 integers.
    if type(of: json) == type(of: NSNumber(value: true)) ||
        type(of: json) == type(of: NSNumber(value: false)) {
      return AIKitData.bool(json as! Bool)
    } else if let o = json as? Int {
      return AIKitData.integer(o)
    } else if let o = json as? Double {
      return AIKitData.double(o)
    } else if let o = json as? String {
      return AIKitData.string(o)
    } else if let o = json as? [String: Any] {
      var result = [String: AIKitData]()
      for (key, value) in o {
        result[key] = convert(value)
      }
      return AIKitData.dictionary(result)
    } else if let o = json as? [Any] {
      var result = [AIKitData]()
      for e in o {
        result.append(convert(e))
      }
      return AIKitData.array(result)
    } else {
      fatalError("Invalid data.")
    }
  }
}

extension AIKitData: ExpressibleByBooleanLiteral {
  public init(booleanLiteral: BooleanLiteralType) {
    self = .bool(booleanLiteral)
  }
}

extension AIKitData: ExpressibleByStringLiteral {
  public init(stringLiteral: StringLiteralType) {
    self = .string(stringLiteral)
  }
}

extension AIKitData: ExpressibleByFloatLiteral {
  public init(floatLiteral: FloatLiteralType) {
    self = .double(floatLiteral)
  }
}

extension AIKitData: ExpressibleByIntegerLiteral {
  public init(integerLiteral: IntegerLiteralType) {
    self = .integer(integerLiteral)
  }
}

extension AIKitData: ExpressibleByNilLiteral {
  public init(nilLiteral: ()) {
    self = .null
  }
}

extension AIKitData : Comparable {
  public static func < (lhs: AIKitData, rhs: AIKitData) -> Bool {
    switch (lhs, rhs) {
    case let (.integer(lhs), .integer(rhs)):
      return lhs < rhs
    case let (.double(lhs), .double(rhs)):
      return lhs < rhs
    case let (.double(lhs), .integer(rhs)):
      return lhs < Double(rhs)
    case let (.integer(lhs), .double(rhs)):
      return Double(lhs) < rhs
    case let (.bool(lhs), .bool(rhs)):
      return rhs && !lhs
    default:
      return lhs.stringRepresentation < rhs.stringRepresentation
    }
  }
}

