//
//  errors.swift
//  FRASLib
//
//  Created by Pavel Rytir on 6/16/18.
//

import Foundation

public struct AIKitError: Error {
  public init(message: String) {
    self.message = message
  }
  let message: String
  var localizedDescription: String {
    "AIKitError: \(message)"
  }
}


