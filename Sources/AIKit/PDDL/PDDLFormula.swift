//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

public enum PDDLFormula: Hashable {
  case atomic(PDDLAtomicTerm)
  indirect case negation(PDDLFormula)
  indirect case and([PDDLFormula])
  indirect case or([PDDLFormula])
  indirect case preference(name: String?, goal: PDDLFormula)
}

extension PDDLFormula {
  
  public func isTrue(in state: Set<PDDLAtomicNameFormula>) -> Bool {
    switch self {
    case let .atomic(f):
      return state.contains(f.asPDDLAtomicNameFormula!)
    case let .negation(f):
      return !f.isTrue(in: state)
    case let .and(formulas):
      return formulas.allSatisfy {$0.isTrue(in: state)}
    case let .or(formulas):
      return formulas.someSatisfies {$0.isTrue(in: state)}
    case .preference:
      fatalError("Not supported 4255")
    }
  }
  
  public var termNames: [String] {
    switch self {
    case let .atomic(a):
      return [a.name]
    case let .negation(n):
      return n.termNames
    case let .and(formulas):
      return formulas.flatMap {$0.termNames}
    case let .or(formulas):
      return formulas.flatMap {$0.termNames}
    case let .preference(name: _, goal: f):
      return f.termNames
    }
  }

  public func removing(
    _ parametersToRemove: [PDDLTerm],
    atLeastOneExists: Set<PDDLTerm>) -> PDDLFormula?
  {
    switch self {
    case let .atomic(atomicTerm):
      if let restrictedAtomicTerm = atomicTerm.removing(
          parametersToRemove,
          atLeastOneExists: atLeastOneExists) {
        return .atomic(restrictedAtomicTerm)
      } else {
        return nil
      }
    case let .negation(formula):
      if let restrictedFormula = formula.removing(
          parametersToRemove,
          atLeastOneExists: atLeastOneExists) {
        return .negation(restrictedFormula)
      } else {
        return nil
      }
    case let .and(formulas):
      let restrictedFormulas = formulas.compactMap {$0.removing(
        parametersToRemove,
        atLeastOneExists: atLeastOneExists)}
      if restrictedFormulas.isEmpty {
        return nil
      } else {
        return .and(restrictedFormulas)
      }
    case let .or(formulas):
      let restrictedFormulas = formulas.compactMap {
        $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
      if restrictedFormulas.isEmpty {
        return nil
      } else {
        return .or(restrictedFormulas)
      }
    case let .preference(name: name , goal: goal):
      if let restrictedGoal = goal.removing(
          parametersToRemove,
          atLeastOneExists: atLeastOneExists) {
        return .preference(name: name, goal: restrictedGoal)
      } else {
        return nil
      }
    }
  }
  
  public var convertSoftToHardGoal: Self {
    switch self {
    case let .preference(name: _, goal: goal):
      return goal
    case let .and(formulas):
      return .and(formulas.map {$0.convertSoftToHardGoal})
    case let .or(formulas):
      return .or(formulas.map {$0.convertSoftToHardGoal})
    case let .negation(goal):
      return .negation(goal.convertSoftToHardGoal)
    case .atomic:
      return self
    }
  }
  
  public func keepingOnlyGoals(_ goalsNumbers: [Int]) -> Self {
    guard case let .and(goals) = self else {
      fatalError("Invalid goals structure")
    }
    var prunedGoals = [PDDLFormula]()
    let goalsNumbersSet = Set(goalsNumbers)
    for (idx, softGoal) in goals.enumerated() {
      if case .preference = softGoal
      {
        if goalsNumbersSet.contains(idx) {
          prunedGoals.append(softGoal)
        }
      } else {
        fatalError("Invalid goals structure")
      }
    }
    return .and(prunedGoals)
  }
  
  public var softGoals: [(name: String?, goal: PDDLAtomicTerm)]? {
    if case let .and(goals) = self {
      var softGoals = [(name: String?, goal: PDDLAtomicTerm)]()
      for softGoal in goals {
        if case let .preference(name: name, goal: goal) = softGoal,
           case let .atomic(atomicTerm) = goal
        {
          softGoals.append((name: name, goal: atomicTerm))
        }
      }
      return softGoals
    }
    return nil
  }
}
