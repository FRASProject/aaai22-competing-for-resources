//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/17/21.
//

import Foundation

public struct PDDLGame {
  public let domain: PDDLDomain
  public let problem: PDDLGameProblem
  public init(domain: PDDLDomain, problem: PDDLGameProblem) {
    self.domain = domain
    self.problem = problem
  }
  public struct Statistics: Codable {
    public let numberOfPlayers: Int
    public let playerMaxValue: [Int]
    public let numberOfUnits: [Int]
    public let numberOfSoftGoals: [Int]
    public var asDictionary: [String : AIKitData] {
      var ds = Dictionary(uniqueKeysWithValues: numberOfUnits.enumerated().map {
        ("player\($0.offset + 1)Units", AIKitData.integer($0.element))})
      ds["numberSoftGoals"] = .integer(numberOfSoftGoals.max()!)
      ds["player1MaxValue"] = .double(Double(playerMaxValue[0]))
      ds["player2MaxValue"] = .double(Double(playerMaxValue[1]))
      return ds
    }
  }
}

extension PDDLGame {
  public var players: [String] {
    problem.initState.players
  }
  public var playersUnits: [[String]] {
    players.map { problem.initState.getUnits(of: $0) }
  }
  public var softGoals: [[(name: String?, goal: PDDLAtomicTerm)]] {
    problem.goals.map {$0.softGoals!}
  }
  /// Number of units indexed player nunmber
  public var numberOfUnits: [Int] {
    playersUnits.map {$0.count}
  }
  public var numberOfSoftGoals: [Int] {
    softGoals.map {$0.count}
  }
  public func goalsWeights(of player: Int) -> [Int] {
    let weights = problem.metrics[player].softGoalsWeights
    return softGoals[player].map {weights[$0.name!]!}
  }
  public var maxGameValue: [Int] {
    players.indices.map {goalsWeights(of: $0).sum}
  }
  public var statistics: Statistics {
    Statistics(
      numberOfPlayers: players.count,
      playerMaxValue: maxGameValue,
      numberOfUnits: numberOfUnits,
      numberOfSoftGoals: numberOfSoftGoals)
  }
  public func createProblemsForGrounding() -> [PDDLPlanningProblem] {
    problem.goals.indices.map {
      PDDLPlanningProblem(
        domain: PDDLDomain(
          name: domain.name,
          requirements: [
            ":adl",
            ":strips",
            ":typing",
            ":equality",
            ":fluents",
            ":durative-actions",
            ":negative-preconditions",
            ":action-costs"
          ],
          types: domain.types,
          constants: domain.constants,
          predicates: domain.predicates,
          functions: domain.functions,
          actions: domain.actions,
          durativeActions: domain.durativeActions),
        problem: PDDLProblem(
          name: problem.name,
          domainName: problem.domainName,
          initState: problem.initState,
          goal: problem.goals[$0].convertSoftToHardGoal,
          metrics: nil)).keepingOnly(player: $0)
    }
//    problem.goals.map {
//      goal: $0.convertSoftToHardGoal,
//    }
    // add these requirements? :negative-preconditions :action-costs
  }
  

  public func getPlanningProblem(for player: Int) -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: problem.getProblem(for: player))
  }
  
}
