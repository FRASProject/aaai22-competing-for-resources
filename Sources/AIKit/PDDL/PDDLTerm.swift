//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public enum PDDLTerm: Hashable {
  case name(String)
  case variable(String)
}

extension PDDLTerm {
  public var termName: String {
    switch self {
    case let .name(name):
      return name
    case let .variable(variable):
      return variable
    }
  }
}
