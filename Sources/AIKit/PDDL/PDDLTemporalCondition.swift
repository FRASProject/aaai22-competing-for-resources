//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/21.
//

import Foundation

public struct PDDLTemporalCondition: Hashable {
  public enum TimeSpecifier {
    case atStart
    case atEnd
    case overAll
  }
  public let timeSpecifier: TimeSpecifier
  public let term: PDDLFormula
}

extension PDDLTemporalCondition {
  public func removing(
    _ parametersToRemove: [PDDLTerm],
    atLeastOneExists: Set<PDDLTerm>) -> PDDLTemporalCondition?
  {
    if let restrictedTerm = term.removing(parametersToRemove, atLeastOneExists: atLeastOneExists) {
      return PDDLTemporalCondition(timeSpecifier: timeSpecifier, term: restrictedTerm)
    } else {
      return nil
    }
  }
}
