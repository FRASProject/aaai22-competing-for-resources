//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/27/20.
//

import Foundation

public struct PDDLDurativeActionSubstituted {
  public let original: PDDLDurativeAction
  public let substituted: PDDLDurativeAction
  public let substitution: [String: String]
}

extension PDDLDurativeAction {
  public func substitute(_ substitutionParameters: [String]) -> PDDLDurativeActionSubstituted {
    precondition(parameters.count == substitutionParameters.count)
    let values = Dictionary(
      uniqueKeysWithValues: zip(parameters, substitutionParameters).map {($0.0.identifier, $0.1)})
    return PDDLDurativeActionSubstituted(
      original: self,
      substituted: PDDLDurativeAction(
        name: name,
        parameters: parameters,
        duration: duration.substitute(values),
        condition: condition.map {$0.substitute(values)},
        effect: effect.map {$0.substitute(values)}),
      substitution: values)
  }
}

extension PDDLTemporalCondition {
  public func substitute(_ values: [String: String]) -> Self {
    PDDLTemporalCondition(
      timeSpecifier: timeSpecifier,
      term: term.substitute(values))
  }
}

extension PDDLActionDuration {
  public func substitute(_ values: [String: String]) -> Self {
    switch self {
    case .number:
      return self
    case let .expression(exp):
      return .expression(exp.substitute(values))
    }
  }
}

extension PDDLFormula {
  public func substitute(_ values: [String: String]) -> Self {
    switch self {
    case let .atomic(term):
      return .atomic(term.substitute(values))
    case let .negation(formula):
      return .negation(formula.substitute(values))
    case let .and(formulas):
      return .and(formulas.map {$0.substitute(values)})
    case let .or(formulas):
      return .or(formulas.map {$0.substitute(values)})
    case let .preference(name: name, goal: goal):
      return .preference(name: name, goal: goal.substitute(values))
    }
  }
}

extension PDDLTemporalEffect {
  public func substitute(_ values: [String: String]) -> Self {
    PDDLTemporalEffect(
      timeSpecifier: timeSpecifier,
      term: term.substitute(values))
  }
}

extension PDDLEffect {
  public func substitute(_ values: [String: String]) -> Self {
    switch self {
    case let .term(term):
      return .term(term.substitute(values))
    case let .negativeTerm(term):
      return .negativeTerm(term.substitute(values))
    case let .assignment(assignment):
      return .assignment(assignment.substitute(values))
    }
  }
  
  public var asPDDLNameLiteral: PDDLNameLiteral? {
    switch self {
    case let .term(term):
      if let atomTerm = PDDLAtomicNameFormula(atomicTerm: term) {
        return PDDLNameLiteral.atomic(atomTerm)
      }
    case let .negativeTerm(term):
      if let atomTerm = PDDLAtomicNameFormula(atomicTerm: term) {
        return PDDLNameLiteral.not(atomTerm)
      }
    case .assignment:
      break
    }
    return nil
  }
}

extension PDDLAtomicNameFormula {
  public init?(atomicTerm: PDDLAtomicTerm) {
    let parameters = atomicTerm.parameters.compactMap { $0.name }
    if parameters.count == atomicTerm.parameters.count {
      self.name = atomicTerm.name
      self.parameters = parameters
    } else {
      return nil
    }
  }
}

extension PDDLAssignmentEffect {
  public func substitute(_ values: [String: String]) -> Self {
    PDDLAssignmentEffect(
      type: type,
      variable: variable.substitute(values),
      expression: expression.substitute(values))
  }
}

extension PDDLExpression {
  public func substitute(_ values: [String: String]) -> Self {
    switch self {
    case let .plus(exp1, exp2):
      return .plus(exp1.substitute(values), exp2.substitute(values))
    case let .minus(exp1, exp2):
      return .minus(exp1.substitute(values), exp2.substitute(values))
    case let .multiply(exp1, exp2):
      return .multiply(exp1.substitute(values), exp2.substitute(values))
    case let .divide(exp1, exp2):
      return .divide(exp1.substitute(values), exp2.substitute(values))
    case let .unaryMinus(exp):
      return .unaryMinus(exp.substitute(values))
    case .number:
      return self
    case let .function(function):
      return .function(function.substitute(values))
    }
  }
}

extension PDDLFunction {
  public func substitute(_ values: [String: String]) -> Self {
    PDDLFunction(
      name: name,
      parameters: parameters.substitute(values))
  }
}

extension PDDLAtomicTerm {
  public func substitute(_ values: [String: String]) -> Self {
    PDDLAtomicTerm(
      name: name,
      parameters: parameters.substitute(values))
  }
}

extension Array where Element == PDDLTerm {
  public func substitute(_ values: [String: String]) -> Self {
    self.map { parameter -> PDDLTerm in
      switch parameter {
      case let .variable(variable):
        return PDDLTerm.name(values[variable] ?? variable)
      case .name:
        return parameter
      }
    }
  }
}

extension PDDLTerm {
  public var variable: String? {
    switch self {
    case let .variable(variable):
      return variable
    case .name:
      return nil
    }
  }
  public var name: String? {
    switch self {
    case let .name(name):
      return name
    case .variable:
      return nil
    }
  }
}
