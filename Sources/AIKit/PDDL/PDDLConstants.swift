//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/6/22.
//

import Foundation

public enum PDDLConstants {
  public static let projectedActionSuffix = "-proj"
}
