//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

public struct PDDLProblem {
  public let name: String
  public let domainName: String
  public private(set) var initState: PDDLInitState
  public private(set) var goal: PDDLFormula
  public private(set) var metrics: PDDLMetric?
  public var predicates: [PDDLNameLiteral] {
    initState.predicates
  }
  public var functions: [PDDLFunctionLiteral] {
    initState.functions
  }
}

extension PDDLProblem {
  public func getUnits(of player: String) -> [String] {
    initState.getUnits(of: player)
  }
  /// Create a new pddl planning problem that only contains specified objects of the given type.
  /// First, it removes the not specified objects of the given type.
  /// In each predicate in the init state we schrink the number of objects of the given type. If the shrinken predicate contains
  /// an object of the given type that is not in identifiers parameter, then it is deleted.
  /// It will do the same for the functions in the init state.
  ///
  /// - Parameters:
  ///   - type: Object type name
  ///   - identifiers: Objects to keep identifiers
  /// - Returns: Restricted pddl planning problem.
  public func restrictingNumberOfObjectsOf(type: String, to identifiers: [String]) -> PDDLProblem {
    //TODO: Finish projections also on goals and metrics
    return PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState.restrictingNumberOfObjectsOf(
        type: type,
        to: identifiers),
      goal: goal,
      metrics: metrics)
  }
  
  public var units: [String] {
    initState.units
  }
  public var players: [String] {
    initState.players
  }
  
  public func keepingOnlyGoals(_ goalsNumbers: [Int]) -> PDDLProblem {
    PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState,
      goal: goal.keepingOnlyGoals(goalsNumbers),
      metrics: metrics)
  }
  
  public func convertingSoftGoalsToHardGoalsRemovingMetrics() -> PDDLProblem {
    PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState,
      goal: goal.convertSoftToHardGoal,
      metrics: nil)
  }
  
  public func keepingOnly(player: String) -> PDDLProblem {
    return PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState.keepingOnly(player: player),
      goal: goal,
      metrics: metrics)
  }
  
  public func transformTemporalToClassical(
    durativePredicatesMapping: [String : String],
    durationConstantPredicates: [String : Int],
    removeDurationFunctions: Bool) -> (
    problem: PDDLProblem,
    unitTimestampDuration: Int,
    maxDuration: Int)
  {
    let (newInitState, unitTimestampDuration, maxDuration) = initState.transformTemporalToClassical(
      durativePredicatesMapping: durativePredicatesMapping,
      durationConstantPredicates: durationConstantPredicates,
      removeDurationFunctions: removeDurationFunctions)
    return (PDDLProblem(
              name: name,
              domainName: domainName,
              initState: newInitState,
              goal: goal,
              metrics: metrics),
            unitTimestampDuration,
            maxDuration)
  }
  public var turns: [PDDLTypedParameter] {
    initState.turns
  }
  
  public func addingTurnsTimeCost() -> PDDLProblem {
    let turnsDurationFunction = turns.map {
      PDDLFunctionLiteral(
        function: PDDLFunction(
          name: "turn-duration",
          parameters: [PDDLTerm.name($0.identifier)]),
        value: Int($0.identifier.dropFirst())!)
    }
    return PDDLProblem(
      name: name,
      domainName: domainName,
      initState: PDDLInitState(
        objects: initState.objects,
        predicates: initState.predicates,
        functions: initState.functions + turnsDurationFunction),
      goal: goal,
      metrics: metrics)
  }
  
  public func addOrderingHeuristics(
    ordering: [(unit: String, goalsOrder: [(factName: String, factParameters: [String])])]) -> PDDLProblem
  {
    let lastCountNumber = ordering.map {$0.goalsOrder.count}.max()!
    let newInitState = initState
      .addingCountsObjectsAndPredicates(lastCountNumber: lastCountNumber)
      .addingOrderingHeuristic(ordering: ordering)
    return PDDLProblem(
      name: name,
      domainName: domainName,
      initState: newInitState,
      goal: goal,
      metrics: metrics)
  }
  
  public func addingTurnsObjectsAndPredicates(lastTurnNumber: Int) -> PDDLProblem {
    return PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState.addingTurnsObjectsAndPredicates(lastTurnNumber: lastTurnNumber),
      goal: goal,
      metrics: metrics)
  }
  
  public func addingCostFunctions(
    _ costFunctions: [PDDLNameLiteral : [Int]],
    lastTimestamp: Int) -> PDDLProblem
  {
    PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState.addingCostFunctions(
        costFunctions,
        lastTimestamp: lastTimestamp),
      goal: goal,
      metrics: metrics)
  }
  
  public mutating func addCostFunction(
    name: String,
    parameters: [String],
    costFunction: [Int])
  {
    initState.addCostFunction(
      name: name,
      parameters: parameters,
      costFunction: costFunction)
  }
  
  public func addingDummyTotalCostFunction() -> PDDLProblem {
    PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState.addingTotalCostFunction(),
      goal: goal,
      metrics: .minimizeTotalCost)
  }
}
