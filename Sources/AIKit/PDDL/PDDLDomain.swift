//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/17/21.
//

import Foundation

public struct PDDLDomain {
  public let name: String
  public let requirements: [String]
  public let types: [PDDLTypedParameter]
  public let constants: [PDDLTypedParameter]
  public let predicates: [PDDLPredicateSkeleton]
  public let functions: [PDDLFunctionSkeleton]
  public let actions: [PDDLAction]
  public let durativeActions: [PDDLDurativeAction]
  public var isClassical: Bool {
    durativeActions.isEmpty
  }
  public static let unitType = "unit"
  public static let playerType = "player"
  public static let totalCostFluent = "total-cost"
  public static let totalCostSyncFluent = "total-cost-sync"
  public static let penaltyPrefix = "penalty-"
  public static let turnsGeqPredicate = "turns-geq"
  public static let turnsAddPredicate = "turns-add"
  public static let unitTurnPredicate = "turn-unit"
  public static let turnObjectType = "turns"
  public static let noOpActionName = "noop"
  public static let totalCostFunction = PDDLFunctionLiteral(
    function: PDDLFunction(name: PDDLDomain.totalCostFluent, parameters: []), value: 0)
  public static let totalCostSyncFunction = PDDLFunctionLiteral(
    function: PDDLFunction(name: PDDLDomain.totalCostSyncFluent, parameters: []), value: 0)
}

extension PDDLDomain {
  
  public func removingActions(_ actionsToRemove: Set<String>) -> PDDLDomain {
    let newActions = actions.filter {!actionsToRemove.contains($0.originalName)}
    let newDurativeActions = durativeActions.filter {!actionsToRemove.contains($0.originalName)}
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates,
      functions: functions,
      actions: newActions,
      durativeActions: newDurativeActions)
  }
  
  public func findAdversarialCriticalDurativeActions(
    playerGroundedActions: [PlanAction],
    adversariesGroundedActions: [PlanAction],
    initState: PDDLInitState) -> [PDDLNameLiteral: CriticalCluster]
  {
    var criticalFacts = [PDDLNameLiteral: (
      critical: Set<PlanAction>,
      adversarial: Set<PlanAction>
    )]()
    let candidates = self.findPossibleAdversarialCriticalDurativeActions()
    
    let adversarialActionNames = Dictionary(
      candidates.map {($0.adversarialAction.name, $0.adversarialAction)},
      uniquingKeysWith: {a, b in a})
    let criticalActionNames = Dictionary(
      candidates.map {($0.criticalAction.name, $0.criticalAction)},
      uniquingKeysWith: {a, b in a})
    let adversarialActionAdversarialEffects = Dictionary(
      grouping: candidates, by: {$0.adversarialAction}).mapValues {
        $0.flatMap {$0.adversarialEffects} }.mapValues {Set($0)}
    let criticalActionAdversarialEffects = Dictionary(
      grouping: candidates, by: {$0.criticalAction}).mapValues {
        $0.flatMap {$0.adversarialEffects} }.mapValues {Set($0)}
    
    for playerGroundedAction in playerGroundedActions {
      if let criticalAction = criticalActionNames[playerGroundedAction.name] {
        let substitutedAction = criticalAction.substitute(playerGroundedAction.parameters)
        let criticalPDDLNameLiterals = criticalActionAdversarialEffects[criticalAction]!.compactMap {
          $0.substitute(substitutedAction.substitution).negation?.asPDDLNameLiteral
        }
        for criticalPDDLNameLiteral in criticalPDDLNameLiterals {
          var (criticalFDRActions, adversarialFDRActions) = criticalFacts[criticalPDDLNameLiteral] ??
            ([],[])
          criticalFDRActions.insert(playerGroundedAction)
          criticalFacts[criticalPDDLNameLiteral] = (criticalFDRActions, adversarialFDRActions)
        }
      }
    }
    for adversaryGroundedAction in adversariesGroundedActions {
      if let adversarialAction = adversarialActionNames[adversaryGroundedAction.name] {
        let substitutedAction = adversarialAction.substitute(adversaryGroundedAction.parameters)
        let criticalPDDLNameLiterals = adversarialActionAdversarialEffects[adversarialAction]!.compactMap {
          $0.substitute(substitutedAction.substitution).negation?.asPDDLNameLiteral
        }
        for criticalPDDLNameLiteral in criticalPDDLNameLiterals {
          var (criticalFDRActions, adversarialFDRActions) = criticalFacts[criticalPDDLNameLiteral] ?? ([],[])
          adversarialFDRActions.insert(adversaryGroundedAction)
          criticalFacts[criticalPDDLNameLiteral] = (criticalFDRActions, adversarialFDRActions)
        }
      }
    }
    
    return Dictionary(
      uniqueKeysWithValues: criticalFacts.filter { initState.contains($0.key)}.map {
      ($0.key, CriticalCluster(
        criticalFact: $0.key,
        criticalActions: $0.value.critical,
        adversarialActions: $0.value.adversarial))
      })
  }
  
  /// Restricts the number of arguments of the given type in each predicate, function, action. Assuming the arguments are commutative.
  /// - Parameters:
  ///   - type: Type
  ///   - number: max number of arguments of the given type
  /// - Returns: Modified domain.
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> PDDLDomain? {
    precondition(types.someSatisfies {$0.identifier == type})
    let newActions = actions.compactMap {$0.restrictingNumberOfObjectsOf(type: type, to: number)}
    let newDurativeActions = durativeActions.compactMap {
      $0.restrictingNumberOfObjectsOf(type: type, to: number)
    }
    if newActions.isEmpty && newDurativeActions.isEmpty {
      return nil
    } else {
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates.map {$0.restrictingNumberOfObjectsOf(type: type, to: number)},
      functions: functions.map {$0.restrictingNumberOfObjectsOf(type: type, to: number)},
      actions: newActions,
      durativeActions: newDurativeActions)
    }
  }
  
  public func restrictingNumberOfObjectsToOneOf(type: String) -> PDDLDomain? {
    precondition(types.someSatisfies {$0.identifier == type})
    let newActions = actions.flatMap {$0.restrictingNumberOfObjectsToOneOf(type: type)}
    let newDurativeActions = durativeActions.flatMap {
      $0.restrictingNumberOfObjectsToOneOf(type: type)
    }
    if newActions.isEmpty && newDurativeActions.isEmpty {
      return nil
    } else {
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates.map {$0.restrictingNumberOfObjectsOf(type: type, to: 1)},
      functions: functions.map {$0.restrictingNumberOfObjectsOf(type: type, to: 1)},
      actions: newActions,
      durativeActions: newDurativeActions)
    }
  }
  
  public func addingTotalCostFluent() -> PDDLDomain {
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates,
      functions: functions + [PDDLFunctionSkeleton(
                                name: PDDLDomain.totalCostFluent,
                                parameters: [])],
      actions: actions,
      durativeActions: durativeActions)
  }
  
  public func addingTotalCostSyncFluent() -> PDDLDomain {
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates,
      functions: functions + [PDDLFunctionSkeleton(
                                name: PDDLDomain.totalCostSyncFluent,
                                parameters: [])],
      actions: actions,
      durativeActions: durativeActions)
  }
  
  public func getDurativeAction(by name: String) -> PDDLDurativeAction? {
    durativeActions.first(where: {$0.name == name})
  }
  
  public var allEffects: [PDDLEffect] {
    let effectsDA = durativeActions.flatMap { $0.effect.map { $0.term } }
    return effectsDA
  }
  public func findPossibleAdversarialCriticalDurativeActions() ->
  [(
    criticalAction: PDDLDurativeAction,
    adversarialAction: PDDLDurativeAction,
    adversarialEffects: [PDDLEffect]
  )]
  {
    let allEffects = Set(self.allEffects.compactMap { $0.name })
    var result = [(PDDLDurativeAction, PDDLDurativeAction, [PDDLEffect])]()
    for a1 in self.durativeActions {
      for a2 in self.durativeActions {
        let adversarialEffects = a1.possiblyAdversarialEffects(to: a2).filter {
          if let negation = $0.negation, !allEffects.contains(negation.name!) {
            return true
          } else {
            return false
          }
        }
        if !adversarialEffects.isEmpty {
          result.append((a2, a1, adversarialEffects))
        }
      }
    }
    return result
  }
  
  public func findPossibleCriticalActionsAndFacts() -> [(
    criticalFact: PDDLEffect,
    criticalActions: [PDDLDurativeAction])]
  {
    let clusters = findPossibleAdversarialCriticalDurativeActions()
    
    let factOrder = clusters.flatMap {
      $0.adversarialEffects.map {$0.negation!}}.removingDuplicates()
    
    var factActions = [PDDLEffect: [PDDLDurativeAction]]()
    for (criticalAction, _, adversarialEffects) in clusters {
      for adversarialEffect in adversarialEffects {
        let criticalFact = adversarialEffect.negation!
        var actions = factActions[criticalFact] ?? []
        if !actions.contains(criticalAction) {
          actions.append(criticalAction)
          factActions[criticalFact] = actions
        }
      }
    }
    return factOrder.map {(criticalFact: $0, criticalActions: factActions[$0]!)}
  }
  public func addingCostFunctions(
    for cooperativeActionNamesParameters: [(name: String, parametersIndices: [Int])]) -> PDDLDomain
  {
    var domain = self
    var addedActions = Set<String>()
    for (actionName, parametersIndices) in cooperativeActionNamesParameters where
      !addedActions.contains(actionName) {
      addedActions.insert(actionName)
      
      domain = domain.addingCostFunction(
        to: actionName,
        actionParametersIndices: parametersIndices,
        costFunctionName: PDDLDomain.penaltyPrefix + actionName)
    }
    return domain
  }
  /// Add cost function to the specified action
  /// - Parameters:
  ///   - actionName: Name of the action
  ///   - parametersIndices: Parameters indices that will be used as parameters for the cost function fluent
  ///   - costFunctionName: Name of the cost function fluent
  /// - Returns: Updated domain
  public func addingCostFunction(
    to actionName:String,
    actionParametersIndices: [Int],
    costFunctionName: String,
    totalCostFluent: String = PDDLDomain.totalCostFluent) -> PDDLDomain
  {
    let actionIndex = actions.firstIndex(where: {$0.name == actionName})!
    let actionToModify = actions[actionIndex]
    let costFunction = PDDLFunctionSkeleton(
      name: costFunctionName,
      parameters: actionParametersIndices.map {actionToModify.parameters[$0]})
    var functions = self.functions
    if let existingCostFunction = functions.first(where: {$0.name == costFunctionName}) {
      precondition(existingCostFunction == costFunction, "Inconsistent cost functions.")
    } else {
      functions.append(costFunction)
    }
    var actions = self.actions
    actions[actionIndex] = actionToModify.addingPenaltyEffect(
      costFunctionName: costFunctionName,
      parameters: actionParametersIndices.map {.variable(actionToModify.parameters[$0].identifier)},
      costFluent: totalCostFluent)
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates,
      functions: functions,
      actions: actions,
      durativeActions: durativeActions)
  }
  public func addingCostFunctions(
    for criticalFacts: [(fact: PDDLEffect, actionNames: [String])]) -> PDDLDomain
  {
    
    func extractCostFunctionNameParameters(fact: PDDLEffect) -> (String, [PDDLTerm]) {
      switch fact {
      case let .term(term):
        return (PDDLDomain.penaltyPrefix + term.name, term.parameters)
      case let .negativeTerm(term):
        return (PDDLDomain.penaltyPrefix + "not-" + term.name, term.parameters)
      case .assignment:
        fatalError("Incorrect critical fact!")
      }
    }
    var domain = self
    for (fact, actionNames) in criticalFacts {
      let (costFunctionName, costFunctionParameters) = extractCostFunctionNameParameters(
        fact: fact)
      //let costFunctionParametersSet = Set(costFunctionParameters.map {$0.variable})
      for actionName in actionNames {
        let action = domain.actions.first(
          where: {$0.name == actionName})!
        
        
        
       // let actionParametersSet = Set(action.parameters.map {$0.identifier})
        
        
        //let relevantActionParameters = action.parameters.filter {
        //  costFunctionParametersSet.contains($0.identifier)}
        domain = domain.addingCostFunction(
          to: actionName,
          actionParametersIndices: action.parameters.map {
            $0.identifier}.indices(of: costFunctionParameters.map {$0.variable!}),
          costFunctionName: costFunctionName)
      }
    }
    return domain
  }
  
  public func getFunction(by name: String) -> PDDLFunctionSkeleton? {
    functions.first(where: {$0.name == name})
  }
  public func getPredicate(by name: String) -> PDDLPredicateSkeleton? {
    predicates.first(where: {$0.name == name})
  }
  
  public func addingOrderHeuristics(criticalActionFact: [String: String]) -> PDDLDomain {
    let newTypes = self.types + [PDDLTypedParameter(identifier: "count", type: "object")]
    var newPredicates = self.predicates
    newPredicates.append(PDDLPredicateSkeleton(
                          name: "unit-credits", parameters: [
                            PDDLTypedVariable(identifier: "u", type: "unit"),
                            PDDLTypedVariable(identifier: "c", type: "count")
                          ]))
    newPredicates.append(PDDLPredicateSkeleton(
                          name: "next-count", parameters: [
                            PDDLTypedVariable(identifier: "c1", type: "count"),
                            PDDLTypedVariable(identifier: "c2", type: "count")
                          ]))
    var addedCriticalFacts = Set<String>()
    for criticalFactName in criticalActionFact.values.sorted() {
      if !addedCriticalFacts.contains(criticalFactName) {
        addedCriticalFacts.insert(criticalFactName)
        let criticalFactVariables = self.predicates.first(where: {$0.name == criticalFactName})!.parameters
        newPredicates.append(PDDLPredicateSkeleton(
                              name: "allowed-\(criticalFactName)", parameters: criticalFactVariables + [
                                PDDLTypedVariable(identifier: "u", type: "unit"),
                                PDDLTypedVariable(identifier: "c", type: "count")
                              ]))
      }
    }
    var newActions = [PDDLAction]()
    for action in self.actions {
      if let factName = criticalActionFact[action.originalName] {
        newActions.append(action.addingUnitCreditTracking(criticalFactName: factName))
      } else {
        newActions.append(action)
      }
    }
    var newDurativeActions = [PDDLDurativeAction]()
    for action in self.durativeActions {
      if let factName = criticalActionFact[action.originalName] {
        newDurativeActions.append(action.addingUnitCreditTracking(criticalFactName: factName))
      } else {
        newDurativeActions.append(action)
      }
    }
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: newTypes,
      constants: constants,
      predicates: newPredicates,
      functions: functions,
      actions: newActions,
      durativeActions: newDurativeActions)
  }
  
  public func addingTurnsTimeCost() -> PDDLDomain {
    precondition(isClassical)
    let newActions = actions.map { $0.addingDurationTurnTotalCostEffect() }
    var newFunctions = functions
    newFunctions.append(
      PDDLFunctionSkeleton(
        name: "turn-duration",
        parameters: [PDDLTypedVariable(identifier: "t", type: "turns")]))
    return PDDLDomain(
      name: name,
      requirements: requirements,
      types: types,
      constants: constants,
      predicates: predicates,
      functions: newFunctions,
      actions: newActions,
      durativeActions: durativeActions)
  }
  
  public var asClassicalWithAgentUnitsAndTurnTimeTracking: (
    domain: PDDLDomain,
    durationPredicates: [String: String],
    durationConstantPredicates: [String: Int]
  )
  {
    // Turns predicates
    var newPredicates = self.predicates
    newPredicates.append(PDDLPredicateSkeleton(
                          name: "turn-unit", parameters: [
                            PDDLTypedVariable(identifier: "u", type: "unit"),
                            PDDLTypedVariable(identifier: "t", type: "turns")
                          ]))
    newPredicates.append(PDDLPredicateSkeleton(
                          name: "turns-geq", parameters: [
                            PDDLTypedVariable(identifier: "t1", type: "turns"),
                            PDDLTypedVariable(identifier: "t2", type: "turns")
                          ]))
    newPredicates.append(PDDLPredicateSkeleton(
                          name: "turns-add", parameters: [
                            PDDLTypedVariable(identifier: "t1", type: "turns"),
                            PDDLTypedVariable(identifier: "t2", type: "turns"),
                            PDDLTypedVariable(identifier: "t3", type: "turns")
                          ]))
    // change requirements
    
    var newActions = self.actions
    var durationPredicates = [String: String]()
    var durationConstantPredicates = [String: Int]()
    for durativeAction in self.durativeActions {
      let durativePredicate: PDDLPredicateSkeleton
      switch durativeAction.duration {
      case let .number(durationNumber):
        durativePredicate = PDDLPredicateSkeleton(
          name: "turns-\(durativeAction.name)",
          parameters: [PDDLTypedVariable(identifier: "t", type: "turns")])
        durationConstantPredicates[durativePredicate.name] = durationNumber
      case .expression:
        let durativeFunction = durativeAction.duration.durationFunction!
        durativePredicate = PDDLPredicateSkeleton(
          name: "turns-\(durativeFunction.name)",
          parameters: durativeFunction.parameters.map {
            PDDLTypedVariable(identifier: $0.variable!, type: nil)
          } + [
            PDDLTypedVariable(identifier: "t", type: "turns")
        ])
        durationPredicates[durativeFunction.name] = durativePredicate.name
      }
      
      newPredicates.append(durativePredicate)
      let turnsPredicate = PDDLAtomicTerm(
        name: durativePredicate.name,
        parameters: durativePredicate.parameters.map { .variable($0.identifier) })
      let action = PDDLAction(durativeAction).addingTurnsTracking(
        turnsPredicate: turnsPredicate)
      newActions.append(action)
    }
    
    // End turns predicates
    
    let newTypes = self.types + [PDDLTypedParameter(identifier: "turns", type: "object")]
    
    return (
      domain: PDDLDomain(
        name: name,
        requirements: [":typing",":action-costs"],
        types: newTypes,
        constants: constants,
        predicates: newPredicates,
        functions: functions,
        actions: newActions,
        durativeActions: []),
      durationPredicates: durationPredicates,
      durationConstantPredicates: durationConstantPredicates
    )
  }
}
