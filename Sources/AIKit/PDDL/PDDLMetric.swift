//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/24/21.
//

import Foundation

public enum PDDLMetricExpression {
  case number(Int)
  case totalTime
  case isViolated(String)
  case function(String)
  indirect case multinary(operator: String, [PDDLMetricExpression])
  indirect case binary(operator: String, PDDLMetricExpression, PDDLMetricExpression)
  indirect case unary(operator: String, PDDLMetricExpression)
}

public enum PDDLMetric {
  public static let minimizeTotalTime = PDDLMetric.minimize(.totalTime)
  public static let minimizeTotalCost = PDDLMetric.minimize(.function(PDDLDomain.totalCostFluent))
  public static let minimizeTwoTotalCost = PDDLMetric.minimize(
    .binary(
      operator: "+",
      .function(PDDLDomain.totalCostFluent),
      .function(PDDLDomain.totalCostSyncFluent)))
  case minimize(PDDLMetricExpression)
  case maximize(PDDLMetricExpression)
  public var isMinTotalTime: Bool {
    if case let .minimize(exp) = self,
       case .totalTime = exp {
      return true
    }
    return false
  }
}

extension PDDLMetric {
  var softGoalsWeights: [String: Int] {
    switch self {
    case let .maximize(exp):
      return exp.softGoalsWeights
    case let .minimize(exp):
      return exp.softGoalsWeights
    }
  }
}

extension PDDLMetricExpression {
  var softGoalsWeights: [String: Int] {
    if case let .multinary(op, exps) = self, op == "+" {
      var result = [String: Int]()
      for exp in exps {
        result.merge(exp.softGoalsWeights, uniquingKeysWith: { a, _ in a })
      }
      return result
    } else if case let .binary(op, exp1, exp2) = self, op == "*",
       case let .number(n) = exp1, case let .isViolated(name) = exp2 {
      return [name: n]
    } else if case let .binary(_, exp1, exp2) = self {
      return exp1.softGoalsWeights.merging(exp2.softGoalsWeights, uniquingKeysWith: { a, _ in a})
    } else if case let .unary(_, exp) = self {
      return exp.softGoalsWeights
    } else {
      return [:]
    }
  }
}
