//
//  PDDLPlanningProblem.swift
//  AIKit
//
//  Created by Pavel Rytir on 10/4/20.
//

import Foundation

public struct PDDLPlanningProblemOld {
  public let raw: String?
  public let name: String
  public let domainName: String
  public let objects: [PDDLObjectOld]
  public let initState: PDDLGenerationStateOld
  public let goalState: PDDLGenerationStateOld
  public let metric: String?
  public static let minimizeTotalTimeMetric = "(:metric minimize (total-time))"
  public static let minimizeTotalCostMetric = "(:metric minimize (total-cost))"
  public init(
    name: String,
    domainName: String,
    objects: [PDDLObjectOld],
    initState: PDDLGenerationStateOld,
    goalState: PDDLGenerationStateOld,
    metric: String?)
  {
    self.name = name
    self.domainName = domainName
    self.objects = objects
    self.initState = initState
    self.goalState = goalState
    self.metric = metric
    self.raw = nil
  }
  public init(raw: String) {
    self.raw = raw
    self.name = ""
    self.domainName = ""
    self.objects = []
    self.initState = PDDLGenerationStateOld()
    self.goalState = PDDLGenerationStateOld()
    self.metric = nil
  }
}

extension PDDLPlanningProblemOld {
  public var stringRepresentation: String {
    if let raw = raw {
      return raw
    }
    var pddl = "(define (problem \(self.name))\n"
    pddl += "(:domain \(self.domainName))\n"
    pddl += "(:objects \n\(objects.objectsStringRepresentation))\n"
    pddl += "(:init\n\(initState.stringRepresentation))\n"
    pddl += "(:goal\n(and \n\(goalState.stringRepresentation))\n)\n"
    
    if let metric = self.metric {
      pddl += metric + "\n"
    }
    pddl += ")\n"
    return pddl
  }
}
