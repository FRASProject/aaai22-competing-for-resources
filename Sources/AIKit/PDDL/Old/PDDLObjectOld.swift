//
//  PDDLObject.swift
//  AIKit
//
//  Created by Pavel Rytir on 10/4/20.
//

import Foundation

public protocol PDDLObjectOld {
  var name: String { get }
  var typeName: String { get }
  func isEqualTo(other: PDDLObjectOld) -> Bool
}

extension PDDLObjectOld where Self: Equatable {
  public func isEqualTo(other: PDDLObjectOld) -> Bool {
    if let o = other as? Self {
      return self == o
    }
    return false
  }
}

extension Array where Element == PDDLObjectOld {
  var objectsStringRepresentation: String {
    var pddl = ""
    let typeObjects = Dictionary(grouping: self, by: {$0.typeName})
    let typeOjectsSorted = typeObjects.sorted(by: {$0.key < $1.key })
    
    for (typeName, objects) in typeOjectsSorted {
      let sortedObjects = objects.sorted(by: {
        $0.typeName < $1.typeName || ($0.typeName == $1.typeName) && $0.name < $1.name
      })
      for object in sortedObjects {
        pddl += "\(object.name)\n"
      }
      pddl += " - \(typeName)\n"
    }
    return pddl
  }
}

extension PDDLObjectOld {
  public func isLess(than other:PDDLObjectOld) -> Bool {
    return self.typeName < other.typeName ||
      (self.typeName == other.typeName) && self.name < other.name
  }
}

struct PDDLIntObject: PDDLObjectOld, Equatable {
  let id: Int?
  let typeName: String
  let prefix: String
  var name: String {
    prefix + (id?.description ?? "")
  }
  public init(prefix: String, id: Int, typeName: String) {
    self.prefix = prefix
    self.id = id
    self.typeName = typeName
  }
  /// Construct a player object from a string.
  /// - Parameter s: Assuming format prefixId
  public init(_ s: String, typeName: String) {
    if let idRange = s.range(of: "\\d*$", options: .regularExpression) {
      self.id = Int(s[idRange])!
      self.prefix = String(s[s.startIndex..<idRange.lowerBound])
    } else {
      self.prefix = s
      self.id = nil
    }
    self.typeName = typeName
    assert(!self.prefix.isEmpty)
  }
}

public struct SimplePDDLObject: PDDLObjectOld, Equatable {
  public init(name: String, typeName: String) {
    self.name = name
    self.typeName = typeName
  }
  public let name: String
  public let typeName: String
}

public struct PDDLPlayer: PDDLObjectOld, Equatable {
  public let id: Int
  public let prefix: String
  public var name: String {
    return prefix + "\(id)"
  }
  public let typeName = "player"
  public init(id: Int) {
    self.id = id
    self.prefix = "p"
  }
  /// Construct a player object from a string.
  /// - Parameter s: Assuming format prefixId
  public init(_ s: String) {
    let idRange = s.range(of: "\\d*$", options: .regularExpression)!
    self.id = Int(s[idRange])!
    self.prefix = String(s[s.startIndex..<idRange.lowerBound])
    assert(!prefix.isEmpty)
  }
}

public struct PDDLTurn: PDDLObjectOld, Equatable {
  public let id: Int
  public var name: String {
    return "t\(id)"
  }
  public let typeName = "turns"
  public init(id: Int) {
    self.id = id
  }
}
