//
//  PDDLState.swift
//  AIKit
//
//  Created by Pavel Rytir on 10/4/20.
//

import Foundation

public struct PDDLGenerationStateOld {
  public private(set) var predicates: [PDDLPredicateOld]
  public private(set) var functions: [PDDLFunctionOld]
  public private(set) var temporalActionsFluents: [PDDLFunctionOld]
  public init() {
    self.predicates = []
    self.functions = []
    self.temporalActionsFluents = []
  }
  init(
    predicates: [PDDLPredicateOld],
    functions: [PDDLFunctionOld],
    temporalActionsFluents: [PDDLFunctionOld])
  {
    self.predicates = predicates
    self.functions = functions
    self.temporalActionsFluents = temporalActionsFluents
  }
  mutating public func add(predicate: PDDLPredicateOld) {
    predicates.append(predicate)
  }
  mutating public func add(function: PDDLFunctionOld) {
    functions.append(function)
  }
  mutating public func add(temporalActionFluent: PDDLFunctionOld) {
    temporalActionsFluents.append(temporalActionFluent)
  }
  mutating public func merge(_ other: PDDLGenerationStateOld) {
    predicates.append(contentsOf: other.predicates)
    functions.append(contentsOf: other.functions)
  }
  
  
  public var predicatesStringRepresentation: String {
    var pddlPredicates = ""
    var predicates = self.predicates
    predicates.sort(by: {$0.name < $1.name})
    for predicate in predicates {
      pddlPredicates += predicate.pDDLString + "\n"
    }
    return pddlPredicates
  }
  
  public var functionsStringRepresentation: String {
    var pddlFunctionsStr = ""
    var functions = self.functions + self.temporalActionsFluents
    functions.sort(by: {$0.name < $1.name})
    for function in functions {
      pddlFunctionsStr += function.pDDLString + "\n"
    }
    return pddlFunctionsStr
  }
  public var stringRepresentation: String {
    predicatesStringRepresentation + functionsStringRepresentation
  }
  
  public func filtered(without object: PDDLObjectOld) -> PDDLGenerationStateOld {
    return PDDLGenerationStateOld(
      predicates: predicates.map { $0.filtered(without: object) },
      functions: functions.map { $0.filtered(without: object) },
      temporalActionsFluents: temporalActionsFluents.map {
        $0.filtered(without: object) }
    )
  }
}
