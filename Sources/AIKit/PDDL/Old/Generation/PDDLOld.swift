//
//  pddl.swift
//  Planner
//
//  Created by Pavel Rytir on 4/21/18.
//

import Foundation

public struct PDDLFunctionOld {
  public init(
    data: ([[PDDLObjectOld]], [Double]),
    name: String,
    arity: Int,
    outputNumberFormat: String = ".2f")
  {
    assert(data.0.count == data.1.count)
    assert(arity > 0, "Use init(nullaryOutput: for arity 0.")
    self.input = data.0
    self.output = data.1
    self.arity = arity
    self.name = name
    self.outputNumberFormat = outputNumberFormat
  }
  public init(
    data: [([PDDLObjectOld], Double)],
    name: String,
    arity: Int,
    outputNumberFormat: String = ".2f")
  {
    assert(arity > 0, "Use init(nullaryOutput: for arity 0.")
    self.input = data.map { $0.0 }
    self.output = data.map { $0.1 }
    self.arity = arity
    self.name = name
    self.outputNumberFormat = outputNumberFormat
  }
  public init(nullaryOutput: Double, name: String, outputNumberFormat : String = ".2f") {
    self.input = nil
    self.output = [nullaryOutput]
    self.arity = 0
    self.name = name
    self.outputNumberFormat = outputNumberFormat
  }
  public var input: [[PDDLObjectOld]]?
  public var output : [Double]
  public let arity : Int
  public let name : String
  public let outputNumberFormat : String
  
  public var pDDLString : String {
    var pddlStr = ""
    if let input = input {
      
      var sortedInputOutput = [([PDDLObjectOld],Double)]()
      for instanceIdx in input.indices {
        sortedInputOutput.append((input[instanceIdx],output[instanceIdx]))
      }
      sortedInputOutput.sort(by: { (a:([PDDLObjectOld],Double),b:([PDDLObjectOld],Double)) -> Bool in
        if a.1 < b.1 {
          return true
        }
        if a.1 > b.1 {
          return false
        }
        let k = max(a.0.count,b.0.count)
        for i in 0..<k {
          if i == a.0.count {
            return true
          }
          if i == b.0.count {
            return false
          }
          if a.0[i].isLess(than: b.0[i]) {
            return true
          }
          if b.0[i].isLess(than: a.0[i]) {
            return false
          }
        }
        return false
      })
      
      // TODO: sort input and output
      for inputOutput in sortedInputOutput {
        var instanceStr = "(= (\(name)"
        for object in inputOutput.0 {
          instanceStr += " \(object.name)"
        }
        instanceStr += ") "
        instanceStr += String(format: "%\(outputNumberFormat)", inputOutput.1) + ")"
        pddlStr += instanceStr + "\n"
      }
    } else {
      // Nullary function (constant)
      pddlStr = "(= (\(name) ) " + String(format: "%\(outputNumberFormat)", output[0]) + ")" + "\n"
    }
    return pddlStr
  }
  
  public var data: [([PDDLObjectOld], Double)] {
    if let input = input {
      var result = [([PDDLObjectOld], Double)] ()
      for i in 0..<input.count {
        result.append((input[i], output[i]))
      }
      return result
    } else {
      return [([], output.first!)]
    }
  }
  
  public func filtered(without object: PDDLObjectOld) -> PDDLFunctionOld {
    if input == nil {
      return self
    }
    
    let filteredData = data.filter { $0.0.allSatisfy { !$0.isEqualTo(other: object) }}
    return PDDLFunctionOld(
      data: filteredData,
      name: name,
      arity: arity,
      outputNumberFormat: outputNumberFormat)
  }
  
}

public enum PDDLArgument {
  case object(PDDLObjectOld)
  case predicate(PDDLPredicateOldOld)
  public var description: String {
    switch self {
    case let .object(o):
      return "\(o)"
    case let .predicate(p):
      return "\(p)"
    }
  }
}

public struct PDDLPredicateOldOld {
  public let name: String
  public let arguments: [PDDLArgument]
  public let separator: String
  public var description: String {
    "(\(name) " + arguments.map {"\($0)"}.joined(separator: separator) + ")" + separator
  }
}

public struct PDDLPredicateOld {
  public init(data: [[PDDLObjectOld]], name: String, arity: Int) {
    self.data = data
    self.arity = arity
    self.name = name
  }
  public var data: [[PDDLObjectOld]]
  public let name: String
  public let arity : Int
  
  public var sortedData: [[PDDLObjectOld]] {
    var sortedData = data
    sortedData.sort(by: { (a: [PDDLObjectOld],b: [PDDLObjectOld]) -> Bool in
      let k = max(a.count,b.count)
      for i in 0..<k {
        if i == a.count {
          return true
        }
        if i == b.count {
          return false
        }
        if a[i].isLess(than: b[i]) {
          return true
        }
        if b[i].isLess(than: a[i]) {
          return false
        }
      }
      return false
    })
    return sortedData
  }
  
  public var pDDLString: String {
    var pddlStr = ""
    for instance in sortedData {
      var instanceStr = "(\(name)"
      for object in instance {
        instanceStr += " \(object.name)"
      }
      instanceStr += ")"
      pddlStr += instanceStr + "\n"
    }
    return pddlStr
  }
  
  public func filtered(without object: PDDLObjectOld) -> PDDLPredicateOld {
    let filteredData = data.filter { $0.allSatisfy { !$0.isEqualTo(other: object) }}
    return PDDLPredicateOld(
      data: filteredData,
      name: name,
      arity: arity)
  }
}
