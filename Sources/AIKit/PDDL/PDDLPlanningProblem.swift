//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/27/21.
//

import Foundation

public struct PDDLPlanningProblem {
  public let domain: PDDLDomain
  public let problem: PDDLProblem
  public let durationOfUnitTimestamp: Int?
  public let maxDuration: Int?
  public var isClassical: Bool {
    domain.isClassical
  }
  public init(domain: PDDLDomain, problem: PDDLProblem) {
    self.domain = domain
    self.problem = problem
    self.durationOfUnitTimestamp = nil
    self.maxDuration = nil
  }
  public init(domain domainPDDL: String, problem problemPDDL: String) throws {
    self.domain = try PDDLDomainProblemParser.parse(domain: domainPDDL)
    self.problem = try PDDLDomainProblemParser.parse(problem: problemPDDL)
    self.durationOfUnitTimestamp = nil
    self.maxDuration = nil
  }
  private init(
    domain: PDDLDomain,
    problem: PDDLProblem,
    durationOfUnitTimestamp: Int?,
    maxDuration: Int?)
  {
    self.domain = domain
    self.problem = problem
    self.durationOfUnitTimestamp = durationOfUnitTimestamp
    self.maxDuration = maxDuration
  }
}

extension PDDLPlanningProblem {
  
  public func addingDummyCostFunction() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain.addingTotalCostFluent(),
      problem: problem.addingDummyTotalCostFunction(),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func keepingOnly(player: Int) -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: problem
        .keepingOnly(player: problem.players[player]),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addTemporalFailingGoalActions() -> PDDLPlanningProblem {
    let goalPredicateNames = problem.goal.termNames.removingDuplicates()
    let predicatesDefinitions = goalPredicateNames.map { name in
      domain.predicates.first(where: { $0.name == name })!}
    let failingGoalActions = predicatesDefinitions.map {
      PDDLDurativeAction.createGoalFailingAction(goalPredicateDefinition: $0)
    }
    return PDDLPlanningProblem(
      domain: PDDLDomain(
        name: domain.name,
        requirements: domain.requirements,
        types: domain.types,
        constants: domain.constants,
        predicates: domain.predicates,
        functions: domain.functions,
        actions: domain.actions,
        durativeActions: domain.durativeActions + failingGoalActions),
      problem: problem,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingFailingGoalActionsWithCost() -> PDDLPlanningProblem {
    precondition(isClassical)
    let goalPredicateNames = problem.goal.termNames.removingDuplicates()
    let predicatesDefinitions = goalPredicateNames.map { name in
      domain.predicates.first(where: { $0.name == name })!}
    let failingGoalActions = predicatesDefinitions.map {
      PDDLAction.createGoalFailingAction(goalPredicateDefinition: $0)
    }
    return PDDLPlanningProblem(
      domain: PDDLDomain(
        name: domain.name,
        requirements: domain.requirements,
        types: domain.types,
        constants: domain.constants,
        predicates: domain.predicates,
        functions: domain.functions,
        actions: domain.actions + failingGoalActions,
        durativeActions: domain.durativeActions),
      problem: problem,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  /// Makes a projection of the planning problem to remaingObjects of all objects of the given type.
  /// See detail in the documentation of the used function.
  /// - Parameters:
  ///   - type: Object type
  ///   - remainingObjects: objects to keep
  /// - Returns: Projected planning problem.
  public func restrictingObjectsOf(type: String, to remainingObjects: [String]) -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain.restrictingNumberOfObjectsOf(type: type, to: remainingObjects.count)!,
      problem: problem.restrictingNumberOfObjectsOf(type: type, to: remainingObjects),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func restrictingToSingleObjectOf(
    type: String,
    to remainingObject: String) -> PDDLPlanningProblem
  {
    PDDLPlanningProblem(
      domain: domain.restrictingNumberOfObjectsToOneOf(type: type)!,
      problem: problem.restrictingNumberOfObjectsOf(type: type, to: [remainingObject]),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingOrderHeuristics(
    criticalActionFact: [String : String],
    allCriticalActionNames: Set<String>,
    ordering: [(unit: String, goalsOrder: [(factName: String, factParameters: [String])])]) -> PDDLPlanningProblem
  {
    let newDomain = domain.removingActions(allCriticalActionNames.subtracting(criticalActionFact.keys))
      .addingOrderHeuristics(criticalActionFact: criticalActionFact)
    let newProblem = problem.addOrderingHeuristics(ordering: ordering)
    return PDDLPlanningProblem(
      domain: newDomain,
      problem: newProblem,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func keepingOnlyGoals(_ goalsNumbers: [Int]) -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: problem.keepingOnlyGoals(goalsNumbers),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  public func convertingSoftGoalsToHardGoalsRemovingMetrics() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: problem.convertingSoftGoalsToHardGoalsRemovingMetrics(),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingTurnsTimeCost() -> PDDLPlanningProblem {
    let newDomain = domain.addingTurnsTimeCost()
    let newProblem = problem.addingTurnsTimeCost()
    return PDDLPlanningProblem(
      domain: newDomain,
      problem: newProblem,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func convertingToClassicalPlanningProblem(
    removeDurationFunctions: Bool) -> PDDLPlanningProblem
  {
    let (classicalDomain, durationPredicates, durationConstantPredicates) = domain.asClassicalWithAgentUnitsAndTurnTimeTracking
    // add critical fact function into domain
    
    let (classicalProblem, unitTimestampDuration, maxDuration) =
      self.problem.transformTemporalToClassical(
        durativePredicatesMapping: durationPredicates,
        durationConstantPredicates: durationConstantPredicates,
        removeDurationFunctions: removeDurationFunctions)
    let problemWithTurns = classicalProblem.addingTurnsObjectsAndPredicates(
      lastTurnNumber: maxDuration)
    let convertedProblem = PDDLPlanningProblem(
      domain: classicalDomain,
      problem: problemWithTurns,
      durationOfUnitTimestamp: unitTimestampDuration,
      maxDuration: maxDuration).addingNoOpAction()
    if problemWithTurns.metrics?.isMinTotalTime ?? false {
      return convertedProblem
        .addingTurnsTimeCost()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    } else {
      return convertedProblem
    }
  }
  
  public func addingTotalCostFunction() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain.addingTotalCostFluent(),
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: problem.initState.addingTotalCostFunction(),
        goal: problem.goal,
        metrics: problem.metrics),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingTwoTotalCostFunction() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain.addingTotalCostFluent().addingTotalCostSyncFluent(),
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: problem.initState.addingTwoTotalCostFunctions(),
        goal: problem.goal,
        metrics: problem.metrics),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingMinimizeTotalCostMetric() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: problem.initState,
        goal: problem.goal,
        metrics: .minimizeTotalCost),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingMinimizeTotalTimeMetric() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: problem.initState,
        goal: problem.goal,
        metrics: .minimizeTotalTime),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingMinimizeTwoTotalCostMetric() -> PDDLPlanningProblem {
    PDDLPlanningProblem(
      domain: domain,
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: problem.initState,
        goal: problem.goal,
        metrics: .minimizeTwoTotalCost),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func addingNoOpAction() -> PDDLPlanningProblem {
    precondition(isClassical)
    let noOpDurationPredicate = PDDLPredicateSkeleton(
      name: "turns-noop",
      parameters: [PDDLTypedVariable(identifier: "t", type: "turns")])
    let noOpDurationTerm = PDDLAtomicTerm(
      name: noOpDurationPredicate.name,
      parameters: [.variable("t")])
    let noOpDurationFormula = PDDLNameLiteral.atomic(PDDLAtomicNameFormula(
                                        name: "turns-noop",
                                        parameters: ["t1"]))
    let noOp = PDDLAction.createNoOpAction(noOpDurationTerm: noOpDurationTerm)
    return PDDLPlanningProblem(
      domain: PDDLDomain(
        name: domain.name,
        requirements: domain.requirements,
        types: domain.types,
        constants: domain.constants,
        predicates: domain.predicates + [noOpDurationPredicate],
        functions: domain.functions,
        actions: domain.actions + [noOp],
        durativeActions: domain.durativeActions),
      problem: PDDLProblem(
        name: problem.name,
        domainName: problem.domainName,
        initState: PDDLInitState(
          objects: problem.initState.objects,
          predicates: problem.initState.predicates + [noOpDurationFormula],
          functions: problem.initState.functions),
        goal: problem.goal,
        metrics: problem.metrics),
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  public func adding(
    actionCooperativeCostFunctions: [(
      action: ActionNameProjectedParameters,
      cost: [Int])]) -> PDDLPlanningProblem
  {
    let newDomain = domain.addingCostFunctions(for: actionCooperativeCostFunctions.map {
      ($0.action.name, $0.action.parametersIndices)
    })
    let costFunctionLastTurn = actionCooperativeCostFunctions.map {$0.cost.count - 1}.max() ?? 1
    let numberOfTurns = max(problem.turns.count - 1, costFunctionLastTurn)
    var newProblem = problem.addingTurnsObjectsAndPredicates(lastTurnNumber: numberOfTurns)
    for (action, costFunction) in actionCooperativeCostFunctions {
      newProblem.addCostFunction(
        name: PDDLDomain.penaltyPrefix + action.name,
        parameters: action.parametersInstance,
        costFunction: PDDLCriticalFactCostUtils.extend(costFunction: costFunction, to: numberOfTurns))
    }
    return PDDLPlanningProblem(
      domain: newDomain,
      problem: newProblem,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
  
  /// Converts cost function indexed by facts into cost function indexed by actions that have
  /// the critical fact in any precondition.
  /// - Parameters:
  ///   - criticalFactCostFunction:
  ///   - allCriticalFactCriticalActions:
  /// - Returns: Cost function with replecad indexation.
  public func convert(
    criticalFactCostFunction: [PDDLNameLiteral : [Int]],
    allCriticalFactCriticalActions: [(
      criticalFact: PDDLEffect,
      criticalActions: [PDDLDurativeAction])
    ]) -> [(
      action: ActionNameProjectedParameters,
      costFunction: [Int])]
  {
    var result = [(action: ActionNameProjectedParameters,
                    costFunction: [Int])]()
    for (criticalFact, costFunction) in criticalFactCostFunction {
      let (domainCriticalFact, domainActions) = allCriticalFactCriticalActions.first(where: {
        let criticalFactToFind = $0.criticalFact
        switch (criticalFact, criticalFactToFind) {
        case let (.atomic(nameLiteral), .term(term)):
          return nameLiteral.name == term.name
        case (.not, _):
          fatalError("Not implemented")
        default:
          return false
        }
      })!
      let domainFactParameters: [String]
      switch domainCriticalFact {
      case let .term(term):
        domainFactParameters = term.parameters.map {$0.variable!}
      default:
        fatalError()
      }
      for domainAction in domainActions {
        let name = domainAction.name
        let unitsCount = domainAction.unitsParamaters.count
        let actionParametersIndices = domainAction.parameters.map {
          $0.identifier}.indices(of: domainFactParameters).map { $0 - unitsCount}
        let instanceParameters = criticalFact.parameters
        result.append((action: ActionNameProjectedParameters(name: name, parametersIndices: actionParametersIndices, parametersInstance: instanceParameters),
                       costFunction: costFunction))
      }
    }
    return result
    
    
    
    
    
    
    
    
//    let criticalFactNames = Dictionary<String,[(String, [String], [Int])]>(
//      grouping: criticalFactCostFunction.compactMap {
//      if case let .atomic(formula) = $0.key {
//        return (formula.name, formula.parameters, $0.value)
//      } else {
//        return nil
//      }
//      }, by: {$0.0}).mapValues { Dictionary(uniqueKeysWithValues: $0.map {($0.1,$0.2)}) }
//
//    let criticalNegativeFactNames = Dictionary<String, ([String], [Int])>(
//      criticalFactCostFunction.compactMap {
//        if case let .not(formula) = $0.key {
//          return (formula.name, (formula.parameters, $0.value))
//        } else {
//          return nil
//        }
//      },
//      uniquingKeysWith: { a, _ in a})
//    precondition(criticalNegativeFactNames.isEmpty, "Finish implementation")
//
//    let factActions = allCriticalFactCriticalActions.filter {
//      switch $0.criticalFact {
//      case let .term(term):
//        return criticalFactNames[term.name] != nil
//      case let .negativeTerm(term):
//        return criticalNegativeFactNames[term.name] != nil
//      case .assignment:
//        fatalError("Should never happen 233")
//      }
//    }.map {($0.criticalFact, $0.criticalActions)}
//    var result = [(actionName: String,
//                   actionParametersIndices: [Int],
//                   parametersInstance: [String],
//                   costFunction: [Int])]()
//    for (fact, actions) in factActions {
//      let factParameters: [String]
//      switch fact {
//      case let .term(term):
//        factParameters = term.parameters.map {$0.variable!}
//      default:
//        fatalError()
//      }
//      for action in actions {
//        let numberOfUnitsParms = action.unitsParamaters.count
//        let parameterIndices = action.parameters.map {
//          $0.identifier}.indices(of: factParameters).map { $0 - numberOfUnitsParms}
//
//
//        result.append((
//                        actionName: action.name,
//                        actionParametersIndices: parameterIndices,
//                        parametersInstance: criticalFactNames[fact.name!.name]!.0,
//                        costFunction: costFunction))
//      }
//    }
//    return result
  }
  
  public func adding(
    criticalFactCostFunction: [PDDLNameLiteral : [Int]],
    criticalFactCriticalActions: [(criticalFact: PDDLEffect, criticalActions: [PDDLDurativeAction])]
    ) -> PDDLPlanningProblem
  {
    precondition(isClassical)
    let criticalFactNames = Set<String>(criticalFactCostFunction.keys.compactMap {
      if case let .atomic(formula) = $0 {
        return formula.name
      } else {
        return nil
      }
    })
    let criticalNegativeFactNames = Set<String>(criticalFactCostFunction.keys.compactMap {
      if case let .not(formula) = $0 {
        return formula.name
      } else {
        return nil
      }
    })
    
    let domainWithCostFunctions = domain.addingCostFunctions(
      for: criticalFactCriticalActions.filter {
        switch $0.criticalFact {
        case let .term(term):
          return criticalFactNames.contains(term.name)
        case let .negativeTerm(term):
          return criticalNegativeFactNames.contains(term.name)
        case .assignment:
          fatalError("Should never happen 233")
        }
      }.map {($0.criticalFact, $0.criticalActions.map {$0.name})})
    
    
    
    let lastDeadlineTimestamp = criticalFactCostFunction.map {$0.value.count - 1}.max() ?? 0
    let lastTurnNumber = max(problem.turns.count - 1, lastDeadlineTimestamp)
    
    let problemWithCostFunctions = problem.addingCostFunctions(
      criticalFactCostFunction,
      lastTimestamp: lastTurnNumber)
    
    let problemWithTurns = problemWithCostFunctions.addingTurnsObjectsAndPredicates(
      lastTurnNumber: lastTurnNumber)
    
    return PDDLPlanningProblem(
      domain: domainWithCostFunctions,
      problem: problemWithTurns,
      durationOfUnitTimestamp: durationOfUnitTimestamp,
      maxDuration: maxDuration)
  }
}
