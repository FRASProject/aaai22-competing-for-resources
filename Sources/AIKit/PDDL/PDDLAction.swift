//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/17/21.
//

import Foundation

public struct PDDLAction {
  public let name: String
  public let parameters: [PDDLTypedVariable]
  public let condition: PDDLFormula?
  public let effect: [PDDLEffect]
  public var originalName: String {
    if let projIndex = name.range(of: PDDLConstants.projectedActionSuffix)?.lowerBound {
      return String(name[..<projIndex])
    } else {
      return name
    }
  }
}

extension PDDLAction {
  public static let goalFailingCost = 1000000
  public static let failingGoalActionPrefix = "fail-to-"
  public static let noopActionName = "noop"
  public static func createNoOpAction(noOpDurationTerm: PDDLAtomicTerm) -> PDDLAction {
    PDDLAction(
      name: PDDLAction.noopActionName,
      parameters: [PDDLTypedVariable(identifier: "u", type: PDDLDomain.unitType)],
      condition: nil,
      effect: []).addingTurnsTracking(turnsPredicate: noOpDurationTerm)
  }
  public static func createGoalFailingAction(
    goalPredicateDefinition: PDDLPredicateSkeleton) -> PDDLAction
  {
    PDDLAction(
      name: PDDLAction.failingGoalActionPrefix + goalPredicateDefinition.name,
      parameters: goalPredicateDefinition.parameters,
      condition: nil,
      effect: [
        PDDLEffect(from: goalPredicateDefinition),
        PDDLEffect.assignment(PDDLAssignmentEffect(
                                type: .increase,
                                variable: PDDLFunction(
                                  name: PDDLDomain.totalCostFluent,
                                  parameters: []),
                                expression: .number(PDDLAction.goalFailingCost)))
      ])
  }
  /// Creates a new action with the parameters of the given type restricted to the given number.
  /// Assumes that the action is commutative in parameters of the type
  /// If the number of parameters of the type is less or equal number, then it returns original action.
  /// If the number of paremeters is more than the number. Then first we identify the excesive parameters and then we modify
  /// the preconditions:
  /// -- if the precondition contains an excesive parameters, then the precondition is replaced with the excesive parameter
  /// removed. If the new precondition does not contain any parameter of the type, the it is completely removed.
  /// --  The same rule is applied to all effects.
  /// - Parameters:
  ///   - type: Type of parameters that should be removed.
  ///   - number: Maximum number of paramets of the type. If the action has more of these parameters. They will be removed.
  /// - Returns: Action with restricted parameters.
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> PDDLAction? {
    let involvedParameters = parameters.filter {$0.type == type}
    let atLeastOneExists = Set(involvedParameters.map {PDDLTerm.variable($0.identifier)})
    let parametersToRemove = involvedParameters.suffix(
      max(involvedParameters.count - number, 0)).map {
        PDDLTerm.variable($0.identifier)}
    let newParameters = parameters.restrictingNumberOfObjectsOf(type: type, to: number)
    let newCondition = condition?.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)
    let newEffect = effect.compactMap {
      $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
    if newEffect.isEmpty {
      return nil
    } else {
      return PDDLAction(
        name: name,
        parameters: newParameters,
        condition: newCondition,
        effect: newEffect)
    }
  }
  public func restrictingNumberOfObjectsToOneOf(type: String) -> [PDDLAction] {
    let involvedParameters = parameters.enumerated().filter {$0.element.type == type}
    let atLeastOneExists = Set(involvedParameters.map {PDDLTerm.variable($0.element.identifier)})
    var newActions = [PDDLAction]()
    for (parameterIndex, _) in involvedParameters {
      let newParameters = parameters.enumerated().filter {
        $0.element.type != type || $0.offset == parameterIndex
      }.map {$0.element}
      let parametersToRemove = parameters.enumerated().filter {
        $0.element.type == type && $0.offset != parameterIndex
      }.map {PDDLTerm.variable($0.element.identifier)}
      let newCondition = condition?.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)
      let newEffect = effect.compactMap {
        $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
      if !newEffect.isEmpty {
        newActions.append(PDDLAction(
          name: name + "-proj\(parameterIndex)",
          parameters: newParameters,
          condition: newCondition,
          effect: newEffect))
      }
    }
    return newActions
  }
  public init(_ durativeAction: PDDLDurativeAction) {
    self.name = durativeAction.name
    self.parameters = durativeAction.parameters
    let conditions = durativeAction.condition.map {$0.term}.removingDuplicates()
    self.condition = .and(conditions)
    let endEffects = durativeAction.effect.filter { $0.timeSpecifier == .atEnd }.map {$0.term}
    let startEffects = durativeAction.effect.filter { $0.timeSpecifier == .atStart }.map {$0.term}
    let negativeStartEffects = startEffects.map {$0.negation!}
    let badEffects = Set(endEffects).intersection(negativeStartEffects)
    let allBadEffects = badEffects.union(badEffects.map {$0.negation!})
    let allEfects = (startEffects + endEffects).subtracting(allBadEffects).removingDuplicates()
    self.effect = allEfects
  }
  
  public func addingUnitCreditTracking(criticalFactName: String) -> PDDLAction {
    let units = parameters.filter {$0.type == PDDLDomain.unitType}
    let firstIndexTurnsParameters = self.parameters.firstIndex(where: {$0.type == PDDLDomain.turnObjectType}) ?? self.parameters.endIndex
    var newParameters = Array(self.parameters[..<firstIndexTurnsParameters])
    newParameters.append(contentsOf: (1...units.count).flatMap {
      [PDDLTypedVariable(identifier: "currentcount\($0)", type: "count"),
      PDDLTypedVariable(identifier: "nextcount\($0)", type: "count")]
    })
    newParameters.append(contentsOf: self.parameters[firstIndexTurnsParameters...])
    
    var criticalFactParameters: [PDDLTerm]? = nil
    for effect in self.effect {
      if case let .negativeTerm(term) = effect, term.name == criticalFactName {
        criticalFactParameters = term.parameters
        break
      }
    }
    precondition(criticalFactParameters != nil)
    
    var additionalPreconditions = [PDDLAtomicTerm]()
    var additionalEffects = [PDDLEffect]()
    for (unitIdx, unit) in units.enumerated() {
      additionalPreconditions.append(
        PDDLAtomicTerm(
          name: "allowed-\(criticalFactName)",
          parameters: criticalFactParameters! + [.variable(unit.identifier), .variable("currentcount\(unitIdx + 1)")]))
      additionalPreconditions.append(
        PDDLAtomicTerm(
          name: "next-count",
          parameters: [.variable("currentcount\(unitIdx + 1)"), .variable("nextcount\(unitIdx + 1)")]))
      additionalPreconditions.append(
        PDDLAtomicTerm(
          name: "unit-credits",
          parameters: [.variable(unit.identifier), .variable("currentcount\(unitIdx + 1)")]))
      additionalEffects.append(
        .term(PDDLAtomicTerm(
                name: "unit-credits",
                parameters: [.variable(unit.identifier),.variable("nextcount\(unitIdx + 1)")])))
      additionalEffects.append(
        .negativeTerm(PDDLAtomicTerm(
                name: "unit-credits",
                parameters: [.variable(unit.identifier),.variable("currentcount\(unitIdx + 1)")])))
    }
    let newPrecondition: PDDLFormula
    if case let .and(preconditions) = self.condition {
      newPrecondition = .and(preconditions + additionalPreconditions.map {.atomic($0)})
    } else {
      newPrecondition = .and(
        additionalPreconditions.map {.atomic($0)} + [self.condition].compactMap{$0})
    }
    return PDDLAction(
      name: self.name,
      parameters: newParameters,
      condition: newPrecondition,
      effect: self.effect + additionalEffects)
  }
  
  public func addingDurationTurnTotalCostEffect() -> PDDLAction {
    let durationParameter = parameters.last!
    precondition(durationParameter.identifier == "t" && durationParameter.type == "turns")
    var newEffect = effect
    newEffect.append(
      PDDLEffect.assignment(
        PDDLAssignmentEffect(
          type: .increase,
          variable: PDDLFunction(
            name: PDDLDomain.totalCostFluent,
            parameters: []),
          expression: .function(PDDLFunction(
            name: "turn-duration",
            parameters: [.variable("t")])))))
    return PDDLAction(
      name: name,
      parameters: parameters,
      condition: condition,
      effect: newEffect)
  }
  
  public func addingTurnsTracking(turnsPredicate: PDDLAtomicTerm) -> Self {
    let units = parameters.filter {$0.type == PDDLDomain.unitType}
    precondition(turnsPredicate.parameters.last! == .variable("t"))
    var newParameters = self.parameters
    newParameters.append(contentsOf: (1...units.count).map {
      PDDLTypedVariable(identifier: "tstart\($0)", type: "turns")
    })
    newParameters.append(contentsOf: [
      PDDLTypedVariable(identifier: "tend", type: "turns"),
      PDDLTypedVariable(identifier: "t", type: "turns"),
    ])
    
    var additionalPreconditions = [turnsPredicate]
    var additionalEffects = [PDDLEffect]()
    for (i, u) in units.enumerated() {
      additionalEffects.append(
        .term(PDDLAtomicTerm(
                name: "turn-unit",
                parameters: [.variable(u.identifier),.variable("tend")])))
      additionalEffects.append(
        .negativeTerm(PDDLAtomicTerm(
                name: "turn-unit",
                parameters: [.variable(u.identifier),.variable("tstart\(i+1)")])))
      
      additionalPreconditions.append(
        PDDLAtomicTerm(
          name: "turn-unit",
          parameters: [.variable(u.identifier), .variable("tstart\(i+1)")]))
      if i != 0 {
        additionalPreconditions.append(
          PDDLAtomicTerm(
            name: "turns-geq",
            parameters: [.variable("tstart\(1)"), .variable("tstart\(i+1)")]))
      }
    }
    additionalPreconditions.append(
      PDDLAtomicTerm(
        name: "turns-add",
        parameters: [.variable("tstart1"), .variable("t"), .variable("tend")]))
    let newPrecondition: PDDLFormula
    if case let .and(preconditions) = self.condition {
      newPrecondition = .and(preconditions + additionalPreconditions.map {.atomic($0)})
    } else {
      newPrecondition = .and(
        additionalPreconditions.map {.atomic($0)} + [self.condition].compactMap{$0})
    }
    return PDDLAction(
      name: self.name,
      parameters: newParameters,
      condition: newPrecondition,
      effect: self.effect + additionalEffects)
  }
  public func addingPenaltyEffect(
    costFunctionName: String,
    parameters: [PDDLTerm],
    costFluent: String) -> Self
  {
    let assignmentEffects = effect.filter {
      if case .assignment = $0 { return true } else { return false}
    }
    assert(assignmentEffects.count < 2)
    let newEffect: [PDDLEffect]
    if assignmentEffects.isEmpty {
      newEffect = effect + [PDDLEffect.assignment(
        PDDLAssignmentEffect(
          type: .increase,
          variable: PDDLFunction(name: costFluent, parameters: []),
          expression: .function(PDDLFunction(
                                  name: costFunctionName,
                                  parameters: parameters + [.variable("tstart1")]))))]
    } else {
      if case let .assignment(otherEffect) = assignmentEffects.first! {
        newEffect = effect.filter {if case .assignment = $0 { return false } else { return true}} +
          [PDDLEffect.assignment(
            PDDLAssignmentEffect(
              type: .increase,
              variable: PDDLFunction(name: costFluent, parameters: []),
              expression: .plus(
                otherEffect.expression,
                .function(PDDLFunction(
                            name: costFunctionName,
                            parameters: parameters + [.variable("tstart1")])))))]
      } else {
        fatalError()
      }
    }
    return PDDLAction(
      name: name,
      parameters: self.parameters,
      condition: condition,
      effect: newEffect)
  }
}
