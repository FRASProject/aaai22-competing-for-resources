//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

extension Array where Element == PDDLFunctionLiteral {
  public func getFunctionValues() -> [Int] {
    map {$0.value}
  }
}
extension Array where Element == PDDLTypedVariable {
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> Self {
    var newParameters = Self()
    var count = number
    for parameter in self {
      if parameter.type == type {
        if count > 0 {
          newParameters.append(parameter)
          count -= 1
        }
      } else {
        newParameters.append(parameter)
      }
    }
    return newParameters
  }
}

extension Array where Element == PDDLTerm {
  public func removing(_ parametersToRemove: [PDDLTerm]) -> Self {
    let paramsToRemove = Set(parametersToRemove)
    let newParameters = self.filter {!paramsToRemove.contains($0)}
    return newParameters
  }
}
