//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/24/20.
//

import Foundation

extension PDDLDomain {
  public var text: String {
    var result = [String]()
    result.append("(define (domain \(name))")
    if !requirements.isEmpty {
      result.append("(:requirements \(requirements.joined(separator: " ")))")
    }
    if !types.isEmpty {
      result.append("(:types \(types.text))")
    }
    if !constants.isEmpty {
      result.append("(:constants \(constants.text))")
    }
    if !predicates.isEmpty {
      let predicatesText = predicates.map {$0.text}.joined(separator: "\n")
      result.append("(:predicates\n\(predicatesText)\n)")
    }
    if !functions.isEmpty {
      let functionsText = functions.map {$0.text}.joined(separator: "\n")
      result.append("(:functions\n\(functionsText)\n)")
    }
    for action in actions {
      result.append(action.text)
    }
    for durativeAction in durativeActions {
      result.append(durativeAction.text)
    }
    result.append(")")
    return result.joined(separator: "\n")
  }
}

extension PDDLProblem {
  public var text: String {
    var result = [String]()
    result.append("(define (problem \(name))")
    result.append("(:domain \(domainName))")
    if !initState.objects.isEmpty {
      result.append("(:objects \(initState.objects.text))")
    }
    result.append(initState.text)
    result.append("(:goal \n \(goal.text)\n)")
    if let metric = metrics {
      result.append(metric.text)
    }
    result.append(")")
    return result.joined(separator: "\n")
  }
}

extension PDDLGameProblem {
  public var text: String {
    var result = [String]()
    result.append("(define (problem \(name))")
    result.append("(:domain \(domainName))")
    if !initState.objects.isEmpty {
      result.append("(:objects \(initState.objects.text))")
    }
    result.append(initState.text)
    for i in goals.indices {
      result.append("(:goal \n \(goals[i].text)\n)")
      if !metrics.isEmpty {
        result.append(metrics[i].text)
      }
    }
    result.append(")")
    return result.joined(separator: "\n")
  }
}

extension PDDLAction {
  public var text: String {
    var result = [String]()
    result.append("(:action \(name)")
    result.append(":parameters (\(parameters.text))")
    if let condition = condition {
      result.append(":precondition \(condition.text)")
    } else {
      
      //result.append(":precondition ()")
    }
    if effect.isEmpty {
      result.append(":effect ()")
    } else {
      let effectsText = effect.map {$0.text}.joined(separator: "\n")
      if effect.count == 1 {
        result.append(":effect \(effectsText)")
      } else {
        result.append(":effect (and \n\(effectsText)\n)")
      }
    }
    result.append("\n)")
    return result.joined(separator: "\n")
  }
}

extension PDDLDurativeAction {
  public var text: String {
    var result = [String]()
    result.append("(:durative-action \(name)")
    result.append(":parameters (\(parameters.text))")
    result.append(":duration \(duration.text)")
    if condition.isEmpty {
      result.append(":condition ()")
    } else {
      let conditionsText = condition.map {$0.text}.joined(separator: "\n")
      result.append(":condition (and \n\(conditionsText)\n)")
    }
    if effect.isEmpty {
      result.append(":effect ()")
    } else {
      let effectsText = effect.map {$0.text}.joined(separator: "\n")
      result.append(":effect (and \n\(effectsText)\n)")
    }
    result.append("\n)")
    return result.joined(separator: "\n")
  }
}

extension PDDLEffect {
  public var text: String {
    switch self {
    case let .negativeTerm(term):
      return "(not \(term.text))"
    case let .term(term):
      return term.text
    case let .assignment(assignment):
      return assignment.text
    }
  }
}

extension PDDLTemporalEffect {
  public var text: String {
    "(\(timeSpecifier.text) \(term.text))"
  }
}

extension PDDLTemporalEffect.TimeSpecifier {
  public var text: String {
    switch self {
    case .atEnd:
      return "at end"
    case .atStart:
      return "at start"
    }
  }
}

extension PDDLAssignmentEffect {
  public var text: String {
    "(\(type.text) \(variable.text) \(expression.text))"
  }
}

extension PDDLAssignmentEffect.AssignmentType {
  public var text: String {
    switch self {
    case .increase:
      return "increase"
    case .decrease:
      return "decrease"
    }
  }
}

extension PDDLTemporalCondition.TimeSpecifier {
  public var text: String {
    switch self {
    case .atEnd:
      return "at end"
    case .atStart:
      return "at start"
    case .overAll:
      return "over all"
    }
  }
}

extension PDDLTemporalCondition {
  public var text: String {
    "(\(timeSpecifier.text) \(term.text))"
  }
}

extension PDDLFormula {
  public var text: String {
    switch self {
    case let .atomic(formula):
      return formula.text
    case let .negation(formula):
      return "(not \(formula.text))"
    case let .and(formulas):
      let formulasText = formulas.map {$0.text}.joined(separator: "\n")
      return "(and \n\(formulasText)\n)"
    case let .or(formulas):
      let formulasText = formulas.map {$0.text}.joined(separator: "\n")
      return "(or \n\(formulasText)\n)"
    case let .preference(name: name, goal: goal):
      return "(preference \(name ?? "") \(goal.text))"
    }
  }
}

extension PDDLInitState {
  public var text: String {
    let predicatesText = predicates.map {$0.text}.joined(separator: "\n")
    let functionsText = functions.map {$0.text}.joined(separator: "\n")
    return "(:init \n \(predicatesText)\n\n \(functionsText)\n)"
  }
}

extension PDDLFunctionLiteral {
  public var text: String {
    return "(= \(function.text) \(value))"
  }
}

extension PDDLNameLiteral {
  public var text: String {
    switch self {
    case let .atomic(formula):
      return formula.text
    case let.not(formula):
      return "(not \(formula.text))"
    }
  }
}

extension PDDLActionDuration {
  public var text: String {
    let duration: String
    switch self {
    case let .expression(expr):
      duration = expr.text
    case let .number(number):
      duration = String(number)
    }
    return "(= ?duration \(duration))"
  }
}

extension PDDLExpression {
  public var text: String {
    switch self {
    case let .number(number):
      return String(number)
    case let .function(function):
      return function.text
    case let .unaryMinus(expr):
      return "(- \(expr.text))"
    case let .divide(expr1, expr2):
      return "(/ \(expr1.text) \(expr2.text))"
    case let .multiply(expr1, expr2):
      return "(* \(expr1.text) \(expr2.text))"
    case let .minus(expr1, expr2):
      return "(- \(expr1.text) \(expr2.text))"
    case let .plus(expr1, expr2):
      return "(+ \(expr1.text) \(expr2.text))"
    }
  }
}

extension PDDLPredicateSkeleton {
  public var text: String {
    return "(\(name) \(parameters.text))"
  }
}

extension PDDLFunctionSkeleton {
  public var text: String {
    return "(\(name) \(parameters.text))"
  }
}

extension PDDLMetric {
  public var text: String {
    switch self {
    case let .maximize(expr):
      return "(:metric maximize \(expr.text))"
    case let .minimize(expr):
      return "(:metric minimize \(expr.text))"
    }
  }
}

extension PDDLMetricExpression {
  public var text: String {
    switch self {
    case let .number(number):
      return String(number)
    case .totalTime:
      return "(total-time)"
    case let .isViolated(name):
      return "(is-violated \(name))"
    case let .unary(operator: op, expr):
      return "(\(op) \(expr.text))"
    case let .binary(operator: op, expr1, expr2):
      return "(\(op) \(expr1.text) \(expr2.text))"
    case let .multinary(operator: op, expresions):
      let expresionsText = expresions.map {$0.text}.joined(separator: "\n")
      return "(\(op)\n\(expresionsText)\n)"
    case let .function(name):
      return "(\(name))"
    }
  }
}

extension PDDLAtomicTerm {
  public var text: String {
    let parametersText = parameters.map {$0.text}.joined(separator: " ")
    return "(\(name) \(parametersText))"
  }
}

extension PDDLTerm {
  public var text: String {
    switch self {
    case let .name(name):
      return name
    case let .variable(variable):
      return "?\(variable)"
    }
  }
}

extension PDDLAtomicNameFormula {
  public var text: String {
    return "(\(name) \(parameters.joined(separator: " ")))"
  }
}

extension PDDLFunction {
  public var text: String {
    if parameters.isEmpty {
      return "(\(name))"
    } else {
      return "(\(name)\(parameters.reduce("", { $0 + " \($1.text)" })))"
    }
  }
}

extension PDDLTypedVariable {
  public var text: String {
    return "?\(identifier)"
  }
}

extension PDDLTypedParameter {
  public var text: String {
    return identifier
  }
}

fileprivate func render(list: [(text: String, type: String?)]) -> String {
  if list.isEmpty {
    return ""
  }
  var type: String? = list.first!.type
  var result = [String]()
  for variable in list {
    if variable.type != type {
      if type != nil {
        result.append("- \(type!)")
      }
      type = variable.type
    }
    result.append("\(variable.text)")
  }
  if type != nil {
    result.append("- \(type!)")
  }
  return result.joined(separator: " ")
}

extension Array where Element == PDDLTypedVariable {
  public var text: String {
    return render(list: self.map { (text: $0.text, type: $0.type) })
  }
}

extension Array where Element == PDDLTypedParameter {
  public var text: String {
    return render(list: self.map { (text: $0.text, type: $0.type) })
  }
}
