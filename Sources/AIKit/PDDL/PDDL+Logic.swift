//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/25/20.
//

import Foundation

extension PDDLEffect {
  public var negation: Self? {
    switch self {
    case let .term(this):
      return PDDLEffect.negativeTerm(this)
    case let .negativeTerm(this):
      return PDDLEffect.term(this)
    case .assignment:
      return nil
    }
  }
  public var asFormula: PDDLFormula? {
    switch self {
    case let .term(this):
      return PDDLFormula.atomic(this)
    case let .negativeTerm(this):
      return PDDLFormula.negation(.atomic(this))
    case .assignment:
      return nil
    }
  }
}

extension PDDLFormula {
  public func inConflict(with effect: PDDLEffect) -> Bool {
    switch effect {
    case let .term(term):
      return impliesNegation(term)
    case let .negativeTerm(term):
      return implies(term)
    case .assignment:
      return false
    }
  }
  
  private func impliesNegation(_ atomicTerm: PDDLAtomicTerm) -> Bool {
    switch self {
    case .atomic:
      return false
    case let .negation(this):
      if case let .atomic(atom) = this, atom == atomicTerm {
        return true
      } else {
        fatalError("Too complicated preconditions/effects 1")
      }
    case let .and(this):
      return !this.allSatisfy {!$0.impliesNegation(atomicTerm)}
    case let .or(this):
      return this.allSatisfy {$0.impliesNegation(atomicTerm)}
    default:
      fatalError("Too complicated preconditions/effects 3")
    }
  }
  private func implies(_ atomicTerm: PDDLAtomicTerm) -> Bool {
    switch self {
    case let .atomic(this):
      return this == atomicTerm
    case let .and(this):
      return !this.allSatisfy {!$0.implies(atomicTerm)}
    case let .or(this):
      return this.allSatisfy {$0.implies(atomicTerm)}
    default:
      fatalError("Too complicated preconditions/effects 2")
    }
  }
}

extension PDDLNameLiteral {
  public func isInstance(of effect: PDDLEffect) -> Bool {
    switch (self, effect) {
    case let (.atomic(selfFormula), .term(effectFormula)):
      return selfFormula.isInstance(of: effectFormula)
    case let (.not(selfFormula), .negativeTerm(effectFormula)):
      return selfFormula.isInstance(of: effectFormula)
    default:
      return false
    }
  }
  public var negation: Self {
    switch self {
    case let .atomic(formula):
      return .not(formula)
    case let .not(formula):
      return .atomic(formula)
    }
  }
}

extension PDDLAtomicNameFormula {
  public func isInstance(of term: PDDLAtomicTerm) -> Bool {
    if name != term.name || parameters.count != term.parameters.count {
      return false
    }
    for (parameterName, parameterOther) in zip(parameters, term.parameters) {
      switch parameterOther {
      case let .name(name):
        if parameterName != name { return false }
      default:
        break
      }
    }
    return true
  }
}

extension PDDLAtomicTerm {
  public func isInstance(of other: Self) -> Bool {
    if name != other.name || parameters.count != other.parameters.count {
      return false
    }
    for (parameter, parameterOther) in zip(parameters, other.parameters) {
      switch (parameter, parameterOther) {
      case let (.name(a), .name(b)):
        if a != b { return false }
      case (.variable, .name):
        return false
      default:
        break
      }
    }
    return true
  }
}
