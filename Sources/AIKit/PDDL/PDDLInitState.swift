//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

public struct PDDLInitState {
  public private(set) var objects: [PDDLTypedParameter]
  public private(set) var predicates: [PDDLNameLiteral]
  public private(set) var functions: [PDDLFunctionLiteral]
}

extension PDDLInitState {
  public func replacing(objects newObjects: [PDDLTypedParameter]) -> PDDLInitState {
    PDDLInitState(
      objects: newObjects,
      predicates: predicates,
      functions: functions)
  }
  
  public func addingCountsObjectsAndPredicates(lastCountNumber: Int) -> PDDLInitState {
    let newObjects = self.objects + (0...lastCountNumber).map {
      PDDLTypedParameter(identifier: "c\($0)", type: "count")
    }
    var countPredicates = [PDDLNameLiteral]()
    for i in 0..<lastCountNumber {
      countPredicates.append(
        .atomic(PDDLAtomicNameFormula(
                  name: "next-count",
                  parameters: ["c\(i)","c\(i + 1)"])))
    }
    return PDDLInitState(
      objects: newObjects,
      predicates: predicates + countPredicates,
      functions: functions)
  }
  
  
  public func addingOrderingHeuristic(ordering: [(unit: String, goalsOrder: [(factName: String, factParameters: [String])])]) -> PDDLInitState
  {
    var unitCreditsPredicates = [PDDLNameLiteral]()
    for unit in self.units {
      unitCreditsPredicates.append(
        .atomic(PDDLAtomicNameFormula(
                  name: "unit-credits",
                  parameters: [unit, "c0"])))
    }
    
    
    var allowedPredicates = [PDDLNameLiteral]()
    for (unit, goalsOrder) in ordering {
      for (goalOrder, goal) in goalsOrder.enumerated() {
        allowedPredicates.append(
          .atomic(PDDLAtomicNameFormula(name: "allowed-\(goal.factName)", parameters: goal.factParameters + [unit, "c\(goalOrder)"])))
      }
    }
    return PDDLInitState(
      objects: objects,
      predicates: predicates + unitCreditsPredicates + allowedPredicates,
      functions: functions)
  }
  
  public func addingTurnsObjectsAndPredicates(lastTurnNumber: Int) -> PDDLInitState {
    let turnsCount = turns.count
    if turnsCount - 1 >= lastTurnNumber {
      return self
    }
    let newObjects = self.objects + (turnsCount...lastTurnNumber).map {
      PDDLTypedParameter(identifier: "t\($0)", type: "turns")
    }
    
    
    
    var turnPredicates = [PDDLNameLiteral]()
    // Greater or equal predicates
    for a in 0...lastTurnNumber {
      for b in 0...a {
        turnPredicates.append(
          .atomic(PDDLAtomicNameFormula(
                    name: PDDLDomain.turnsGeqPredicate,
                    parameters: ["t\(a)","t\(b)"])))
      }
    }
    for a in 0...lastTurnNumber {
      for b in 0...lastTurnNumber {
        let c = min(lastTurnNumber, a + b)
        turnPredicates.append(
          .atomic(PDDLAtomicNameFormula(
                    name: PDDLDomain.turnsAddPredicate,
                    parameters: ["t\(a)","t\(b)","t\(c)"])))
      }
    }
    
    let newPredicates: [PDDLNameLiteral]
    if turnsCount > 0 {
      newPredicates = predicates.filter {
        $0.name != PDDLDomain.turnsGeqPredicate && $0.name != PDDLDomain.turnsAddPredicate
      } + turnPredicates
    } else {
      newPredicates = predicates + turnPredicates
    }
    
    return PDDLInitState(
      objects: newObjects,
      predicates: newPredicates,
      functions: functions)
  }
  
  public func transformTemporalToClassical(
    durativePredicatesMapping: [String : String],
    durationConstantPredicates: [String : Int],
    removeDurationFunctions: Bool) -> (
    problem: PDDLInitState,
    unitTimestampDuration: Int,
    maxDuration: Int)
  {
    let actionsDurations = functions.filter {
      durativePredicatesMapping[$0.function.name] != nil}.getFunctionValues() + durationConstantPredicates.values
    let unitTimestampDuration = actionsDurations.gcd!
    let maxDuration = actionsDurations.max()!
    let newInitState = addingDurationPredicates(
      mapping: durativePredicatesMapping,
      mappingConstant: durationConstantPredicates,
      unitTimestampDuration: unitTimestampDuration,
      removeDurationFunctions: removeDurationFunctions)
      .addingUnitTurnTrackingPredicates(for: units)
    return (newInitState,
            unitTimestampDuration,
            maxDuration)
  }
  public func keepingOnly(player: String) -> PDDLInitState {
    let otherPlayers = players.filter {$0 != player}
    //TODO: This only checks name, but not type.
    return PDDLInitState(
      objects: objects.filter { $0.type != PDDLDomain.playerType || $0.identifier == player },
      predicates: predicates.filter { predicate in
        otherPlayers.allSatisfy { !predicate.contains(object: $0) }
      },
      functions: functions)
  }
  public func restrictingNumberOfObjectsOf(type: String, to identifiers: [String]) -> PDDLInitState {
    let objectsOfInterest = objects.filter {$0.type == type}
    let allObjectsOfInterestSet = Set(objectsOfInterest.map {$0.identifier})
    let objectsToRemoveSet = allObjectsOfInterestSet.subtracting(identifiers)
    let numberOfRestrictedObjects = identifiers.count
    let newObjects = objects.filter {
      $0.type != type || !objectsToRemoveSet.contains($0.identifier)
    }
    let newInitState = PDDLInitState(
      objects: newObjects,
      predicates: predicates.compactMap {
        $0.shrinking(allObjectsOfType: allObjectsOfInterestSet,
                     to: numberOfRestrictedObjects).removing(
                      parameters: objectsToRemoveSet)},
      functions: functions.compactMap {
        $0.shrinking(
          allObjectsOfType: allObjectsOfInterestSet,
          to: numberOfRestrictedObjects).deletingIfParametersPresent(objectsToRemoveSet)
      })
    return newInitState
  }
  public func getUnits(of player: String) -> [String] {
    predicates.filter {$0.name == MultiUnitGameConstants.hasUnitPredicateName}.map {
      $0.parameters
    }.filter {
      $0[1] == player || $0[0] == player
    }.map {
      $0[0] == player ? $0[1] : $0[0]
    }
  }
  public var turns: [PDDLTypedParameter] {
    objects.filter {$0.type == "turns"}
  }
  public var players: [String] {
    objects.filter {$0.type == AIKitConstants.playerObjectType}.map {$0.identifier}
  }
  public var units: [String] {
    objects.filter {$0.type == PDDLDomain.unitType}.map {$0.identifier}
  }
  public func contains(_ fact: PDDLNameLiteral) -> Bool {
    if predicates.contains(fact) {
      return true
    }
    if case .not = fact, !predicates.contains(fact.negation) {
      return true
    }
    return false
  }
  public func addingDurationPredicates(
    mapping: [String: String],
    mappingConstant: [String : Int],
    unitTimestampDuration: Int,
    removeDurationFunctions: Bool
  ) -> PDDLInitState
  {
    var newPredicates = predicates
    for function in functions {
      if let predicateName = mapping[function.function.name] {
        let turnNumber = function.value / unitTimestampDuration
        newPredicates.append(.atomic(PDDLAtomicNameFormula(
                                      name: predicateName,
                                      parameters: function.function.parameters.map {
                                        $0.name!} + ["t\(turnNumber)"])))
      }
    }
    for (predicateName, actionDuration) in mappingConstant {
      let turnNumber = actionDuration / unitTimestampDuration
      newPredicates.append(.atomic(PDDLAtomicNameFormula(
                                    name: predicateName,
                                    parameters: ["t\(turnNumber)"])))
    }
    let newFunctions: [PDDLFunctionLiteral]
    if removeDurationFunctions {
      newFunctions = functions.filter { mapping[$0.function.name] == nil }
    } else {
      newFunctions = functions
    }
    return PDDLInitState(objects: objects, predicates: newPredicates, functions: newFunctions)
  }
  public func addingUnitTurnTrackingPredicates(for units: [String]) -> PDDLInitState {
    var newPredicates = predicates
    for unit in units {
      newPredicates.append(
        .atomic(PDDLAtomicNameFormula(
                  name: PDDLDomain.unitTurnPredicate,
                  parameters: [unit,"t0"])))
    }
    return PDDLInitState(objects: objects, predicates: newPredicates, functions: functions)
  }
  
  public func addingTotalCostFunction() -> PDDLInitState {
    PDDLInitState(
      objects: objects, predicates: predicates,
      functions: functions + [PDDLDomain.totalCostFunction])
  }
  
  public func addingTwoTotalCostFunctions() -> PDDLInitState {
    PDDLInitState(
      objects: objects,
      predicates: predicates,
      functions: functions + [
        PDDLDomain.totalCostFunction,
        PDDLDomain.totalCostSyncFunction
      ])
  }
  
  public mutating func addCostFunction(
    name: String,
    parameters: [String],
    costFunction: [Int])
  {
    for (timestamp, costValue) in costFunction.enumerated() {
      functions.append(PDDLFunctionLiteral(
                            function: PDDLFunction(
                              name: name,
                              parameters: parameters.map {PDDLTerm.name($0)} +
                                [.name("t\(timestamp)")]),
                            value: costValue))
    }
  }
  
  public func addingCostFunctions(
    _ costFunctions: [PlanAction: [Int]],
    lastTimestamp: Int) -> PDDLInitState
  {
    var newFunctions = [PDDLFunctionLiteral]()
    for (action, costFunction) in costFunctions.sorted(by: {$0.key.name < $1.key.name}) {
      let lastCostValue = costFunction.last!
      let extendedCostFunction = costFunction + [Int].init(
        repeating: lastCostValue,
        count: lastTimestamp - (costFunction.count - 1))
      for (timestamp, costValue) in extendedCostFunction.enumerated() {
        newFunctions.append(PDDLFunctionLiteral(
                              function: PDDLFunction(
                                name: PDDLDomain.penaltyPrefix + action.name,
                                parameters: action.parameters.map {PDDLTerm.name($0)} +
                                  [.name("t\(timestamp)")]),
                              value: costValue))
      }
    }
    return PDDLInitState(
      objects: objects,
      predicates: predicates,
      functions: functions + newFunctions)
  }
  
  public func addingCostFunctions(
    _ costFunctions: [PDDLNameLiteral : [Int]],
    lastTimestamp: Int) -> PDDLInitState
  {
    var newFunctions = [PDDLFunctionLiteral]()
    for predicate in predicates where costFunctions[predicate] != nil {
      let costFunctionName: String
      let predicateParameters: [String]
      switch predicate {
      case let .atomic(formula):
        costFunctionName = PDDLDomain.penaltyPrefix + formula.name
        predicateParameters = formula.parameters
      case let .not(formula):
        costFunctionName = PDDLDomain.penaltyPrefix + "not-" + formula.name
        predicateParameters = formula.parameters
      }
      let parameters = predicateParameters.map {PDDLTerm.name($0)}
      
      let costFunction = costFunctions[predicate]!
      let lastCostValue = costFunction.last ?? 0
      let extendedCostFunction = costFunction + [Int].init(
        repeating: lastCostValue,
        count: lastTimestamp - (costFunction.count - 1))
      
      for (timestamp, costValue) in extendedCostFunction.enumerated() {
        newFunctions.append(PDDLFunctionLiteral(
                              function: PDDLFunction(
                                name: costFunctionName,
                                parameters: parameters + [.name("t\(timestamp)")]),
                              value: costValue))
      }
    }
    
    return PDDLInitState(
      objects: objects,
      predicates: predicates,
      functions: functions + newFunctions)
  }
}
