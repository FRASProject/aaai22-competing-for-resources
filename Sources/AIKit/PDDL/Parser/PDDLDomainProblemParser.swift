//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/13/20.
//

import Foundation
import Antlr4

public enum PDDLDomainProblemParser {
  private static func prepareParser(for input: String) throws -> PDDLParser {
    let inputStream = ANTLRInputStream(input)
    let lexer = PDDLLexer(inputStream)
    let tokenStream = CommonTokenStream(lexer)
    return try PDDLParser(tokenStream)
  }
  public static func parse(gameProblem input: String) throws -> PDDLGameProblem {
    let parser = try prepareParser(for: input)
    let problemContext = try parser.gameproblem()
    let listener = PDDLGameListener()
    try ParseTreeWalker.DEFAULT.walk(listener, problemContext)
    return listener.gameProblem!
  }
  public static func parse(domain input: String) throws -> PDDLDomain {
    let parser = try prepareParser(for: input)
    let problemContext = try parser.domain()
    let listener = PDDLGameListener()
    try ParseTreeWalker.DEFAULT.walk(listener, problemContext)
    return listener.gameDomain!
  }
  public static func parse(problem input: String) throws -> PDDLProblem {
    let parser = try prepareParser(for: input)
    let problemContext = try parser.problem()
    let listener = PDDLGameListener()
    try ParseTreeWalker.DEFAULT.walk(listener, problemContext)
    return listener.problem!
  }
}

fileprivate final class PDDLGameListener: PDDLBaseListener {
  var gameDomain: PDDLDomain? = nil
  var gameProblem: PDDLGameProblem? = nil
  var problem: PDDLProblem? = nil
  
  private var types: [PDDLTypedParameter]? = nil
  private var constants: [PDDLTypedParameter]? = nil
  private var typedVariableList: [[PDDLTypedVariable]] = [] // typedVariableList
  private var typedNameList: [[PDDLTypedParameter]] = []
  private var duration: PDDLActionDuration? = nil
  private var temporalCondition: [PDDLTemporalCondition] = [] // timedGD, all->condition and
  private var temporalEffect: [PDDLTemporalEffect] = [] // timedEffect, all -> daEffect
  private var pddlFunction: [PDDLFunction] = [] //fHead
  private var pddlExpression: [PDDLExpression] = [] //fExp
  private var pddlAtomicTerm: [PDDLAtomicTerm] = [] // atomicTermFormula
  private var pddlTerm: [PDDLTerm] = [] //term
  private var pddlFormula: [PDDLFormula] = [] // goalDesc
  private var pddlEffect: [PDDLEffect] = [] // cEffect, all -> effect
  private var durativeActions: [PDDLDurativeAction] = []
  private var actions: [PDDLAction] = []
  private var atomicFormulaSkeletons: [PDDLPredicateSkeleton] = []
  private var atomicFunctionSkeletons: [PDDLFunctionSkeleton] = []
  private var condition: PDDLFormula? = nil
  
  private var problemObjects: [PDDLTypedParameter] = []
  private var atomicNameFormulas: [PDDLAtomicNameFormula] = []
  private var nameLiterals: [PDDLNameLiteral] = []
  private var functionLiterals: [PDDLFunctionLiteral] = []
  private var initState: PDDLInitState? = nil
  
  private var goals: [PDDLFormula] = []
  
  private var metricExpression: [PDDLMetricExpression] = []
  private var metrics: [PDDLMetric] = []
  private var requirements: [String] = []
  
  override func exitRequireDef(_ ctx: PDDLParser.RequireDefContext) {
    for requireKey in ctx.REQUIRE_KEY() {
      self.requirements.append(requireKey.getText())
    }
  }
  
  override func exitMetricSpec(_ ctx: PDDLParser.MetricSpecContext) {
    if ctx.children![2].getText() == "minimize" {
      self.metrics.append(.minimize(self.metricExpression.popLast()!))
    } else if ctx.children![2].getText() == "maximize" {
      self.metrics.append(.maximize(self.metricExpression.popLast()!))
    } else {
      fatalError("Parsing error - metric")
    }
  }
  
  override func exitGoal(_ ctx: PDDLParser.GoalContext) {
    self.goals.append(self.pddlFormula.popLast()!)
  }
  
  override func exitMetricFExp(_ ctx: PDDLParser.MetricFExpContext) {
    if let number = ctx.NUMBER()?.getText() {
      self.metricExpression.append(.number(Int(number)!))
    } else if ctx.getText() == "total-time" {
      self.metricExpression.append(.totalTime)
    } else if ctx.children![1].getText() == "is-violated" {
      self.metricExpression.append(
        .isViolated(ctx.children![2].getText()))
    } else if ctx.children!.count > 5 {
      let numberOfArguments = ctx.children!.count - 3
      var arguments = [PDDLMetricExpression]()
      for _ in 0..<numberOfArguments {
        arguments.append(self.metricExpression.popLast()!)
      }
      arguments.reverse()
      let exp = PDDLMetricExpression.multinary(
        operator: ctx.children![1].getText(),
        arguments)
      self.metricExpression.append(exp)
    } else if ctx.children!.count == 5 {
      let p2 = self.metricExpression.popLast()!
      let p1 = self.metricExpression.popLast()!
      let exp = PDDLMetricExpression.binary(
        operator: ctx.children![1].getText(),
        p1,
        p2)
      self.metricExpression.append(exp)
    } else if ctx.children!.count == 4 {
      let exp = PDDLMetricExpression.unary(
        operator: ctx.children![1].getText(),
        self.metricExpression.popLast()!)
      self.metricExpression.append(exp)
    } else if let functionSymbol = ctx.functionSymbol() {
      self.metricExpression.append(.function(functionSymbol.getText()))
    } else {
      fatalError("Parsing metric error.")
    }
  }
  
  override func exitObjectDecl(_ ctx: PDDLParser.ObjectDeclContext) {
    self.problemObjects = self.typedNameList.popLast()!
  }
  
  override func exitInitState(_ ctx: PDDLParser.InitStateContext) {
    self.initState = PDDLInitState(
      objects: [],
      predicates: self.nameLiterals,
      functions: self.functionLiterals)
    self.nameLiterals = []
    self.functionLiterals = []
  }
  
  
  override func exitDomain(_ ctx: PDDLParser.DomainContext) {
    self.gameDomain = PDDLDomain(
      name: ctx.domainName()!.children![2].getText(),
      requirements: self.requirements,
      types: self.types!,
      constants: self.constants ?? [],
      predicates: self.atomicFormulaSkeletons,
      functions: self.atomicFunctionSkeletons,
      actions: self.actions,
      durativeActions: self.durativeActions)
    self.requirements = []
    self.types = nil
    self.actions = []
    self.durativeActions = []
    self.atomicFormulaSkeletons = []
    self.atomicFunctionSkeletons = []
  }
  
  override func exitGameproblem(_ ctx: PDDLParser.GameproblemContext) {
    self.gameProblem = PDDLGameProblem(
      name: ctx.problemDecl()!.children![2].getText(),
      domainName: ctx.problemDomain()!.children![2].getText(),
      initState: self.initState!.replacing(objects: self.problemObjects),
      goals: self.goals,
      metrics: self.metrics)
    self.problemObjects = []
    self.initState = nil
    self.goals = []
    self.metrics = []
  }
  
  override func exitProblem(_ ctx: PDDLParser.ProblemContext) {
    self.problem = PDDLProblem(
      name: ctx.problemDecl()!.children![2].getText(),
      domainName: ctx.problemDomain()!.children![2].getText(),
      initState: self.initState!.replacing(objects: self.problemObjects),
      goal: goals.first!,
      metrics: metrics.first)
    self.problemObjects = []
    self.initState = nil
    self.goals = []
    self.metrics = []
  }
  
  override func exitNameLiteral(_ ctx: PDDLParser.NameLiteralContext) {
    if ctx.children!.count >= 2 && ctx.children![1].getText() == "not" {
      self.nameLiterals.append(.not(self.atomicNameFormulas.popLast()!))
    } else {
      self.nameLiterals.append(.atomic(self.atomicNameFormulas.popLast()!))
    }
  }
  
  override func exitFLiteral(_ ctx: PDDLParser.FLiteralContext) {
    self.functionLiterals.append(
      PDDLFunctionLiteral(
        function: self.pddlFunction.popLast()!,
        value: Int(ctx.NUMBER()!.getText())!))
  }
  
  override func exitActionDef(_ ctx: PDDLParser.ActionDefContext) {
    self.actions.append(
      PDDLAction(
        name: ctx.actionSymbol()!.getText(),
        parameters: self.typedVariableList.popLast()!,
        condition: self.condition,
        effect: self.pddlEffect))
    self.condition = nil
    self.pddlEffect = []
  }
  
  override func exitConstraints(_ ctx: PDDLParser.ConstraintsContext) {
    fatalError("Constraints not implemented in the parser.")
  }
  
  override func exitDerivedDef(_ ctx: PDDLParser.DerivedDefContext) {
    fatalError("Derived definitions not implemented in the parser.")
  }
  
  override func exitAtomicFormulaSkeleton(_ ctx: PDDLParser.AtomicFormulaSkeletonContext) {
    self.atomicFormulaSkeletons.append(
      PDDLPredicateSkeleton(
        name: ctx.predicate()!.getText(),
        parameters: self.typedVariableList.popLast()!))
  }
  
  override func exitAtomicFunctionSkeleton(_ ctx: PDDLParser.AtomicFunctionSkeletonContext) {
    self.atomicFunctionSkeletons.append(
      PDDLFunctionSkeleton(
        name: ctx.functionSymbol()!.getText(),
        parameters: self.typedVariableList.popLast()!))
  }
  
  override func exitConstantsDef(_ ctx: PDDLParser.ConstantsDefContext) {
    self.constants = self.typedNameList.popLast()!
  }
  
  override func exitTypesDef(_ ctx: PDDLParser.TypesDefContext) {
    self.types = self.typedNameList.popLast()!
  }
  
  override func exitDurativeActionDef(_ ctx: PDDLParser.DurativeActionDefContext) {
    let action = PDDLDurativeAction(
      name: ctx.actionSymbol()!.getText(),
      parameters: self.typedVariableList.popLast()!,
      duration: self.duration!,
      condition: self.temporalCondition,
      effect: self.temporalEffect)
    self.duration = nil
    self.temporalCondition = []
    self.temporalEffect = []
    self.durativeActions.append(action)
  }
  
  override func exitTimedEffect(_ ctx: PDDLParser.TimedEffectContext) {
    switch ctx.children![2].getText() {
    case "start":
      temporalEffect.append(
        PDDLTemporalEffect(
          timeSpecifier: .atStart,
          term: pddlEffect.popLast()!))
    case "end":
      temporalEffect.append(
        PDDLTemporalEffect(
          timeSpecifier: .atEnd,
          term: pddlEffect.popLast()!))
    default:
      fatalError("Parsing error - effects")
    }
  }
  
  override func exitPEffect(_ ctx: PDDLParser.PEffectContext) {
    if ctx.children![0].getText() == "(" {
      switch ctx.children![1].getText() {
      case "not":
        self.pddlEffect.append(PDDLEffect.negativeTerm(pddlAtomicTerm.popLast()!))
      case "increase":
        let fHead = self.pddlFunction.popLast()!
        let exp = self.pddlExpression.popLast()!
        self.pddlEffect.append(PDDLEffect.assignment(
                                PDDLAssignmentEffect(
                                  type: .increase,
                                  variable: fHead,
                                  expression: exp)))
      case "decrease":
        self.pddlEffect.append(PDDLEffect.assignment(
                                PDDLAssignmentEffect(
                                  type: .decrease,
                                  variable: self.pddlFunction.popLast()!,
                                  expression: self.pddlExpression.popLast()!)))
      default:
        fatalError("Error in parsing pddl")
      }
    } else {
      self.pddlEffect.append(PDDLEffect.term(pddlAtomicTerm.popLast()!))
    }
  }
  
  
  override func enterTerm(_ ctx: PDDLParser.TermContext) {
    if let v = ctx.VARIABLE()?.getText() {
      pddlTerm.append(.variable(String(v.dropFirst())))
    } else {
      pddlTerm.append(.name(ctx.name()!.getText()))
    }
  }
  
  override func exitTimedGD(_ ctx: PDDLParser.TimedGDContext) {
    switch ctx.children![2].getText() {
    case "start":
      self.temporalCondition.append(
        PDDLTemporalCondition(
          timeSpecifier: .atStart,
          term: self.pddlFormula.popLast()!))
    case "end":
      self.temporalCondition.append(
        PDDLTemporalCondition(
          timeSpecifier: .atEnd,
          term: self.pddlFormula.popLast()!))
    case "all":
      self.temporalCondition.append(
        PDDLTemporalCondition(
          timeSpecifier: .overAll,
          term: self.pddlFormula.popLast()!))
    default:
      fatalError("Parsing error")
    }
  }
  
  override func exitPrecondition(_ ctx: PDDLParser.PreconditionContext) {
    self.condition = pddlFormula.popLast()!
  }
  
  override func exitGoalDesc(_ ctx: PDDLParser.GoalDescContext) {
    if ctx.atomicTermFormula() != nil {
      pddlFormula.append(.atomic(pddlAtomicTerm.popLast()!))
    } else {
      let op = ctx.children![1].getText()
      let numberOfArguments = ctx.children!.count - 3
      switch op {
      case "not":
        pddlFormula.append(.negation(pddlFormula.popLast()!))
      case "and":
        let formula = PDDLFormula.and(self.pddlFormula.popAndRevert(last: numberOfArguments))
        self.pddlFormula.append(formula)
      case "or":
        let formula = PDDLFormula.or(self.pddlFormula.popAndRevert(last: numberOfArguments))
        self.pddlFormula.append(formula)
      case "preference":
        let formula = PDDLFormula.preference(
          name: ctx.name()?.getText(),
          goal: self.pddlFormula.popLast()!)
        self.pddlFormula.append(formula)
      default:
        fatalError("Parsing pddl error pddlformula")
      }
    }
  }
  
  override func exitAtomicTermFormula(_ ctx: PDDLParser.AtomicTermFormulaContext) {
    let name = ctx.predicate()!.getText()
    self.pddlAtomicTerm.append(
      PDDLAtomicTerm(name: name, parameters: self.pddlTerm))
    self.pddlTerm = []
  }
  
  override func exitAtomicNameFormula(_ ctx: PDDLParser.AtomicNameFormulaContext) {
    let name = ctx.predicate()!.getText()
    var parameters = [String]()
    for p in ctx.name() {
      parameters.append(p.getText())
    }
    self.atomicNameFormulas.append(PDDLAtomicNameFormula(
      name: name,
      parameters: parameters))
  }
  
  override func exitFHead(_ ctx: PDDLParser.FHeadContext) {
    let name = ctx.functionSymbol()!.getText()
    self.pddlFunction.append(PDDLFunction(
      name: name,
      parameters: self.pddlTerm))
    self.pddlTerm = []
  }
  
  override func exitFExp(_ ctx: PDDLParser.FExpContext) {
    if let number = ctx.NUMBER() {
      pddlExpression.append(PDDLExpression.number(Int(number.getText())!))
    } else if ctx.fHead() != nil {
      pddlExpression.append(PDDLExpression.function(self.pddlFunction.popLast()!))
    }
  }
  
  override func enterTypedNameList(_ ctx: PDDLParser.TypedNameListContext) {
    var newTypedNameList = [PDDLTypedParameter]()
    for typeName in ctx.singleTypeNameList() {
      let t = typeName.r_type()!.getText()
      for name in typeName.name() {
        newTypedNameList.append(
          PDDLTypedParameter(
            identifier: name.getText(),
            type: t))
      }
    }
    for name in ctx.name() {
      newTypedNameList.append(
        PDDLTypedParameter(identifier: name.getText(), type: nil))
    }
    self.typedNameList.append(newTypedNameList)
  }
  
  override func enterTypedVariableList(_ ctx: PDDLParser.TypedVariableListContext) {
    var newTypedVariableList = [PDDLTypedVariable]()
    for typeVar in ctx.singleTypeVarList() {
      let t = typeVar.r_type()!.getText()
      for v in typeVar.VARIABLE() {
        newTypedVariableList.append(
          PDDLTypedVariable(
            identifier: String(v.getText().dropFirst()),
            type: t))
      }
    }
    for v in ctx.VARIABLE() {
      newTypedVariableList.append(
        PDDLTypedVariable(identifier: String(v.getText().dropFirst()), type: nil))
    }
    self.typedVariableList.append(newTypedVariableList)
  }
  
  override func exitDurationConstraint(_ ctx: PDDLParser.DurationConstraintContext) {
    if let duration = ctx.simpleDurationConstraint().first {
      assert(self.duration == nil)
      precondition(duration.children![1].getText() == "=", "Wrong duration operator")
      precondition(duration.children![2].getText() == "?duration")
      let durationValue = duration.durValue()!
      if let number = durationValue.NUMBER()?.getText() {
        self.duration = PDDLActionDuration.number(Int(number)!)
      } else {
        self.duration = .expression(self.pddlExpression.popLast()!)
      }
    }
  }
  
  override func exitProbConstraints(_ ctx: PDDLParser.ProbConstraintsContext) {
    fatalError("Not implemented in the parses.")
  }
}

