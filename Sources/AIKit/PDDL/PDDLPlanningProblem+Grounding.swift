//
//  File.swift
//  
//
//  Created by Pavel Rytir on 2/18/21.
//

import Foundation

extension PDDLPlanningProblem {  
  public func createSubProblemsForUnits() -> [PDDLPlanningProblem] {
    precondition(problem.players.count == 1)
    let player = problem.players.first!
    let units = problem.getUnits(of: player).sorted()
    let subproblems = units.map {
      self.restrictingObjectsOf(type: PDDLDomain.unitType, to: [$0]).addTemporalFailingGoalActions()
    }
    return subproblems
  }
}
