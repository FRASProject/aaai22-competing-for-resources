//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/22/20.
//

import Foundation

public struct PDDLDurativeAction: Hashable {
  public let name: String
  public let parameters: [PDDLTypedVariable]
  public let duration: PDDLActionDuration
  public let condition: [PDDLTemporalCondition]
  public let effect: [PDDLTemporalEffect]
  public var originalName: String {
    if let projIndex = name.range(of: PDDLConstants.projectedActionSuffix)?.lowerBound {
      return String(name[..<projIndex])
    } else {
      return name
    }
  }
}

extension PDDLDurativeAction {
  public static let goalFailingDuration = 1000000
  public static func createGoalFailingAction(
    goalPredicateDefinition: PDDLPredicateSkeleton) -> PDDLDurativeAction
  {
    PDDLDurativeAction(
      name: PDDLAction.failingGoalActionPrefix + goalPredicateDefinition.name,
      parameters: goalPredicateDefinition.parameters,
      duration: .number(PDDLDurativeAction.goalFailingDuration),
      condition: [],
      effect: [
        PDDLTemporalEffect(timeSpecifier: .atEnd, term: PDDLEffect(from: goalPredicateDefinition))
      ])
  }
  
  public static func createTemporalNoOpAction(duration: Int) -> PDDLDurativeAction {
    PDDLDurativeAction(
      name: "noop",
      parameters: [PDDLTypedVariable(identifier: "u", type: PDDLDomain.unitType)],
      duration: .number(duration),
      condition: [],
      effect: [])
  }
  
  public var unitsParamaters: [(offset: Int, name: String)] {
    parameters.enumerated().filter { $0.element.type == PDDLDomain.unitType }.map {
      (offset: $0.offset, name: $0.element.identifier)}
  }
  
  /// See documentation for normal action
  /// - Parameters:
  ///   - type:
  ///   - number:
  /// - Returns: 
  public func restrictingNumberOfObjectsOf(type: String, to number: Int) -> PDDLDurativeAction? {
    let involvedParameters = parameters.filter {$0.type == type}
    let atLeastOneExists = Set(involvedParameters.map {PDDLTerm.variable($0.identifier)})
    let parametersToRemove = involvedParameters.suffix(
      max(involvedParameters.count - number, 0)).map {
        PDDLTerm.variable($0.identifier)}
    let newParameters = parameters.restrictingNumberOfObjectsOf(type: type, to: number)
    let newDuration = duration.removing(parametersToRemove) ?? .number(0)
    let newCondition = condition.compactMap {
      $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
    let newEffect = effect.compactMap {
      $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
    if !newEffect.isEmpty {
      return PDDLDurativeAction(
        name: name,
        parameters: newParameters,
        duration: newDuration,
        condition: newCondition,
        effect: newEffect)
    } else {
      return nil
    }
  }
  
  public func restrictingNumberOfObjectsToOneOf(type: String) -> [PDDLDurativeAction] {
    let involvedParameters = parameters.enumerated().filter {$0.element.type == type}
    let atLeastOneExists = Set(involvedParameters.map {PDDLTerm.variable($0.element.identifier)})
    var newActions = [PDDLDurativeAction]()
    for (parameterIndex, _) in involvedParameters {
      let newParameters = parameters.enumerated().filter {
        $0.element.type != type || $0.offset == parameterIndex
      }.map {$0.element}
      let parametersToRemove = parameters.enumerated().filter {
        $0.element.type == type && $0.offset != parameterIndex
      }.map {PDDLTerm.variable($0.element.identifier)}
      let newDuration = duration.removing(parametersToRemove) ?? .number(0)
      let newCondition = condition.compactMap {
        $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
      let newEffect = effect.compactMap {
        $0.removing(parametersToRemove, atLeastOneExists: atLeastOneExists)}
      if !newEffect.isEmpty {
        newActions.append(PDDLDurativeAction(
          name: name + "-proj\(parameterIndex)",
          parameters: newParameters,
          duration: newDuration,
          condition: newCondition,
          effect: newEffect))
      }
    }
    return newActions
  }
  
  public func addingUnitCreditTracking(criticalFactName: String) -> PDDLDurativeAction {
    let units = parameters.filter {$0.type == PDDLDomain.unitType}
    var newParameters = parameters
    newParameters.append(contentsOf: (1...units.count).flatMap {
      [PDDLTypedVariable(identifier: "currentcount\($0)", type: "count"),
      PDDLTypedVariable(identifier: "nextcount\($0)", type: "count")]
    })
    var criticalFactParameters: [PDDLTerm]? = nil
    for effect in self.effect {
      if case let .negativeTerm(term) = effect.term, term.name == criticalFactName {
        criticalFactParameters = term.parameters
        break
      }
    }
    precondition(criticalFactParameters != nil)
    var additionalPreconditions = [PDDLTemporalCondition]()
    var additionalEffects = [PDDLTemporalEffect]()
    for (unitIdx, unit) in units.enumerated() {
      additionalPreconditions.append(
        PDDLTemporalCondition(
          timeSpecifier: .atStart,
          term: .atomic(PDDLAtomicTerm(
            name: "allowed-\(criticalFactName)",
            parameters: criticalFactParameters! + [.variable(unit.identifier), .variable("currentcount\(unitIdx + 1)")]))))
      additionalPreconditions.append(
        PDDLTemporalCondition(
          timeSpecifier: .atStart,
          term: .atomic(PDDLAtomicTerm(
            name: "next-count",
            parameters: [.variable("currentcount\(unitIdx + 1)"), .variable("nextcount\(unitIdx + 1)")]))))
      additionalPreconditions.append(
        PDDLTemporalCondition(
          timeSpecifier: .atStart,
          term: .atomic(PDDLAtomicTerm(
            name: "unit-credits",
            parameters: [.variable(unit.identifier), .variable("currentcount\(unitIdx + 1)")]))))
      
      additionalEffects.append(
        PDDLTemporalEffect(
          timeSpecifier: .atEnd,
          term: .term(PDDLAtomicTerm(
            name: "unit-credits",
            parameters: [.variable(unit.identifier),.variable("nextcount\(unitIdx + 1)")]))))
      additionalEffects.append(
        PDDLTemporalEffect(
          timeSpecifier: .atEnd,
          term: .negativeTerm(PDDLAtomicTerm(
            name: "unit-credits",
            parameters: [.variable(unit.identifier),.variable("currentcount\(unitIdx + 1)")]))))
    }
    return PDDLDurativeAction(
      name: name,
      parameters: newParameters,
      duration: duration,
      condition: condition + additionalPreconditions,
      effect: effect + additionalEffects)
  }
  
  public func possiblyAdversarialEffects(to other: Self) -> [PDDLEffect] {
    var confictingEffects = Set<PDDLEffect>()
    for oneEffect in self.effect {
      for oneCondition in other.condition {
        if oneCondition.term.inConflict(with: oneEffect.term) {
          confictingEffects.insert(oneEffect.term)
        }
      }
    }
    return Array(confictingEffects)
  }
  
  public func getPreconditions(
    time: PDDLTemporalCondition.TimeSpecifier) -> [PDDLAtomicNameFormula]
  {
    var result = [PDDLAtomicNameFormula]()
    for cond in condition where cond.timeSpecifier == time {
      if case let .atomic(f) = cond.term, let af = f.asPDDLAtomicNameFormula {
        result.append(af)
      } else {
        fatalError("Improve 322")
      }
    }
    return result
  }
  
  public func isApplicable(
    in state: Set<PDDLAtomicNameFormula>,
    time: PDDLTemporalCondition.TimeSpecifier) -> Bool
  {
    for cond in condition where cond.timeSpecifier == time {
      if !cond.term.isTrue(in: state) {
        return false
      }
    }
    return true
  }
  
  public func getEffects(time: PDDLTemporalEffect.TimeSpecifier) -> [PDDLEffect] {
    effect.filter { $0.timeSpecifier == time }.map { $0.term }
  }
}
