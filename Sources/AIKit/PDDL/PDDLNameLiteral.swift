//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/21.
//

import Foundation

public enum PDDLNameLiteral: Hashable, Comparable, Codable {
  case atomic(PDDLAtomicNameFormula)
  case not(PDDLAtomicNameFormula)
}

extension PDDLNameLiteral {
  public var name: String {
    switch self {
    case let .atomic(f):
      return f.name
    case let .not(f):
      return f.name
    }
  }
  public var parameters: [String] {
    switch self {
    case let .atomic(f):
      return f.parameters
    case let .not(f):
      return f.parameters
    }
  }
  public var positive: Bool {
    switch self {
    case .atomic:
      return true
    case .not:
      return false
    }
  }
  public func contains(object: String) -> Bool {
    switch self {
    case let .atomic(f):
      return f.parameters.contains(object)
    case let .not(f):
      return f.parameters.contains(object)
    }
  }
  public func shrinking(allObjectsOfType: Set<String>, to number: Int) -> PDDLNameLiteral {
    switch self {
    case let .atomic(f):
      return .atomic(f.shrinking(allObjectsOfType: allObjectsOfType, to: number))
    case let .not(f):
      return .not(f.shrinking(allObjectsOfType: allObjectsOfType, to: number))
    }
  }
  public func removing(parameters parametersToRemove: Set<String>) -> PDDLNameLiteral? {
    switch self {
    case let .atomic(f):
      if let rf = f.removing(parameters: parametersToRemove) {
        return .atomic(rf)
      }
    case let .not(f):
      if let rf = f.removing(parameters: parametersToRemove) {
        return .not(rf)
      }
    }
    return nil
  }
}
