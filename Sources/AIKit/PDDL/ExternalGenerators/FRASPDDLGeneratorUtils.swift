//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/10/20.
//
import Foundation

public enum FRASPDDLGeneratorUtils {
  public static func generateInitPlanPDDL(
    temporalGameDomain: String,
    temporalGameInstance: String,
    classicalPlannerDomain: String,
    player: Int
  ) throws -> (classicalProblemPDDL: String,
               costPredicateStatistics: [String: [Int: Double]])
  {
    let pddlGeneratorPath = ExperimentManager.defaultManager.systemConfig["initPDDLGenerator"]!.string!
    let plannersPath = ExperimentManager.defaultManager.systemConfig[
      "initPDDLGeneratorPlannersPath"]!.string!
    let temporaryDirectory = "tmp_" + UUID().uuidString
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    let tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try temporalGameDomain.write(
      to: tempDirURL.appendingPathComponent("game_domain.pddl"),
      atomically: true,
      encoding: .utf8)
    try temporalGameInstance.write(
      to: tempDirURL.appendingPathComponent("game_problem.pddl"),
      atomically: true,
      encoding: .utf8)
    try classicalPlannerDomain.write(
      to: tempDirURL.appendingPathComponent("planner_domain.pddl"),
      atomically: true,
      encoding: .utf8)
    
    let generator = Process()
    generator.executableURL = URL(fileURLWithPath: pddlGeneratorPath)
    generator.arguments = [
      "-i",
      "--fixed",
      URL(fileURLWithPath: pddlGeneratorPath).deletingLastPathComponent().path+"/",
      plannersPath,
      tempDirURL.appendingPathComponent("planner_domain.pddl").path,
      tempDirURL.appendingPathComponent("game_domain.pddl").path,
      tempDirURL.appendingPathComponent("game_problem.pddl").path,
      "2",
      String(player),
      tempDirURL.appendingPathComponent("planner_problem.pddl").path,
      tempDirURL.appendingPathComponent("stats.json").path,
    ]
    print("TEMPDIR " + tempDirURL.path)
    generator.currentDirectoryURL = tempDirURL
    print("command: \(pddlGeneratorPath) " + generator.arguments!.joined(separator: " "))
    try generator.run()
    generator.waitUntilExit()
    if generator.terminationStatus != 0 {
      throw AIKitError(
        message: "Init pddl generator returned non-zero exit code: \(generator.terminationStatus)")
    }
    let problemPDDL = try String(
      contentsOf: tempDirURL.appendingPathComponent("planner_problem.pddl"),
      encoding: .utf8)
    let stats = try JSONSerialization.jsonObject(
      with: Data(contentsOf: tempDirURL.appendingPathComponent("stats.json"),
                 options: [])) as! [String: [String: Double]]

    let statsFixed: [String: [Int: Double]] = stats.mapValues {
      (v: [String: Double]) -> [Int: Double] in
      Dictionary<Int, Double>(v.map {
                                (Int($0.key)!, $0.value)}, uniquingKeysWith: { a, b in a})
    }
    
    try? FileManager.default.removeItem(at: tempDirURL)
    
    return (
      classicalProblemPDDL: problemPDDL,
      costPredicateStatistics: statsFixed
    )
  }
  
  public static func generateBestResponsePlanPDDL(
    temporalGameDomain: String,
    temporalGameInstance: String,
    classicalPlannerDomain: String,
    player: Int,
    opponentStrat: MixedStrategy<String>) throws -> (
      classicalProblemPDDL: String,
      costPredicateStatistics: [String: [Int: Double]])
  {
    let pddlGeneratorPath = ExperimentManager.defaultManager.systemConfig["BRPDDLGenerator"]!.string!
    let plannersPath = ExperimentManager.defaultManager.systemConfig[
      "BRPDDLGeneratorPlannersPath"]!.string!
    let temporaryDirectory = "tmp_" + UUID().uuidString
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    let tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try temporalGameDomain.write(
      to: tempDirURL.appendingPathComponent("game_domain.pddl"),
      atomically: true,
      encoding: .utf8)
    try temporalGameInstance.write(
      to: tempDirURL.appendingPathComponent("game_problem.pddl"),
      atomically: true,
      encoding: .utf8)
    try classicalPlannerDomain.write(
      to: tempDirURL.appendingPathComponent("planner_domain.pddl"),
      atomically: true,
      encoding: .utf8)
    
    var strategyWeightsStr = [String]()
    var planPaths = [String]()
    for idx in opponentStrat.distribution.indices
      where opponentStrat.distribution[idx] > AIKitConfig.probabilityEpsilon {
        strategyWeightsStr.append(String(opponentStrat.distribution[idx]))
        try opponentStrat.strategies[idx].write(
          to: tempDirURL.appendingPathComponent("plan_\(idx)"),
          atomically: true,
          encoding: .utf8)
        planPaths.append(tempDirURL.appendingPathComponent("plan_\(idx)").path)
    }
    let strategyString = "\"[" + strategyWeightsStr.joined(separator: ",") + "]\""
    
    let generator = Process()
    generator.executableURL = URL(fileURLWithPath: pddlGeneratorPath)
    var arguments = [
      "-r",
      "--fixed",
      URL(fileURLWithPath: pddlGeneratorPath).deletingLastPathComponent().path+"/",
      plannersPath,
      tempDirURL.appendingPathComponent("planner_domain.pddl").path,
      tempDirURL.appendingPathComponent("game_domain.pddl").path,
      tempDirURL.appendingPathComponent("game_problem.pddl").path,
      "2",
      String(player),
      strategyString,
      tempDirURL.appendingPathComponent("planner_problem.pddl").path,
      tempDirURL.appendingPathComponent("stats.json").path,
    ]
    arguments.append(contentsOf: planPaths)
    
    generator.arguments = arguments
    generator.currentDirectoryURL = tempDirURL
    print("command: \(pddlGeneratorPath) " + generator.arguments!.joined(separator: " "))
    try generator.run()
    generator.waitUntilExit()
    if generator.terminationStatus != 0 {
      throw AIKitError(
        message: "Best response pddl generator returned non-zero exit code: \(generator.terminationStatus)")
    }
    let problemPDDL = try String(
      contentsOf: tempDirURL.appendingPathComponent("planner_problem.pddl"),
      encoding: .utf8)
    
    let stats = try JSONSerialization.jsonObject(
      with: Data(contentsOf: tempDirURL.appendingPathComponent("stats.json"),
                 options: [])) as! [String: [String: Double]]

    let statsFixed: [String: [Int: Double]] = stats.mapValues {
      (v: [String: Double]) -> [Int: Double] in
      Dictionary<Int, Double>(v.map {
                                (Int($0.key)!, $0.value)}, uniquingKeysWith: { a, b in a})
    }
    
    
    try? FileManager.default.removeItem(at: tempDirURL)
    return (classicalProblemPDDL: problemPDDL,
            costPredicateStatistics: statsFixed)
  }
  
  public static func generateSamplingPlannerPlanPDDL(
    temporalGameDomain: String,
    temporalGameInstance: String,
    config: SamplingConfig,
    player: Int) throws -> (
      classicalProblemPDDL: String,
      costPredicateStatistics: [String: [Int: Double]])
  {
    
    let pddlGeneratorPath = ExperimentManager.defaultManager.systemConfig[
      "samplingPDDLGenerator"]!.string!
    let plannersPath = ExperimentManager.defaultManager.systemConfig[
      "samplingPDDLGeneratorPlannersPath"]!.string!
    let temporaryDirectory = "tmp_" + UUID().uuidString
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    let tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try temporalGameDomain.write(
      to: tempDirURL.appendingPathComponent("game_domain.pddl"),
      atomically: true,
      encoding: .utf8)
    try temporalGameInstance.write(
      to: tempDirURL.appendingPathComponent("game_problem.pddl"),
      atomically: true,
      encoding: .utf8)
    
    let generator = Process()
    generator.executableURL = URL(fileURLWithPath: "/usr/bin/java")
    var arguments = [
      "-jar",
      pddlGeneratorPath,
      tempDirURL.appendingPathComponent("game_domain.pddl").path,
      tempDirURL.appendingPathComponent("game_problem.pddl").path,
      String(player),
      String(config.numberOfEstimates),
      tempDirURL.path,
      plannersPath,
      config.deadlinesDistributionType.argument
    ]
    if config.deadlinesDistributionType == .timeBased {
      arguments.append(config.selectOnlyLastInterferingActions ? "true" : "false" )
    }
    generator.arguments = arguments
    print("command: /usr/bin/java " + generator.arguments!.joined(separator: " "))
    generator.currentDirectoryURL = tempDirURL
    try generator.run()
    generator.waitUntilExit()
    if generator.terminationStatus != 0 {
      throw AIKitError(
        message: "Sampling pddl generator returned non-zero exit code: \(generator.terminationStatus)")
    }
    let problemPDDL = try String(
      contentsOf: tempDirURL.appendingPathComponent("planner_problem.pddl"),
      encoding: .utf8)
    
    let stats = try JSONSerialization.jsonObject(
      with: Data(contentsOf: tempDirURL.appendingPathComponent("stats.json"),
                 options: [])) as! [[String: [String: Double]]]
    
    var statsMerged = [String: [Int: Double]]()
    
    for resource_x in stats {
      let (resource, x) = resource_x.first!
      let xx = Dictionary<Int, Double>(uniqueKeysWithValues: x.map {(Int($0.key)!, $0.value)})
      statsMerged[resource] = xx
    }
    
    try? FileManager.default.removeItem(at: tempDirURL)
    return (classicalProblemPDDL: problemPDDL,
            costPredicateStatistics: statsMerged)
  }
  
  public struct SamplingConfig {
    public let numberOfEstimates: Int
    public let deadlinesDistributionType: SamplingPlannerMode
    public let selectOnlyLastInterferingActions: Bool
    public init(from parameters: AIKitData) {
      self.numberOfEstimates = parameters["ESTNO"]!.integer!
      self.deadlinesDistributionType = FRASPDDLGeneratorUtils.SamplingPlannerMode(
        parameters["distr"]?.string ?? "timeBased")
      self.selectOnlyLastInterferingActions = parameters["lastIntAct"]?.bool ?? false
      precondition(
        (deadlinesDistributionType == .timeBased) || !selectOnlyLastInterferingActions,
        "Invalid sampling config")
    }
  }
  
  public enum SamplingPlannerMode {
    case uniform
    case timeBased
    public init(_ s: String) {
      switch s {
      case "uniform":
        self = .uniform
      case "timeBased":
        self = .timeBased
      default:
        fatalError("Unknown sampling type")
      }
    }
    public var argument: String {
      switch self {
      case .timeBased:
        return "time-based"
      case .uniform:
        return "uniform"
      }
    }
  }
}

extension String: PureStrategy {}


extension PDDLTemporalGameOld {
  public func generateSamplingPlannerPlaningProblem(
    for player: Int,
    config: FRASPDDLGeneratorUtils.SamplingConfig) throws -> (
      PDDLPlanningOld,
      [String: [Int: Double]]
    )
  {
    
    let (classicalProblemPDDL, stats) = try FRASPDDLGeneratorUtils.generateSamplingPlannerPlanPDDL(
      temporalGameDomain: domain.stringRepresentation,
      temporalGameInstance: instance.stringRepresentation,
      config: config,
      player: player)
    let classicalPddlPlanning = PDDLPlanningOld(
      instance: PDDLPlanningProblemOld(raw: classicalProblemPDDL),
      domain: PDDLDomainOld(stringRepresentation: classicalPlannerDomain))
    return (classicalPddlPlanning, stats)
  }
}
