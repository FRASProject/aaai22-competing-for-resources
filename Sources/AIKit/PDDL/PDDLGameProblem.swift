//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation

public struct PDDLGameProblem {
  public let name: String
  public let domainName: String
  public let initState: PDDLInitState
  public let goals: [PDDLFormula]
  public let metrics: [PDDLMetric]
}

extension PDDLGameProblem {
  public func getProblem(for player: Int) -> PDDLProblem {
    PDDLProblem(
      name: name,
      domainName: domainName,
      initState: initState,
      goal: goals[player],
      metrics: metrics[player])
  }
  
}
