//
//  File.swift
//  
//
//  Created by Pavel Rytir on 8/18/21.
//

import Foundation

public struct ActionNameProjectedParameters: Hashable, Equatable, Comparable {
  public let name: String
  public let parametersIndices: [Int]
  public let parametersInstance: [String]
  public func shiftingParametersIndices(by offset: Int) -> ActionNameProjectedParameters {
    return ActionNameProjectedParameters(
      name: name,
      parametersIndices: parametersIndices.map {$0 + offset},
      parametersInstance: parametersInstance)
  }
  public static func < (lhs: ActionNameProjectedParameters, rhs: ActionNameProjectedParameters) -> Bool {
    lhs.name < rhs.name ||
    (lhs.name == rhs.name && lhs.parametersIndices.lexicographicallyPrecedes(rhs.parametersIndices)) ||
    (lhs.name == rhs.name && lhs.parametersIndices == rhs.parametersIndices &&
     lhs.parametersInstance.lexicographicallyPrecedes(rhs.parametersInstance))
  }
}
