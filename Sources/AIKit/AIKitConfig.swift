//
//  config.swift
//  FRASLib
//
//  Created by Pavel Rytir on 5/20/18.
//

import Foundation

// FIXME: There are some other structures with configs. It should (may) be merged into one.
/// A structure with configuration containing some magic constants.
public enum AIKitConfig {
  /// Deadlines in best response computation are increased by this to compensate rounding errors.
  public static let deadlinesRoundingEpsilon = 1.0
  
  /// Finish time of simulation in the computation of best response. - Just some big number FIXME: maybe add some default like infinity in the simulation core.
  public static let bestResponseSimulationFinishTime = 10e20
  
  /// Default number of simulation runs in the monte carlo simulation.
  public static let defaultNumberOfSimulationsInMonteCarlo = 5000
  
  /// Top bound for deadlines. We just need some big number.
  public static let bestResponseTopBound = 10e9
  
  /// Maximum reward that UAV can get if it collects resource before all deadlines.
  public static let maximumDeadlineReward = 100.0
  
  /// Maximum number of different deadlines.
  public static let maximumNumberOfDeadlinesPerResource = 1
  
  /// Probabililty less than this should be considered 0.0
  public static let probabilityEpsilon = 0.001
  
  /// Default epsilon for comparing doubles. I.e. x=y <-> abs(x-y)<epsilon
  public static let defaultEpsilon = 0.04 // was 0.2
  
  public static let epsilonForDeadlinesComparison = 0.1
  
  /// This is used as an artificial collection time for uncollected resources.
  public static let greatestDeadline = 10000.00
  
  public static let maxSoftGoalDeadlineCost = 100
  
  public static let uctExplorationParameterC = sqrt(2.0)
}




