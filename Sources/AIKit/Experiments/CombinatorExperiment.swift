//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/29/21.
//

import Foundation

open class CombinatorExperiment<E: ExperimentInitiableByConfig>: ExperimentProtocol
where E.ResultData: ExperimentDataResultDictionary
{
  public typealias ResultData = Result
  public var parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  
  public struct Result: ExperimentData {
    public let state: ExperimentDataItemState
    public let gamesResults: [ExperimentDataWithIndex<E.ResultData>]
  }
  private let configKeys: [String]
  public private(set) var combinedExperiments: [E]
  public init(configKeys: [String]) {
    self.configKeys = configKeys
    self.combinedExperiments = []
    self.parameters = ExperimentParameters()
  }
  open func runBody() throws {
    var experimentParametersCombinations = [ExperimentParameters]()
    
    for experimentConfigs in ExperimentManager.defaultManager.experimentConfig.arrayOfDictionaries! {
      var experimentParametersSubCombinations = [ExperimentParameters()]
      for key in configKeys {
        let keyConfig = experimentConfigs[key]!
        let configs = keyConfig.arrayOfDictionaries!
        let keyExperimentCombinations = experimentParametersSubCombinations.flatMap {
          $0.merging(from: configs)
        }
        experimentParametersSubCombinations = keyExperimentCombinations
      }
      experimentParametersCombinations.append(contentsOf: experimentParametersSubCombinations)
    }
    
    let experiments = experimentParametersCombinations.map { experimentParameters in
      E(parameters: experimentParameters)
    }
    try runSubexperiments(experiments)
    combinedExperiments = experiments.filter {!$0.isFailed}
    save(result: Result(
      state: .finished,
      gamesResults: experiments.compactMap {$0.asExperimentDataWithIndex}))
  }
  
  open var fileNamePrefix: String {
    "GamesAll"
  }
  open var fileNameSufix: String {
    ".json"
  }
  open var recompute: Bool {
    true
  }
  open func runAfterFinish() throws {
    let table = AIKitTable<AIKitData>(rows: combinedExperiments.compactMap {$0.resultDataTable})
    let stringTable = table.convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
    
    try stringTable.toText(type: .screen, colsPadding: true).write(
      toFile: "allGamesResults.txt",
      atomically: false,
      encoding: .utf8)
    
    try stringTable.toText(type: .csv, colsPadding: false).write(
      toFile: "allGamesResults.csv",
      atomically: false,
      encoding: .utf8)
    
    try stringTable.toText(type: .tex, colsPadding: false).write(
      toFile: "allGamesResults.tex",
      atomically: false,
      encoding: .utf8)
  }
}
