//
//  File.swift
//  
//
//  Created by Pavel Rytir on 3/15/21.
//

import Foundation

public enum DoublePruningDoubleOracleExperiment {
  public static func run(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    config: AIKitData,
    outputDirName: String?) throws -> DoubleOracleResults<TemporalPlan, TemporalPlan>
  {
    if config["V"]?.string == "noBackup" {
      
      let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.makeDoublePruning(
        for: game,
           config: config)
      let result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: outputDirName,
        plansCachePath: nil)
      return result
    } else if config["V"]?.string == "normalBackup" {
      let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.makeDoublePruningWithNormalBackup(
        for: game,
           config: config)
      let result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: outputDirName,
        plansCachePath: nil)
      return result
    } else if config["V"]?.string == "onlyInit" {
      let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.makeDoublePruningOnlyInit(
        for: game,
           config: config)
      let result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: outputDirName,
        plansCachePath: nil)
      return result
    } else {
      fatalError("Invalid config")
    }
  }
}
