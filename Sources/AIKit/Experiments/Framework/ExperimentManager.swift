//
//  ExperimentManager.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 9/16/20.
//

import Foundation
import MathLib

public class ExperimentManager {
  public let systemConfig: [String: AIKitData]
  public let experimentConfig: AIKitData
  public let fastDownwardAliases: [String: [String]]
  public let analytics: ExperimentAnalytics?
  public let maxNumberOfConcurrentTasks: Int
  public let currentWorkingDirectory: String
  public var games: [Tuple<String, String>: PDDLGameFDRRepresentationAnalyticsLogic]
  public let semaphore1: DispatchSemaphore
  private init(
    systemConfig: [String: AIKitData],
    experimentConfig: AIKitData,
    fastDownwardAliases: [String: [String]],
    analytics: ExperimentAnalytics?,
    maxNumberOfConcurrentTasks: Int,
    currentWorkingDirectory: String)
  {
    self.systemConfig = systemConfig
    self.experimentConfig = experimentConfig
    self.fastDownwardAliases = fastDownwardAliases
    self.maxNumberOfConcurrentTasks = maxNumberOfConcurrentTasks
    self.currentWorkingDirectory = currentWorkingDirectory
    self.analytics = analytics
    self.games = [:]
    self.semaphore1 = DispatchSemaphore(value: 1)
  }
  
  public func findGameInstanceDirectory(_ config: AIKitData) -> String {
    let domains_path = self.systemConfig["DOMAINS_PATH"]!.string!
    let game = config["GAME"]!.string!
    let domain = config["DOMAIN"]!.string!
    let instanceDir = domains_path + "/" + domain + "/" + game
    return instanceDir
  }
  
  public func findDescription(
    _ config: AIKitData) -> (String, String, String)?
  {
    let domains_path = self.systemConfig["DOMAINS_PATH"]!.string!
    let game = config["GAME"]!.string!
    let domain = config["DOMAIN"]!.string!
    let domainURL = URL(fileURLWithPath: domains_path).appendingPathComponent(domain)
    guard let domainDescription = try? String(
            contentsOf: domainURL.appendingPathComponent(
              "description.txt")),
          let problemNumber = try? String(
            contentsOf: domainURL.appendingPathComponent(game).appendingPathComponent(
              "number.txt")),
          let problemDescription = try? String(
            contentsOf: domainURL.appendingPathComponent(game).appendingPathComponent(
              "description.txt"))
          else {
      return nil
    }
    return (
      domainDescription.trimmingCharacters(in: .newlines),
      problemNumber.trimmingCharacters(in: .newlines),
      problemDescription.trimmingCharacters(in: .newlines))
  }
  
  public func getGame(
    _ config: AIKitData) throws -> PDDLGameFDRRepresentationAnalyticsLogic
  {
    let game = config["GAME"]!.string!
    let domain = config["DOMAIN"]!.string!
    if let fdrGame = games[Tuple(domain, game)] {
      return fdrGame
    } else {
      let fdrGame = try PDDLGameFDRRepresentationAnalyticsLogic(config: config)
      games[Tuple(domain, game)] = fdrGame
      return fdrGame
    }
  }
  
  public func getPlanningGame(_ config: AIKitData) throws -> PlanningGameFDRRepresentation {
    let (domainFile, problemFile, domainGameConfigFileName, problemGameConfigFileName) = findPlanningGameDomainAndProblem(config)
    let domainGameConfig: PlanningGame.Config
    if FileManager.default.fileExists(atPath: problemGameConfigFileName) {
      domainGameConfig = try JSONDecoder().decode(
        PlanningGame.Config.self,
        from: Data(contentsOf: URL(fileURLWithPath: problemGameConfigFileName)))
    } else {
      domainGameConfig = try JSONDecoder().decode(
        PlanningGame.Config.self,
        from: Data(contentsOf: URL(fileURLWithPath: domainGameConfigFileName)))
    }
    
//    let adversaryActionsNames: [String]?
//    if FileManager.default.fileExists(atPath: adversaryActionsFileName) {
//      adversaryActionsNames = try JSONDecoder().decode(
//        Array<String>.self,
//        from: Data(contentsOf: URL(fileURLWithPath: adversaryActionsFileName)))
//    } else {
//      adversaryActionsNames = nil
//    }
    let game = try PlanningGame(
      domain: String(contentsOfFile: domainFile),
      problem: String(contentsOfFile: problemFile),
      config: domainGameConfig)
    return PlanningGameFDRRepresentation(game: game)
  }
  
  public func findPlanningGameDomainAndProblem(_ config: AIKitData) -> (
    domainFile: String,
    problemFile: String,
    domainGameConfigFileName: String,
    problemGameConfigFileName: String
  )
  {
    let planningProblemsPath = self.systemConfig["PLANNING_PROBLEMS_PATH"]!.string!
    let problem = config["PROBLEM"]!.string!
    let domain = config["DOMAIN"]!.string!
    let domainFile = planningProblemsPath + "/" + domain + "/domain.pddl"
    let problemFile = planningProblemsPath + "/" + domain + "/" + problem + ".pddl"
    let domainGameConfigFileName = planningProblemsPath + "/" + domain + "/domainGameConfig.json"
    let problemGameConfigFileName = planningProblemsPath + "/" + domain + "/\(problem)_gameConfig.json"
    return (
      domainFile: domainFile,
      problemFile: problemFile,
      domainGameConfigFileName: domainGameConfigFileName,
      problemGameConfigFileName: problemGameConfigFileName
    )
  }
  
  public func findGameDomainAndInstance(
    _ config: AIKitData) -> (
    gameDomainFile: String,
    gameInstancePath: String,
    plannerDomainFile: String,
    plannerTemporalDomainFile: String,
    grounderCacheDir: String
    )
  {
    let domains_path = self.systemConfig["DOMAINS_PATH"]!.string!
    let game = config["GAME"]!.string!
    let domain = config["DOMAIN"]!.string!
    let domainFile = domains_path + "/" + domain + "/game_domain.pddl"
    let plannerDomainFile = domains_path + "/" + domain + "/planner_classical_domain.pddl"
    let plannerTemporalDomainFile = domains_path + "/" + domain + "/planner_temporal_domain.pddl"
    let instanceFile = domains_path + "/" + domain + "/" + game + "/game_problem.pddl"
    let grounderCacheDir = domain + "/" + game + "/groundingCache"  // Relative
    return (
      gameDomainFile: domainFile,
      gameInstancePath: instanceFile,
      plannerDomainFile: plannerDomainFile,
      plannerTemporalDomainFile: plannerTemporalDomainFile,
      grounderCacheDir: grounderCacheDir
    )
  }
  
  public static func initDefault(
    experimentConfig: AIKitData,
    systemConfig: [String: AIKitData],
    fastDownwardAliases: [String: [String]],
    maxNumberOfConcurrentTasks: Int,
    currentWorkingDirectory: String,
    analytics: String?) {
    let analyticsObject: ExperimentAnalytics?
    if analytics == "NEPTUNE" {
      analyticsObject = NeptuneExperimentAnalytics(
        projectName: systemConfig["NEPTUNE_PROJECT_NAME"]!.string!,
        token: systemConfig["NEPTUNE_API_TOKEN"]!.string!)
    } else {
      analyticsObject = nil
    }
    ExperimentManager.manager = ExperimentManager(
      systemConfig: systemConfig,
      experimentConfig: experimentConfig,
      fastDownwardAliases: fastDownwardAliases,
      analytics: analyticsObject,
      maxNumberOfConcurrentTasks: maxNumberOfConcurrentTasks,
      currentWorkingDirectory: currentWorkingDirectory
    )
  }
  private static var manager: ExperimentManager!
  public static var defaultManager: ExperimentManager {
    if manager != nil {
      return manager
    } else {
      fatalError("Manager not initialized")
    }
  }
}
