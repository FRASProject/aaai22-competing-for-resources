//
//  ExperimentParameters.swift
//  AIKit
//
//  Created by Pavel Rytir on 9/22/20.
//

import Foundation
import MathLib

public struct ExperimentParameters : Hashable, Equatable {
  public var data: AIKitData {
    .dictionary(parameters.merging(index.data, uniquingKeysWith: { _, b in b }))
  }
  public let parameters: [String: AIKitData]
  public let index: ExperimentIndex
  public var pathKeys: [[String]] {
    index.pathKeys
  }
  public let onlyValuePathKeys: Set<String>
  public let filteredKeys: Set<String>
  
  public var fileNamePrefix: String?
  public var fileNameSufix: String?
  private static let backwardsCompabilitySuffix = "__BACKCOMP"
  
  public func addingParameter(key: String, value: AIKitData) -> ExperimentParameters {
    var newParameters = parameters
    newParameters[key] = value
    return ExperimentParameters(
      index: index,
      parameters: newParameters,
      fileNamePrefix: fileNamePrefix,
      fileNameSufix: fileNameSufix,
      filteredKeys: filteredKeys,
      onlyValuePathKeys: onlyValuePathKeys)
  }
  
  public func subParameters(_ key: String) -> ExperimentParameters {
    ExperimentParameters(
      index: index,
      parameters: parameters[key]!.dictionary!,
      fileNamePrefix: fileNamePrefix,
      fileNameSufix: fileNameSufix,
      filteredKeys: filteredKeys,
      onlyValuePathKeys: onlyValuePathKeys)
  }
  
  public var withLastDirPushedDown: ExperimentParameters {
    return ExperimentParameters(
      index: ExperimentIndex(
        data: index.data,
        pathKeys: index.pathKeys + [[]]), parameters: parameters,
      fileNamePrefix: fileNamePrefix,
      fileNameSufix: fileNameSufix,
      filteredKeys: filteredKeys,
      onlyValuePathKeys: onlyValuePathKeys)
  }
  
  public var indexDictionary: [String: AIKitData] {
    index.data
  }
  
  public subscript(_ key: String) -> AIKitData? {
    data[key]
    //index.data[key] ?? parameters[key]
  }
  public var indexValues: [AIKitData] {
    index.indexValues
  }
  public var indexKeys: [String] {
    index.indexKeys
  }
  
  public var fileNameKeys: [String] {
    pathKeys.last!
  }
  public var fileNameInfix: String {
    if pathKeys.isEmpty {
      return ""
    } else {
      return pathItem(pathKeys.count - 1)
    }
  }
  public var fileName: String {
    (self.fileNamePrefix ?? "") + fileNameInfix + (self.fileNameSufix ?? "")
  }
  
  
  
  public var pathItems: [String] {
    pathKeys.indices.map {pathItem($0)}
  }
  
  public var pathDir: String {
    pathItems.dropLast().joined(separator: "/")
  }
  public var pathFileName: String {
    if pathDir.isEmpty || fileName.isEmpty {
      return pathDir + fileName
    } else {
      return pathDir + "/" + fileName
    }
  }
  public var pathLastDir: String {
    pathItem(pathKeys.count - 2)
  }
  public var pathLastDirFileNameInfix: String {
    pathLastDir + (fileNameInfix.isEmpty ? "" : "_" + fileNameInfix)
  }
  
  public var stringRepresentation: String {
    pathItems.joined(separator: ":")
  }
  
  public func isPrefix(of other: ExperimentParameters) -> Bool {
    let dataIndex = self.indexValues
    let keysIndex = self.indexKeys
    let otherDataIndex = other.indexValues
    let otherKeysIndex = other.indexKeys
    for i in 0..<keysIndex.count {
      if keysIndex[i] != otherKeysIndex[i] || dataIndex[i] != otherDataIndex[i] {
        return false
      }
    }
    return true
  }

  private init(
    index: ExperimentIndex,
    parameters: [String: AIKitData],
    fileNamePrefix: String?,
    fileNameSufix: String?,
    filteredKeys: Set<String>,
    onlyValuePathKeys: Set<String>
    )
  {
    self.index = index
    self.parameters = parameters
    self.fileNamePrefix = fileNamePrefix
    self.fileNameSufix = fileNameSufix
    self.filteredKeys = filteredKeys
    self.onlyValuePathKeys = onlyValuePathKeys
  }
  public init() {
    self.index = ExperimentIndex()
    self.parameters = [:]
    self.fileNamePrefix = nil
    self.fileNameSufix = nil
    self.filteredKeys = []
    self.onlyValuePathKeys = []
  }
  
  
  public func addingFileParam(_ key: String, value: AIKitData) -> Self {
    let newIndex = index.addingKeysSorted(
      toLevel: index.lastLevel,
      keyValue: [key: value])
    return ExperimentParameters(
      index: newIndex,
      parameters: parameters,
      fileNamePrefix: fileNamePrefix,
      fileNameSufix: fileNameSufix,
      filteredKeys: filteredKeys,
      onlyValuePathKeys: onlyValuePathKeys)
  }
  private func pathItem(_ n: Int) -> String {
    index.pathKeys[n].filter {!filteredKeys.contains($0)}
    .filter {index.data[$0]!.stringRepresentation != ""}
    .map {
      if onlyValuePathKeys.contains($0) {
        return index.data[$0]!.stringRepresentation
      } else {
        return "\($0)-\(index.data[$0]!.stringRepresentation)"
      }
    }.joined(separator: "_")
  }
  
  public func merging(
    from additionalParameters: [[String: AIKitData]],
    subDictKey: String? = nil) -> [ExperimentParameters]
  {
    additionalParameters.flatMap { merging(from: $0, subDictKey:subDictKey) }
  }
  
  public func merging(
    from additionalParameters: [String: AIKitData],
    subDictKey: String? = nil) -> [ExperimentParameters]
  {
    let indexParams = additionalParameters.filter { $0.key.contains("__") }
    let nonIndexParams = additionalParameters.filter { !$0.key.contains("__") }
    
    var staticKeysDict = [String: AIKitData]()
    var dynamicKeysDict = [String: [AIKitData]]()
    var subDictKeysDict = [[[String: [AIKitData]]]]()
    for (key, value) in indexParams {
      if key == "___" {
        subDictKeysDict.append(
          value.array!.map { $0.dictionary!.mapValues {$0.array!} })
      } else if let list = value.array {
        dynamicKeysDict[key] = list
      } else {
        staticKeysDict[key] = value
      }
    }
    
    let (dynamicCombinations, filteredItems) = createCombinations(
      dynamic: dynamicKeysDict,
      subDicts: subDictKeysDict)
    var result = [ExperimentParameters]()
    for dynamicCombination in dynamicCombinations {
      let newFilteredKeys = filteredItems.filter {dynamicCombination[$0.key] == $0.value}.map {
        $0.key
      }
      let newLevels = generateDataDictAndIndexElements(dynamicCombination.merging(staticKeysDict, uniquingKeysWith: { e, _ in e }))
      let newIndexParameters = Dictionary(uniqueKeysWithValues: newLevels.flatMap {$0})
      let newParameters: [String: AIKitData]
      if let subDictKey = subDictKey {
        var newParametersToUpdate = self.parameters
        newParametersToUpdate[subDictKey] = .dictionary(nonIndexParams.merging(
          newIndexParameters,
          uniquingKeysWith: { _, e in e }))
        newParameters = newParametersToUpdate
      } else {
        newParameters = self.parameters.merging(
          nonIndexParams,
          uniquingKeysWith: { _, e in e }).merging(
            newIndexParameters,
            uniquingKeysWith: { _, e in e })
      }
      let newIndex = index.appendingLevelsKeysSorted(newLevels + [[:]])
      let onlyValueLevelsKeys = newLevels.dropLast().flatMap {$0.keys}
      let experimentParameters = ExperimentParameters(
        index: newIndex,
        parameters: newParameters,
        fileNamePrefix: self.fileNamePrefix,
        fileNameSufix: self.fileNameSufix,
        filteredKeys: self.filteredKeys.union(
          newFilteredKeys.map {
            String($0.dropLast(ExperimentParameters.backwardsCompabilitySuffix.count))}),
        onlyValuePathKeys: Set(onlyValueLevelsKeys).union(onlyValuePathKeys))
      result.append(experimentParameters)
    }
    return result
  }
  
  private func substitute(
    into dict: [String: AIKitData],
    values: [String: AIKitData]) -> [String: AIKitData]
  {
    Dictionary(uniqueKeysWithValues: dict.map { (key, value) -> (String, AIKitData) in
      if let v = value.string, v.hasPrefix("__"), v.hasSuffix("__") {
        let valueKey = String(v.dropFirst(2).dropLast(2))
        return (key, values[valueKey] ?? value)
      } else if let d = value.dictionary {
        return (key, .dictionary(substitute(into: d, values: values)))
      } else if let a = value.array {
        let aNew = a.map { e -> AIKitData in
          if let v = e.string, v.hasPrefix("__"), v.hasSuffix("__") {
            let valueKey = String(v.dropFirst(2).dropLast(2))
            return values[valueKey] ?? e
          } else {
            return e
          }
        }
        return (key, .array(aNew))
      }
      else {
        return (key, value)
      }
    })
  }
    
  private func createCombinations(
    dynamic: [String: [AIKitData]],
    subDicts: [[[String: [AIKitData]]]]) -> (
    [[String: AIKitData]],
      [String: AIKitData])
  {
    var firstItems = [String: AIKitData]()
    firstItems.merge(dynamic.map {($0.key, $0.value.first!)}, uniquingKeysWith: { a, b in a})
    var result = generateCartesianProduct(of: dynamic)
    for subDict in subDicts {
      var sum = [[String: AIKitData]]()
      for subSubDict in subDict {
        firstItems.merge(subSubDict.map {($0.key, $0.value.first!)}, uniquingKeysWith: { a, b in a})
        sum.append(contentsOf: generateCartesianProduct(of: subSubDict))
      }
      result = DictionaryUtils.cartesianProduct(result, sum)
    }
    firstItems = Dictionary(uniqueKeysWithValues: firstItems.filter {
      $0.key.contains(ExperimentParameters.backwardsCompabilitySuffix)
    })
    return (result, firstItems)
  }
  
  private func generateDataDictAndIndexElements(
    _ d: [String: AIKitData]) -> [[String: AIKitData]]
  {
    let pathItems = d.filter {$0.key.last == "D"}
    var lastDirItems = d.filter {$0.key.last != "D"}.map {
      (key: String($0.key[..<$0.key.firstIndex(of: "_")!]), value: $0.value)
    }
    lastDirItems.sort(by: { $0.0 < $1.0})
    var pathItemsLevel = pathItems.map { (k,v) -> (Int, String, String) in
      let dirLevel = Int(k[k.index(after: k.lastIndex(of: "_")!)..<k.index(before: k.endIndex)])!
      let newKey = String(k[..<k.firstIndex(of: "_")!])
      return (dirLevel, newKey, v.stringRepresentation)
    }
    pathItemsLevel.sort(by: {$0.0 < $1.0})
    var levels = pathItemsLevel.map { [$0.1: AIKitData.string($0.2)] }
    levels.append(Dictionary(uniqueKeysWithValues: lastDirItems))
    var newDict = Dictionary<String, AIKitData>(
      uniqueKeysWithValues: pathItemsLevel.map {($0.1, AIKitData.string($0.2))})
    newDict.merge(lastDirItems, uniquingKeysWith: { a, b in a })
    return levels.map { substitute(into: $0, values: newDict) }
  }
}

