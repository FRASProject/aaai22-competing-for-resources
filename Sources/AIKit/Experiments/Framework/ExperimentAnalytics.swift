//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/3/20.
//

import Foundation

public protocol ExperimentAnalytics {
  func log(
    name: String,
    description: String,
    parameters: [String: AIKitData],
    metrics: [String: AIKitData],
    images: [String: AIKitData],
    markFilename: String?) throws
}

