//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/16/20.
//

import Foundation

public protocol ExperimentProtocol: AnyObject  {
  associatedtype ResultData: Codable
  var parameters: ExperimentParameters { get }
  var resultData: ResultData? { get set }
  var fromCache: Bool? { get set }
  var failError: Error? { get set }
  var description: String { get }
  var recompute: Bool { get }
  //func run() throws
  func runBody() throws
  func runBeforeStart() throws
  func runAfterFinish() throws
  var cachePathFileName: String { get }
  var fileNamePrefix: String { get }
  var fileNameSufix: String { get }
}

extension ExperimentProtocol {
  public func runBeforeStart() throws {}
  public func runAfterFinish() throws {}
  public var description: String {
    parameters.pathDir
  }
  public var cachePathFileName: String {
    var resultParameters = parameters
    resultParameters.fileNamePrefix = fileNamePrefix
    resultParameters.fileNameSufix = fileNameSufix
    return resultParameters.pathFileName
  }
}

extension ExperimentProtocol {
  public func run() throws {
    let fileName = URL(fileURLWithPath: cachePathFileName)
    let stateFileName = URL(fileURLWithPath: cachePathFileName + ".state.json")
    if FileManager.default.fileExists(atPath: stateFileName.path) {
      let state = try JSONDecoder().decode(ExperimentDataItemState.self, from: Data(contentsOf: stateFileName))
      if state == .failedDontRecompute { return }
    }
    if FileManager.default.fileExists(atPath: fileName.path) && !recompute {
      let result = try JSONDecoder().decode(ResultData.self, from: Data(contentsOf: fileName))
      resultData = result
      fromCache = true
      return
    }
    
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    print("STARTING: \(description): \(formatter.string(from: Date()))")
    try runBeforeStart()
    try runBody()
    
    guard let result = resultData else {
      print("NOT FINISHED: \(description): \(formatter.string(from: Date()))")
      return
    }
    print("FINISHED: \(description): \(formatter.string(from: Date()))")
    let directory = fileName.deletingLastPathComponent()
    if !FileManager.default.fileExists(atPath: directory.path) {
      try FileManager.default.createDirectory(
        at: directory,
        withIntermediateDirectories: true,
        attributes: nil)
    }
    let encoder = JSONEncoder()
    encoder.outputFormatting = .prettyPrinted
    try encoder.encode(result).write(to: fileName)
    try encoder.encode(ExperimentDataItemState.finished).write(to: stateFileName)
    try runAfterFinish()
    //printFailed(experiments: failedSubexperiments)
  }
  public func save(result: ResultData) {
    self.resultData = result
    self.fromCache = false
  }
  public var isFailed: Bool {
    failError != nil
  }
  
  public var experimentDirectory: String {
    parameters.pathDir
  }
  public var asExperimentDataWithIndex: ExperimentDataWithIndex<ResultData>? {
    guard let data = resultData else {
      return nil
    }
    return ExperimentDataWithIndex(index: parameters.index, data: data)
  }
  
  public func runSubexperiments<SE: ExperimentProtocol>(_ subexperiments: [SE]) throws {
    _ = try ExperimentUtils.runParallel(subexperiments)
//    self.subexperiments.append(contentsOf: subexperiments.filter {
//      $0.failError == nil
//    })
    let failed = subexperiments.filter {
      $0.failError != nil
    }
    if !failed.isEmpty {
      print("Failed experiments:")
      print(failed.map {$0.description + " : " + ($0.failError?.localizedDescription ?? "error description NA")}.joined(separator: "\n"))
    }
  }
  @available(*, deprecated)
  public var isAllResultsFailedDontRecompute: Bool {
    fatalError()
    //resultData?.state == .failedDontRecompute
  }
//  public func printFailed(experiments: [ExperimentProtocol]) {
//    if !experiments.isEmpty {
//      print("Failed experiments:")
//      print(experiments.map {$0.description + " : " + ($0.failError?.localizedDescription ?? "error description NA")}.joined(separator: "\n"))
//    }
//  }
}

extension ExperimentProtocol where ResultData : ExperimentDataResultDictionary {
  public var resultDictionary: [String: AIKitData]? {
    if let result = resultData {
      let d = result.asDictionary
      let id = parameters.indexDictionary
      return d.merging(id, uniquingKeysWith: { a, b in
        if a != b {
          return .array([a,b])
        } else {
          return a
        }
      })
    } else {
      return nil
    }
  }
  
  public var resultDataTable: AIKitTable<AIKitData>? {
    if let resultDictionary = resultDictionary {
      return AIKitTable(rows: [resultDictionary])
    } else {
      return nil
    }
  }
}


extension Sequence where Element: ExperimentProtocol {
  public func grouping(by keys: [String]) -> [ExperimentDataFilter:[Element]] {
    Dictionary(grouping: self, by: {$0.parameters.createFilter(for: keys)})
  }
}


