//
//  AggregateTableGenerator.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 10/1/20.
//

import Foundation
import MathLib

//public class AggregateTableGenerator: Experiment<AggregateTableGenerator.Result> {
//  public struct Result : ExperimentDataResultDictionary, ExperimentData {
//    public var asDictionary: [String : AIKitData] {
//      fatalError()
//    }
//    
//    public let table: [String: [String]]
//    public let fileNameWithoutExtension: String
//    public let state: ExperimentDataItemState
//  }
//  
//  public let rows: [[String: AIKitData]]
//  public let cols: [String]?
//  public let format: [String: Int]?
//  public let defaultFormat: Int?
//  
//  public let filenamePrefix: String
//  
//  public init(
//    rows: [[String: AIKitData]],
//    cols: [String]?,
//    filenamePrefix: String,
//    parameters: ExperimentParameters,
//    format: [String: Int]? = nil,
//    defaultFormat: Int? = nil)
//  {
//    self.rows = rows
//    self.cols = cols
//    self.format = format
//    self.defaultFormat = defaultFormat
//    self.filenamePrefix = filenamePrefix
//    super.init(parameters: parameters)
//  }
//  
//  public override func runBody() throws {
//    
//    let (result, _) = try AggregateTableGenerator.generateTables(
//      parameters: self.parameters,
//      filenamePrefix: filenamePrefix,
//      rows: rows,
//      cols: cols,
//      format: format,
//      defaultFormat: defaultFormat)
//    
//    save(result: result)
//    
//  }
//  
//  public static func generateTables(
//    parameters: ExperimentParameters,
//    filenamePrefix: String,
//    rows: [[String: AIKitData]],
//    cols: [String]?,
//    format: [String: Int]? = nil,
//    defaultFormat: Int? = nil) throws -> (
//      result: Result,
//      parameters: ExperimentParameters)
//  {
//    var table = [String: [String]]()
//    let mergedCols = Array(rows.map {
//                            $0.keys }.reduce(Set<String>()) { $0.union($1) })
//    for row in rows {
//      for colName in mergedCols {
//        let cellData = row[colName] ?? .string("NA")
//        var colData = table[colName, default: []]
//        colData.append(cellData.stringFormatedRepresentation(doubleDecimalDigits: format?[colName] ?? defaultFormat))
//        table[colName] = colData
//      }
//    }
//    var parameters = parameters
//    parameters.fileNamePrefix = "\(filenamePrefix)aggregateStatistics"
//    parameters.fileNameSufix = ".json"
////    let fileNameWithoutExtension = (parameters.pathFileName == "/" ? "" : parameters.pathFileName + "/") +
////      "aggregateStatistics"
//    try JSONEncoder().encode(table).write(
//      to: URL(fileURLWithPath: parameters.pathFileName))
//    
//
//    parameters.fileNameSufix = ".csv"
//    try TableUtils.generate(table, type: .csv, cols: cols).write(
//      toFile: parameters.pathFileName,
//      atomically: false,
//      encoding: .utf8)
//    parameters.fileNameSufix = ".tex"
//    try TableUtils.generate(table, type: .tex, cols: cols).write(
//      toFile: parameters.pathFileName,
//      atomically: false,
//      encoding: .utf8)
//    return (
//      result: Result(
//        table: table,
//        fileNameWithoutExtension: parameters.pathFileName, state: .finished),
//      parameters: parameters
//    )
//  }
//}
