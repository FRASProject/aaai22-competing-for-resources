//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/5/20.
//

import Foundation

public struct NeptuneExperimentAnalytics: ExperimentAnalytics {
  private let token: String
  public let projectName: String
  public init(projectName: String, token: String) {
    self.token = token
    self.projectName = projectName
  }
  public func log(
    name: String,
    description: String,
    parameters: [String : AIKitData],
    metrics: [String : AIKitData],
    images: [String : AIKitData],
    markFilename: String?) throws
  {
    
    if let markFilename = markFilename,
       FileManager.default.fileExists(atPath: markFilename) {
      return
    }
    
    
    let params = AIKitData.dictionary([
      "name": AIKitData.string(name),
      "description": AIKitData.string(description),
      "params": AIKitData.dictionary(parameters),
      "log_metric": AIKitData.dictionary(metrics),
      "properties": AIKitData.dictionary([:]),
      "tags": AIKitData.array([]),
      "upload_source_files": AIKitData.array([]),
      "log_text": AIKitData.dictionary([:]),
      "log_image": AIKitData.dictionary(images),
      "log_artifact": AIKitData.array([])
    ])
      
    let paramsData = try JSONSerialization.data(withJSONObject: params.any, options: [])
    
    
    
    let proc = Process()
    proc.executableURL = URL(fileURLWithPath: "/usr/bin/env")
    proc.arguments = [
      "python3",
      "-m",
      "neptunecontrib.sync.with_json",
      "--api_token",
      token,
      "--project_name",
      projectName,
      "--filepath",
      "/dev/stdin"
    ]
    let stdinPipe = Pipe()
    proc.standardInput = stdinPipe
    let stdoutPipe = Pipe()
    proc.standardOutput = stdoutPipe
    try proc.run()
    let readHandle = stdoutPipe.fileHandleForReading
    let writeHandle = stdinPipe.fileHandleForWriting
    writeHandle.write(paramsData)
    writeHandle.closeFile()
    proc.waitUntilExit()
    let stdoutText = String(data: readHandle.readDataToEndOfFile(), encoding: .utf8)
    if proc.terminationStatus != 0 {
      throw AIKitError(
        message: "Neptune analytics returned non-zero code \(proc.terminationStatus)")
    }
    let experimentId = stdoutText ?? "No neptune output data"
    if let markFilename = markFilename {
      try experimentId.write(
        toFile: markFilename,
        atomically: false,
        encoding: .utf8)
    }
  }
}
