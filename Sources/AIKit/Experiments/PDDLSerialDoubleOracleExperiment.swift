//
//  File.swift
//  
//
//  Created by Pavel Rytir on 2/1/21.
//

import Foundation

//public class PDDLSerialDoubleOracleExperiment:
//  Experiment<DoubleOracleResults<TemporalPlan, TemporalPlan>>
//{
//  public private(set) var saveData: Bool
////  public let mode: IterativeBestResponse.Mode.Short
//  
//  public var description: String {
//    "PDDLSerialDoubleOracle " + super.description
//  }
//  
//  public init(
//    parameters: ExperimentParameters,
//    mode: IterativeBestResponse.Mode.Short,
//    saveData: Bool = true)
//  {
//    self.saveData = saveData
//    super.init(parameters: parameters)
//  }
//  
//  override public var fileNamePrefix: String {
//    "AllResults"
//  }
//  override public var fileNameSufix: String {
//    ".json"
//  }
//  override public var recompute: Bool {
//    false
//  }
//  
//  public override func runBody() throws
//  {
//    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
//    
//    let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.makeSerial(
//      for: game,
//      config: parameters.data)
//    let result = try DoubleOracleUtils.runDoubleOracle(
//      doubleOracleConfiguration: doubleOracleConfiguration,
//      outputDirName: saveData ? parameters.pathFileName : nil,
//      plansCachePath: nil, saveResult: false)
//    save(result: result)
//  }
//}
