//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/23/21.
//

import Foundation

@available(*, deprecated)
public enum ExperimentPlanningGames {
  @available(*, deprecated)
  public static func run(with config: AIKitData) throws {
    
    let initExperimentParameters = ExperimentParameters()
    let games = config["games"]!.array!.map { $0.dictionary! }
    let gameParameters = games.flatMap {
      initExperimentParameters.merging(from: $0)
    }
//
//
//    let doParameters = gameParameters.flatMap {
//      $0.merging(from: config["DoubleOracle"]!.dictionary!)
//    }
//    let doExperiments = doParameters.map {
//      PlanningGameDOExperiment(parameters: $0)
//    }
//    let failedDOExperiments = try ExperimentUtils.runParallel(doExperiments)
//    let doSymkParameters = gameParameters.map {
//      $0.merging(from: config["DoubleOracleSymK"]!.dictionary!)
//    }
//
//
//
//    let doSymkExperiments = doSymkParameters.map { $0.map {
//      PlanningGameDOExperiment(parameters: $0)
//    }
//    }
//
//
//
//    let failedDOSymkExperiments = try ExperimentUtils.runParallel(doSymkExperiments.flatMap {$0})
//
//    let doTable = AIKitTable(rows: doExperiments.map {$0.resultDataTable})
//
//
//    let symkDoRows = doSymkExperiments.map { gameExperiments -> AIKitTable<ExperimentParameterData> in
//      let doSymk = gameExperiments.map { exp -> [String: ExperimentParameterData] in
//        let k = exp.parameters["topK"]!.integer!
//        return ["totalTime_\(k)": .double(exp.resultData!.data!.totalTime), "iterations_\(k)": .integer(exp.resultData!.data!.iterations), "p1Value_\(k)": .double(exp.resultData!.data!.player1EquilibriumValue), "p1Support_\(k)": .integer(exp.resultData!.data!.player1EquilibriumSupportSize)]
//      }
//      let cols = doSymk.reduce(["PROBLEM": gameExperiments.first!.parameters["PROBLEM"],
//                                "DOMAIN": gameExperiments.first!.parameters["DOMAIN"]]) {
//        $0.merging($1, uniquingKeysWith: { a, _ in a})
//      }
//      return AIKitTable(columns: cols.mapValues {[$0]})
//    }
//
//    let symkTable = AIKitTable(rows: symkDoRows)
//
//    let finalTable = doTable.joining(with: symkTable, on: ["PROBLEM", "DOMAIN"])
//
//    let finalTable2 = finalTable.convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
//
//    try finalTable2.toText(type: .screen, colsPadding: true).write(toFile: "allResultSymk.txt", atomically: false, encoding: .utf8)
//
//    return
    
    
    let parameters = gameParameters.flatMap {
      $0.merging(from: config["DoubleOracle"]!.dictionary!, subDictKey: "DoubleOracle")
    }.flatMap {
      $0.merging(from: config["kPlans"]!.dictionary!, subDictKey: "kPlans")
    }
    let experiments = parameters.map {
      PlanningGamesExperiment(parameters: $0)
    }
    let failedExperiments = try ExperimentUtils.runParallel(experiments)
    print("Failed: \(failedExperiments.count)")
    let table = AIKitTable(rows: experiments.map {$0.resultData!.tableRow })
    let table2 = table.convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
    let table2Test = try table2.toText(type: .screen, cols: [
      "DOMAIN", "PROBLEM", "player2EquilibriumValue", "player2EquilibriumValueABSTRDO",
      "player2EquilibriumValueKPLANSRESTRICTDO", "totalTime", "totalTimeABSTRDO", "totalTimeKPLANSRESTRICTDO", "totalTimeKPlans", "numberOfKPlans"
    ], colsPadding: true)
    //print(table2Test)
    try table2Test.write(toFile: "allResult.txt", atomically: false, encoding: .utf8)
    
    try table2.toText(type: .screen, colsPadding: true).write(
      toFile: "allResultsAll.txt",
      atomically: false,
      encoding: .utf8)
    
    try table2.toText(
      type: .screen,
      cols: ["DOMAIN", "PROBLEM", "varNumber", "abstractVarNumber"]).write(toFile: "vars.txt", atomically: false, encoding: .utf8)
    
    let charts1 = experiments.map {$0.resultData!.chart1Data}
    try ChartUtils.makeFigure(
      chart: ChartUtils.FigureParametersData(
        figureTitle: "Number of k plans vs Player 2 value",
        plots: charts1,
        parameters: charts1.first!.parameters),
      fileNames: ["plotsP2ValXnumK.png"])
    let charts2 = experiments.map {$0.resultData!.chart2Data}
    try ChartUtils.makeFigure(
      chart: ChartUtils.FigureParametersData(
        figureTitle: "symK computation time vs Player 2 value",
        plots: charts2,
        parameters: charts1.first!.parameters),
      fileNames: ["plotsP2ValXtime.png"])
    
  }
}
