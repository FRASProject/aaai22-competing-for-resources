//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/23/21.
//

import Foundation

//@available(*, deprecated)
//public enum ExperimentPDDLDO {
//  @available(*, deprecated)
//  public static func run(with config: [String: AIKitData]) throws {
//    
//    
//    var currentData = [String: AIKitTable<AIKitData>]()
//    var lastRegeneration = Date().addingTimeInterval(-60)
//    func updateDataAndRegenarate(name: String, table: AIKitTable<AIKitData>) throws {
//      currentData[name] = table
//      if -lastRegeneration.timeIntervalSinceNow > 60.0 {
//        try Self.generateTables(currentData)
//        lastRegeneration = Date()
//      }
//    }
//    
//    let initExperimentParameters = ExperimentParameters()
//    let games = config["games"]!.array!.map { $0.dictionary! }
//    let gameParameters = games.flatMap {
//      initExperimentParameters.merging(from: $0)
//    }
//    let gamesSerial = config["gamesSerial"]!.array!.map { $0.dictionary! }
//    let gameSerialParameters = gamesSerial.flatMap {
//      initExperimentParameters.merging(from: $0)
//    }
//    let gamesSerialSimAnn = config["gamesSerialSimAnn"]!.array!.map { $0.dictionary! }
//    let gameSerialParametersSimAnn = gamesSerialSimAnn.flatMap {
//      initExperimentParameters.merging(from: $0)
//    }
//    let gamesStats = AIKitTable(rows: try (Set(gameParameters).union(gameSerialParameters).union(gameSerialParametersSimAnn)).map {try GamesUtils.getGameStatistics($0)})
//    currentData["gameStats"] = gamesStats
//    
//    let doParameters = gameParameters.flatMap {
//      $0.merging(from: config["DoubleOracle"]!.dictionary!)
//    }
//    
//    let doExperiments = doParameters.map { PDDLDoubleOracleExperiment( parameters: $0) }
//    var failedExperiments = [(experiment: String, error: Error)]()
//    
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(doExperiments, callback: {
//      try! updateDataAndRegenarate(name: "DO", table: $0)
//    }))
//    
//    let serialDoParameters = gameSerialParameters.flatMap {
//      $0.merging(from: config["SerialDoubleOracle"]!.dictionary!)
//    }
//    let serialSimAnnDoParameters = gameSerialParametersSimAnn.flatMap {
//      $0.merging(from: config["SerialSimAnnDoubleOracle"]!.dictionary!)
//    }
//    let serialDoExperiments = serialDoParameters.map {
//      PDDLSerialDoubleOracleExperiment( parameters: $0, mode: .costDecreasing) }
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(serialDoExperiments, callback: {
//      try! updateDataAndRegenarate(name: "serialDO", table: $0)
//    }))
//
//    let serialSimAnnDoExperiments = serialSimAnnDoParameters.map {
//      PDDLSerialDoubleOracleExperiment( parameters: $0, mode: .simulatedAnnealing) }
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(serialSimAnnDoExperiments, callback: {
//      try! updateDataAndRegenarate(name: "serialSimAnnDO", table: $0)
//    }))
//    
//    let maxNumberOfUnitsForExploitability = config["Exploitability"]?["maxUnits"]?.integer
//    
//    let serialDOExploitability = serialDoExperiments.filter {
//      let numberOfUnits = gamesStats.select(
//        key: ["DOMAIN" : $0.parameters["DOMAIN"]!,
//              "GAME": $0.parameters["GAME"]!])[0, "p1Units"]?.integer
//      
//      if let maxUnits = maxNumberOfUnitsForExploitability,
//         let units = numberOfUnits,
//         units <= maxUnits {
//        return true
//      }
//      return false
//      
//    }.map {
//      PDDLStrategyExpoitabilityExperiment(
//        strategy: $0.resultData!,
//        strategyState: $0.resultData!.state,
//        parameters: $0.parameters.merging(
//          from: config["Exploitability"]!.dictionary!).first!)
//      
//    }
//    
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(serialDOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "serialDOExploitability", table: $0)
//    }))
//    
//    let lamaSerialDOExploitability = serialDOExploitability.filter {$0.isAllResultsFailedDontRecompute}.map {
//      explExperiment -> PDDLStrategyExpoitabilityExperiment in
//      let parameter = explExperiment.parameters.withLastDirPushedDown.merging(
//        from: config["LAMAExploitability"]!.dictionary!).first!
//      return PDDLStrategyExpoitabilityExperiment(
//        player1Strategy: explExperiment.player1Strategy, player2Strategy: explExperiment.player2Strategy, parameters: parameter)
//    }
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(lamaSerialDOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "serialDOLamaExploitability", table: $0)
//    }))
//    
//    let serialSimAnnDOExploitability = serialSimAnnDoExperiments.map {
//      PDDLStrategyExpoitabilityExperiment(
//        strategy: $0.resultData!,
//        strategyState: $0.resultData!.state,
//        parameters: $0.parameters.merging(
//        from: config["Exploitability"]!.dictionary!).first!)
//      
//
//    }
//    
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(serialSimAnnDOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "serialSimAnnDOExploitability", table: $0)
//    }))
//    
//    let lamaSerialSimAnnDOExploitability = serialSimAnnDOExploitability.filter {$0.isAllResultsFailedDontRecompute}.map {
//      explExperiment -> PDDLStrategyExpoitabilityExperiment in
//      let parameter = explExperiment.parameters.withLastDirPushedDown.merging(
//        from: config["LAMAExploitability"]!.dictionary!).first!
//      return PDDLStrategyExpoitabilityExperiment(
//        player1Strategy: explExperiment.player1Strategy, player2Strategy: explExperiment.player2Strategy, parameters: parameter)
//    }
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(lamaSerialSimAnnDOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "serialSimAnnDOLamaExploitability", table: $0)
//    }))
//    
//    
//    let dOExploitability = doExperiments.map {
//      PDDLStrategyExpoitabilityExperiment(
//        strategy: $0.resultData!,
//        strategyState: $0.resultData!.state,
//        parameters: $0.parameters.merging(
//          from: config["Exploitability"]!.dictionary!).first!)
//      
//    }
//    
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(dOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "DOExploitability", table: $0)
//    }))
//    
//    
//    
//    let lamaDOExploitability = dOExploitability.filter {$0.isAllResultsFailedDontRecompute}.map {
//      explExperiment -> PDDLStrategyExpoitabilityExperiment in
//      let parameter = explExperiment.parameters.withLastDirPushedDown.merging(
//        from: config["LAMAExploitability"]!.dictionary!).first!
//      return PDDLStrategyExpoitabilityExperiment(
//        player1Strategy: explExperiment.player1Strategy, player2Strategy: explExperiment.player2Strategy, parameters: parameter)
//    }
//    failedExperiments.append(contentsOf: try ExperimentUtils.runParallelStr(lamaDOExploitability, callback: {
//      try! updateDataAndRegenarate(name: "DOLamaExploitability", table: $0)
//    }))
//    
//        
//    try Self.generateTables(currentData)
//    
//    if !failedExperiments.isEmpty {
//      print("Failed experiments:")
//      print(failedExperiments.map {$0.experiment + " : " + $0.error.localizedDescription}.joined(separator: "\n"))
//    }
//    
//  }
//  
//  public static func generateTables(_ data: [String: AIKitTable<AIKitData>]) throws {
//    let lamaRenaming = [
//      "player1BaseBestValP1": "LAMA_player1BaseBestValP1",
//      "player2BaseBestValP1": "LAMA_player2BaseBestValP1",
//      "player2BaseBestValP2": "LAMA_player2BaseBestValP2",
//      "player1BaseBestValP2": "LAMA_player1BaseBestValP2",
//      "p1Exploitability": "LAMA_p1Exploitability",
//      "p2Exploitability": "LAMA_p2Exploitability"
//    ]
//    
//    let lamaExplTable = data["DOLamaExploitability"]?.renamingColumns(newNames: lamaRenaming)
//    
//    let lamaExplSerialExplTable = data["serialDOLamaExploitability"]?.renamingColumns(newNames: lamaRenaming)
//    
//    let lamaExplSimAnnExplTable = data["serialSimAnnDOLamaExploitability"]?.renamingColumns(newNames: lamaRenaming)
//    
//    let serialSimAnnDOExplTable = data["serialSimAnnDOExploitability"]
//    
//    let serialExplTable = data["serialDOExploitability"]
//    
//    let explTable = data["DOExploitability"]
//    let serialDoDataTable = data["serialDO"]
//    let serialSimAnnDoDataTable = data["serialSimAnnDO"]
//    let doDataTable = data["DO"]
//    let gamesStats = data["gameStats"]!
//    
//    let finalTable = doDataTable?.joiningMaxLeft(
//      with: explTable).joiningMaxLeft(with: lamaExplTable).appendingBottom(
//        serialDoDataTable?.joiningMaxLeft(
//          with: serialExplTable).joiningMaxLeft(
//            with: lamaExplSerialExplTable)).appendingBottom(
//              serialSimAnnDoDataTable?.joiningMaxLeft(
//                with: serialSimAnnDOExplTable).joiningMaxLeft(
//                  with: lamaExplSimAnnExplTable))
//      .joiningMaxLeft(with: gamesStats)
//    guard let finalTable = finalTable else { return }
//    let finalTable2 = finalTable.mapCells(
//      cols: ["player1EquilibriumValue", "p1Exploitability", "LAMA_p1Exploitability"]) {
//        if let maxValue = $0.0[$0.1, "p1MaxGameValue"]?.double {
//          return $0.3?.double.map { AIKitData($0 / maxValue) }
//        } else {
//          return nil
//        }
//      }
//    
//    let runningTimeTable1 = finalTable2.keepingColumns([
//      "DOMAIN", "ALG", "heur", "AdvSyncR", "p1Units", "totalTime", "iterations"])
//    
//    for (key, table) in runningTimeTable1.grouping(by: [
//      "DOMAIN",
//      "ALG",
//      "heur",
//      "AdvSyncR"
//    ]).subtables()
//    {
//      let tableFileName = key.compactMap {$0.1?.string}.joined(separator: "_")
//      let table2 = table.grouping(by: ["p1Units"]).avg().convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
//      try table2.toText(
//        type: .screen,
//        cols: ["p1Units", "totalTime", "iterations"],
//        colsPadding: true
//      ).write(
//        toFile: tableFileName + ".txt",
//        atomically: false,
//        encoding: .utf8)
//    }
//    
//    let finalTable3 = finalTable2.keepingColumns([
//      "DOMAIN", "ProblemNo", "ALG", "heur", "AdvSyncR", "p1Exploitability"])
//    let finalTable4 = finalTable3.grouping(by: ["DOMAIN", "ALG", "heur", "AdvSyncR"]).avg()
//    
//    let stringTable2 = finalTable4.sorted(by: ["DOMAIN", "ALG", "heur", "AdvSyncR"]).convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
//    
//    
//    let stringTable = finalTable2.sorted(by: ["DOMAIN","GAME","totalTime"]).convertingCellsToString(format: nil, defaultFormat: 2, valuesInQuotationMarks: false)
//    
//    
//    
//    
//    try stringTable.toText(type: .csv).write(
//      toFile: "aggregateStatistics.csv",
//      atomically: false,
//      encoding: .utf8)
//    
//    let tableColumns = [
//      "DOMAIN",
//      //"GAME",
//      "ProblemNo",
//      "ALG",
//      "heur",
//      "AdvSyncR",
//      "p1Exploitability",
//      "p1MaxGameValue",
//      "temperature",
//      "step",
//      "p1Units",
//      "p1Goals",
//      "player1EquilibriumValue",
//      "LAMA_p1Exploitability",
//      "totalTime",
//      "iterations"
//    ]
//    
//    let tableColumns2 = [
//      "DOMAIN",
//      "ALG",
//      "heur",
//      "AdvSyncR",
//      "p1Exploitability",
//    ]
//    
//    try stringTable.toText(
//      type: .csv,
//      cols: tableColumns).write(
//        toFile: "aggregateStatisticsShort.csv",
//        atomically: false,
//        encoding: .utf8)
//    try stringTable.toText(
//      type: .tex,
//      cols: tableColumns).write(
//        toFile: "aggregateStatisticsShort.tex",
//        atomically: false,
//        encoding: .utf8)
//    let textTable = try stringTable.toText(
//      type: .screen,
//      cols: tableColumns,
//      colsPadding: true)
//    try textTable.write(
//      toFile: "aggregateStatisticsShortTable.txt",
//      atomically: false,
//      encoding: .utf8)
//    print(textTable)
//    
//    let textTable2 = try stringTable2.toText(
//      type: .screen,
//      cols: tableColumns2,
//      colsPadding: true)
//    
//    try textTable2.write(
//      toFile: "aggregateStatisticsShortTable2.txt",
//      atomically: false,
//      encoding: .utf8)
//    print(textTable2)
//    
//    
//  }
//}
