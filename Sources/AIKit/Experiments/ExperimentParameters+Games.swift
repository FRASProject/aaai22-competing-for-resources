//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/10/20.
//

import Foundation

extension ExperimentParameters {
  public var domainProblem: String? {
    guard let domain = data["DOMAIN"]?.string,
          let problem = data["GAME"]?.string else {
      return nil
    }
    return domain + problem
  }
}
