//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/5/22.
//

import Foundation

public enum PDDLTemporalDoubleOracleConfigurationFactory {
  public static func makeBestResponseTemporalPlanners(
    config: AIKitData
  ) throws -> (p1BR: PDDLTemporalPlanner, p2BR: PDDLTemporalPlanner)
  {
    guard let p1BestResponsePlannerConfig = config["p1BestResponsePlanner"],
          let p2BestResponsePlannerConfig = config["p2BestResponsePlanner"]
    else {
      fatalError("Invalid configuration file!")
    }
    return (
      p1BR: try PDDLPlannerFactory.makeTemporal(
        by: p1BestResponsePlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig),
      p2BR: try PDDLPlannerFactory.makeTemporal(
        by: p2BestResponsePlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig))
  }
  public static func makeTemporalPlanners(
    config: AIKitData
  ) throws -> (p1Init: PDDLTemporalPlanner, p2Init: PDDLTemporalPlanner, p1BR: PDDLTemporalPlanner, p2BR: PDDLTemporalPlanner)
  {
    guard let p1InitPlannerConfig = config["p1InitPlanner"],
          let p2InitPlannerConfig = config["p2InitPlanner"]
    else {
      fatalError("Invalid configuration file!")
    }
    let (p1BR, p2BR) = try Self.makeBestResponseTemporalPlanners(config: config)
    return (
      p1Init: try PDDLPlannerFactory.makeTemporal(
        by: p1InitPlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig),
      p2Init: try PDDLPlannerFactory.makeTemporal(
        by: p2InitPlannerConfig,
        frasConfig: ExperimentManager.defaultManager.systemConfig),
      p1BR: p1BR,
      p2BR: p2BR)
  }
}
