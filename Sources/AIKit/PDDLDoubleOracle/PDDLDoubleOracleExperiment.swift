//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/23/21.
//

import Foundation

public class PDDLDoubleOracleExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  //DoubleOracleResults<TemporalPlan, TemporalPlan>
  public var parameters: ExperimentParameters
  public var resultData: ResultData?
  public var fromCache: Bool?
  public var failError: Error?
  
  public required init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  
  public private(set) var saveData: Bool = true
  
  public var description: String {
    "PDDLDoubleOracle " + parameters.pathDir
  }
  
  private func checkSkippingCondition(game: PDDLGameFDRRepresentationAnalyticsLogic) -> Bool {
    let heuristics = parameters["heuristics"]?.string ?? "none"
    let numberOfUnits = game.game.numberOfUnits.max()!
    if let maxUnits = parameters["maxUnitsNoneHeuristics"]?.integer,
       heuristics == "none",
       numberOfUnits > maxUnits
    {
      return true
    }
    if let maxUnits = parameters["maxUnitsPruningHeuristics"]?.integer,
       heuristics == "pruning",
       numberOfUnits > maxUnits
    {
      return true
    }
    return false
  }
  
  public func runBody() throws
  {
    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
    let result: DoubleOracleResults<TemporalPlan, TemporalPlan>
    switch parameters["ALG"]?.string {
    case "DO":
      if checkSkippingCondition(game: game) { return }
      let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.make(
        for: game,
           config: parameters.data)
      result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: saveData ? parameters.pathFileName : nil,
        plansCachePath: nil, saveResult: false)
    case "DOADP":
      let doubleOracleConfiguration = try DistributedAdversarialDOConfigurationFactory.makeDistributedAdversarial(
        for: game,
           config: parameters.data)
      result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: saveData ? parameters.pathFileName : nil,
        plansCachePath: nil, saveResult: false)
    case "DOSERIAL":
      let doubleOracleConfiguration = try PDDLDoubleOracleConfigurationFactory.makeSerial(
        for: game,
           config: parameters.data)
      result = try DoubleOracleUtils.runDoubleOracle(
        doubleOracleConfiguration: doubleOracleConfiguration,
        outputDirName: saveData ? parameters.pathFileName : nil,
        plansCachePath: nil, saveResult: false)
    case "DOPRUNING":
      result = try DoublePruningDoubleOracleExperiment.run(
        game: game,
        config: parameters.data,
        outputDirName: saveData ? parameters.pathFileName : nil)
    default:
      fatalError("Invalid configurations")
    }
    save(result: Result(doResult: result))
    generateDOGraph()
  }
  
  public var fileNamePrefix: String {
    "AllResults"
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var recompute: Bool {
    false
  }
  
  private func generateDOGraph() {
      if let resultData = resultData?.doResult {
      try? ChartUtils.makeFigure(
        chart: ChartUtils.FigureParametersData(
          figureTitle: "Double oracle",
          plots: [
            resultData.progressValueChart,
            resultData.oraclesComputationTimeChart,
            resultData.strategySupportChart
          ],
          parameters: resultData.progressValueChart.parameters),
        fileNames: [experimentDirectory + "/doCharts.png"])
    }
  }
  public struct Result: Codable, ExperimentDataResultDictionary {
    public let doResult: DoubleOracleResults<TemporalPlan, TemporalPlan>
    public init(doResult: DoubleOracleResults<TemporalPlan, TemporalPlan>) {
      self.doResult = doResult
    }
    public func encode(to encoder: Encoder) throws {
      try doResult.encode(to: encoder)
    }
    public init(from decoder: Decoder) throws {
      doResult = try DoubleOracleResults<TemporalPlan, TemporalPlan>(from: decoder)
    }
    public var asDictionary: [String : AIKitData] {
      let d = doResult.asDictionary
      return d.merging(
        [
          "groundingTime": AIKitData(doResult.groundingTime),
          "simAnnealTime": AIKitData(doResult.simmulatedAnnealingTime)
        ],
        uniquingKeysWith: { a, _ in a })
    }
    public var player1Plans: [TemporalPlan] { doResult.player1Plans }
    public var player2Plans: [TemporalPlan] { doResult.player2Plans }
    public var totalTime: TimeInterval { doResult.totalTime }
    public var player1Strategy: MixedStrategy<TemporalPlan> {doResult.player1Strategy}
    public var player2Strategy: MixedStrategy<TemporalPlan> {doResult.player2Strategy}
    public var player1EquilibriumValue: Double {doResult.player1EquilibriumValue}
    public var player2EquilibriumValue: Double {doResult.player2EquilibriumValue}
    public var player1Equilibrium: [Double] {doResult.player1Equilibrium}
    public var player2Equilibrium: [Double] {doResult.player2Equilibrium}
    public var player1EquilibriumSupportSize: Int {doResult.player1EquilibriumSupportSize}
    public var player2EquilibriumSupportSize: Int {doResult.player2EquilibriumSupportSize}
  }
}
