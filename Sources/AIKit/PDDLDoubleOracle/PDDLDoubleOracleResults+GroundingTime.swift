//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/8/22.
//

import Foundation

extension DoubleOracleResults where PSP1 == TemporalPlan, PSP2 == TemporalPlan {
  public var groundingTime: Double? {
    if let a = statistics.first?.plannerLog.array {
      let d1 = a[0].dictionary
      let d2 = a[1].dictionary
      if let g1Duration = d1?["groundingDuration"]?.double,
         let g2Duration = d2?["groundingDuration"]?.double
      {
        return g1Duration + g2Duration
      }
    }
    return nil
  }
  public var planningTime: Double? {
    if let a = statistics.first?.plannerLog.array {
      let d1 = a[0].dictionary
      let d2 = a[1].dictionary
      if let g1Duration = d1?["planningDuration"]?.double,
         let g2Duration = d2?["planningDuration"]?.double
      {
        return g1Duration + g2Duration
      }
    }
    return nil
  }
  public var simmulatedAnnealingTime: Double? {
    let simAnnDurations = statistics.compactMap {
      $0.plannerLog.dictionary?["simAnnDuration"]?.double
    }
    if simAnnDurations.isEmpty {
      return nil
    } else {
      return simAnnDurations.sum
    }
  }
}
