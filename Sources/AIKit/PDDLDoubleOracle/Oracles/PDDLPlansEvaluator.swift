//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/23/21.
//

import Foundation

public struct PDDLPlansEvaluator: PureStrategyEvaluator {
  public typealias PSP1 = TemporalPlan
  public typealias PSP2 = TemporalPlan
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let totalWeight: Double
  
  public init(game: PDDLGameFDRRepresentationAnalyticsLogic) {
    self.game = game
    self.totalWeight = Double(game.totalGoalWeights(of: 0))
    // Zero sum games only
    precondition(game.totalGoalWeights(of: 0) == game.totalGoalWeights(of: 1))
  }
  
  public func evaluate(stratP1: PSP1, stratP2: PSP2) -> (Double, Double) {
    var simulation = PlanSimulation(game: game)
    simulation.simulate(plans: [stratP1, stratP2])
    let player1Value = simulation.currentValues[0] ?? 0.0
    let player2Value = simulation.currentValues[1] ?? 0.0

    let residuum = totalWeight - player1Value - player2Value
    
    return (
      player1Value + (residuum / 2),
      player2Value + (residuum / 2))
  }
}
