//
//  File.swift
//  
//
//  Created by Pavel Rytir on 3/11/21.
//

import Foundation

public class PDDLDoublePruningOracle: InitOracle, BestResponseOracle {
  public typealias PS = TemporalPlan
  public let game: PDDLGameFDRRepresentationAnalyticsLogic
  let pddlPlanner: PDDLPlanner
  private var player1SureSet: Set<Int>
  private var player2SureSet: Set<Int>
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    pddlPlanner: PDDLPlanner)
  {
    self.game = game
    self.pddlPlanner = pddlPlanner
    self.player1SureSet = []
    self.player2SureSet = []
  }
  
  public func findPlans(for player: Int, maxNumberOfPlans: Int, plansCacheDir: URL?) throws -> (
    plans: [TemporalPlan],
    log: AIKitData,
    computationDuration: [TimeInterval])
  {
    assert(player == 1 || player == 2)
    let player = player - 1
    let opponent = (player + 1) % 2
    
    if player == 0 {
      precondition(player1SureSet.isEmpty)
    } else {
      precondition(player2SureSet.isEmpty)
    }
    
    let actionTimesPlayer = game.groundedPlayersProblems[player].findEarliestActionTimes()
    let actionTimesOpponent = game.groundedPlayersProblems[opponent]
      .findEarliestActionTimes()
    
    var possibleGoals = Set<Int>()
    var deadlines = [PDDLNameLiteral: Int]()
    
    for (goalNumber, goal) in game.softGoalsCriticalActions[player].enumerated() {
      for cluster in goal {
        let criticalActionTime = actionTimesPlayer[
          game.groundedPlayersProblems[player].operatorIndex[cluster.criticalAction]!]!
        let advarsarialActionsMinTime = cluster.adversarialActions.map {
          actionTimesOpponent[
            game.groundedPlayersProblems[opponent].operatorIndex[$0]!]!
        }.min()!
        if criticalActionTime <= advarsarialActionsMinTime {
          possibleGoals.insert(goalNumber)
          precondition(deadlines[cluster.criticalFact] == nil ||
                        deadlines[cluster.criticalFact] == advarsarialActionsMinTime)
          deadlines[cluster.criticalFact] = advarsarialActionsMinTime
        }
      }
    }
    let weightedFactsCostFunction = Dictionary(
      uniqueKeysWithValues: deadlines.map { (key, value) -> (PDDLNameLiteral, [Int]) in
      let weight = game.getWeight(of: key, for: player)
      return (key, [Int](repeating: 0, count: value) + [weight/2, weight])
      }).merging(game.criticalFacts.map {($0, [0])}, uniquingKeysWith: { a, _ in a })
    
    if possibleGoals.isEmpty {
      return ([TemporalPlan()], "No goals, computation skipped", [0.0])
    }
    
    let problem = game.game.getPlanningProblem(for: player)
      .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      .keepingOnly(player: player)
      .adding(
        criticalFactCostFunction: weightedFactsCostFunction,
        criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
      .keepingOnlyGoals(Array(possibleGoals).sorted())
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingTotalCostFunction()
      .addingMinimizeTotalCostMetric()
    
    let planCache = plansCacheDir.map {$0.appendingPathComponent("initPlan1Player\(player).json")}
    let (plannerOutput, _, log, _, duration) = try pddlPlanner.findFirst(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      planCache: planCache)
    
    if let plannerOutput = plannerOutput.last {
      let plan = TemporalPlan(
        classicalPlan: plannerOutput.parsed!,
        durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
        units: problem.problem.units,
        removingNoopActions: true)
      let sureSet = extractSureSet(
        from: plan,
        player: player,
        opponent: opponent,
        actionTimesOpponent: actionTimesOpponent)
      if player == 0 {
        self.player1SureSet = sureSet
      } else {
        self.player2SureSet = sureSet
      }

      return ([plan], .string(log), duration)
    } else {
      throw AIKitError(message: "Cannot find init plan for player \(player + 1)")
    }
  }
  
  public var shortDescription: String {
    "DoublePruning"
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<TemporalPlan>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [TemporalPlan],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    assert(player == 1 || player == 2)
    let player = player - 1
    let opponent = (player + 1) % 2
    
    let normalCostFunction = PDDLCriticalFactCostUtils.extractCostFunction(
      from: opponentStrat,
      units: Set(game.playersUnits[opponent]),
      actionCriticalFacts: game.actionWithoutUnitsCriticalFacts).merging(
        game.criticalFacts.map {($0, [0])}, uniquingKeysWith: { a, _ in a })
    var weightedFactsCostFunction = PDDLCriticalFactCostUtils.scaleCostFunction(
      normalCostFunction,
      weights: Dictionary(
        uniqueKeysWithValues: normalCostFunction.map {
          ($0.key, game.getWeight(of: $0.key, for: player))}))
    
    let actionTimesOpponent = game.groundedPlayersProblems[opponent].findEarliestActionTimes()
    
    let sureSet = player == 0 ? player1SureSet : player2SureSet
    
    for sureGoal in sureSet {
      let cluster = game.softGoalsCriticalActions[player][sureGoal]
      var currentCriticalFact: PDDLNameLiteral? = nil
      var criticalFactMinTimes = [Int]()
      for (_ ,criticalFact, adversarialActions) in cluster {
        precondition(currentCriticalFact == nil || currentCriticalFact == criticalFact)
        currentCriticalFact = criticalFact
        let advarsarialActionsMinTime = adversarialActions.map {
          actionTimesOpponent[
            game.groundedPlayersProblems[opponent].operatorIndex[$0]!]!
        }.min()!
        criticalFactMinTimes.append(advarsarialActionsMinTime)
      }
      let weight = game.getWeight(of: currentCriticalFact!, for: player)
      weightedFactsCostFunction[currentCriticalFact!] = [Int](
        repeating: 0, count: criticalFactMinTimes.min()!) + [weight]
    }
    
    let reachableGoals = SoftGoalsHeuristics.determineReachableGoalsByPruningHeuristics(
      from: opponentStrat,
      game: game,
      player: player)
    
    let unreachableGoalsTotalCost = game.goalsWeights(of: player).enumerated().filter {
      !reachableGoals.contains($0.offset) }.map {$0.element}.sum
    
    if reachableGoals.isEmpty {
      return (
        [TemporalPlan()],
        [Double(unreachableGoalsTotalCost)],
        "No goals, computation skipped",
        0.0)
    }
    
    let problem = game.game.getPlanningProblem(for: player)
      .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      .keepingOnly(player: player)
      .adding(
        criticalFactCostFunction: weightedFactsCostFunction,
        criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
      .keepingOnlyGoals(reachableGoals)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingTotalCostFunction()
      .addingMinimizeTotalCostMetric()
    let (plannerOutput, cost, log, _, duration) = try pddlPlanner.findFirst(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      planCache: plansCacheFile)
    
    let plan = TemporalPlan(
      classicalPlan: plannerOutput.last!.parsed!,
      durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
      units: problem.problem.units,
      removingNoopActions: true)
    
//    let newSureSet = extractSureSet(
//      from: plan,
//      player: player,
//      opponent: opponent,
//      actionTimesOpponent: actionTimesOpponent)
//    if player == 0 {
//      self.player1SureSet = newSureSet
//    } else {
//      self.player2SureSet = newSureSet
//    }
    //
    return ([plan], cost.last == nil ? [] : [cost.last! + Double(unreachableGoalsTotalCost)], .string(log), duration.last ?? 0)
  }
  
  /// Determines goals that are satisfied before the minimal time of adversary actions.
  /// - Parameters:
  ///   - plan: Plan
  ///   - player: player
  ///   - opponent: oppoent
  ///   - actionTimesOpponent: Minimal actions time of the opponent
  /// - Returns: Goals numbers
  private func extractSureSet(
    from plan: TemporalPlan,
    player: Int,
    opponent: Int,
    actionTimesOpponent: [Int: Int]
    ) -> Set<Int>
  {
    var sureSet = Set<Int>()
    let unitsParameters = Set(game.playersUnits[player])
    let planDeadlines = plan.extractTimes(
      of: Set(game.actionWithoutUnitsCriticalFacts.keys),
      unitsParameters: unitsParameters)
    for (goalNumber, goal) in game.softGoalsCriticalActions[player].enumerated() {
      for cluster in goal {
        let criticalActionTime = planDeadlines[
          cluster.criticalAction.removing(parameters: Set(game.playersUnits[player]))] ?? Int.max
        let advarsarialActionsMinTime = cluster.adversarialActions.map {
          actionTimesOpponent[
            game.groundedPlayersProblems[opponent].operatorIndex[$0]!]!
        }.min()!
        if criticalActionTime < advarsarialActionsMinTime {
          sureSet.insert(goalNumber)
        }
      }
    }
    return sureSet
  }
}
