//
//  PDDLInitOracle.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

public class PDDLInitOracle: InitOracle {
  public enum Heuristics {
    case none
    case pruning
    case ordering
    case orderingSimAnneal
    public init(_ s: String) {
      switch s {
      case "none":
        self = .none
      case "pruning":
        self = .pruning
      case "ordering":
        self = .ordering
      case "orderingSimAnneal":
        self = .orderingSimAnneal
      default:
        fatalError("Invalid BR heuristics")
      }
    }
  }
  public typealias PS = TemporalPlan
  public let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let pddlPlanner: PDDLPlanner
  let heuristics: Heuristics
  
  /// Short text description of the planner.
  public var shortDescription: String {
    "PDDLInitOracle"
  }
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    pddlPlanner: PDDLPlanner,
    heuristics: Heuristics)
  {
    self.game = game
    self.pddlPlanner = pddlPlanner
    self.heuristics = heuristics
  }
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?
  ) throws -> (
    plans: [PS],
    log: AIKitData,
    computationDuration: [TimeInterval])
  {
    assert(player == 1 || player == 2)
    let problem = game.game.getPlanningProblem(
      for: player - 1)
      .keepingOnly(player: player - 1)
      .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingDummyCostFunction()
    let planCache = plansCacheDir.map {$0.appendingPathComponent("initPlan1Player\(player).json")}
    let (plannerOutput, _, log, _, duration) = try pddlPlanner.findFirst(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      planCache: planCache)
    let groundingDuration = game.groundingComputationDuration / Double(game.players.count)
    if let plannerOutput = plannerOutput.last {
      let plan = TemporalPlan(
        classicalPlan: plannerOutput.parsed!,
        durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
        units: problem.problem.units,
        removingNoopActions: true)
      
      return (
        [plan],
        .dictionary([
          "log": .string(log),
          "planningDuration": .double(duration.sum),
          "groundingDuration": .double(groundingDuration)]),
        duration.map {$0 + groundingDuration })
    } else {
      throw AIKitError(message: "Cannot find init plan for player \(player)")
    }
  }
}

extension TemporalPlan: PureStrategy {}
