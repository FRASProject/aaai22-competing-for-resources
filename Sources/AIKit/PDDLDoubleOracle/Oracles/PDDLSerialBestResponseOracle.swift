//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/31/21.
//

import Foundation

public class PDDLSerialBestResponseOracle: BestResponseOracle {
  public typealias PS = TemporalPlan
  public let game: PDDLGameFDRRepresentationAnalyticsLogic
  let mode: IterativeBestResponse.Mode
  private let pddlPlanner: PDDLPlanner
  private let pddlGrounder: FastDownwardGrounder
  //private let actionInstances: [[[String: Set<PlanAction>]]]
  //private let planningProblemPlayer1: PDDLPlanningProblem
  //private let planningProblemPlayer2: PDDLPlanningProblem
  private let config: AIKitData
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    mode: IterativeBestResponse.Mode,
    config: AIKitData,
    pddlPlanner: PDDLPlanner) throws
  {
    self.game = game
    self.config = config
    self.mode = mode
    self.pddlPlanner = pddlPlanner
//    self.planningProblemPlayer1 = game.game.getPlanningProblem(for: 0)
//      .keepingOnly(player: 0)
//      .convertingSoftGoalsToHardGoalsRemovingMetrics()
//    self.planningProblemPlayer2 = game.game.getPlanningProblem(for: 1)
//      .keepingOnly(player: 1)
//      .convertingSoftGoalsToHardGoalsRemovingMetrics()
    self.pddlGrounder = FastDownwardGrounder(temporal: true)
//    self.actionInstances = try [
//      planningProblemPlayer1.groundUnitSubproblems(grounder: pddlGrounder),
//      planningProblemPlayer2.groundUnitSubproblems(grounder: pddlGrounder)
//    ]
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<PS>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [PS],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    assert(player == 1 || player == 2)
    assert(opponent == 1 || opponent == 2)
    assert(player != opponent)
    let player = player - 1
    let opponent = opponent - 1
    
    let planCacheDir = plansCacheFile.map {$0.deletingLastPathComponent()}
    let cacheFilePrefix = plansCacheFile.map {$0.lastPathComponent} ?? ""
    let planCache = planCacheDir.map {$0.appendingPathComponent(cacheFilePrefix + "jointPlan.sol")}
    //    let playerObject = game.players[player]
    //    let problem = game.game.getPlanningProblem(for: player)
    //      .keepingOnly(player: player)
    //      .convertingSoftGoalsToHardGoalsRemovingMetrics()
    
    let normalFactsCostFunction = PDDLCriticalFactCostUtils.extractCostFunction(
      from: opponentStrat,
      units: Set(game.playersUnits[opponent]),
      actionCriticalFacts: game.serialActionWithoutUnitsCriticalFacts)
    let weightedFactsCostFunction = PDDLCriticalFactCostUtils.scaleCostFunction(
      normalFactsCostFunction,
      weights: Dictionary(
        uniqueKeysWithValues: normalFactsCostFunction.map {
          ($0.key, game.serialGetWeight(of: $0.key, for: player))}))
    
    let factsWithoutCost = game.criticalFacts.subtracting(weightedFactsCostFunction.keys)
    let maxDeadlineTimestamp = weightedFactsCostFunction.values.map {$0.count}.max() ?? 1
    let costFunction = weightedFactsCostFunction.merging(
      factsWithoutCost.map {($0, [Int](repeating: 0, count: maxDeadlineTimestamp))},
      uniquingKeysWith: {a, _ in a})
    
    let cooperativeGame = try IterativeBestResponse(
      game: game,
      mode: mode,
      config: config,
      player: player,
      planCacheDir: planCacheDir,
      cacheFilePrefix: cacheFilePrefix,
      planner: pddlPlanner,
      costFunction: costFunction,
      verbose: true)
    
    try cooperativeGame.run()
    let plan = cooperativeGame.currentJointPlan
    let log = cooperativeGame.currentPlansLog
    let duration = cooperativeGame.totalDuration!
    if let planCache = planCache {
      try plan.text.write(to: planCache, atomically: false, encoding: .utf8)
    }
    
    let cost = cooperativeGame.currentJointPlanCost
    return ([plan], [Double(cost)], log, duration)
  }
  
  public var shortDescription: String {
    "PDDLBestResponseOracle"
  }
}
