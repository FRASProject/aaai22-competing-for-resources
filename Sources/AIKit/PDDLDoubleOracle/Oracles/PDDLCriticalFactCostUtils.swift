//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/31/21.
//

import Foundation
import MathLib

public enum PDDLCriticalFactCostUtils {
  public static let maxCost = 100.0
  
  public static func merge(
    f1: [(
      action: ActionNameProjectedParameters,
      cost: [Int])],
    f2: [(
      action: ActionNameProjectedParameters,
      cost: [Int])]) ->
  [(
    action: ActionNameProjectedParameters,
    cost: [Int])]
  {
    var intersection = [(
                          action: ActionNameProjectedParameters,
                          cost: [Int])]()
    var intersectionNameSet = Set<Tuple<String, [String]>>()
    for (action1,  costFunction1) in f1 {
      for (action2, costFunction2) in f2 {
        if action1.name == action2.name && action1.parametersInstance == action2.parametersInstance {
          intersectionNameSet.insert(Tuple(action1.name, action1.parametersInstance))
          precondition(action1.parametersIndices == action2.parametersIndices, "Invalid data")
          let lastTimestamp = max(costFunction1.count, costFunction2.count) - 1
          let ef1 = Self.extend(costFunction: costFunction1, to: lastTimestamp)
          let ef2 = Self.extend(costFunction: costFunction2, to: lastTimestamp)
          intersection.append((action: action1,
                                cost: zip(ef1, ef2).map {$0.0 + $0.1} ))
        }
      }
    }
    let result = f1.filter {
      !intersectionNameSet.contains(Tuple($0.action.name, $0.action.parametersInstance))} +
      f2.filter {
        !intersectionNameSet.contains(Tuple($0.action.name, $0.action.parametersInstance))} + intersection
    return result
  }
  
  public static func extend(costFunction: [Int], to lastTimestamp: Int) -> [Int] {
    let lastCostValue = costFunction.last ?? 0
    let extendedCostFunction = costFunction + [Int].init(
      repeating: lastCostValue,
      count: lastTimestamp - (costFunction.count - 1))
    return extendedCostFunction
  }
  
  @available(*, deprecated, message: "Use cost function in Temporal plan struct.")
  public static func computeCost(
    of plan: TemporalPlan,
    player: Int,
    costFunction: [PDDLNameLiteral : [Int]],
    factMaxCost: Int,
    game: PDDLGameFDRRepresentationAnalyticsLogic) -> Int
  {
    let planDeadlines = game.extractDeadlines(from: plan, player: player)
    let cost = planDeadlines.map {
      let factCostFunction = costFunction[$0.key]!
      if $0.value >= factCostFunction.count {
        return factMaxCost
      } else {
        return factCostFunction[$0.value]
      }
    }.reduce(0, +)
    return cost
  }
  
  public static func scaleCostFunction(
    _ costFunction: [PDDLNameLiteral : [Int]],
    weights: [PDDLNameLiteral: Int]) -> [PDDLNameLiteral : [Int]]
  {
    let weightedFactsCostFunction = Dictionary(
      uniqueKeysWithValues: costFunction.map { (key, value) in
      (key, value.map { ($0 * weights[key]!) / 100 })
    })
    return weightedFactsCostFunction
  }
  
  public static func printCostFunction(_ costFunction: [PDDLNameLiteral : [Int]]) {
    let output = costFunction.sorted(by: { $0.key < $1.key}).map {
      ($0.key.text,
       $0.value.map {String($0).padding(toLength: 3, withPad: " ", startingAt: 0)}.joined(separator: "_"))
    }
    if let factWidth = output.map({ $0.0.count }).max(),
       let maxTime = costFunction.values.map({$0.count}).max()
    {
      print(String(repeating: " ", count: factWidth + 3) + (0...maxTime).map {
        String($0).padding(toLength: 3, withPad: " ", startingAt: 0)
      }.joined(separator: " "))
      output.forEach {
        print($0.0.padding(toLength: factWidth, withPad: " ", startingAt: 0) + " ::" + $0.1)
      }
    }
  }
  
  public static func extractCostFunction(
    from opponentStrat: MixedStrategy<TemporalPlan>,
    units: Set<String>,
    actionCriticalFacts: [PlanAction : [PDDLNameLiteral]]
    ) -> [PDDLNameLiteral : [Int]]
  {
    var factsDeadlines = [PDDLNameLiteral: [Int: Double]]()
    for (deadlines, probability) in zip(
      opponentStrat.strategies.map {
        $0.extractDeadlines(
          actionCriticalFacts: actionCriticalFacts,
          unitsParameters: units)},
      opponentStrat.distribution
    ) where probability > AIKitConfig.probabilityEpsilon
    {
      for (fact, time) in deadlines {
        var factDeadline = factsDeadlines[fact] ?? [:]
        factDeadline[time, default: 0.0] += probability
        factsDeadlines[fact] = factDeadline
      }
    }
    return convert(factsDeadlines: factsDeadlines)
  }
  public static func convert(
    factsDeadlines: [PDDLNameLiteral: [Int: Double]]) -> [PDDLNameLiteral : [Int]]
  {
    let factsCostFunction = factsDeadlines.mapValues {
      (deadlines: [Int: Double]) -> [Int] in
      let maxTime = deadlines.keys.max()! + 1
      var currentLevel = 0.0
      var costFunction = [Int]()
      for time in 0...maxTime {
        if let probability = deadlines[time] {
          let newLevel = currentLevel + probability
          let midLevel = (newLevel + currentLevel) / 2
          costFunction.append(Int((midLevel * Self.maxCost).rounded()))
          currentLevel = newLevel
        } else {
          costFunction.append(Int((currentLevel * Self.maxCost).rounded()))
        }
      }
      return costFunction
    }
    return factsCostFunction
  }
}
