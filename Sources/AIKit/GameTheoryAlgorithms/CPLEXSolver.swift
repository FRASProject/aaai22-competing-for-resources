//
//  cplexNormFormLP.swift
//  GText
//
//  Created by Pavel R on 06/04/2018.
//  Copyright © 2018 Pavel Rytir. All rights reserved.
//

import Foundation
import cplex
import MathLib

public enum CPLEXSolver {
  /// Find Nash equilibrium of row player of a normal form game using CPLEX solver.
  /// - Parameters:
  ///   - utilityMatrix: Utitlity matrix.
  ///   - verbose:
  /// - Returns: Probablistic distribution over rows (equilibrium) and value of the equilibrium of row player.
  static public func findEquilibriumOfNFGame(
    utilityMatrix: Matrix,
    verbose: Bool = false) throws -> (Double,[Double])
  {
    func check(status: Int32) throws {
      if status != 0 {
        throw CPLEXError(message: "cplex error")
      }
    }
    
    var status : Int32 = 0
    var env = CPXopenCPLEX (&status)
    try check(status: status)
    
    status = CPXsetintparam (env, CPXPARAM_ScreenOutput, verbose ? CPX_ON : CPX_OFF)
    try check(status: status)
    
    status = CPXsetintparam (env, CPXPARAM_Read_DataCheck,
                             CPX_DATACHECK_WARN)
    try check(status: status)
    
    var lp = CPXcreateprob (env, &status, "normFormLP")
    try check(status: status)
    
    
    status = CPXchgobjsen (env, lp, CPX_MAX);  /* Problem is maximization */
    try check(status: status)
    
    //let L = "L".utf8CString[0]
    let E = "E".utf8CString[0]
    let G = "G".utf8CString[0]
    var rhs = [Double](repeating: 0.0, count: utilityMatrix.columns + 1)
    var sense = [CChar](repeating: G, count: utilityMatrix.columns + 1)
    sense[utilityMatrix.columns] = E
    rhs[utilityMatrix.columns] = 1.0
    
    status = CPXnewrows (env, lp, Int32(utilityMatrix.columns + 1), rhs, sense, nil, nil);
    try check(status: status)
    
    var obj = [Double](repeating: 0.0, count: utilityMatrix.rows + 1)
    obj[utilityMatrix.rows] = 1.0
    
    let ub = [Double](repeating: CPX_INFBOUND, count: utilityMatrix.rows + 1)
    var lb = [Double](repeating: 0.0, count: utilityMatrix.rows + 1)
    lb[utilityMatrix.rows] = -CPX_INFBOUND
    
    status = CPXnewcols (env, lp, Int32(utilityMatrix.rows + 1), obj, lb, ub, nil, nil);
    try check(status: status)
    
    var rowList = [Int32]()
    var colList = [Int32]()
    var valList = [Double]()
    
    for col in 0..<utilityMatrix.columns {
      for row in 0..<utilityMatrix.rows{
        if utilityMatrix[row, col] != 0.0 {
          rowList.append(Int32(col)) // It's switched!
          colList.append(Int32(row))
          valList.append(utilityMatrix[row, col])
        }
      }
      rowList.append(Int32(col))
      colList.append(Int32(utilityMatrix.rows))
      valList.append(-1.0) // -V
    }
    
    for row in 0..<utilityMatrix.rows {
      rowList.append(Int32(utilityMatrix.columns))
      colList.append(Int32(row))
      valList.append(1.0)
    }
    
    
    status = CPXchgcoeflist (env, lp, Int32(valList.count), rowList, colList, valList);
    try check(status: status)
    
    status = CPXlpopt (env, lp);
    try check(status: status)
    
    let cur_numrows = CPXgetnumrows (env, lp);
    let cur_numcols = CPXgetnumcols (env, lp);
    
    var solstat : Int32 = 0
    var objval = 0.0
    
    var x = [Double](repeating: 0.0, count: Int(cur_numcols))
    var pi = [Double](repeating: 0.0, count: Int(cur_numrows))
    var slack = [Double](repeating: 0.0, count: Int(cur_numrows))
    var dj = [Double](repeating: 0.0, count: Int(cur_numcols))
    
    status = CPXsolution (env, lp, &solstat, &objval, &x, &pi, &slack, &dj);
    try check(status: status)
    
    //status = CPXwriteprob (env, lp, "/Users/pavel/lpex1.lp", nil);
    
    if lp != nil {
      status = CPXfreeprob(env, &lp)
      try check(status: status)
    }
    if env != nil {
      status = CPXcloseCPLEX(&env)
      try check(status: status)
    }
    
    x.removeLast()
    
    return (objval, x)
  }
  
  struct CPLEXError: Error {
    let message : String
  }
  
}
