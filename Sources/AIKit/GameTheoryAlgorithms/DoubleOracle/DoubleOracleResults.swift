//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/17/21.
//

import Foundation
import MathLib

public struct DoubleOracleResults<PSP1: PureStrategy, PSP2: PureStrategy> {
  public let statistics : [DoubleOracleStatItem]
  public let player1UtilityMatrix : Matrix
  public let player2UtilityMatrix : Matrix
  public let player1Plans: [PSP1]
  public let player2Plans: [PSP2]
  public let player1Equilibrium : [Double]
  public let player2Equilibrium : [Double]
  public let player1EquilibriumValue : Double
  public let player2EquilibriumValue : Double
  public let state: ExperimentDataItemState
}

extension DoubleOracleResults {
  public var totalTime: TimeInterval {
    statistics.map {$0.stepDuration}.sum
  }
  public var iterations: Int {
    statistics.count - 1
  }
  public var player1EquilibriumSupportSize: Int {
    player1Equilibrium.filter { $0 > AIKitConfig.probabilityEpsilon}.count
  }
  public var player2EquilibriumSupportSize: Int {
    player2Equilibrium.filter { $0 > AIKitConfig.probabilityEpsilon}.count
  }
  public var player1Strategy: MixedStrategy<PSP1> {
    MixedStrategy(player1Plans, distribution: player1Equilibrium)
  }
  public var player2Strategy: MixedStrategy<PSP2> {
    MixedStrategy(player2Plans, distribution: player2Equilibrium)
  }
}

extension DoubleOracleResults : ExperimentDataResultDictionary {
  public var asDictionary: [String : AIKitData] {
    [
      "player1EquilibriumValue": .double(player1EquilibriumValue),
      "player2EquilibriumValue": .double(player2EquilibriumValue),
      "totalTime": .double(totalTime),
      "iterations": .integer(statistics.count - 1),
      "player1EqSupport": .integer(player1EquilibriumSupportSize),
      "player2EqSupport": .integer(player2EquilibriumSupportSize)
    ]
  }
}

extension DoubleOracleResults: GameStrategyProfile {
  public typealias MSP1 = MixedStrategy<PSP1>
  public typealias MSP2 = MixedStrategy<PSP2>
  public var player1: MSP1 {
    return MSP1(self.player1Plans, distribution: self.player1Equilibrium)
  }
  public var player2: MSP2 {
    return MSP2(self.player2Plans, distribution: self.player2Equilibrium)
  }
}

extension DoubleOracleResults {
  public func getFinalResults(
    after iteration: Int) -> (
    player1Plans: [PSP1],
    player1Equilibrium: [Double],
    player1EquilibriumValue: Double,
    player2Plans: [PSP2],
    player2Equilibrium: [Double],
    player2EquilibriumValue: Double)
  {
    let iterationStatistics = statistics[iteration]
    let newPlayer1Plans = Array(player1Plans[..<iterationStatistics.player1Equilibrium.count])
    let newPlayer2Plans = Array(player2Plans[..<iterationStatistics.player2Equilibrium.count])
    assert(newPlayer1Plans.count == iterationStatistics.player1Equilibrium.count)
    assert(newPlayer2Plans.count == iterationStatistics.player2Equilibrium.count)
    return (
      player1Plans: newPlayer1Plans,
      player1Equilibrium: iterationStatistics.player1Equilibrium,
      player1EquilibriumValue: iterationStatistics.player1EqVal,
      player2Plans: newPlayer2Plans,
      player2Equilibrium: iterationStatistics.player2Equilibrium,
      player2EquilibriumValue: iterationStatistics.player2EqVal)
  }
}

extension DoubleOracleResults: ExperimentData {}
