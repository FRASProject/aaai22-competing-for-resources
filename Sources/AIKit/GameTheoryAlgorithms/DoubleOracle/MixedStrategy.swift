//
//  DoubleOracleMixedStrategy.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

public struct MixedStrategy<PS: PureStrategy>: MixedStrategyProtocol, Codable {
  public var strategies: [PS]
  public var distribution: [Double]
  public init (_ strategies: [PS], distribution: [Double]) {
    assert(strategies.count == distribution.count)
    self.strategies = strategies
    self.distribution = distribution
  }
  public init<MSP: MixedStrategyProtocol>(_ strategy: MSP) where MSP.PS == PS {
    self.distribution = strategy.distribution
    self.strategies = strategy.strategies
  }
}


public protocol MixedStrategyProtocol {
  associatedtype PS: PureStrategy
  var strategies: [PS] { get }
  var distribution: [Double] { get }
}

public protocol GameStrategyProfile {
  associatedtype MSP1: MixedStrategyProtocol
  associatedtype MSP2: MixedStrategyProtocol
  var player1: MSP1 { get }
  var player2: MSP2 { get }
}

public struct GameStrategyProfileStruct<MSP: MixedStrategyProtocol>: GameStrategyProfile {
  public let player1: MSP
  public let player2: MSP
  public init(player1: MSP, player2: MSP) {
    self.player1 = player1
    self.player2 = player2
  }
}

