//
//  BestResponseOracle.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/2/20.
//

import Foundation

/// Any type implementing this protocol can be used as a best reposne planner in Double Oracle.
public protocol BestResponseOracle {
  associatedtype PS: PureStrategy
  associatedtype OPPONENTPS: PureStrategy
  //associatedtype MSP:MixedStrategyProtocol
  /// Finds a best response plan.
  /// - Parameters:
  ///   - player: Player for which the palnner finds a plan.
  ///   - opponent: Opponent.
  ///   - opponentStrat: Opponent's strategy.
  ///   - utilityLowerBound: TODO: remove this?
  ///   - plansCacheFile: Cache file. TODO: Replace this by iteration number?
  ///   - continueFunction: TODO: remove this? - solution checker is used instead.
  ///   - solutionChecker: A type that check if a solution is good enough to terminate the planner.
  func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<OPPONENTPS>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [PS],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  /// Return a short description of the planner.
  var shortDescription : String { get }
}

