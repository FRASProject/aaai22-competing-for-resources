//
//  PureStrategyEvaluator.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

public protocol PureStrategyEvaluator {
  associatedtype PSP1: PureStrategy
  associatedtype PSP2: PureStrategy
  func evaluate(stratP1: PSP1, stratP2: PSP2) throws -> (Double, Double)
}

extension PureStrategyEvaluator {
  public func evaluate<MSP1: MixedStrategyProtocol, MSP2: MixedStrategyProtocol>(
    stratP1: MSP1,
    stratP2: MSP2) throws -> (Double, Double) where MSP1.PS == PSP1, MSP2.PS == PSP2
  {
    try NormalFormGame.evaluateStrats(
      evaluator: self,
      player1Plans: stratP1.strategies,
      player2Plans: stratP2.strategies,
      player1PlansDistribution: stratP1.distribution,
      player2PlansDistribution: stratP2.distribution)
  }
}
