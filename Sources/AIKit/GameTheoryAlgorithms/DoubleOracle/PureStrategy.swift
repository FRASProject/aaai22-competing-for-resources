//
//  DoubleOraclePureStrategy.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

public protocol PureStrategy: Hashable, Codable {}
