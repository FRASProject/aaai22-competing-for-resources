//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/17/21.
//

import Foundation


public protocol SolutionChecker {
  func isGoodEnough(strategy: Any, player: Int) throws -> Bool
}

public struct AlwaysGoodSolutionChecker: SolutionChecker {
  public init() {}
  public func isGoodEnough(strategy: Any, player: Int) -> Bool {
    return true
  }
}

public struct AlwaysBadSolutionChecker: SolutionChecker {
  public init() {}
  public func isGoodEnough(strategy: Any, player: Int) -> Bool {
    return false
  }
}
