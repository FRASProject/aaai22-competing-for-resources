//
//  DoubleOracleConfigurations.swift
//  FRASLib
//
//  Created by Pavel Rytir on 9/2/18.
//

import Foundation

/// Settings of DO algorithm.
public struct DoubleOracleConfiguration<
  E: PureStrategyEvaluator,
  IOP1: InitOracle,
  BROP1: BestResponseOracle,
  IOP2: InitOracle,
  BROP2: BestResponseOracle>
where E.PSP1 == IOP1.PS,
      E.PSP1 == BROP1.PS,
      E.PSP2 == IOP2.PS,
      E.PSP2 == BROP2.PS
{
  public let numberOfInitialPlans: Int
  public let maximumNumberOfAttemptsToImproveEquilibriumApprox: Int
  public let initialPlansPlannerP1: IOP1
  public let initialPlansPlannerP2: IOP2
  public let bestResponsePlannerP1: BROP1
  public let bestResponsePlannerP2: BROP2
  public let strategiesEvaluator: E
  public let epsilon : Double
  public let name : String
  public init(
    numberOfInitialPlans: Int,
    maximumNumberOfAttemptsToImproveEquilibriumApprox: Int,
    initialPlansPlannerP1: IOP1,
    initialPlansPlannerP2: IOP2,
    bestResponsePlannerP1: BROP1,
    bestResponsePlannerP2: BROP2,
    strategiesEvaluator: E,
    epsilon : Double,
    name : String) {
    self.numberOfInitialPlans = numberOfInitialPlans
    self.maximumNumberOfAttemptsToImproveEquilibriumApprox = maximumNumberOfAttemptsToImproveEquilibriumApprox
    self.initialPlansPlannerP1 = initialPlansPlannerP1
    self.initialPlansPlannerP2 = initialPlansPlannerP2
    self.bestResponsePlannerP1 = bestResponsePlannerP1
    self.bestResponsePlannerP2 = bestResponsePlannerP2
    self.strategiesEvaluator = strategiesEvaluator
    self.epsilon = epsilon
    self.name = name
  }
}

extension DoubleOracleConfiguration : CustomStringConvertible {
  public var description: String {
    return name
  }
}


