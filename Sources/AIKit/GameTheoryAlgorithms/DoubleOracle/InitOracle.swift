//
//  InitOracle.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

/// Any type implemeting this protocol can be used in Double Oracle algorithm as an initial planner.
public protocol InitOracle {
  associatedtype PS: PureStrategy
  /// Finds initital plans.
  /// - Parameters:
  ///   - player: Player
  ///   - maxNumberOfPlans: Planner return maxNumberOfPlans or less.
  ///   - plansCacheDir: Cache directory
  /// - Returns: An array of plans, planner log, computation duration.
  func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (  // TODO: Maybe remove it? Planner dependent.
    plans: [PS],
    log: AIKitData,
    computationDuration: [TimeInterval])
  
  /// Short text description of the planner.
  var shortDescription : String { get }
}
