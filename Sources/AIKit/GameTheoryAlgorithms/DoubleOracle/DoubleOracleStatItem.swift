//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/17/21.
//

import Foundation

public struct DoubleOracleStatItem : Codable {
  let iteration : Int
  let player: Int?
  let elapsedTime : TimeInterval
  let stepDuration : TimeInterval
  let player1EqVal : Double
  let player2EqVal : Double
  let previousPlayer1EqVal: Double?
  let previousPlayer2EqVal: Double?
  let player1Equilibrium : [Double]
  let player2Equilibrium : [Double]
  let bestResponseValueP1 : Double?
  let bestResponseValueP2 : Double?
  let step : String
  let plannerLog : AIKitData
  let plannerDescription : String
  public var player1EquilibriumSupportSize: Int {
    player1Equilibrium.filter {$0 > AIKitConfig.probabilityEpsilon}.count
  }
  public var player2EquilibriumSupportSize: Int {
    player2Equilibrium.filter {$0 > AIKitConfig.probabilityEpsilon}.count
  }
}
