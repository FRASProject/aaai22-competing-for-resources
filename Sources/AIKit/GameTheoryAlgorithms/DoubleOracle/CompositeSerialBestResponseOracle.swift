//
//  File.swift
//  
//
//  Created by Pavel Rytir on 3/18/21.
//

import Foundation

public class CompositeSerialBestResponseOracle<
  O1: BestResponseOracle,
  O2: BestResponseOracle>: BestResponseOracle where O1.PS == O2.PS, O1.OPPONENTPS == O2.OPPONENTPS
{
  public typealias PS = O1.PS
  public typealias OPPONENTPS = O1.OPPONENTPS
  private let oracle1: O1
  private let oracle2: O2
  public init(oracle1: O1, oracle2: O2) {
    self.oracle1 = oracle1
    self.oracle2 = oracle2
  }
  private func addSuffix(to url: URL, suffix: String) -> URL {
    let filename = url.lastPathComponent
    let dotIndex = filename.lastIndex(of: ".")
    let newFileName: String
    if let dotIndex = dotIndex {
      newFileName = filename[..<dotIndex] + suffix + filename[dotIndex...]
    } else {
      newFileName = filename + suffix
    }
    return url.deletingLastPathComponent().appendingPathComponent(newFileName)
  }
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<OPPONENTPS>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [PS],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    let planCacheO1 = plansCacheFile == nil ? nil : addSuffix(to: plansCacheFile!, suffix: "_O1")
    let planCacheO2 = plansCacheFile == nil ? nil : addSuffix(to: plansCacheFile!, suffix: "_O2")
    let result1 = try oracle1.findBestResponse(
      player: player,
      to: opponent,
      opponentStrat: opponentStrat,
      utilityLowerBound: utilityLowerBound,
      plansCacheFile: planCacheO1,
      continueFunction: continueFunction,
      solutionChecker: solutionChecker)
    if !result1.bestResponseStrategy.isEmpty,
       try solutionChecker.isGoodEnough(strategy: result1.bestResponseStrategy, player: player)
    {
      return result1
    }
    print("trying oracle2")
    let result2 = try oracle2.findBestResponse(
      player: player,
      to: opponent,
      opponentStrat: opponentStrat,
      utilityLowerBound: utilityLowerBound,
      plansCacheFile: planCacheO2,
      continueFunction: continueFunction,
      solutionChecker: solutionChecker)
    return (
      result2.bestResponseStrategy,
      result2.cost,
      .dictionary(["logoracle1": result1.log,
       "logoracle2": result2.log]),
      result1.computationDuration + result2.computationDuration)
  }
  
  public var shortDescription: String {
    "Composite of o1: \(oracle1.shortDescription) and o2: \(oracle2.shortDescription)"
  }
}
