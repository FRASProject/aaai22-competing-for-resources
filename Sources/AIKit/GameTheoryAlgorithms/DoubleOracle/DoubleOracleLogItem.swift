//
//  DoubleOracleLog.swift
//  FRASLib
//
//  Created by Pavel Rytir on 9/30/18.
//

import Foundation
import MathLib

public struct DoubleOracleLogItem : Codable {
  public enum Step : String, Codable {
    case initialization
    case player1BestPureResponse
    case player2BestPureResponse
    case player1BestResponseFailureNotImprovedEquilibriumApprox
    case player2BestResponseFailureNotImprovedEquilibriumApprox
    case player1BestResponseFailure
    case player1BestResponseFailurePlanAlreadyAdded
    case player2BestResponseFailure
    case player2BestResponseFailurePlanAlreadyAdded
    case algorithmConverged
  }
  public let iteration: Int
  public let elapsedTime: TimeInterval
  public let stepDuration: TimeInterval
  public let step: Step
  public let player1Equilibrium: [Double]
  public let player2Equilibrium: [Double]
  public let player1EquilibriumValue: Double
  public let player2EquilibriumValue: Double
  public let previousPlayer1EquilibriumValue: Double?
  public let previousPlayer2EquilibriumValue: Double?
  public let plannerLog: AIKitData
  public let plannerDescription: String
  public let bestResponseValueP1: Double?
  public let bestResponseValueP2: Double?
  public var player: Int? {
    switch step {
    case .player1BestPureResponse,
         .player1BestResponseFailureNotImprovedEquilibriumApprox,
         .player1BestResponseFailure,
         .player1BestResponseFailurePlanAlreadyAdded:
      return 1
    case .player2BestPureResponse,
         .player2BestResponseFailureNotImprovedEquilibriumApprox,
         .player2BestResponseFailure,
         .player2BestResponseFailurePlanAlreadyAdded:
      return 2
    case .initialization, .algorithmConverged:
      return nil
    }
  }
}

extension DoubleOracleLogItem {
  public var statItem : DoubleOracleStatItem {
    return DoubleOracleStatItem(
      iteration: iteration,
      player: player,
      elapsedTime: elapsedTime,
      stepDuration: stepDuration,
      player1EqVal: player1EquilibriumValue,
      player2EqVal: player2EquilibriumValue,
      previousPlayer1EqVal: previousPlayer1EquilibriumValue,
      previousPlayer2EqVal: previousPlayer2EquilibriumValue,
      player1Equilibrium: player1Equilibrium,
      player2Equilibrium: player2Equilibrium,
      bestResponseValueP1:bestResponseValueP1,
      bestResponseValueP2: bestResponseValueP2,
      step: step.shortDescription,
      plannerLog: plannerLog,
      plannerDescription: plannerDescription)
  }
}

extension DoubleOracleLogItem : CustomStringConvertible {
  public var description : String {
    let str = String(
      format: "EQUILIBRIUM (P1: %3.2f : P2: %3.2f) --- ",
      player1EquilibriumValue,
      player2EquilibriumValue)
    if player == 1 {
      return str + step.description + String(
        format:"(P1BR value: %3.2f (PREVEQ: %3.2f)",
        bestResponseValueP1 ?? -1,
        previousPlayer1EquilibriumValue ?? -1)
    } else if player == 2 {
      return str + step.description + String(
        format:"(P2BR value %3.2f (PREVEQ: %3.2f))",
        bestResponseValueP2 ?? -1,
        previousPlayer2EquilibriumValue ?? -1)
    } else {
      return str + step.description
    }
  }
}

extension DoubleOracleLogItem.Step : CustomStringConvertible {
  public var description: String {
    switch self {
    case .initialization:
      return "INIT"
    case .player1BestPureResponse:
      return "P1 BEST RESPONSE -- "
    case .player2BestPureResponse:
      return "P2 BEST RESPONSE -- "
    case .player1BestResponseFailure:
      return "P1 BEST RESPONSE CANNOT BE COMPUTED"
    case .player2BestResponseFailure:
      return "P2 BEST RESPONSE CANNOT BE COMPUTED"
    case .player1BestResponseFailurePlanAlreadyAdded:
      return "P1 BEST RESPONSE PLAN ALREADY ADDED - skipping"
    case .player2BestResponseFailurePlanAlreadyAdded:
      return "P2 BEST RESPONSE PLAN ALREADY ADDED - skipping"
    case .algorithmConverged:
      return "ALGORITHM CONVERGED - POSSIBLE EQUILIBRIUM FOUND"
    case .player1BestResponseFailureNotImprovedEquilibriumApprox:
      return "P1 BEST RESPONSE NOT BETTER THAN EQ"
    case .player2BestResponseFailureNotImprovedEquilibriumApprox:
      return "P2 BEST RESPONSE NOT BETTER THAN EQ"
    }
  }
  public var shortDescription : String {
    switch self {
    case .initialization:
      return "I"
    case .player1BestPureResponse:
      return "BRP1"
    case .player2BestPureResponse:
      return "BRP2"
    case .player1BestResponseFailure:
      return "FBRP1"
    case .player1BestResponseFailureNotImprovedEquilibriumApprox:
      return "FBRP1EQNOTIMPROVED"
    case .player1BestResponseFailurePlanAlreadyAdded:
      return "FBRP1PLANALREADYADDED"
    case .player2BestResponseFailure:
      return "FBRP2"
    case .player2BestResponseFailurePlanAlreadyAdded:
      return "FBRP2PLANALREADYADDED"
    case .player2BestResponseFailureNotImprovedEquilibriumApprox:
      return "FBRP2EQNOTIMPROVED"
    case .algorithmConverged:
      return "CONV"
    }
  }
}


