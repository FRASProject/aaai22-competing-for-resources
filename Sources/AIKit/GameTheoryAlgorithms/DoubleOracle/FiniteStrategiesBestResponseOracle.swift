//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/2/21.
//

import Foundation
import MathLib

public final class FiniteStrategiesBestResponseOracle<PS, OPPONENTPS, E: PureStrategyEvaluator> :
  BestResponseOracle where E.PSP1 == PS, E.PSP2 == OPPONENTPS
{
  public let pureStrategies: [PS]
  private let evaluator: E
  private var valueCache: [Tuple<PS, OPPONENTPS>: Double]
  public init(pureStrategies: [PS], evaluator: E) {
    self.pureStrategies = pureStrategies
    self.evaluator = evaluator
    self.valueCache = [:]
  }
  public var shortDescription: String {
    "FiniteStrategiesBestResponseOracle"
  }
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<OPPONENTPS>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [PS],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    var maxValue = -Double.infinity
    var maximizingStrategy = pureStrategies.first!
    for pureStrategy in pureStrategies {
      let mixedValue = try zip(opponentStrat.strategies, opponentStrat.distribution).map {
        (p2Strat, p2Prob) -> Double in
        if valueCache[Tuple(pureStrategy, p2Strat)] == nil {
          valueCache[Tuple(pureStrategy, p2Strat)] = try evaluator.evaluate(stratP1: pureStrategy, stratP2: p2Strat).0
        }
        let pureStrategyValue = valueCache[Tuple(pureStrategy, p2Strat)]!
        return pureStrategyValue * p2Prob
      }.sum
      if mixedValue > maxValue {
        maxValue = mixedValue
        maximizingStrategy = pureStrategy
      }
    }
    return ([maximizingStrategy], [-maxValue], .string("NA"), 0.0)
  }
}
