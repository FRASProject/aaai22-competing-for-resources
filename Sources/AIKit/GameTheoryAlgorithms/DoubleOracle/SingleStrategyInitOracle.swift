//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/2/21.
//

import Foundation

public final class SingleStrategyInitOracle<PS: PureStrategy>: InitOracle {
  public let initStrategy: PS
  public init(initStrategy: PS) {
    self.initStrategy = initStrategy
  }
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (
    plans: [PS],
    log: AIKitData,
    computationDuration: [TimeInterval])
  {
    return (plans: [initStrategy], .string("NA"), [0.0])
  }
  public var shortDescription: String {
    "FiniteStrategiesInitOracle"
  }
  
}
