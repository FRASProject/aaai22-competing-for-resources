//
//  File.swift
//  
//
//  Created by Pavel Rytir on 2/5/21.
//

import Foundation

public protocol IterativeBestResponseOracle {
  associatedtype P: Hashable
  func findBestResponse(of agent: Int, to plans: [P]) -> P
}

public class IterativeBestResponse {
  enum Phase {
    case cost
    case cooperative
  }
  public enum Mode {
    public enum Short {
      case costDecreasing
      case simulatedAnnealing
    }
    case costDecreasing(maxNumberOfIterations: Int)
    case simulatedAnnealing(temperature: Double, step: Double, constantK: Double)
  }
  public typealias P = TemporalPlan
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let costAdversarySyncRatio: Double
  private let cooperativeCostBase: Int
  private let cooperativeCostSpread: Int
  private let heuristics: PDDLInitOracle.Heuristics
  // Without timestamps
  private let actionInstances: [[String: Set<PlanAction>]]
  private let actionCriticalFacts: [PlanAction: [PDDLNameLiteral]]
  private let criticalFactCriticalActions: [(
    criticalFact: PDDLEffect,
    criticalActions: [PDDLDurativeAction])
  ]
  
  private let costFunction: [PDDLNameLiteral : [Int]]
  private lazy var criticalFactsWeights: [PDDLNameLiteral : Int] = Dictionary(
    uniqueKeysWithValues: costFunction.map {
      ($0.key, game.getWeight(of: $0.key, for: player))})
  public let startTime: Date
  public private(set) var finishTime: Date?
  private let player: Int
  private let units: [String]
  private let unitsSet: Set<String>
  private let planCacheDir: URL?
  private let cacheFilePrefix: String
  private let planner: PDDLPlanner
  private let verbose: Bool
  private let config: AIKitData
  private var agentPlans: [[(plan:P, cost: Double, log: String, duration: Double)]]
  private var agentPlansSet: [Set<P>]
  public private(set) var currentJointPlan: P
  public private(set) var currentJointPlanCost: Int
  public private(set) var currentNotExecutableActions: Set<PlanAction>
  private var currentPhase: Phase
  private let mode: Mode
  private var agentsNumberOfFailedBestResponses: [Int]
  public var currentPlans: [P] {
    agentPlans.map {$0.last!.plan}
  }
  public var totalDuration: Double? {
    if let finishTime = finishTime {
      return finishTime.timeIntervalSince(startTime)
    }
    return nil
  }
  public var currentPlansLog: AIKitData {
    let log = agentPlans.enumerated().map {
      " Unit: \($0.offset) - log: \($0.element.last!.log) "}.joined(separator: "\n")
    return .dictionary([
      "log": .string(log),
      "iterations": .integer(agentPlans.reduce(0, { $0 + $1.count })),
      "plannerCosts": .array(agentPlans.map { .array($0.map {.double($0.cost)} )})
    ])
  }
  
  private func append(plan: (plan:P, cost: Double, log: String, duration: Double), for unit: Int) {
    agentPlans[unit].append(plan)
    agentPlansSet[unit].insert(plan.plan)
    updateJointPlan()
  }
  
  private func updateJointPlan() {
    let (jointPlan, notExecutedActions) = PlanMerger.merging(
      plans: currentPlans,
      initState: game.game.problem.initState,
      in: game.game.domain)
    self.currentJointPlan = jointPlan
    self.currentNotExecutableActions = Set(
      notExecutedActions.map {PlanAction($0).removing(parameters: unitsSet)})
    self.currentJointPlanCost = calculateCost(of: currentJointPlan)
  }
  
  private func jointPlan(except unit: Int) -> P {
    let (plan, _) = PlanMerger.merging(
      plans: currentPlans.removingElement(at: unit),
      initState: game.game.problem.initState,
      in: game.game.domain)
    return plan
  }
  
  private func calculateCost(of plan: P) -> Int {
    plan.cost(
      function: costFunction,
      actionCriticalFacts: actionCriticalFacts,
      unitsParameters: unitsSet,
      missingGoalPenalty: Dictionary(uniqueKeysWithValues: game.serialSoftGoalsCriticalActions[player].enumerated().map {($0.element.first!.criticalFact, game.serialGetWeight(of: $0.element.first!.criticalFact, for: player))}))
  }
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    mode: Mode,
    config: AIKitData,
    player: Int,
    planCacheDir: URL?,
    cacheFilePrefix: String,
    planner: PDDLPlanner,
    costFunction: [PDDLNameLiteral : [Int]] = [:],
    verbose: Bool = false) throws
  {
    self.startTime = Date()
    self.currentPhase = .cost
    self.game = game
    self.config = config
    self.heuristics = PDDLInitOracle.Heuristics(config["heuristics"]?.string ?? "none")
    self.mode = mode
    self.verbose = verbose
    self.costAdversarySyncRatio = config["AdvSyncR"]?.double ?? 1.0
    self.cooperativeCostBase = config["coopCostBase"]?.integer ?? 0
    self.cooperativeCostSpread = config["coopCostSpread"]?.integer ?? 0
    self.planner = planner
    self.player = player
    self.planCacheDir = planCacheDir
    self.cacheFilePrefix = cacheFilePrefix
    self.actionInstances = game.actionInstancesPlayersUnits[player]
    self.costFunction = costFunction
    self.criticalFactCriticalActions = game.game.domain.findPossibleCriticalActionsAndFacts()
    self.agentPlans = []
    self.agentPlansSet = []
    self.units = game.playersUnits[player]
    self.agentsNumberOfFailedBestResponses = [Int](repeating: 0, count: units.count)
    let unitsSet = Set(units)
    
    self.actionCriticalFacts = game.serialActionWithoutUnitsCriticalFacts
    self.unitsSet = unitsSet
    self.currentJointPlan = TemporalPlan(actions: [])
    self.currentNotExecutableActions = []
    self.currentJointPlanCost = 0
    if verbose {
      print("CBR: Starting cooperative best response algorithm.")
      print("CBR: Finding init plans.")
    }
    self.agentPlans = try units.indices.map {
      [try findInitPlan(for: $0)]
    }
    self.agentPlansSet = agentPlans.map { Set($0.map {$0.plan}) }
    updateJointPlan()
    if verbose {
      print("CBR: Found init plans. Current cost: \(currentJointPlanCost) Non exec actions: \(currentNotExecutableActions.count) -- \(currentNotExecutableActions.map {$0.description}.joined(separator: "; "))")
    }
  }
  /// Finds all cooperative actions that are currently not executable, but the unit could help with the execution.
  /// - Parameter agent: Unit number
  /// - Returns: CooperativeAction: Possible start times.
  private func findCooperativePoints(for unitNumber: Int) -> [PlanAction: Set<Int>] {
    // compare plans of i and joint plan
    var result = [PlanAction: Set<Int>]()
    let unit = units[unitNumber]
    let (_, notExecutableActions) = PlanMerger.merging(
      plans: currentPlans.removingElement(at: unitNumber),
      initState: game.game.problem.initState,
      in: game.game.domain)
    for action in notExecutableActions where !action.parameters.contains(unit){
      result[PlanAction(action).removing(parameters: unitsSet), default: []].insert(action.start)
    }
    return result
  }
  
  private func removeNotApplicableActions(
    _ cooperativeCostFunction: [(action: ActionNameProjectedParameters, cost: [Int])],
    unit: Int) ->
  [(action: ActionNameProjectedParameters, cost: [Int])]
  {
    var processedActions = [String: Set<PlanAction>]()
    var actionParameterIndices = [String: [Int]]()
    for (action, _) in cooperativeCostFunction {
      processedActions[action.name, default: []].insert(
        PlanAction(name: action.name, parameters: action.parametersInstance))
      actionParameterIndices[action.name] = action.parametersIndices
    }
    let projectedActionInstances = Dictionary(uniqueKeysWithValues: self.actionInstances[unit].filter {
      processedActions[$0.0] != nil
    }.map { actionName, instancesSet in
      (actionName, Set(instancesSet.map { instanceAction in
                        PlanAction(
                          name: actionName,
                          parameters: actionParameterIndices[actionName]!.map { instanceAction.parameters[$0] })}))
    })
    
    return cooperativeCostFunction.filter {
      projectedActionInstances[$0.action.name]?.contains(PlanAction(name: $0.action.name, parameters: $0.action.parametersInstance)) ?? false
    }
    
    
  }
  /// Add zero cost functions indexed by missing actions.
  /// - Parameters:
  ///   - cooperativeCostFunction:
  ///   - unit:
  /// - Returns:
  private func fillMissingActions(
    _ cooperativeCostFunction: [(
      action: ActionNameProjectedParameters,
      cost: [Int])],
    unit: Int) ->
  [(
    action: ActionNameProjectedParameters,
    cost: [Int])]
  {
    var processedActions = [String: Set<PlanAction>]()
    var actionParameterIndices = [String: [Int]]()
    for (action, _) in cooperativeCostFunction {
      processedActions[action.name, default: []].insert(
        PlanAction(name: action.name, parameters: action.parametersInstance))
      actionParameterIndices[action.name] = action.parametersIndices
    }
    
    let projectedActionInstances = self.actionInstances[unit].filter {
      processedActions[$0.0] != nil
    }.map { actionName, instancesSet in
      (actionName, Set(instancesSet.map { instanceAction in
                        PlanAction(
                          name: actionName,
                          parameters: actionParameterIndices[actionName]!.map {
                            instanceAction.parameters[$0] })}))
    }
    
    let missingActions = projectedActionInstances.map {
      ($0.0, $0.1.subtracting(processedActions[$0.0]!))
    }.filter {!$0.1.isEmpty}
    let missingInstances = missingActions.flatMap { actionName, instances in
      instances.map {(ActionNameProjectedParameters(
                        name: actionName,
                        parametersIndices: actionParameterIndices[actionName]!,
                        parametersInstance: $0.parameters), cost: [0])}
    }
    
    let filtered = removeNotApplicableActions(cooperativeCostFunction, unit: unit)
    
    return filtered + missingInstances
  }
  
  /// Finds the best response of the given unit to all the others units.
  /// First, it identifies cooperative points in the joint plan. That means at which time and by which actions can unit contribute to
  /// critical cooperative actions.
  /// Then, based on cooperative points it construct cooperation cost function. This function has non-zero value outside
  /// the cooperation points and zero at cooperation points.
  ///
  /// - Parameter agent: Unit number
  /// - Throws:
  /// - Returns: Unit's plan
  private func findBestResponse(of unit: Int) throws -> (
    plan: P,
    cost: Double,
    log: String,
    duration: Double)
  {
    // For every agent except current find cooperative points. Merge them and create cost function.
    let currentCoopRatio: Double
    switch self.currentPhase {
    case .cost:
      currentCoopRatio = 0.0
    case .cooperative:
      currentCoopRatio = self.costAdversarySyncRatio
    }
    let cooperativePoints = findCooperativePoints(for: unit)
    let cooperativeCostFunction = cooperativePoints.mapValues { actionTimes -> [Int] in
      let maxTime = actionTimes.max()! + 1
      return (0...maxTime).map {
        actionTimes.isDisjoint(
          with: (($0-cooperativeCostSpread...($0+cooperativeCostSpread)))) ? Int(
            (PDDLCriticalFactCostUtils.maxCost * currentCoopRatio).rounded()) + self.cooperativeCostBase : self.cooperativeCostBase }
    }.compactMap { (action, costFunction) -> [(ActionNameProjectedParameters, [Int])]? in
      let actionRestricted = action.removing(parameters: unitsSet)
      return actionCriticalFacts[actionRestricted]?.map {
        (
          ActionNameProjectedParameters(
            name: action.name,
            parametersIndices: actionRestricted.parameters.indices(of: $0.parameters),
            parametersInstance: $0.parameters),
          costFunction
        )}
    }.flatMap {$0}
    let plan = try findBestResponseToTeamUnitsPlan(
      for: unit,
      actionCostFunction: cooperativeCostFunction)
    return plan
  }
  
  
  public func runSimulatedAnnealing(temperature: Double, step: Double, constantK: Double) throws {
    var temperature = temperature
    var currentUnit = units.count - 1
    while temperature >= 0 {
      currentUnit = (currentUnit + 1) % units.count
      if verbose {
        print("Current temperature: \(temperature). Current cost: \(currentJointPlanCost)")
      }
      self.currentPhase = .cooperative
      let plan = try findBestResponse(of: currentUnit)
      var candidatePlans = currentPlans
      candidatePlans[currentUnit] = plan.plan
      let (candidateJointPlan, _) = PlanMerger.merging(
        plans: candidatePlans,
        initState: game.game.problem.initState,
        in: game.game.domain)
      //let candidateNotExecActions = Set(
//        candidateNotExecTemporalActions.map {PlanAction($0).removing(parameters: unitsSet)})
      
      let candidatePlanCost = calculateCost(of: candidateJointPlan)
      
      if candidatePlanCost < currentJointPlanCost {
        agentPlans[currentUnit].append(plan)
        agentPlansSet[currentUnit].insert(plan.plan)
        
        updateJointPlan()
      } else {
        self.currentPhase = .cost
        let plan = try findBestResponse(of: currentUnit)
        var candidatePlans = currentPlans
        candidatePlans[currentUnit] = plan.plan
        let (candidateJointPlan, _) = PlanMerger.merging(
          plans: candidatePlans,
          initState: game.game.problem.initState,
          in: game.game.domain)
        //let candidateNotExecActions = Set(
//          candidateNotExecTemporalActions.map {PlanAction($0).removing(parameters: unitsSet)})
        
        let candidatePlanCost = calculateCost(of: candidateJointPlan)
        let delta = Double(candidatePlanCost - currentJointPlanCost)
        if delta < 0 || Double.random(in: 0..<1) < exp(-delta / (temperature * constantK)) {
          agentPlans[currentUnit].append(plan)
          agentPlansSet[currentUnit].insert(plan.plan)
          
          updateJointPlan()
        }
      }
      if currentUnit == units.count - 1 {
        temperature -= step
      }
    }
    if verbose {
      print("Final plan cost: \(currentJointPlanCost)")
    }
    finishTime = Date()
  }
  
  /// Runs the iterative serial best response algorithm.
  /// The algorithm is initialized by arbitrary plans for all units in the initializer.
  /// It starts with current unit set to 0
  /// while the maximum number of iterations is not reached do:
  ///   It computes the best response of the current unit to the other units.
  ///   It evaluates if the new plan improves the joint plan.
  ///   If yes, then this plan is added to the join plan,
  ///   If not, it marks that the current unit failed to improve the joint plan.
  ///   Set current unit to the next one or the first one in case of the last unit.
  ///   The while loop is break, if no unit can improve the joint plan.
  ///
  /// - Parameter maxNumberOfIterations:
  /// - Throws:
  public func run(maxNumberOfIterations: Int) throws {
    var currentUnit = units.count - 1
    for i in 0..<maxNumberOfIterations {
      currentUnit = (currentUnit + 1) % units.count
      if agentsNumberOfFailedBestResponses.min()! > 0 {
        if verbose {
          print("CBR: Finished after \(i) iterations.")
        }
        // Proceed to the next phase.
        if currentPhase == .cost {
          if verbose {
            print("CBR: Switching to the cooperative phase.")
          }
          currentPhase = .cooperative
          for unit in self.units.indices {
            agentsNumberOfFailedBestResponses[unit] = 0
          }
        } else {
          break
        }
      }
      let plan = try findBestResponse(of: currentUnit)
      var candidatePlans = currentPlans
      candidatePlans[currentUnit] = plan.plan
      let (candidateJointPlan, candidateNotExecTemporalActions) = PlanMerger.merging(
        plans: candidatePlans,
        initState: game.game.problem.initState,
        in: game.game.domain)
      let candidateNotExecActions = Set(
        candidateNotExecTemporalActions.map {PlanAction($0).removing(parameters: unitsSet)})
      
      let candidatePlanCost = calculateCost(of: candidateJointPlan)
      if verbose {
        print("CBR: Found BR\(agentPlans[currentUnit].count) plan unit: \(currentUnit)/\(units.count - 1)  Cost \(candidatePlanCost) Not exec actions: \(candidateNotExecActions.count) -- \(candidateNotExecActions.map {$0.description}.joined(separator: "; "))")
      }
      if !(candidatePlanCost < currentJointPlanCost ||
           candidatePlanCost == currentJointPlanCost && candidateNotExecActions.count < currentNotExecutableActions.count) {
        if verbose {
          print("CBR: Plan cost \(candidatePlanCost) no lower than current \(currentJointPlanCost). Skipping.")
        }
        agentsNumberOfFailedBestResponses[currentUnit] += 1
        continue
      }
      agentPlans[currentUnit].append(plan)
      agentPlansSet[currentUnit].insert(plan.plan)
      for unit in self.units.indices {
        agentsNumberOfFailedBestResponses[unit] = 0
      }
      
      updateJointPlan()
    }
    finishTime = Date()
  }
  
  public func run() throws {
    switch mode {
    case let .costDecreasing(maxNumberOfIterations):
      try run(maxNumberOfIterations: maxNumberOfIterations)
    case let .simulatedAnnealing(temperature, step, constantK):
      try runSimulatedAnnealing(temperature: temperature, step: step, constantK: constantK)
    }
  }
  
  /// Find a plan for the given unit
  /// - Parameters:
  ///   - unit: unit name
  ///   - problem: planing problem
  /// - Throws:
  /// - Returns: plan
  private func findInitPlan(for unitIndex: Int) throws -> (
    plan:TemporalPlan,
    cost: Double,
    log: String,
    duration: Double)
  {
    let problem = game.game.getPlanningProblem(for: player)
          .keepingOnly(player: player)
    let unit = units[unitIndex]
    let modifiedProblem: PDDLPlanningProblem
    let parameterWithPrefixesToRemove: [String]
    switch heuristics {
    case .none:
      parameterWithPrefixesToRemove = []
      modifiedProblem = problem.convertingSoftGoalsToHardGoalsRemovingMetrics()
        .restrictingObjectsOf(
        type: PDDLDomain.unitType,
        to: [unit])
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
        .adding(criticalFactCostFunction: costFunction,
                criticalFactCriticalActions: criticalFactCriticalActions)
        .addingFailingGoalActionsWithCost()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    case .pruning, .orderingSimAnneal:
      fatalError("Implement me!")
    case .ordering:
      parameterWithPrefixesToRemove = ["c"] // remove count parameters.
      let ordering = SoftGoalsHeuristics.calculateDeleteRelaxationOrderHeuristics(
        factsCostFunction: costFunction,
        criticalFactsWeights: criticalFactsWeights,
        groundedProblem: game.groundedPlayersProblems[player],
        softGoals: game.softGoals[player],
        goalCriticalActionClusters: game.goalCriticalActionClusters[player],
        playersUnits: game.playersUnits[player],
        debugVerbose: verbose)
      let goalCriticalActions = game.softGoalsCriticalActions[player].map {
        $0.map {game.groundedPlayersProblems[player].operatorIndex[$0.criticalAction]!}}
      let convertedOrdering: [String: [Int]] = SoftGoalsHeuristics.convert(
        relation: ordering,
        fdrCoding: game.groundedPlayersProblems[player],
        goalCriticalActions: goalCriticalActions,
        units: game.playersUnits[player])
      let reachableGoals = convertedOrdering.values.map {Set($0)}.reduce(Set()) {$0.union($1)}.sorted()
      let (criticalActionFact, orderingConverted) = SoftGoalsHeuristics.convertDataForOrderHeuristics(
        ordering: convertedOrdering,
        game: game,
        player: player)
      print("Ordering : \(ordering)")
      modifiedProblem = problem.restrictingObjectsOf(
        type: PDDLDomain.unitType,
        to: [unit])
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
        .adding(criticalFactCostFunction: costFunction,
                criticalFactCriticalActions: criticalFactCriticalActions)
        .keepingOnlyGoals(reachableGoals)
        .addingOrderHeuristics(
          criticalActionFact: criticalActionFact,
          allCriticalActionNames: Set(game.playerAllCriticalAction[player].map {$0.name}),
          ordering: orderingConverted)
        .convertingSoftGoalsToHardGoalsRemovingMetrics()
        .addingFailingGoalActionsWithCost()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    }


    let planCache = planCacheDir.map {
      $0.appendingPathComponent("\(cacheFilePrefix)_init_\(unit).json")}
    let (plannerOutput, cost, log, _, duration) = try planner.findFirst(
      problemPDDL: modifiedProblem.problem.text,
      domainPDDL: modifiedProblem.domain.text,
      planCache: planCache)
    if let plannerOutput = plannerOutput.last {
      let plan = TemporalPlan(
        classicalPlan: plannerOutput.parsed!.filterOutFailingGoalsActions(),
        durationOfUnitTimestamp: modifiedProblem.durationOfUnitTimestamp!,
        units: game.playersUnits[player],
        removingNoopActions: true)
        .removingParametersWith(prefixes: parameterWithPrefixesToRemove)
      
      return (plan, cost.last!, log, duration.last!)
    } else {
      throw AIKitError(message: "Cannot find init plan for unit \(unit)")
    }
    
  }
  
  /// Crop the cost fo function given by the opponent by deadlines of the teammates. After the team unit deadline the cost no
  /// longer increases.
  /// - Parameter teamDeadlines:
  /// - Returns: New cost function
  private func getCostFunctionCroppedBy(teamDeadlines: [PDDLNameLiteral: Int]) -> [PDDLNameLiteral: [Int]] {
    Dictionary(uniqueKeysWithValues: self.costFunction.map { criticalFact, factCostFunction in
      if let teamDeadline = teamDeadlines[criticalFact], teamDeadline < factCostFunction.count {
        let cropedCostFunction = Array(factCostFunction[...teamDeadline]) + [Int](
          repeating: factCostFunction[teamDeadline], count: factCostFunction.count - teamDeadline - 1)
        assert(factCostFunction.count == cropedCostFunction.count)
        return (criticalFact, cropedCostFunction)
      } else {
        return (criticalFact, factCostFunction)
      }
    })
  }
  
  /// Calculates a plan with respect to the actionCostFunction + self.costFunction
  /// - Parameters:
  ///   - agent: Unit number
  ///   - problem: planning problem
  ///   - actionCostFunction: cost function
  /// - Throws:
  /// - Returns: Best response plan.
  private func findBestResponseToTeamUnitsPlan(
    for unitIndex: Int,
    actionCostFunction: [(action: ActionNameProjectedParameters,
      costFunction: [Int])]) throws -> (
        plan:TemporalPlan,
        cost: Double,
        log: String,
        duration: Double)
  {
    let problem = game.game.getPlanningProblem(for: player)
          .keepingOnly(player: player)
    let unit = units[unitIndex]
    let teamDeadlines = self.jointPlan(except: unitIndex).extractDeadlines(
      actionCriticalFacts: actionCriticalFacts,
      unitsParameters: unitsSet)
    let cropedCostFunction = getCostFunctionCroppedBy(teamDeadlines: teamDeadlines)
    let convertedCostFunction = problem.convert(
      criticalFactCostFunction: cropedCostFunction,
      allCriticalFactCriticalActions: criticalFactCriticalActions)
    let mergedFunction = PDDLCriticalFactCostUtils.merge(
      f1: convertedCostFunction.map {
        ($0.action.shiftingParametersIndices(by: 1),
         $0.costFunction) },
      f2: actionCostFunction.map {
        ($0.action.shiftingParametersIndices(by: 1),
         $0.costFunction) })
    let filledMergedFunction = fillMissingActions(mergedFunction, unit: unitIndex)
    
    let modifiedProblem: PDDLPlanningProblem
    let parameterWithPrefixesToRemove: [String]
    switch heuristics {
    case .none:
      parameterWithPrefixesToRemove = []
      modifiedProblem = problem.convertingSoftGoalsToHardGoalsRemovingMetrics()
        .restrictingObjectsOf(
        type: PDDLDomain.unitType,
        to:[unit])
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      //.addingFailingGoalActionsWithCost()
        .addingNoOpAction()
        .adding(actionCooperativeCostFunctions: filledMergedFunction)
        .addingFailingGoalActionsWithCost()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    case .pruning, .orderingSimAnneal:
      fatalError("Implement me!")
    case .ordering:
      parameterWithPrefixesToRemove = ["c"] // remove count parameters.
      let ordering = SoftGoalsHeuristics.calculateDeleteRelaxationOrderHeuristics(
        factsCostFunction: costFunction,
        criticalFactsWeights: criticalFactsWeights,
        groundedProblem: game.groundedPlayersProblems[player],
        softGoals: game.softGoals[player],
        goalCriticalActionClusters: game.goalCriticalActionClusters[player],
        playersUnits: game.playersUnits[player],
        debugVerbose: verbose)
      let goalCriticalActions = game.softGoalsCriticalActions[player].map {
        $0.map {game.groundedPlayersProblems[player].operatorIndex[$0.criticalAction]!}}
      let convertedOrdering: [String: [Int]] = SoftGoalsHeuristics.convert(
        relation: ordering,
        fdrCoding: game.groundedPlayersProblems[player],
        goalCriticalActions: goalCriticalActions,
        units: game.playersUnits[player])
      let reachableGoals = convertedOrdering.values.map {Set($0)}.reduce(Set()) {$0.union($1)}.sorted()
      let (criticalActionFact, orderingConverted) = SoftGoalsHeuristics.convertDataForOrderHeuristics(
        ordering: convertedOrdering,
        game: game,
        player: player)
      print("Ordering : \(ordering)")
      modifiedProblem = problem.restrictingObjectsOf(
        type: PDDLDomain.unitType,
        to:[unit])
        .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      //.addingFailingGoalActionsWithCost()
        .addingNoOpAction()
        .adding(actionCooperativeCostFunctions: filledMergedFunction)
        .keepingOnlyGoals(reachableGoals)
        .addingOrderHeuristics(
          criticalActionFact: criticalActionFact,
          allCriticalActionNames: Set(game.playerAllCriticalAction[player].map {$0.name}),
          ordering: orderingConverted)
        .convertingSoftGoalsToHardGoalsRemovingMetrics()
        .addingFailingGoalActionsWithCost()
        .addingTotalCostFunction()
        .addingMinimizeTotalCostMetric()
    }
    
    let planCache = planCacheDir.map {
      $0.appendingPathComponent("\(cacheFilePrefix)_br\(agentPlans[unitIndex].count)_\(unit).json")}
    let (plannerOutput, cost, log, _, duration) = try planner.findFirst(
      problemPDDL: modifiedProblem.problem.text,
      domainPDDL: modifiedProblem.domain.text,
      planCache: planCache)
    if let plannerOutput = plannerOutput.last {
      let plan = TemporalPlan(
        classicalPlan: plannerOutput.parsed!.filterOutFailingGoalsActions().filterOutNoOpActions(),
        durationOfUnitTimestamp: modifiedProblem.durationOfUnitTimestamp!,
        units: game.playersUnits[player],
        removingNoopActions: true)
        .removingParametersWith(prefixes: parameterWithPrefixesToRemove)
      
      return (plan, cost.last!, log, duration.last!)
    } else {
      throw AIKitError(message: "Cannot find init plan for unit \(unit)")
    }
  }
}
