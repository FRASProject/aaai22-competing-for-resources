//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/4/21.
//

import Foundation
import MathLib

public final class SamplingAdversarialPlanningExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  public typealias D = PDDLDoubleOracleExperiment.Result
  public typealias B = TemporalPlannerExperiment.Result
  public struct GameValues: ExperimentDataResultDictionary, Codable {
    public let player1Value: Double
    public let player2Value: Double
    public var asDictionary: [String : AIKitData] {
      ["player1Value": .double(player1Value), "player2Value": .double(player2Value)]
    }
  }
  public struct AggregateGameValues: ExperimentDataResultDictionary, Codable {
    public let avgP1Value: Double
    public let avgP2Value: Double
    public let stdP1Value: Double
    public let stdP2Value: Double
    public let minP1Value: Double
    public let minP2Value: Double
    public let maxP1Value: Double
    public let maxP2Value: Double
    public var asDictionary: [String : AIKitData] {
      [
        "avgP1Value": .double(avgP1Value),
        "avgP2Value": .double(avgP2Value),
        "stdP1Value": .double(stdP1Value),
        "stdP2Value": .double(stdP2Value),
        "minP1Value": .double(minP1Value),
        "minP2Value": .double(minP2Value),
        "maxP1Value": .double(maxP1Value),
        "maxP2Value": .double(maxP2Value),
      ]
    }
    public init(player1Values: [Double], player2Values: [Double]) {
      avgP1Value = player1Values.avg!
      avgP2Value = player2Values.avg!
      stdP1Value = player1Values.std!
      stdP2Value = player2Values.std!
      minP1Value = player1Values.min()!
      minP2Value = player2Values.min()!
      maxP1Value = player1Values.max()!
      maxP2Value = player2Values.max()!
    }
  }
  public struct SamplingExploitability: ExperimentDataResultDictionary, Codable {
    public let p1Exploitabity: Double
    public let p2Exploitabity: Double
    public let p1ExploitabityStd: Double
    public let p2ExploitabityStd: Double
    public var asDictionary: [String : AIKitData] {
      [
        "p1Exploitabity": .double(p1Exploitabity),
        "p2Exploitabity": .double(p2Exploitabity),
        "p1ExploitabityStd": .double(p1ExploitabityStd),
        "p2ExploitabityStd": .double(p2ExploitabityStd)
      ]
    }
  }
  public struct Result: ExperimentDataResultDictionary, Codable {
    public let gameStatistics: PDDLGame.Statistics
    public let doResults: [ExperimentDataWithIndex<D>]
    public let baseLineResults: [ExperimentDataWithIndex<B>]
    public let samplingResults: [ExperimentDataWithIndex<SamplingPlannerExperiment.Result>]
    public let baseVsDOValues: [ExperimentDataWithMultiIndex<GameValues>]
    public let samplingVsDOValues: [ExperimentDataWithMultiIndex<AggregateGameValues>]
    public let samplingVsBaseValues: [ExperimentDataWithMultiIndex<AggregateGameValues>]
    public let baselineExploitability: [ExperimentDataWithIndex<PDDLStrategyExpoitabilityExperiment.Result>]
    public let samplingExploitability: [ExperimentDataWithIndex<SamplingExploitability>]
    public let samplingMultiPlansExploitability: [ExperimentDataWithIndex<SamplingExploitability>]
    public let commonIndex: ExperimentIndex
    public var asDictionary: [String : AIKitData] {
      let ds = gameStatistics.asDictionary
      let d = doResults.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(ds, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let b = baseLineResults.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(d, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let s = samplingResults.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(b, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let bd = baseVsDOValues.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(s, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let sd = samplingVsDOValues.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(bd, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let sb = samplingVsBaseValues.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(sd, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let be = baselineExploitability.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(sb, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let se = samplingExploitability.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(be, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      
      let sme = samplingMultiPlansExploitability.map {
        $0.resultDictionaryWithIndexSuffix(without: commonIndex)
      }.reduce(se, {$0.merging($1, uniquingKeysWith: { a, _ in a})})
      return sme
    }
  }
  public let parameters: ExperimentParameters
  public var resultData: ResultData?
  public var failError: Error?
  public var fromCache: Bool?
  
  public init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  
  public let fileNameSufix: String = ".json"
  public let fileNamePrefix: String = "SamplingResults"
  public let recompute: Bool = true
  
  public func runBody() throws {
    
    // Double oracle
    let doubleOracleConfig = ExperimentManager.defaultManager.experimentConfig["DoubleOracle"]!.arrayOfDictionaries!
    let doParameters = parameters.merging(from: doubleOracleConfig)
    let doExperiments = doParameters.map {
      PDDLDoubleOracleExperiment(parameters: $0)
    }
    try runSubexperiments(doExperiments)
    let doResults = doExperiments.compactMap {
      $0.asExperimentDataWithIndex
    }
    
    let p1EquilibriumValue = doResults.first!.data.player1EquilibriumValue
    let p2EquilibriumValue = doResults.first!.data.player2EquilibriumValue
    
    // Baseline planner
    let baselineConfig = ExperimentManager.defaultManager.experimentConfig["Baseline"]!.dictionary!
    let baselineParameters = parameters.merging(from: baselineConfig)
    let baselineExperiments = baselineParameters.map {
      TemporalPlannerExperiment(parameters: $0)
    }
    try runSubexperiments(baselineExperiments)
    let baselineResults = baselineExperiments.compactMap {$0.asExperimentDataWithIndex}
    
    // Sampling planner
    let samplingConfig = ExperimentManager.defaultManager.experimentConfig["Sampling"]!.dictionary!
    let samplingParameters = parameters.merging(from: samplingConfig)
    let samplingExperiments = samplingParameters.map {
      SamplingPlannerExperiment(parameters: $0)
    }
    try runSubexperiments(samplingExperiments)
    let samplingResults = samplingExperiments.compactMap {$0.asExperimentDataWithIndex}
    
    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
    let strategyEvaluator = PDDLPlansEvaluator(game: game)
    
    // Game values
    
    let baseVsDOValues = try doResults.flatMap { doResult in
      try baselineResults.filter {$0.data.player1 != nil}.map {
        baseResult -> ExperimentDataWithMultiIndex<GameValues>  in
        let (p1Value, p2Value) = try NormalFormGame.evaluateStrats(
          evaluator: strategyEvaluator,
          player1Plans: [baseResult.data.player1!],
          player2Plans: doResult.data.player2Plans,
          player1PlansDistribution: [1.0],
          player2PlansDistribution: doResult.data.player2Equilibrium)
        return ExperimentDataWithMultiIndex(
          indices: [baseResult.index, doResult.index],
          data: GameValues(
            player1Value: p1Value,
            player2Value: p2Value))
      }
    }
    
    let samplingVsDOValues = try doResults.flatMap { doResult in
      try samplingResults.map { samplingResult -> ExperimentDataWithMultiIndex<AggregateGameValues> in
        let trialsValues = try samplingResult.data.results.map { trialResult -> (Double, Double) in
          let (p1Value, p2Value) = try NormalFormGame.evaluateStrats(
            evaluator: strategyEvaluator,
            player1Plans: [trialResult.player1],
            player2Plans: doResult.data.player2Plans,
            player1PlansDistribution: [1.0],
            player2PlansDistribution: doResult.data.player2Equilibrium)
          return (p1Value, p2Value)
        }
        let player1Values = trialsValues.map {$0.0}
        let player2Values = trialsValues.map {$0.1}
        return ExperimentDataWithMultiIndex(
          indices: [samplingResult.index, doResult.index],
          data: AggregateGameValues(player1Values: player1Values, player2Values: player2Values))
      }
    }
    
    let samplingVsBaselineValues = try baselineResults.filter {$0.data.player2 != nil}.flatMap {
      baseResult in
      try samplingResults.map { samplingResult -> ExperimentDataWithMultiIndex<AggregateGameValues> in
        let trialsValues = try samplingResult.data.results.map { trialResult -> (Double, Double) in
          let (p1Value, p2Value) = try NormalFormGame.evaluateStrats(
            evaluator: strategyEvaluator,
            player1Plans: [trialResult.player1],
            player2Plans: [baseResult.data.player2!],
            player1PlansDistribution: [1.0],
            player2PlansDistribution: [1.0])
          return (p1Value, p2Value)
        }
        let player1Values = trialsValues.map {$0.0}
        let player2Values = trialsValues.map {$0.1}
        return ExperimentDataWithMultiIndex(
          indices: [samplingResult.index, baseResult.index],
          data: AggregateGameValues(player1Values: player1Values, player2Values: player2Values))
      }
    }
    
    // Exploitability
    let exploitabilityConfig = ExperimentManager.defaultManager.experimentConfig["Exploitability"]!.dictionary!
 
    let baselineExploitabilityExperiments = baselineExperiments.filter {
      $0.resultData != nil }.map {
      PDDLStrategyExpoitabilityExperiment(
        player1Strategy: $0.resultData?.player1.map { MixedStrategy([$0], distribution: [1.0]) },
        player2Strategy: $0.resultData?.player2.map { MixedStrategy([$0], distribution: [1.0]) },
        parameters: $0.parameters.merging(from: exploitabilityConfig).first!)
    }
    try runSubexperiments(baselineExploitabilityExperiments)
    
    let baseExplResults = baselineExploitabilityExperiments.compactMap {$0.asExperimentDataWithIndex}
    
    let samplingExploitabilityExperiments = samplingExperiments.filter {
      $0.resultData != nil }.map { samplingExperiment in
        (samplingExperiment.parameters,
         samplingExperiment.resultData!.results.map {
          PDDLStrategyExpoitabilityExperiment(
            player1Strategy: MixedStrategy([$0.player1], distribution: [1.0]),
            player2Strategy: MixedStrategy([$0.player2], distribution: [1.0]),
            parameters: samplingExperiment.parameters.merging(from: exploitabilityConfig).first!)
        })
      }
    try samplingExploitabilityExperiments.forEach {
      try runSubexperiments($0.1)
    }
    
    let samplingExploitabilityResults = samplingExploitabilityExperiments.map {
      ie -> ExperimentDataWithIndex<SamplingExploitability> in
      let player1ExploitabilityValues = ie.1.map {
        $0.resultData!.exploitability["player2BaseBestValP2"]! - p2EquilibriumValue}
      let player2ExploitabilityValues = ie.1.map {
        $0.resultData!.exploitability["player1BaseBestValP1"]! - p1EquilibriumValue}
      return ExperimentDataWithIndex(
        index: ie.0.index,
        data: SamplingExploitability(
          p1Exploitabity: player1ExploitabilityValues.avg!,
          p2Exploitabity: player2ExploitabilityValues.avg!,
          p1ExploitabityStd: player1ExploitabilityValues.std!,
          p2ExploitabityStd: player2ExploitabilityValues.std!
        ))
    }
    
    let numberMultiPlansExploitability = ExperimentManager.defaultManager.experimentConfig["mixedSamplingExploitability"]!["numberOfPlans"]!.integer!
    
    let samplingMultiPlansExploitabilityExperiments = samplingExperiments.filter {
      $0.resultData != nil }.map {
        PDDLStrategyExpoitabilityExperiment(
          player1Strategy: MixedStrategy(
            $0.resultData!.results[..<numberMultiPlansExploitability].map {$0.player1},
            distribution: [Double](
              repeating: 1.0 / Double(numberMultiPlansExploitability),
              count: numberMultiPlansExploitability)),
          player2Strategy: MixedStrategy(
            $0.resultData!.results[..<numberMultiPlansExploitability].map {$0.player2},
            distribution: [Double](
              repeating: 1.0 / Double(numberMultiPlansExploitability),
              count: numberMultiPlansExploitability)),
          parameters: $0.parameters.merging(from: exploitabilityConfig).first!,
        fileNamePrefix: "Multi\(numberMultiPlansExploitability)PlanExploitability")
      }
    try runSubexperiments(samplingMultiPlansExploitabilityExperiments)
    let samplingMultiPlansExploitabilityResults = samplingMultiPlansExploitabilityExperiments.compactMap {
      $0.asExperimentDataWithIndex}.map {
        ExperimentDataWithIndex(
          index: $0.index,
          data: SamplingExploitability(
            p1Exploitabity: $0.data.exploitability["player2BaseBestValP2"]! - p2EquilibriumValue,
            p2Exploitabity: $0.data.exploitability["player1BaseBestValP1"]! - p1EquilibriumValue,
            p1ExploitabityStd: 0.0,
            p2ExploitabityStd: 0.0))
      }
    
    // Final result
    let result = Result(
      gameStatistics: game.game.statistics,
      doResults: doResults,
      baseLineResults: baselineResults,
      samplingResults: samplingResults,
      baseVsDOValues: baseVsDOValues,
      samplingVsDOValues: samplingVsDOValues,
      samplingVsBaseValues: samplingVsBaselineValues,
      baselineExploitability: baseExplResults,
      samplingExploitability: samplingExploitabilityResults,
      samplingMultiPlansExploitability: samplingMultiPlansExploitabilityResults,
      commonIndex: parameters.index)
    save(result: result)
  }
  
  public func runAfterFinish() throws {
    guard let resultData = resultData else { return }
    var datasets = [ChartUtils.Dataset]()
    datasets.append(ChartUtils.Dataset(
      name: "Sampling vs Double Oracle",
      type: .errorbar,
      data: [
        "x": ChartUtils.Series(resultData.samplingVsDOValues.map {
          $0.getIndexValue(for: "ESTNO")!.integer!}),
        "y": ChartUtils.Series(resultData.samplingVsDOValues.map {
          $0.data.avgP1Value
        }),
        "yerr-": ChartUtils.Series(resultData.samplingVsDOValues.map {
          $0.data.avgP1Value - $0.data.minP1Value
        }),
        "yerr+": ChartUtils.Series(resultData.samplingVsDOValues.map {
          $0.data.maxP1Value - $0.data.avgP1Value
        })]))
    let equilibriumPlot = ChartUtils.Dataset(
      name: "Equilibrium value",
      type: .line,
      data: [
        "x": ChartUtils.Series(resultData.samplingVsDOValues.map {
          $0.getIndexValue(for: "ESTNO")!.integer!}),
        "y": ChartUtils.Series([Double](repeating: resultData.doResults.first!.data.player1EquilibriumValue, count: resultData.samplingVsDOValues.count))])
    datasets.append(equilibriumPlot)
    if let doVsBasePlayer1Value = resultData.baseVsDOValues.first?.data.player1Value {
      datasets.append(ChartUtils.Dataset(
        name: "Baseline vs Double Oracle",
        type: .line,
        data: [
          "x": ChartUtils.Series(resultData.samplingVsDOValues.map {
            $0.getIndexValue(for: "ESTNO")!.integer!}),
          "y": ChartUtils.Series([Double](
            repeating: doVsBasePlayer1Value,
            count: resultData.samplingVsDOValues.count))]))
    }
    try? ChartUtils.makeFigure(
      chart: ChartUtils.FigureParametersData(
        figureTitle: "Sampling",
        plots: [ChartUtils.ChartParametersData(
          plotTitle: "Sampling",
          datasets: datasets,
          parameters: [
            "xlabel": "NoEstimates",
            "ylabel": "p1Value",
            "scale": "log",
            "legend_location": "lower right"
          ])],
        parameters: [:]),
      fileNames: [experimentDirectory + "/samplingAndBaseVsDOChart.png"])
    
    datasets = []
    datasets.append(ChartUtils.Dataset(
      name: "Sampling vs Baseline",
      type: .errorbar,
      data: [
        "x": ChartUtils.Series(resultData.samplingVsBaseValues.map {
          $0.getIndexValue(for: "ESTNO")!.integer!}),
        "y": ChartUtils.Series(resultData.samplingVsBaseValues.map {
          $0.data.avgP1Value
        }),
        "yerr-": ChartUtils.Series(resultData.samplingVsBaseValues.map {
          $0.data.avgP1Value - $0.data.minP1Value
        }),
        "yerr+": ChartUtils.Series(resultData.samplingVsBaseValues.map {
          $0.data.maxP1Value - $0.data.avgP1Value
        })]))
    datasets.append(equilibriumPlot)
    try? ChartUtils.makeFigure(
      chart: ChartUtils.FigureParametersData(
        figureTitle: "Sampling",
        plots: [ChartUtils.ChartParametersData(
          plotTitle: "Sampling",
          datasets: datasets,
          parameters: [
            "xlabel": "NoEstimates",
            "ylabel": "p1Value",
            "scale": "log",
            "legend_location": "lower right"
          ])],
        parameters: [:]),
      fileNames: [experimentDirectory + "/samplingVsBaseChart.png"])
    
    datasets = []
    datasets.append(ChartUtils.Dataset(
      name: "Sampling",
      type: .errorbar,
      data: [
        "x": ChartUtils.Series(resultData.samplingExploitability.map {
          $0.getIndexValue(for: "ESTNO")!.integer!}),
        "y": ChartUtils.Series(resultData.samplingExploitability.map {
          $0.data.p1Exploitabity
        }),
        "yerr-": ChartUtils.Series(resultData.samplingExploitability.map {
          $0.data.p1ExploitabityStd
        }),
        "yerr+": ChartUtils.Series(resultData.samplingExploitability.map {
          $0.data.p1ExploitabityStd
        })]))
    datasets.append(ChartUtils.Dataset(
      name: "Sampling uniform strategy",
      type: .line,
      data: [
        "x": ChartUtils.Series(resultData.samplingMultiPlansExploitability.map {
          $0.getIndexValue(for: "ESTNO")!.integer!}),
        "y": ChartUtils.Series(resultData.samplingMultiPlansExploitability.map {
          $0.data.p1Exploitabity
        })]))
    let p2EquilibriumValue = resultData.doResults.first!.data.player2EquilibriumValue
    if let player2BaseBestValP2 = resultData.baselineExploitability.first!.data.exploitability["player2BaseBestValP2"] {
      let baselineP1Exploitability = player2BaseBestValP2 - p2EquilibriumValue
      datasets.append(ChartUtils.Dataset(
        name: "Baseline",
        type: .line,
        data: [
          "x": ChartUtils.Series(resultData.samplingExploitability.map {
            $0.getIndexValue(for: "ESTNO")!.integer!}),
          "y": ChartUtils.Series([Double](
            repeating: baselineP1Exploitability,
            count: resultData.samplingExploitability.count))]))
    }
    try? ChartUtils.makeFigure(
      chart: ChartUtils.FigureParametersData(
        figureTitle: "Sampling exploitability",
        plots: [ChartUtils.ChartParametersData(
          plotTitle: "Sampling exploitability",
          datasets: datasets,
          parameters: [
            "xlabel": "NoEstimates",
            "ylabel": "Exploitability",
            "scale": "log",
            "legend_location": "lower right"
          ])],
        parameters: [:]),
      fileNames: [experimentDirectory + "/exploitability.png"])
    
  }
}
