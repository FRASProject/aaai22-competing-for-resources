//
//  PDDLPlannerController.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 9/17/20.
//

import Foundation

public class TemporalPlannerExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  public struct Result: ExperimentDataResultDictionary, Codable {
    public let player1: TemporalPlan?
    public let player2: TemporalPlan?
    public let computationTimePlayer1: Double?
    public let computationTimePlayer2: Double?
    public var asDictionary: [String : AIKitData] {
      return [
        "baselineDurationP1": AIKitData(computationTimePlayer1),
        "baselineDurationP2": AIKitData(computationTimePlayer2)]
    }
  }
  
  public var parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  
  public var fileNamePrefix: String {
    "TemporalPlannerResults"
  }
  
  public var fileNameSufix: String {
    ".json"
  }
  
  public let recompute = false
  
  public required init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  
  public func runBody() throws
  {
    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
    let planner = try PDDLPlannerFactory.makeTemporal(
      by: parameters["planner"]!,
      frasConfig: ExperimentManager.defaultManager.systemConfig)
    
    let player1Problem = game.game.getPlanningProblem(for: 0)
      .keepingOnly(player: 0)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingMinimizeTotalTimeMetric()
    
    let p1PlannerOutput = try? planner.findFirst(
      problem: player1Problem,
      planCache: URL(fileURLWithPath: parameters.pathFileName + "/p1Plan"))
    let p1Plan = p1PlannerOutput?.0.last
    
    let player2Problem = game.game.getPlanningProblem(for: 1)
      .keepingOnly(player: 1)
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingMinimizeTotalTimeMetric()
    
    let p2PlannerOutput = try? planner.findFirst(problem: player2Problem,
      planCache: URL(fileURLWithPath: parameters.pathFileName + "/p2Plan"))
    let p2Plan = p2PlannerOutput?.0.last
    
    let result = Result(
      player1: p1Plan,
      player2: p2Plan,
      computationTimePlayer1: p1PlannerOutput?.4.last,
      computationTimePlayer2: p2PlannerOutput?.4.last
    )
    save(result: result)
  }
}
