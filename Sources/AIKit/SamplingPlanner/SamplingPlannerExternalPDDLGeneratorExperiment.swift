//
//  SamplingPlannerController.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 9/18/20.
//

import Foundation

public class SamplingPlannerExternalPDDLGeneratorExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  public struct TrialResult: Codable {
    public let player1: TemporalPlan
    public let player2: TemporalPlan
    public let computationTimePlayer1: Double
    public let computationTimePlayer2: Double
    public let player1DeadlinesStats: [String: [Int: Double]]
    public let player2DeadlinesStats: [String: [Int: Double]]
  }
  public struct Result: ExperimentData, Codable {
    public let results: [TrialResult]
    public let state: ExperimentDataItemState
  }
  
  public var parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  public let debugAllPlans: Bool = false
  
  public required init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  
  public func runBody() throws {
    let numberOfTrials = parameters["numberOfTrials"]?.integer ?? 1
    let samplingConfig = FRASPDDLGeneratorUtils.SamplingConfig(
      from: parameters.data)
    let outputDirURL = URL(fileURLWithPath: parameters.pathFileName, isDirectory: true)
    let fileManager = FileManager.default
    try fileManager.createDirectory(
      at: outputDirURL,
      withIntermediateDirectories: true,
      attributes: nil)
    
    guard let p1PlannerConfig = parameters["p1Planner"] else {
      fatalError("Invalid configuration file!")
    }
    guard let p2PlannerConfig = parameters["p2Planner"] else {
      fatalError("Invalid configuration file!")
    }
    let p1PddlPlanner = try PDDLPlannerFactory.make(
      by: p1PlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let p2PddlPlanner = try PDDLPlannerFactory.make(
      by: p2PlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let game = try PDDLTemporalGameOld(config: parameters.data)
    
    let p1Planner = SamplingAdversaryActionPlannerExternalPDDLGenerator(
      planner: p1PddlPlanner,
      config: samplingConfig,
      game: game)
    let p2Planner = SamplingAdversaryActionPlannerExternalPDDLGenerator(
      planner: p2PddlPlanner,
      config: samplingConfig,
      game: game)
    
    
    var subResults = [TrialResult]()
    for iterationNumber in 1...numberOfTrials {
      
      let planCacheDir = (iterationNumber == 1 || debugAllPlans) ?
      outputDirURL.appendingPathComponent("trial_\(iterationNumber)") : nil
      
      let p1PlannerStartTime = Date()
      let p1Plan = try p1Planner.findPlans(
        for: 1,
           maxNumberOfPlans: 1,
           plansCacheDir: planCacheDir)
      let p1TotalComputationTime = -p1PlannerStartTime.timeIntervalSinceNow
      let p1PlanDeadlinesStats = p1Planner.lastStats!
      
      let p2PlannerStartTime = Date()
      let p2Plan = try p2Planner.findPlans(
        for: 2,
           maxNumberOfPlans: 1,
           plansCacheDir:  planCacheDir)
      let p2TotalComputationTime = -p2PlannerStartTime.timeIntervalSinceNow
      let p2PlanDeadlinesStats = p2Planner.lastStats!
      
      let result = TrialResult(
        player1: p1Plan.plans.first!,
        player2: p2Plan.plans.first!,
        computationTimePlayer1: p1TotalComputationTime,
        computationTimePlayer2: p2TotalComputationTime,
        player1DeadlinesStats: p1PlanDeadlinesStats,
        player2DeadlinesStats: p2PlanDeadlinesStats)
      subResults.append(result)
    }
    save(result: Result(
      results: subResults,
      state: .finished))
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var fileNamePrefix: String {
    "SamplingPlannerResults"
  }
  public let recompute: Bool = false
}
