//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/22.
//

import Foundation
import MathLib

public class SamplingAdversaryActionPlanner {
  let planner: PDDLPlanner
  let game: PDDLGameFDRRepresentationAnalyticsLogic
  let samplingConfig: FRASPDDLGeneratorUtils.SamplingConfig
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    samplingConfig: FRASPDDLGeneratorUtils.SamplingConfig,
    planner: PDDLPlanner)
  {
    self.game = game
    self.samplingConfig = samplingConfig
    self.planner = planner
  }
  public func findPlan(for player: Int, planCacheFile: URL?) throws -> (
    bestResponseStrategy: [TemporalPlan],
    cost: [Double],
    log: AIKitData,
    computationDuration: TimeInterval,
    samplingDuration: TimeInterval,
    costFunction: [PDDLNameLiteral: [Int]])
  {
    precondition(player == 0 || player == 1)
    let adversary = (player + 1) % 2
    let samplingStarted = Date()
    let normalCostFunction = generateSamplingCostFunction(
      numberOfEstimates: samplingConfig.numberOfEstimates,
      player: player,
      adversary: adversary
    )
    let samplingDuration = -samplingStarted.timeIntervalSinceNow
    let criticalFactsWeights = Dictionary(
      uniqueKeysWithValues: normalCostFunction.map {
        ($0.key, game.getWeight(of: $0.key, for: player))})
    let weightedFactsCostFunction = PDDLCriticalFactCostUtils.scaleCostFunction(
      normalCostFunction,
      weights: criticalFactsWeights)
    let problem = game.game.getPlanningProblem(for: player)
      .convertingToClassicalPlanningProblem(removeDurationFunctions: true)
      .keepingOnly(player: player)
      .adding(
        criticalFactCostFunction: weightedFactsCostFunction,
        criticalFactCriticalActions: game.game.domain.findPossibleCriticalActionsAndFacts())
      .convertingSoftGoalsToHardGoalsRemovingMetrics()
      .addingTotalCostFunction()
      .addingMinimizeTotalCostMetric()
    let (plannerOutput, cost, log, _, duration) = try planner.findBest(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      planCache: planCacheFile)
    if !plannerOutput.isEmpty {
      let plan = TemporalPlan(
        classicalPlan: plannerOutput.last!.parsed!,
        durationOfUnitTimestamp: problem.durationOfUnitTimestamp!,
        units: problem.problem.units,
        removingNoopActions: true)
      return (
        [plan],
        cost,
        .string(log),
        duration.last! + samplingDuration,
        samplingDuration,
        weightedFactsCostFunction)
    } else {
      return (
        [],
        [],
        .string(log),
        (duration.last ?? 0) + samplingDuration,
        samplingDuration,
        weightedFactsCostFunction)
    }
  }
  
  public func generateSamplingCostFunction(
    numberOfEstimates: Int,
    player: Int,
    adversary: Int) -> [PDDLNameLiteral : [Int]]
  {
    let actionCriticalFacts = game.actionCriticalFacts
    let operators = game.groundedPlayersProblems[adversary].operators
    var factsDeadlines = [PDDLNameLiteral: [Int: Double]]()
    let adversaryStrategyEstimate = estimateAdversaryStrategy(
      numberOfEstimates: numberOfEstimates,
      player: player,
      adversary: adversary)
    for (criticalActionDeadlines, probability) in adversaryStrategyEstimate {
      for (criticalAction, deadline) in criticalActionDeadlines {
        for criticalFact in actionCriticalFacts[operators[criticalAction].asPlanAction]! {
          factsDeadlines[criticalFact, default: [:]][deadline, default: 0.0] += probability
        }
      }
    }
    return PDDLCriticalFactCostUtils.convert(factsDeadlines: factsDeadlines)
  }
  
  public func estimateAdversaryStrategy(
    numberOfEstimates: Int,
    player: Int,
    adversary: Int) -> [[Int: Int]: Double]
  {
    let operatorIndices = game.groundedPlayersProblems[adversary].operatorIndex
    let clusters = game.goalCriticalActionClusters[player].map {
      Set($0.adversarialActions.flatMap {$0.flatMap {$0.map {operatorIndices[$0]!}}})
    }
    var strategy = [[Int: Int]: Double]()
    for _ in 1...numberOfEstimates {
      let newPureStrategy = game.groundedPlayersProblems[adversary].estimateTimeAsAdversary(
        clusters: clusters)
      strategy[newPureStrategy, default: 0.0] += 1 / Double(numberOfEstimates)
    }
    return strategy
  }
}
