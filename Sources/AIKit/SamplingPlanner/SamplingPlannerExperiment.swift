//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/19/22.
//

import Foundation

public class SamplingPlannerExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = Result
  public struct Result: ExperimentDataResultDictionary, Codable {
    public let results: [TrialResult]
    public var avgComputationTimePlayer1: Double {
      results.map {$0.computationTimePlayer1}.avg!
    }
    public var stdComputationTimePlayer1: Double {
      results.map {$0.computationTimePlayer1}.std!
    }
    public var avgComputationTimePlayer2: Double {
      results.map {$0.computationTimePlayer2}.avg!
    }
    public var stdComputationTimePlayer2: Double {
      results.map {$0.computationTimePlayer2}.std!
    }
    public var avgSamplingTimePlayer1: Double {
      results.map {$0.samplingTimePlayer1}.avg!
    }
    public var stdSamplingTimePlayer1: Double {
      results.map {$0.samplingTimePlayer1}.std!
    }
    public var avgSamplingTimePlayer2: Double {
      results.map {$0.samplingTimePlayer2}.avg!
    }
    public var asDictionary: [String : AIKitData] {
      [
        "avgComputationTimePlayer1": .double(stdComputationTimePlayer1),
        "avgComputationTimePlayer2": .double(avgComputationTimePlayer2),
        "avgSamplingTimePlayer1": .double(avgSamplingTimePlayer1),
        "avgSamplingTimePlayer2": .double(avgSamplingTimePlayer2),
        "stdComputationTimePlayer1": .double(stdComputationTimePlayer1),
        "stdComputationTimePlayer2": .double(stdComputationTimePlayer2)
      ]
    }
  }
  public struct TrialResult: Codable {
    public let player1: TemporalPlan
    public let player2: TemporalPlan
    public let computationTimePlayer1: Double
    public let computationTimePlayer2: Double
    public let samplingTimePlayer1: Double
    public let samplingTimePlayer2: Double
    public let player1CostFunction: [PDDLNameLiteral: [Int]]
    public let player2CostFunction: [PDDLNameLiteral: [Int]]
    public let p1Log: AIKitData
    public let p2Log: AIKitData
  }
  public let parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  public let recompute = false
  public let fileNamePrefix = "SamplingPlannerResults"
  public let fileNameSufix = "json"
  private let debugAllPlans = false
  public required init(parameters: ExperimentParameters) {
    self.parameters = parameters
  }
  public func runBody() throws {
    let numberOfTrials = parameters["numberOfTrials"]?.integer ?? 1
    let samplingConfig = FRASPDDLGeneratorUtils.SamplingConfig(
      from: parameters.data)
    let game = try PDDLGameFDRRepresentationAnalyticsLogic.getInstance(by: parameters.data)
    guard let p1PlannerConfig = parameters.data["p1Planner"] else {
      fatalError("Invalid configuration file!")
    }
    guard let p2PlannerConfig = parameters.data["p2Planner"] else {
      fatalError("Invalid configuration file!")
    }
    let p1PddlPlanner = try PDDLPlannerFactory.make(
      by: p1PlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let p2PddlPlanner = try PDDLPlannerFactory.make(
      by: p2PlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let p1Planner = SamplingAdversaryActionPlanner(
      game: game,
      samplingConfig: samplingConfig,
      planner: p1PddlPlanner)
    let p2Planner = SamplingAdversaryActionPlanner(
      game: game,
      samplingConfig: samplingConfig,
      planner: p2PddlPlanner)
    
    let outputDirURL = URL(fileURLWithPath: parameters.pathFileName, isDirectory: true)
    var subResults = [TrialResult]()
    for iterationNumber in 1...numberOfTrials {
      let planCacheP1 = (iterationNumber == 1 || debugAllPlans) ?
      outputDirURL.appendingPathComponent("player_1_trial_\(iterationNumber).json") : nil
      let planCacheP2 = (iterationNumber == 1 || debugAllPlans) ?
      outputDirURL.appendingPathComponent("player_2_trial_\(iterationNumber).json") : nil
      
      let (
        p1Plans,
        _,
        p1Log,
        p1Duration,
        p1SamplingDuration,
        p1CostFunction) = try p1Planner.findPlan(for: 0, planCacheFile: planCacheP1)
      let (
        p2Plans,
        _,
        p2Log,
        p2Duration,
        p2SamplingDuration,
        p2CostFunction) = try p2Planner.findPlan(for: 1, planCacheFile: planCacheP2)
      let trialResult = TrialResult(
        player1: p1Plans.first!,
        player2: p2Plans.first!,
        computationTimePlayer1: p1Duration + p1SamplingDuration,
        computationTimePlayer2: p2Duration + p2SamplingDuration,
        samplingTimePlayer1: p1SamplingDuration,
        samplingTimePlayer2: p2SamplingDuration,
        player1CostFunction: p1CostFunction,
        player2CostFunction: p2CostFunction,
        p1Log: p1Log,
        p2Log: p2Log)
      subResults.append(trialResult)
    }
    resultData = Result(results: subResults)
  }
}
