//
//  DIRandomAdversaryActionPlanner.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 7/18/20.
//

import Foundation

public class SamplingAdversaryActionPlannerExternalPDDLGenerator: InitOracle {
  public typealias PS = TemporalPlan
  let pddlPlanner: PDDLRawPlanner
  public let game: PDDLTemporalGameOld
  public var config: FRASPDDLGeneratorUtils.SamplingConfig
  public let shortDescription = "Sampling planner"
  public private(set) var lastStats: [String: [Int: Double]]?
  
  public init(
    planner: PDDLRawPlanner,
    config: FRASPDDLGeneratorUtils.SamplingConfig,
    game: PDDLTemporalGameOld)
  {
    self.pddlPlanner = planner
    self.game = game
    self.lastStats = nil
    self.config = config
  }
    
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (
    plans: [PS],
    log: AIKitData,
    computationDuration: [TimeInterval])
  {
    let planCache = plansCacheDir?.appendingPathComponent("rapPlan\(player).json")
    let (classicalProblemPDDL, stats) = try game.generateSamplingPlannerPlaningProblem(
      for: player,
      config: config)
    self.lastStats = stats
    
    let (rawPlan, _, log , duration) = try pddlPlanner.findFirst(
      problemPDDL: classicalProblemPDDL.instance.stringRepresentation,
      domainPDDL: classicalProblemPDDL.domain.stringRepresentation,
      planCache: planCache)
    if let rawPlan = rawPlan.last {
      let (parsedPlan, _) = PlanParser.parsePlannerOutput(
        plannerOutput: rawPlan)
      let temporalPlan = TemporalPlan(
        classicalPlan: parsedPlan.parsed!,
        durationOfUnitTimestamp: 1,
        units: classicalProblemPDDL.instance.objects.filter {$0.typeName == PDDLDomain.unitType}.map {$0.name},
        removingNoopActions: true)
      return ([temporalPlan], .string(log), [duration.last!])
    } else {
      throw AIKitError(message: "Cannot find init plan for player \(player)")
    }
  }
}
