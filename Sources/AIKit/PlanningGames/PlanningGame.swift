//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/23/21.
//

import Foundation

public struct PlanningGame {
  //public static let adversaryCostCoefficient = 205 //5 - for transport domain
  public static let operatorCostMultiplier = 1
  public struct Config: Codable {
    public enum Mode: String, Codable {
      case costAdditive, costMultiplicative, costZero
    }
    public let mode: Mode
    public let adversaryActionsNames: [String]
    public let adversaryCostCoefficient: Int
  }
  public let problem: PDDLPlanningProblem
  public let config: Config
  
  public init(
    domain: String,
    problem: String,
    config: Config
  ) throws {
    let domain = try PDDLDomainProblemParser.parse(domain: domain)
    let problem = try PDDLDomainProblemParser.parse(problem: problem)
    self.problem = PDDLPlanningProblem(domain: domain, problem: problem)
    self.config = config
//    if let adversaryActionsNames = adversaryActionsNames {
//      self.adversaryActionsNames = adversaryActionsNames
//    } else {
//      self.adversaryActionsNames = domain.actions.map {$0.name}
//    }
  }
}

public final class PlanningGameFDRRepresentation {
  public let game: PlanningGame
  public lazy var representationSymkFDR: FDRCoding = {
    let symkGrounder = FastDownwardGrounder(
      temporal: false,
      runPreprocessing: true,
      configKey: "SymKFastDownwardGrounderPath",
      configKeyPreprocessing: "SymKFastDownwardPreprocessingPath")
    let fdrSymKRepresentation = try! FastDownwardSASParser.parseSAS(
      symkGrounder.ground(game.problem, maxTime: 1800)!.sas)
    return fdrSymKRepresentation
  }()
  public lazy var representationFDR: FDRCoding = {
//    let grounder = FastDownwardGrounder(temporal: false)
//    let outputSAS = try! grounder.ground(problemPDDL: game.problem.problem.text, domainPDDL: game.problem.domain.text)!.sa    
    let outputSAS = try! PythonInteroperabilityManager.defaultManager.fastdownward_translate(
      domainPddl: game.problem.domain.text,
      problemPddl: game.problem.problem.text,
      verbose: false)
    let representation = FastDownwardSASParser.parseSAS(
      outputSAS,
      operatorCostMultiplier: PlanningGame.operatorCostMultiplier)
    return representation
  }()
  public lazy var criticalOperatorsIndices: [Int] = {
    let nameSet = Set(game.config.adversaryActionsNames)
    return representationFDR.operators.filter {
      nameSet.contains($0.value.name)
    }.map {$0.key}.sorted()
  }()
  public init(game: PlanningGame) {
    self.game = game
  }
}
