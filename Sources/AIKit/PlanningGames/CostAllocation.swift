//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/23/21.
//

import Foundation

public struct CostAllocation: Hashable, Codable {
  public let allocation: [Int: Int]
  public subscript(_ idx: Int) -> Int? {
    return allocation[idx]
  }
}

extension CostAllocation: PureStrategy {}
