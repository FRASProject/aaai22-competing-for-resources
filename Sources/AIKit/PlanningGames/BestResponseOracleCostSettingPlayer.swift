//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public final class BestResponseOracleCostSettingPlayer: BestResponseOracle {
  public typealias PS = CostAllocation
  public typealias OPPONENTPS = ClassicalPlan
  
  public var shortDescription: String {
    "BestResponseOracleCostSettingPlayer"
  }
  private let representationFDR: FDRCoding
  private let config: PlanningGame.Config
  private let criticalOperatorsIndices: [Int]
  public init(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int],
    config: PlanningGame.Config) {
      self.representationFDR = representationFDR
      self.criticalOperatorsIndices = criticalOperatorsIndices
      self.config = config
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<ClassicalPlan>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [CostAllocation],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    //let numberOfOperators = game.representationFDR.operators.count
    let operatorNameIndex = representationFDR.operatorNameIndex
    let operators = representationFDR.operators
    let startTime = Date()
    var maxCost = 0.0
    var maximizingOperatorIndex = criticalOperatorsIndices.first!
//    operators.filter {$0.value.name == "pick-up"}.forEach {
//      print($0.value.description)
//    }
    for operatorIndex in criticalOperatorsIndices {
      let cost = zip(opponentStrat.distribution, opponentStrat.strategies).map {
        (prob, plan) -> Double in
        Double(plan.actions.map { action -> Int in
          let actionIndex = operatorNameIndex[action]!
          if operatorIndex == actionIndex {
            switch config.mode {
            case .costMultiplicative:
              return operators[actionIndex]!.cost * config.adversaryCostCoefficient
            case .costAdditive:
              return operators[actionIndex]!.cost + config.adversaryCostCoefficient
            case .costZero:
              return operators[actionIndex]!.cost
            }
          } else {
            switch config.mode {
            case .costAdditive, .costMultiplicative:
              return operators[actionIndex]!.cost
            case .costZero:
              return 0
            }
          }
        }.sum) * prob
      }.sum
      if cost > maxCost {
        maxCost = cost
        maximizingOperatorIndex = operatorIndex
      }
    }
    print("Maxcost action: \(representationFDR.operators[maximizingOperatorIndex]!.description)")
    let costAllocation: CostAllocation
    switch config.mode {
    case .costMultiplicative, .costAdditive:
      costAllocation = CostAllocation(
        allocation: [maximizingOperatorIndex: config.adversaryCostCoefficient])
    case .costZero:
      costAllocation = CostAllocation(
        allocation: [maximizingOperatorIndex: operators[maximizingOperatorIndex]!.cost])
    }
    return (
      bestResponseStrategy: [costAllocation],
      cost: [maxCost],
      log: "NA",
      computationDuration: -startTime.timeIntervalSinceNow
    )
  }
}
