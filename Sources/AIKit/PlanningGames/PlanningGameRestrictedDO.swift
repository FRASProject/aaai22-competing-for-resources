//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/2/21.
//

import Foundation

public final class PlanningGameRestrictedDO: ExperimentProtocol {
  public typealias ResultData = DoubleOracleResults<ClassicalPlan, CostAllocation>
  public private(set) var saveData: Bool = false
  
  public var description: String {
    "PlanningGameDoubleOracle " + parameters.pathDir
  }
  public let planningGame: PlanningGameFDRRepresentation
  public let planningStrategies: [ClassicalPlan]
  
  public var parameters: ExperimentParameters
  public var resultData: ResultData?
  public var fromCache: Bool?
  public var failError: Error?
  
  public init(
    parameters: ExperimentParameters,
    planningGame: PlanningGameFDRRepresentation,
    planningStrategies: [ClassicalPlan])
  {
    self.planningGame = planningGame
    self.planningStrategies = planningStrategies
    self.parameters = parameters
  }
  
  public func runBody() throws
  {
    guard !planningStrategies.isEmpty else {
      return
    }
    let doubleOracleConfiguration = try PlanningGamesFactory.makeRestricted(
      representationFDR: planningGame.representationFDR,
      criticalOperatorsIndices: planningGame.criticalOperatorsIndices,
      strategies: planningStrategies,
      config: parameters.data,
      gameConfig: planningGame.game.config)
    let result = try DoubleOracleUtils.runDoubleOracle(
      doubleOracleConfiguration: doubleOracleConfiguration,
      outputDirName: saveData ? parameters.pathFileName : nil,
      plansCachePath: nil, saveResult: false)
    save(result: result)
  }
  
  public var fileNamePrefix: String {
    "AllResultsRestricted"
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var recompute: Bool {
    false
  }
}
