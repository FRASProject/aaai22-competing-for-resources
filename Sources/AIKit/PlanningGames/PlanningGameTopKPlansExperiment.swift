//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/31/21.
//

import Foundation

public final class PlanningGameTopKPlansExperiment: ExperimentProtocol {
  public typealias ResultData = Result
  public struct Result: ExperimentData, ExperimentDataResultDictionary {
    public let plans: [ClassicalPlan]
    public let plansComputationDurations: [Double]
    public let state: ExperimentDataItemState
    public var asDictionary: [String : AIKitData] {
      ["totalTimeKPlans": .double(plansComputationDurations.last ?? 0),
       "numberOfKPlans": .integer(plans.count)]
    }
  }
  private let costPlayerStrategy: MixedStrategy<CostAllocation>
  public let game: PlanningGameFDRRepresentation
  public var parameters: ExperimentParameters
  public var resultData: Result?
  public var fromCache: Bool?
  public var failError: Error?
  public init(
    costPlayerStrategy: MixedStrategy<CostAllocation>,
    game: PlanningGameFDRRepresentation,
    parameters: ExperimentParameters) {
      self.costPlayerStrategy = costPlayerStrategy
      self.game = game
      self.parameters = parameters
    }
  public func runBody() throws {
    
    let planningPDDLProblem = game.game.problem
    let symkGrounder = FastDownwardGrounder(
      temporal: false,
      runPreprocessing: true,
      configKey: "SymKFastDownwardGrounderPath",
      configKeyPreprocessing: "SymKFastDownwardPreprocessingPath")
    let fdrSymKRepresentation = try FastDownwardSASParser.parseSAS(
      symkGrounder.ground(planningPDDLProblem, maxTime: 1800)!.sas,
      operatorCostMultiplier: PlanningGame.operatorCostMultiplier)
    let translationTable = game.representationFDR.operatorIdxTranslationTableTo(other: fdrSymKRepresentation)
    let translatedCostPlayerStrategy = MixedStrategy(
      costPlayerStrategy.strategies.map {
        CostAllocation(
          allocation: Dictionary(
            uniqueKeysWithValues: $0.allocation.compactMap {
              if let operatorIdx = translationTable[$0.key] {
                return (operatorIdx, $0.value)
              } else {
                return nil
              }
            }))
      },
      distribution: costPlayerStrategy.distribution)
    
    let planner = try FDRPlannerFactory.make(
      by: parameters.data["kPlans"]!,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let bestResponseOracle = BestResponseOraclePlanningPlayer(
      representationFDR: fdrSymKRepresentation,
      config: game.game.config,
      planner: planner,
      criticalOperatorsIndices: game.criticalOperatorsIndices)
    _ = try bestResponseOracle.findBestResponse(
      player: 1,
      to: 2,
      opponentStrat: translatedCostPlayerStrategy,
      utilityLowerBound: 0,
      plansCacheFile: nil,
      continueFunction: nil,
      solutionChecker: AlwaysBadSolutionChecker())
    let kPlans = bestResponseOracle.plans
    let kPlansComputationDurations = bestResponseOracle.plansComputationDurations
    print("costs: \(bestResponseOracle.plansCosts)")
    print("kplans: \(kPlans.count) - \(Set(kPlans).count)")
    
    if !kPlans.isEmpty {
      let commonActions = kPlans.map {Set($0.actions)}.reduce(Set(kPlans.first!.actions)) {
        $0.intersection($1)
      }
      print("Common: \(commonActions)")
    }
    
    let result = Result(
      plans: bestResponseOracle.plans,
      plansComputationDurations: kPlansComputationDurations,
      state: .finished)
    save(result: result)
  }
  public var fileNamePrefix: String {
    "kPlans"
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var recompute: Bool {
    false
  }
}
