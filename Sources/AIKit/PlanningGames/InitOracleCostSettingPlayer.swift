//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public final class InitOracleCostSettingPlayer: InitOracle {
  public typealias PS = CostAllocation
  
  public var shortDescription: String {
    "InitOracleCostSettingPlayer"
  }
  private let criticalOperatorsIndices: [Int]
  private let representationFDR: FDRCoding
  private let config: PlanningGame.Config
  public init(
    criticalOperatorsIndices: [Int],
    representationFDR: FDRCoding,
    config: PlanningGame.Config)
  {
    self.criticalOperatorsIndices = criticalOperatorsIndices
    self.config = config
    self.representationFDR = representationFDR
  }
  
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (
      plans: [CostAllocation],
      log: AIKitData,
      computationDuration: [TimeInterval])
  {
    let middleOperatorIdx = criticalOperatorsIndices[criticalOperatorsIndices.count / 2]
    let allocation: CostAllocation
    switch config.mode {
    case .costAdditive, .costMultiplicative:
      allocation = CostAllocation(allocation: [middleOperatorIdx: config.adversaryCostCoefficient])
    case .costZero:
      allocation = CostAllocation(
        allocation: [middleOperatorIdx: representationFDR.operators[middleOperatorIdx]!.cost])
    }
    return (plans: [allocation], log: "NA", computationDuration: [0])
  }
}
