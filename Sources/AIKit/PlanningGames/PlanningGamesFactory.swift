//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public enum PlanningGamesFactory {
  public static func make(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int],
    config: AIKitData,
    gameConfig: PlanningGame.Config
  ) throws -> DoubleOracleConfiguration<
    PlanCostFunctionEvaluator,
    InitOraclePlanningPlayer,
    BestResponseOraclePlanningPlayer,
    InitOracleCostSettingPlayer,
    BestResponseOracleCostSettingPlayer>
  {
    guard let initPlannerConfig = config["initPlanner"],
          let bestResponsePlannerConfig = config["bestResponsePlanner"]
    else {
      fatalError("Invalid configuration file!")
    }
    let initPlanner = try FDRPlannerFactory.make(
      by: initPlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let initSymk = initPlannerConfig["pddlPlannerName"]?.string == "FDSYMK"
    let bestResponsePlanner = try FDRPlannerFactory.make(
      by: bestResponsePlannerConfig,
      frasConfig: ExperimentManager.defaultManager.systemConfig,
      fastDownwardAliases: ExperimentManager.defaultManager.fastDownwardAliases)
    let brSymk = bestResponsePlannerConfig["pddlPlannerName"]?.string == "FDSYMK"
    let planningInitOracle = InitOraclePlanningPlayer(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: gameConfig,
      planner: initPlanner,
      mode: initSymk ? .allPlans : .lastPlanOnly)
    let costAllocationInitOracle = InitOracleCostSettingPlayer(
      criticalOperatorsIndices: criticalOperatorsIndices,
      representationFDR: representationFDR,
      config: gameConfig)
    let planningBestResponseOracle = BestResponseOraclePlanningPlayer(
      representationFDR: representationFDR,
      config: gameConfig,
      planner: bestResponsePlanner,
      criticalOperatorsIndices: criticalOperatorsIndices,
      mode: brSymk ? .allPlans : .lastPlanOnly)
    let costAllocationBestResponseOracle = BestResponseOracleCostSettingPlayer(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: gameConfig)
    let evaluator = PlanCostFunctionEvaluator(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: gameConfig)
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: planningInitOracle,
      initialPlansPlannerP2: costAllocationInitOracle,
      bestResponsePlannerP1: planningBestResponseOracle,
      bestResponsePlannerP2: costAllocationBestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
  public static func makeRestricted(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int],
    strategies: [ClassicalPlan],
    config: AIKitData,
    gameConfig: PlanningGame.Config
  ) throws -> DoubleOracleConfiguration<
    PlanCostFunctionEvaluator,
    SingleStrategyInitOracle<ClassicalPlan>,
    FiniteStrategiesBestResponseOracle<
      ClassicalPlan,
      CostAllocation,
      PlanCostFunctionEvaluator>,
    InitOracleCostSettingPlayer,
    BestResponseOracleCostSettingPlayer>
  {
    let evaluator = PlanCostFunctionEvaluator(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: gameConfig)
    let planningInitOracle = SingleStrategyInitOracle(initStrategy: strategies.first!)
    let costAllocationInitOracle = InitOracleCostSettingPlayer(
      criticalOperatorsIndices: criticalOperatorsIndices,
      representationFDR: representationFDR,
      config: gameConfig)
    let planningBestResponseOracle = FiniteStrategiesBestResponseOracle(
      pureStrategies: strategies,
      evaluator: evaluator)
    let costAllocationBestResponseOracle = BestResponseOracleCostSettingPlayer(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: gameConfig)
    
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: planningInitOracle,
      initialPlansPlannerP2: costAllocationInitOracle,
      bestResponsePlannerP1: planningBestResponseOracle,
      bestResponsePlannerP2: costAllocationBestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
}
