//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public final class InitOraclePlanningPlayer: InitOracle {
  public typealias PS = ClassicalPlan
  public var shortDescription: String {
    "InitOraclePlanningPlaye"
  }
  private let planner: FDRPlanner
  private let representationFDR: FDRCoding
  private let criticalOperatorsIndices: [Int]
  private let config: PlanningGame.Config
  private let mode: BestResponseOraclePlanningPlayer.Mode
  
  public init(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int],
    config: PlanningGame.Config,
    planner: FDRPlanner,
    mode: BestResponseOraclePlanningPlayer.Mode = .lastPlanOnly)
  {
    self.representationFDR = representationFDR
    self.planner = planner
    self.criticalOperatorsIndices = criticalOperatorsIndices
    self.config = config
    self.mode = mode
  }
  
  public func findPlans(
    for player: Int,
    maxNumberOfPlans: Int,
    plansCacheDir: URL?) throws -> (
      plans: [ClassicalPlan],
      log: AIKitData,
      computationDuration: [TimeInterval])
  {
    let planCache = plansCacheDir.map {$0.appendingPathComponent("initPlan1Player\(player).json")}
    let fdrProblem: FDRCoding
    switch config.mode {
    case .costMultiplicative, .costAdditive:
      fdrProblem = representationFDR
    case .costZero:
      fdrProblem = representationFDR.settingCostFunction(
        Dictionary(uniqueKeysWithValues: criticalOperatorsIndices.map {($0, 0)}),
        zeroElsewhere: false)
    }
    let (plannerOutput, _, log, _, duration) = try planner.findFirst(
      representationFDR: fdrProblem.text,
      planCache: planCache)
    if plannerOutput.isEmpty {
      throw AIKitError(message: "Cannot find init plan for player \(player)")
    }
    switch mode {
    case .lastPlanOnly:
      let plan = plannerOutput.last!.parsed!
      return ([plan], .string(log), duration)
    case .allPlans:
      let plans = plannerOutput.map {$0.parsed!}
      return (plans, .string(log), duration)
    }
  }
}
