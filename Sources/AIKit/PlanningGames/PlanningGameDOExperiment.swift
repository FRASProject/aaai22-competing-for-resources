//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/23/21.
//

import Foundation

public class PlanningGameDOExperiment: ExperimentInitiableByConfig {
  public typealias ResultData = DoubleOracleResults<ClassicalPlan, CostAllocation>
  public var parameters: ExperimentParameters
  public var resultData: ResultData?
  public var fromCache: Bool?
  public var failError: Error?
  public private(set) var saveData: Bool = true
  
  public var description: String {
    "PlanningGameDoubleOracle " + parameters.pathDir
  }
  private let representationFDR: FDRCoding
  private let criticalOperatorsIndices: [Int]
  private let gameConfig: PlanningGame.Config
  public init(
    representationFDR: FDRCoding,
    criticalOperatorsIndices: [Int],
    gameConfig: PlanningGame.Config,
    parameters: ExperimentParameters)
  {
    self.representationFDR = representationFDR
    self.criticalOperatorsIndices = criticalOperatorsIndices
    self.gameConfig = gameConfig
    self.parameters = parameters
  }
  public required init(parameters: ExperimentParameters) {
    let planningGame = try! ExperimentManager.defaultManager.getPlanningGame(parameters.data)
    if parameters.data["initPlanner"]?["initPlanner"]?["pddlPlannerName"]?.string == "FDSYMK" ||
        parameters.data["initPlanner"]?["bestResponsePlanner"]?["pddlPlannerName"]?.string == "FDSYMK"
    {
      self.representationFDR = planningGame.representationSymkFDR
    } else {
      self.representationFDR = planningGame.representationFDR
    }
    self.criticalOperatorsIndices = planningGame.criticalOperatorsIndices
    self.gameConfig = planningGame.game.config
    self.parameters = parameters
  }
  public func runBody() throws
  {
    let doubleOracleConfiguration = try PlanningGamesFactory.make(
      representationFDR: representationFDR,
      criticalOperatorsIndices: criticalOperatorsIndices,
      config: parameters.data,
      gameConfig: gameConfig)
    let result = try DoubleOracleUtils.runDoubleOracle(
      doubleOracleConfiguration: doubleOracleConfiguration,
      outputDirName: saveData ? parameters.pathFileName : nil,
      plansCachePath: nil, saveResult: false)
    save(result: result)
  }
  
  public var fileNamePrefix: String {
    "AllResults"
  }
  public var fileNameSufix: String {
    ".json"
  }
  public var recompute: Bool {
    false
  }
  
}

