//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/24/21.
//

import Foundation

public final class BestResponseOraclePlanningPlayer: BestResponseOracle {
  public enum Mode {
    case lastPlanOnly, allPlans
  }
  public typealias PS = ClassicalPlan
  public typealias OPPONENTPS = CostAllocation
  public var shortDescription: String {
    "BestResponseOraclePlanningPlayer"
  }
  
  private let planner: FDRPlanner
  private let config: PlanningGame.Config
  private let representationFDR: FDRCoding
  private let criticalOperatorsIndices: [Int]
  private let mode: Mode
  public private(set) var plans: [PS]
  public private(set) var plansComputationDurations: [Double]
  public private(set) var plansCosts: [Double]
  public init(
    representationFDR: FDRCoding,
    config: PlanningGame.Config,
    planner: FDRPlanner,
    criticalOperatorsIndices: [Int],
    mode: Mode = .lastPlanOnly
  ) {
    self.representationFDR = representationFDR
    self.planner = planner
    self.config = config
    self.plans = []
    self.plansComputationDurations = []
    self.plansCosts = []
    self.criticalOperatorsIndices = criticalOperatorsIndices
    self.mode = mode
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<CostAllocation>,
    utilityLowerBound: Double, plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [ClassicalPlan],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    
    let filteredOpponentStrat = zip(opponentStrat.distribution, opponentStrat.strategies)
      .filter {
        $0.0 > AIKitConfig.defaultEpsilon
      }
    
    let involvedOperators = Dictionary(filteredOpponentStrat.map {$0.1}.flatMap {$0.allocation.keys}.map{($0, 1)}, uniquingKeysWith: {a, _ in a})
    
    let costFunctionNotReduced = filteredOpponentStrat.map {
      (prob, allocation) in
      allocation.allocation.merging(involvedOperators, uniquingKeysWith: {a, _ in a}).mapValues {
        prob * Double($0)}
    }
    let costFunction = costFunctionNotReduced.reduce([:]) {
      $0.merging($1, uniquingKeysWith: + )
    }
    let fdrProblem: FDRCoding
    switch config.mode {
    case .costMultiplicative:
      fdrProblem = representationFDR.mutliplyingCostFunctionBy(factors: costFunction)
    case .costAdditive:
      fdrProblem = representationFDR.increasingCostFunctionBy(
        increments: costFunction.mapValues {Int($0.rounded())})
    case .costZero:
      let criticalOperatorsCost = Dictionary(
        uniqueKeysWithValues: criticalOperatorsIndices.map {($0, 0)})
      fdrProblem = representationFDR.settingCostFunction(
        costFunction.mapValues {Int($0.rounded())}.merging(
          criticalOperatorsCost,
          uniquingKeysWith: { a, _ in a }),
        zeroElsewhere: false)
    }
    let (plannerOutput, cost, log, _, duration) = try planner.findBest(
      representationFDR: fdrProblem.text,
      planCache: plansCacheFile)
    self.plans = plannerOutput.map {$0.parsed!}
    self.plansCosts = cost
    self.plansComputationDurations = duration
    if !plannerOutput.isEmpty {
      switch mode {
      case .lastPlanOnly:
        return ([plannerOutput.last!.parsed!], [cost.last!], .string(log), duration.last!)
      case .allPlans:
        let plans = plannerOutput.map {
          $0.parsed!
        }
        return (plans, cost, .string(log), duration.last!)
      }
    } else {
      return ([], [], .string(log), duration.last ?? 0)
    }
  }
  
  
}
