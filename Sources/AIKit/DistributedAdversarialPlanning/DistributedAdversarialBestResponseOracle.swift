//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/5/21.
//

import Foundation

public final class DistributedAdversarialBestResponseOracle: BestResponseOracle {
  public typealias PS = TemporalPlan
  public typealias OPPONENTPS = TemporalPlan
  
  private let game: PDDLGameFDRRepresentationAnalyticsLogic
  private let pddlPlanner: PDDLTemporalPlanner
  private let config: AIKitData
  
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    pddlPlanner: PDDLTemporalPlanner,
    config: AIKitData)
  {
    self.game = game
    self.pddlPlanner = pddlPlanner
    self.config = config
  }
  
  public func findBestResponse(
    player: Int,
    to opponent: Int,
    opponentStrat: MixedStrategy<TemporalPlan>,
    utilityLowerBound: Double,
    plansCacheFile: URL?,
    continueFunction: (() -> Bool)?,
    solutionChecker: SolutionChecker) throws -> (
      bestResponseStrategy: [TemporalPlan],
      cost: [Double],
      log: AIKitData,
      computationDuration: TimeInterval)
  {
    assert(player == 1 || player == 2)
    assert(opponent == 1 || opponent == 2)
    assert(player != opponent)
    let planCacheDir = plansCacheFile.map {$0.deletingLastPathComponent()}
    let cacheFilePrefix = plansCacheFile.map {$0.lastPathComponent} ?? ""
    let planCache = planCacheDir.map {$0.appendingPathComponent(cacheFilePrefix + "jointPlan.sol")}
    let player = player - 1
    let opponent = opponent - 1
    let normalCostFunction = PDDLCriticalFactCostUtils.extractCostFunction(
      from: opponentStrat,
      units: Set(game.playersUnits[opponent]),
      actionCriticalFacts: game.actionWithoutUnitsCriticalFacts).merging(
        game.criticalFacts.map {($0, [0])}, uniquingKeysWith: { a, _ in a })
    let criticalFactsWeights = Dictionary(
      uniqueKeysWithValues: normalCostFunction.map {
        ($0.key, game.getWeight(of: $0.key, for: player))})
    let weightedFactsCostFunction = PDDLCriticalFactCostUtils.scaleCostFunction(
      normalCostFunction,
      weights: criticalFactsWeights)
    PDDLCriticalFactCostUtils.printCostFunction(weightedFactsCostFunction)
    let distributedPlanner = try DistributedAdversarialPlanner(
      game: game,
      player: player,
      planner: pddlPlanner,
      costFunction: weightedFactsCostFunction,
      config: config,
      planCacheDir: planCacheDir,
      cacheFilePrefix: cacheFilePrefix)
    let (plan, planningDuration, simAnnDuration) = try distributedPlanner.computeOptimizedPlan()
    if let planCache = planCache {
      try plan.text.write(to: planCache, atomically: false, encoding: .utf8)
    }
    return (
      [plan],
      [0],
      .dictionary([
        "planningDuration": .double(planningDuration),
        "simAnnDuration": .double(simAnnDuration)]),
      planningDuration + simAnnDuration)
  }
  public let shortDescription: String = "DistributedAdversarialBRPlanner"
}
