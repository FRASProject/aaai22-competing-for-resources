//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/24/21.
//

import Foundation

public enum DistributedAdversarialDOConfigurationFactory {
  public static func makeDistributedAdversarial(
    for game: PDDLGameFDRRepresentationAnalyticsLogic,
    config parameters: AIKitData
  ) throws -> DoubleOracleConfiguration<
    PDDLPlansEvaluator,
    DistributedAdversarialInitOracle,
    DistributedAdversarialBestResponseOracle,
    DistributedAdversarialInitOracle,
    DistributedAdversarialBestResponseOracle>
  {
    let planners: (
      p1Init: PDDLTemporalPlanner,
      p2Init: PDDLTemporalPlanner,
      p1BR: PDDLTemporalPlanner,
      p2BR: PDDLTemporalPlanner)

    if let temporalPlanners = try? PDDLTemporalDoubleOracleConfigurationFactory.makeTemporalPlanners(
      config: parameters)
    {
      planners = temporalPlanners
    } else {
      let classicalPlanners = try PDDLDoubleOracleConfigurationFactory.makePlanners(
        config: parameters)
      planners = (
        p1Init: ClassicalToTemporalAdapter(classicalPlanner: classicalPlanners.p1Init),
        p2Init: ClassicalToTemporalAdapter(classicalPlanner: classicalPlanners.p2Init),
        p1BR: ClassicalToTemporalAdapter(classicalPlanner: classicalPlanners.p1BR),
        p2BR: ClassicalToTemporalAdapter(classicalPlanner: classicalPlanners.p2BR)
      )
    }
    
    let evaluator = PDDLPlansEvaluator(game: game)
    let player1InitOracle = DistributedAdversarialInitOracle(
      game: game,
      pddlPlanner: planners.p1Init,
      config: parameters)
    let player2InitOracle = DistributedAdversarialInitOracle(
      game: game,
      pddlPlanner: planners.p2Init,
      config: parameters)
    let player1BestResponseOracle = DistributedAdversarialBestResponseOracle(
      game: game,
      pddlPlanner: planners.p1BR,
      config: parameters)
    let player2BestResponseOracle = DistributedAdversarialBestResponseOracle(
      game: game,
      pddlPlanner: planners.p2BR,
      config: parameters)
    let configuration = DoubleOracleConfiguration(
      numberOfInitialPlans: 1,
      maximumNumberOfAttemptsToImproveEquilibriumApprox: 2,
      initialPlansPlannerP1: player1InitOracle,
      initialPlansPlannerP2: player2InitOracle,
      bestResponsePlannerP1: player1BestResponseOracle,
      bestResponsePlannerP2: player2BestResponseOracle,
      strategiesEvaluator: evaluator,
      epsilon: AIKitConfig.defaultEpsilon,
      name: "Configuration name not used")
    return configuration
  }
}
