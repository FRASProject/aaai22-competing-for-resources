//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/6/21.
//

import Foundation
import python3embed
import CAIKit

@globalActor
public actor PythonManager {
  public static var shared = PythonManager()
  private var modules: [String: UnsafeMutablePointer<PyObject>]
  private var initialized: Bool
  private var initializedFastDownward: Bool
  private init() {
    self.modules = [:]
    self.initialized = false
    self.initializedFastDownward = false
  }
  public func initPython() {
    guard initialized == false else { return }
    Py_Initialize()
    initialized = true
  }
  public func deinitPython() {
    for (_, module) in modules {
      Py_DecRef(module)
    }
    self.modules = [:]
    Py_Finalize()
    initialized = false
  }
  deinit {
    deinitPython()
  }
  public func importModule(name: String) throws {
    precondition(initialized)
    let moduleName = PyUnicode_DecodeFSDefault(name)
    let module = PyImport_Import(moduleName)
    Py_DecRef(moduleName)
    guard let module = module else {
      PyErr_Print()
      throw AIKitError(message: "Python: cannot import module \(name)")
    }
    modules[name] = module
  }
  public func addModulePath(_ path: String) {
    precondition(initialized)
    let sysPath = PySys_GetObject("path")
    let modulePath = PyUnicode_FromString(path)
    PyList_Append(sysPath, modulePath)
    Py_DecRef(modulePath)
  }
  public func initFastDownward() {
    guard initializedFastDownward == false else { return }
    guard let translatorPath = ExperimentManager.defaultManager
            .systemConfig["FastDownwardGrounderPythonModulePath"]?.string else {
              fatalError("Cannot initialize FastDownward translator")
            }
    initPython()
    addModulePath(translatorPath)
    initializedFastDownward = true
  }
  public func fastdownward_translate(
    domainPddl: String,
    problemPddl: String,
    verbose: Bool) throws -> (String, TimeInterval)
  {
    initFastDownward()
    if modules["translate2"] == nil {
      try importModule(name: "translate2")
    }
    let translateModule = modules["translate2"]!
    let pFunc = PyObject_GetAttrString(translateModule, "main2")
    guard let pFunc = pFunc else {
      throw AIKitError(message: "Python: cannot find function translate2.main2")
    }
    guard PyCallable_Check(pFunc) != 0 else {
      Py_DecRef(pFunc)
      throw AIKitError(message: "Python: cannot execute function translate2.main2")
    }
    let pythonDomainString = PyUnicode_DecodeFSDefault(domainPddl)!
    let pythonProblemString = PyUnicode_DecodeFSDefault(problemPddl)!
    let pythonVerbose = PyLong_FromLong(verbose ? 1 : 0)!
    let pythonTuple = PyTuple_New(3)!
    PyTuple_SetItem(pythonTuple, 0, pythonDomainString) // Reference stolen
    PyTuple_SetItem(pythonTuple, 1, pythonProblemString) // Reference stolen
    PyTuple_SetItem(pythonTuple, 2, pythonVerbose) // Reference stolen
    let startTime = Date()
    let returnValue = PyObject_CallObject(pFunc, pythonTuple)
    Py_DecRef(pythonTuple)
    let returnValueString = PyObject_Str(returnValue)
    let returnValueString2 = PyUnicode_AsEncodedString(returnValueString, "utf-8", "strict")
    let returnValueString2Buffer = PyBytes_AsString(returnValueString2)
    let finalString = String(cString: returnValueString2Buffer!)
    Py_DecRef(returnValue)
    Py_DecRef(returnValueString)
    Py_DecRef(returnValueString2)
    Py_DecRef(pFunc)
    return (finalString, -startTime.timeIntervalSinceNow)
  }
}

public final class PythonInteroperabilityManager {
  private static var _defaultManager: PythonInteroperabilityManager?
  private static let semaphore = DispatchSemaphore(value: 1)
  public static var defaultManager: PythonInteroperabilityManager {
    PythonInteroperabilityManager.semaphore.wait()
    if let manager = _defaultManager {
      PythonInteroperabilityManager.semaphore.signal()
      return manager
    } else {
      _defaultManager = PythonInteroperabilityManager()
      PythonInteroperabilityManager.semaphore.signal()
      return _defaultManager!
    }
  }
  private var modules: [String: UnsafeMutablePointer<PyObject>]
  private init() {
    self.modules = [:]
    Py_Initialize()
    if let translatorPath = ExperimentManager.defaultManager
        .systemConfig["FastDownwardGrounderPythonModulePath"]?.string
    {
      let sysPath = PySys_GetObject("path")
      let modulePath = PyUnicode_FromString(translatorPath)
      PyList_Append(sysPath, modulePath)
      Py_DecRef(modulePath)
    }
  }
  deinit {
    for (_, module) in modules {
      Py_DecRef(module)
    }
    self.modules = [:]
    Py_Finalize()
  }
  private func importModule(name: String) throws {
    let moduleName = PyUnicode_DecodeFSDefault(name)
    let module = PyImport_Import(moduleName)
    Py_DecRef(moduleName)
    guard let module = module else {
      PyErr_Print()
      throw AIKitError(message: "Python: cannot import module \(name)")
    }
    modules[name] = module
  }
  
  public func fastdownward_translate(
    domainPddl: String,
    problemPddl: String,
    verbose: Bool) throws -> String
  {
    PythonInteroperabilityManager.semaphore.wait()
    defer {
      PythonInteroperabilityManager.semaphore.signal()
    }
    if modules["translate2"] == nil {
      try importModule(name: "translate2")
    }
    let translateModule = modules["translate2"]!
    let pFunc = PyObject_GetAttrString(translateModule, "main2")
    guard let pFunc = pFunc else {
      throw AIKitError(message: "Python: cannot find function translate2.main2")
    }
    guard PyCallable_Check(pFunc) != 0 else {
      Py_DecRef(pFunc)
      throw AIKitError(message: "Python: cannot execute function translate2.main2")
    }
    let pythonDomainString = PyUnicode_DecodeFSDefault(domainPddl)!
    let pythonProblemString = PyUnicode_DecodeFSDefault(problemPddl)!
    let pythonVerbose = PyLong_FromLong(verbose ? 1 : 0)!
    let pythonTuple = PyTuple_New(3)!
    PyTuple_SetItem(pythonTuple, 0, pythonDomainString) // Reference stolen
    PyTuple_SetItem(pythonTuple, 1, pythonProblemString) // Reference stolen
    PyTuple_SetItem(pythonTuple, 2, pythonVerbose) // Reference stolen
    let returnValue = PyObject_CallObject(pFunc, pythonTuple)
    Py_DecRef(pythonTuple)
    let returnValueString = PyObject_Str(returnValue)
    let returnValueString2 = PyUnicode_AsEncodedString(returnValueString, "utf-8", "strict")
    let returnValueString2Buffer = PyBytes_AsString(returnValueString2)
    let finalString = String(cString: returnValueString2Buffer!)
    Py_DecRef(returnValue)
    Py_DecRef(returnValueString)
    Py_DecRef(returnValueString2)
    Py_DecRef(pFunc)
    return finalString
  }
  
//  public func fastdownward_translate_old(
//    domainPddl: String,
//    problemPddl: String,
//    verbose: Bool) throws -> String
//  {
//    PythonInteroperabilityManager.semaphore.wait()
//    let sas_representation_c = CAIKit.fastdownward_translate(
//      domainPddl,
//      problemPddl,
//      verbose ? 1 : 0)
//    PythonInteroperabilityManager.semaphore.signal()
//    guard let sas_representation_c = sas_representation_c else {
//      throw AIKitError(message: "PDDL translation error.")
//    }
//    let sasRepresentation = String(cString: sas_representation_c)
//    return sasRepresentation
//  }
}


