//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/28/20.
//

import Foundation

public struct CriticalCluster: Hashable {
  public let criticalFact: PDDLNameLiteral
  public let criticalActions: Set<PlanAction>
  public let adversarialActions: Set<PlanAction>
}

public struct SoftGoalCriticalCluster: Hashable {
  public private(set) var criticalFacts: [PDDLNameLiteral]
  public private(set) var criticalActions: [[PlanAction]]
  /// Adversarial actions with index i are adversarial to critical action with index i.
  /// Indices are: criticialFactNumber, criticalActionNumber, adversarialActionNumber(with respect to the critical action)
  public private(set) var adversarialActions: [[[PlanAction]]]
  public mutating func addCriticalFact(
    _ fact: PDDLNameLiteral,
    criticalActions: [PlanAction],
    adversarialActions: [[PlanAction]])
  {
    self.criticalFacts.append(fact)
    self.criticalActions.append(criticalActions)
    self.adversarialActions.append(adversarialActions)
  }
  public init() {
    self.criticalFacts = []
    self.criticalActions = []
    self.adversarialActions = []
  }
  public func minCost(
    actionTimes: [Int:Int],
    operatorIndex: [PlanAction: Int],
    factsCostFunction: [PDDLNameLiteral : [Int]]) -> (cost: Int, maxCostReached: Bool)
  {
    var totalCost = 0
    var maxCostReached = false
    for i in criticalFacts.indices {
      var maxCost = true
      var minCost = Int.max
      let criticalFact = criticalFacts[i]
      let deadlines = factsCostFunction[criticalFact] ?? [0]
      for criticalAction in criticalActions[i] {
        let criticalActionTime = actionTimes[operatorIndex[criticalAction]!] ?? Int.max
        let cost: Int
        if criticalActionTime < deadlines.count {
          cost = deadlines[criticalActionTime]
        } else {
          cost = deadlines.last!
        }
        if cost == 0 || cost != deadlines.last! {
          maxCost = false
        }
        minCost = min(minCost, cost)
        
      }
      precondition(minCost != Int.max)
      totalCost += minCost
      if maxCost {
        maxCostReached = true
      }
    }
    return (totalCost, maxCostReached)
  }
}
