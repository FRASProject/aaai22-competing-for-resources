//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/15/21.
//

import Foundation
import MathLib

@available(*, deprecated)
public struct GoalsOrderOptimizer: SimulatedAnnealing {
  public typealias State = [String: [Int]]
  public let initState: State
  public let factsCostFunction: [PDDLNameLiteral : [Int]]
  public let groundedProblem: TemporalProblemFDRCoding
  public let softGoals: [(name: String?, goal: PDDLAtomicTerm)]
  public let goalCriticalActionClusters: [SoftGoalCriticalCluster]
  public let playersUnitsSet: Set<String>
  let costNormalizer: Double
  private let initEarliestActionTimes: [Int: Int]
  public init(
    initState: State,
    factsCostFunction: [PDDLNameLiteral : [Int]],
    groundedProblem: TemporalProblemFDRCoding,
    softGoals: [(name: String?, goal: PDDLAtomicTerm)],
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    playersUnitsSet: Set<String>,
    costNormalizer: Double
  ) {
    self.initState = initState
    self.factsCostFunction = factsCostFunction
    self.groundedProblem = groundedProblem
    self.softGoals = softGoals
    self.goalCriticalActionClusters = goalCriticalActionClusters
    self.playersUnitsSet = playersUnitsSet
    self.initEarliestActionTimes = groundedProblem.findEarliestActionTimes()
    self.costNormalizer = costNormalizer
  }
  
  public func cost(of order: [String : [Int]]) -> Double? {
    var order = order
    var actionTimes = initEarliestActionTimes
    var clustersToFinish = Set<Int>(order.values.flatMap {$0})
    var sa = Set<Int>()
    while !clustersToFinish.isEmpty {
      let goalsNotAvailable = Set(order.values.flatMap {$0.dropFirst()})
      let availableGoals = Set(order.values.compactMap {$0.first}.filter {!goalsNotAvailable.contains($0)})
      
      let actionsToConsider = order.values
        .filter{ !$0.isEmpty && availableGoals.contains($0.first!) }
        .compactMap { unitOrder -> (Int, [PlanAction])? in
        let goalNumber = unitOrder.first!
        let goalCriticalActions = goalCriticalActionClusters[unitOrder.first!]
          .criticalActions.flatMap {$0}.filter {
            let unitParameters = Set($0.parameters.filter {playersUnitsSet.contains($0)})
            let currentGoalUnits = Set(order.filter {$0.value.first == goalNumber}.map {$0.key})
            return unitParameters == currentGoalUnits
          }
        if goalCriticalActions.isEmpty { return nil }
        return (goalNumber, goalCriticalActions)
      }
      assert(actionsToConsider.allSatisfy {clustersToFinish.contains($0.0)})
      if actionsToConsider.isEmpty {
        // Deadlocking order
        return nil
      }
      for (goalNumber, goalCriticalActions) in actionsToConsider
      where clustersToFinish.contains(goalNumber)
      {
        let minAction = goalCriticalActions.map {groundedProblem.operatorIndex[$0]!}.map {
          ($0, actionTimes[$0]!)}.min(by: {$0.1 < $1.1})!
        sa.insert(minAction.0)
        clustersToFinish.remove(goalNumber)
        order = order.compactMapValues {
          if $0.first == goalNumber {
            if $0.count == 1 { return nil }
            else { return Array($0.dropFirst()) }
          } else {
            return $0
          }
        }
      }
      let saTime = actionTimes.filter {sa.contains($0.key)}
      actionTimes = groundedProblem.findEarliestActionTimes(withActionTimes: saTime)
    }
    let cost = goalCriticalActionClusters.map {$0.minCost(actionTimes: actionTimes, operatorIndex: groundedProblem.operatorIndex, factsCostFunction: factsCostFunction).cost}.sum
    return Double(cost) * costNormalizer
  }
  
  public func randomNextState(from order: [String : [Int]]) -> [String : [Int]]? {
    Bool.random() ? randomTranspositionAtRandomUnit(order) : randomlyAssignGoalToOtherUnit(order)
  }
  
  public func randomTranspositionAtRandomUnit(_ order: [String : [Int]])  -> [String : [Int]] {
    var order = order
    guard let unitOrder = order.filter({ $0.value.count > 1 }).randomElement() else {
      return order
    }
    var (unit, goalsOrder) = unitOrder
    //let first = Int.random(in: 0..<goalsOrder.count)
    let second = Int.random(in: 1..<goalsOrder.count)
    let first = second - 1
    goalsOrder.swapAt(first, second)
    order[unit] = goalsOrder
    assert(Set(goalsOrder).count == goalsOrder.count)
    return order
  }
  public func randomlyAssignGoalToOtherUnit(_ order: [String : [Int]])  -> [String : [Int]] {
    let goalsInOrder = Set(order.values.flatMap {$0})
    let goalToFlip = goalsInOrder.randomElement()!
    let unitGoalOrder = order.values.compactMap {$0.firstIndex(of: goalToFlip)}
    let avgUnitGoalOrder = unitGoalOrder.sum / unitGoalOrder.count
    var order = order.mapValues { $0.filter { $0 != goalToFlip } }.filter {!$0.value.isEmpty}
    let goalCriticalActions = goalCriticalActionClusters[goalToFlip].criticalActions.flatMap {$0}
    let randomAction = goalCriticalActions.randomElement()!
    let units = Set(randomAction.parameters.filter {playersUnitsSet.contains($0)})
    units.forEach {
      assert(!order[$0, default: []].contains(goalToFlip))
      let insertPosition = min(avgUnitGoalOrder, order[$0]?.count ?? 0)
      order[$0, default: []].insert(goalToFlip, at: insertPosition)
    }
    return order
  }
}
