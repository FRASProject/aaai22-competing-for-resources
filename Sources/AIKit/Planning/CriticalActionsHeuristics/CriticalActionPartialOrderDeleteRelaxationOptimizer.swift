//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/13/22.
//

import Foundation
import MathLib

public struct CriticalActionPartialOrderDeleteRelaxationOptimizer: CriticalActionsPartialOrderRandomTransitions, SimulatedAnnealing {
  public typealias State = RelationDiGraphPath
  let game: PDDLGameFDRRepresentationAnalyticsLogic
  let player: Int
  private let groundedProblem: TemporalProblemFDRCoding
  let currentPlayerSoftGoalsCriticalActions: [[(
    criticalAction: PlanAction,
    criticalFact: PDDLNameLiteral,
    adversarialActions: Set<PlanAction>
  )]]
  public var initState: State
  let costNormalizer: Double
  let costFunction: [PDDLNameLiteral: [Int]]
  private let initEarliestActionTimes: [Int: Int]
  public init(
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int,
    initState: State,
    costFunction: [PDDLNameLiteral: [Int]]
  ) {
    self.game = game
    self.player = player
    self.initState = initState
    self.groundedProblem = game.groundedPlayersProblems[player]
    self.costFunction = costFunction
    self.initEarliestActionTimes = game.groundedPlayersProblems[player].findEarliestActionTimes()
    self.currentPlayerSoftGoalsCriticalActions = game.softGoalsCriticalActions[player]
    self.costNormalizer = 1000.0 / Double(game.game.maxGameValue.max()!)
  }
  public func cost(of order: RelationDiGraphPath) -> Double? {
    var order = order
    var actionTimes = initEarliestActionTimes
    //var clustersToFinish = Set<Int>(order.values.flatMap {$0})
    var sa = Set<Int>()
    while order.graph.numberOfVertices > 0 { //} !clustersToFinish.isEmpty {
      let actionsToConsider = order.graph.diVertices.filter {
        order.graph.incomingNeighborsId(of: $0.key).isEmpty}.map {$0.key}
      precondition(!actionsToConsider.isEmpty)
      let minAction = actionsToConsider.map {
        ($0, actionTimes[$0]!)}.min(by: {$0.1 < $1.1})!
      sa.insert(minAction.0)
      let saTime = actionTimes.filter {sa.contains($0.key)}
      actionTimes = groundedProblem.findEarliestActionTimes(withActionTimes: saTime)
      order.graph.removeVertexWith(id: minAction.0)
    }
    let cost = game.goalCriticalActionClusters[player].map {
      $0.minCost(actionTimes: actionTimes, operatorIndex: groundedProblem.operatorIndex, factsCostFunction: costFunction).cost}.sum
    return Double(cost) * costNormalizer
  }
}
