//
//  File.swift
//  
//
//  Created by Pavel Rytir on 9/16/21.
//

import Foundation
import MathLib

public enum SoftGoalsHeuristics {
  public static func convertDataForOrderHeuristics(
    ordering: [String: [Int]],
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int) -> (
      criticalActionFact: [String: String],
      ordering: [(unit: String, goalsOrder: [(factName: String, factParameters: [String])])])
  {
    var criticalActionFact = [String: String]()
    var newOrdering = [(unit: String, goalsOrder: [(factName: String, factParameters: [String])])]()
    for (unit, goalsOrdering) in ordering {
      var newUnitGoalsOrdering = [(factName: String, factParameters: [String])]()
      for goalNumber in goalsOrdering {
        let cluster = game.goalCriticalActionClusters[player][goalNumber]
        let fact = cluster.criticalFacts.first!
        precondition(cluster.criticalFacts.count == 1)
        let criticalActions = cluster.criticalActions.first!
        for action in criticalActions {
          criticalActionFact[action.name] = fact.name
        }
        newUnitGoalsOrdering.append((factName: fact.name, factParameters: fact.parameters))
      }
      newOrdering.append((unit: unit, goalsOrder: newUnitGoalsOrdering))
    }
    newOrdering.sort(by: { $0.unit < $1.unit })
    return (criticalActionFact: criticalActionFact, ordering: newOrdering)
  }
  
  public static func optimizeOrder(
    order: RelationDiGraphPath,
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int,
    costFunction: [PDDLNameLiteral: [Int]],
    startTemperature: Double,
    temperatureStep: Double
  ) -> RelationDiGraphPath
  {
    let optimizer = CriticalActionPartialOrderDeleteRelaxationOptimizer(
      game: game,
      player: player,
      initState: order,
      costFunction: costFunction)
//    let optimizer = GoalsOrderOptimizer(
//      initState: order,
//      factsCostFunction: factsCostFunction,
//      groundedProblem: groundedProblem,
//      softGoals: softGoals,
//      goalCriticalActionClusters: goalCriticalActionClusters,
//      playersUnitsSet: playersUnits,
//      costNormalizer: costNormalizer)
    let optimizedOrder = optimizer.startSimulatedAnnealing(
      startTemperature: startTemperature,
      temperatureStep: temperatureStep,
      verbose: true)
    if let initCost = optimizer.cost(of: order),
       let optimizedCost = optimizer.cost(of: optimizedOrder),
       initCost < optimizedCost
    {
      // Tweak: Initial cost has lower cost than the optimized.
      // Could happen if the initial state is near to optimal.
      // We just return the initial state.
      return order
    }
    return optimizedOrder
  }
  
  public static func convert(
    relation: RelationDiGraphPath,
    fdrCoding: TemporalProblemFDRCoding,
    goalCriticalActions: [[Int]],
    units: [String]
  ) -> [(unitId: Int, goalsOrder: [Int])] {
    let order = RelationDiGraphPath(graph: relation.graph, transitiveClosure: true)
    // proconditions(relation.transitiveClosure)
    var unitOrders = [(unitId: Int, goalsOrder: [Int])]()
    for (unitId, unit) in units.enumerated() {
      let unitCriticalActions = order.elements.filter {fdrCoding.operators[$0].parameters.contains(unit)}
      let sortedCriticalAction = unitCriticalActions.sorted(by: {order.isARelatedToB(a: $0, b: $1)})
      let goalsOrder = sortedCriticalAction.map { criticalAction in
        goalCriticalActions.firstIndex(where: {$0.contains(criticalAction)})!}
      unitOrders.append((unitId: unitId, goalsOrder: goalsOrder))
    }
    return unitOrders
  }
  
  public static func convert(
    relation: RelationDiGraphPath,
    fdrCoding: TemporalProblemFDRCoding,
    goalCriticalActions: [[Int]],
    units: [String]
  ) -> [String: [Int]]
  {
    Dictionary(
      uniqueKeysWithValues: convert(
        relation: relation,
        fdrCoding: fdrCoding,
        goalCriticalActions: goalCriticalActions,
        units: units).map {(units[$0.unitId], $0.goalsOrder)})
  }
  
  public static func calculateDeleteRelaxationOrderHeuristics(
    factsCostFunction: [PDDLNameLiteral : [Int]],
    criticalFactsWeights: [PDDLNameLiteral : Int],
    groundedProblem: TemporalProblemFDRCoding,
    softGoals: [(name: String?, goal: PDDLAtomicTerm)],
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    playersUnits: [String],
    debugVerbose: Bool = false
  ) -> RelationDiGraphPath
  {
    let actionTimes = Self.calculateDeleteRelaxationActionTimes(
      factsCostFunction: factsCostFunction,
      criticalFactsWeights: criticalFactsWeights,
      groundedProblem: groundedProblem,
      softGoals: softGoals,
      goalCriticalActionClusters: goalCriticalActionClusters,
      playersUnits: playersUnits,
      debugVerbose: debugVerbose)
    let ordering = Self.constructRelation(
      actionTimes: actionTimes,
      groundedProblem: groundedProblem)
    return ordering
  }
  
  @available(*, deprecated)
  public static func calculateOrderHeuristicsOld(
    factsCostFunction: [PDDLNameLiteral : [Int]],
    criticalFactsWeights: [PDDLNameLiteral : Int],
    groundedProblem: TemporalProblemFDRCoding,
    softGoals: [(name: String?, goal: PDDLAtomicTerm)],
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    playersUnits: [String],
    debugVerbose: Bool = false
  ) -> [String: [Int]]
  {
    let actionTimes = Self.calculateDeleteRelaxationActionTimes(
      factsCostFunction: factsCostFunction,
      criticalFactsWeights: criticalFactsWeights,
      groundedProblem: groundedProblem,
      softGoals: softGoals,
      goalCriticalActionClusters: goalCriticalActionClusters,
      playersUnits: playersUnits,
      debugVerbose: debugVerbose)
    let ordering = Self.determineOrdering(
      actionTimes: actionTimes,
      groundedProblem: groundedProblem,
      goalCriticalActionClusters: goalCriticalActionClusters,
      playersUnits: playersUnits)
    return ordering
  }
  
  public static func calculateDeleteRelaxationActionTimes(
    factsCostFunction: [PDDLNameLiteral : [Int]],
    criticalFactsWeights: [PDDLNameLiteral : Int],
    groundedProblem: TemporalProblemFDRCoding,
    softGoals: [(name: String?, goal: PDDLAtomicTerm)],
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    playersUnits: [String],
    debugVerbose: Bool = false
  ) -> [Int: Int]
  {
    if debugVerbose { print(factsCostFunction) }
    var actionTimes = groundedProblem.findEarliestActionTimes()
    var clustersToFinish = Set<Int>(softGoals.indices)
    let maxCostClusters = Self.findMaxCostClusters(
      currentClusters: clustersToFinish,
      actionTimes: actionTimes,
      groundedProblem: groundedProblem,
      goalCriticalActionClusters: goalCriticalActionClusters,
      factsCostFunction: factsCostFunction)
    if debugVerbose { print("Removing maxclusters: \(maxCostClusters)") }
    clustersToFinish.subtract(maxCostClusters)
    var sa = Set<Int>()
    while !clustersToFinish.isEmpty {
      var currentCost = Int.max
      var currentStartTime = Int.max
      var currentDeadlineGap = Int.max
      var minActionIndex: Int? = nil
      let actionsToConsider = clustersToFinish.map {
        goalCriticalActionClusters[$0]
      }.flatMap {$0.criticalActions.flatMap {$0}}
      for action in actionsToConsider.sorted() {
        //print("forloopaction: \(action)")
        let actionIndex = groundedProblem.operatorIndex[action]!
        let saPrime = sa.union([actionIndex])
        let saPrimeTime = actionTimes.filter {saPrime.contains($0.key)}
        let timePrime = groundedProblem.findEarliestActionTimes(withActionTimes: saPrimeTime)
        let saPrimeCost = clustersToFinish.map {
          goalCriticalActionClusters[$0].minCost(
            actionTimes: timePrime,
            operatorIndex: groundedProblem.operatorIndex,
            factsCostFunction: factsCostFunction).cost}.reduce(0, +)
        let medianDeadlineForAction = Self.findMedianDeadline(
          action: action,
          factsCostFunction: factsCostFunction,
          criticalFactsWeights: criticalFactsWeights,
          goalCriticalActionClusters: goalCriticalActionClusters)
        let deadlineGap = medianDeadlineForAction - timePrime[actionIndex]!
        if debugVerbose {
          print("considered action \(action) cost: \(saPrimeCost) starttime: \(timePrime[actionIndex]!) deadline gap: \(deadlineGap) median deadline: \(medianDeadlineForAction)")
        }
        if saPrimeCost < currentCost || (saPrimeCost == currentCost &&  deadlineGap < currentDeadlineGap) {
          currentCost = saPrimeCost
          currentDeadlineGap = deadlineGap
          minActionIndex = actionIndex
          currentStartTime = timePrime[actionIndex]!
        }
      }
      if minActionIndex == nil {
        break
      }
      sa.insert(minActionIndex!)
      if debugVerbose {
        print("min action: \(PlanAction(groundedProblem.operators[minActionIndex!])) cost : \(currentCost) Startime: \(currentStartTime)  deadline gap: \(currentDeadlineGap)")
      }
      let clustersToRemove = clustersToFinish.map {
        ($0, goalCriticalActionClusters[$0])}.filter {
          $0.1.criticalActions.flatMap {$0}.map {
            groundedProblem.operatorIndex[$0]!}.someSatisfies {
              $0 == minActionIndex!}}.map {$0.0}
      if debugVerbose {
        print("min cost: \(currentCost) cluster to remove: \(clustersToRemove)")
      }
      clustersToFinish.subtract(clustersToRemove)
      let saTime = actionTimes.filter {sa.contains($0.key)}
      //print("Min action cost: \(currentCost)")
      //for (actionId, actionTime) in saTime {
        //print("Action: \(PlanAction(game.groundedPlayersProblems[player].operators[actionId])) Time: \(actionTime)")
      //}
      actionTimes = groundedProblem.findEarliestActionTimes(withActionTimes: saTime)
      let maxCostClusters = Self.findMaxCostClusters(
        currentClusters: clustersToFinish,
        actionTimes: actionTimes,
        groundedProblem: groundedProblem,
        goalCriticalActionClusters: goalCriticalActionClusters,
        factsCostFunction: factsCostFunction)
      clustersToFinish.subtract(maxCostClusters)
    }
    return actionTimes.filter {sa.contains($0.key)}
  }
  
  private static func findMedianDeadline(
    action: PlanAction,
    factsCostFunction: [PDDLNameLiteral : [Int]],
    criticalFactsWeights: [PDDLNameLiteral : Int],
    goalCriticalActionClusters: [SoftGoalCriticalCluster]) -> Int
  {
    let cluster = goalCriticalActionClusters.first(where: {$0.criticalActions.flatMap {$0}.contains(action)})!
    let deadlines = factsCostFunction[cluster.criticalFacts.first!] ?? [0]
    let weight = criticalFactsWeights[cluster.criticalFacts.first!]!
    let maxCost = deadlines.max()!
    if maxCost < weight / 2 || maxCost == 0 {
      return Int.max
    }
    let sumHalf = deadlines.sum / 2
    let cumsum = deadlines.cumsum
    let medianDeadline = cumsum.firstIndex(where: { $0 > sumHalf })!
    return medianDeadline == 0 ? 0 : medianDeadline - 1
  }
  
  private static func constructRelation(
    actionTimes: [Int : Int],
    groundedProblem: TemporalProblemFDRCoding) -> RelationDiGraphPath
  {
    let criticalActions = actionTimes.keys.sorted()
    var influenceGraph = IntDiGraph(isolatedVertices: criticalActions)
    var graphIdsGenerator = IntGraphIndicesGenerator(graph: influenceGraph)
    for action1 in criticalActions {
      for action2 in criticalActions where actionTimes[action1]! < actionTimes[action2]! {
        if groundedProblem.action1InfluencesAction2(action1: action1, action2: action2) {
          if !influenceGraph.isEdge(from: action1, to: action2) {
            _ = influenceGraph.addEdge(from: action1, to: action2, indexGenerator: &graphIdsGenerator.edgeIdGenerator)
          }
        }
      }
    }
    let partialOrder = RelationDiGraphPath(graph: influenceGraph, transitiveClosure: true)
    return partialOrder
  }
  
  private static func determineOrdering(
    actionTimes: [Int : Int],
    groundedProblem: TemporalProblemFDRCoding,
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    playersUnits: [String]
    ) -> [String: [Int]]
  {
    var unitSoftGoals = [String: [(Int,Int)]]()
    for (actionIndex, actionTime) in actionTimes {
      let action = PlanAction( groundedProblem.operators[actionIndex])
      var actionGoals = [Int]()
      for (goalIndex, goal) in goalCriticalActionClusters.enumerated() {
        if goal.criticalActions.flatMap({$0}).contains(action) {
          actionGoals.append(goalIndex)
        }
      }
      let units = Set(playersUnits).intersection(action.parameters)
      for unit in units {
        unitSoftGoals[unit, default: []].append(contentsOf: actionGoals.map {(actionTime, $0)})
      }
    }
    let ordering = unitSoftGoals.mapValues {$0.sorted(by: {$0.0 < $1.0}).map {$0.1}}
    return ordering
  }
  
  private static func findMaxCostClusters(
    currentClusters: Set<Int>,
    actionTimes: [Int:Int],
    groundedProblem: TemporalProblemFDRCoding,
    goalCriticalActionClusters: [SoftGoalCriticalCluster],
    factsCostFunction: [PDDLNameLiteral : [Int]]) -> Set<Int>
  {
    var maxCostClusters = Set<Int>()
    for (clusterIdx, cluster) in currentClusters.map({($0, goalCriticalActionClusters[$0])})
    {
      let (_, maxCost) = cluster.minCost(
        actionTimes: actionTimes,
        operatorIndex: groundedProblem.operatorIndex,
        factsCostFunction: factsCostFunction)
      //print("Cluster \(clusterIdx) cost: \(cost)")
      if maxCost {
        maxCostClusters.insert(clusterIdx)
      }
    }
    return maxCostClusters
  }
  
  public static func determineReachableGoalsByPruningHeuristics(
    from opponentStrat: MixedStrategy<TemporalPlan>,
    game: PDDLGameFDRRepresentationAnalyticsLogic,
    player: Int) -> [Int]
  {
    let actionTimes = game.groundedPlayersProblems[player].findEarliestActionTimes()
    var maxDeadlines = [PDDLNameLiteral: Int]()
    for (deadlines, probability) in zip(
      opponentStrat.strategies.map {
        $0.extractDeadlines(
          actionCriticalFacts: game.actionWithoutUnitsCriticalFacts,
          unitsParameters: Set(game.playersUnits[(player + 1) % 2]))},
      opponentStrat.distribution
    ) where probability > AIKitConfig.probabilityEpsilon
    {
      for (fact, time) in deadlines {
        maxDeadlines[fact] = max(maxDeadlines[fact] ?? time, time)
      }
    }
    //var reachableFacts = Set<PDDLNameLiteral>()
    var reachableGoals = Set<Int>()
    
    for (goalNumber, goal) in game.softGoalsCriticalActions[player].enumerated() {
      var goalReachable = false
      for cluster in goal {
        let maxDeadline = maxDeadlines[cluster.criticalFact] ?? Int.max
        let actionEarliestStartTime = actionTimes[
          game.groundedPlayersProblems[player].operatorIndex[cluster.criticalAction]!]!
        if actionEarliestStartTime <= maxDeadline {
          goalReachable = true
          break
        }
      }
      if goalReachable {
        reachableGoals.insert(goalNumber)
      }
    }
    
//    for (fact, maxDeadline) in maxDeadlines {
//      let actions = game.criticalFactCriticalActions[player][fact]!
//      for action in actions {
//        let actionEarliestStartTime = actionTimes[
//          game.groundedPlayersProblems[player].operatorIndex[action]!]!
//        if actionEarliestStartTime <= maxDeadline {
//          reachableFacts.insert(fact)
//        }
//      }
//    }
//    return Array(Set(reachableFacts.flatMap {game.getGoalNumber(for: $0, player: player)})).sorted()
    return Array(reachableGoals)
  }
}
