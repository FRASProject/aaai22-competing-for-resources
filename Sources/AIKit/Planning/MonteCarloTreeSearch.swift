//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/22.
//

import Foundation
import MathLib

public protocol MonteCarloTreeSearch where Action == ActionSelector.Action {
  associatedtype Node: Hashable
  associatedtype Action
  associatedtype ActionSelector: MonteCarloTreeSearchActionSelector
  func isTerminal(_ node: Node) -> Bool
  var root: Node { get }
  func reward(_ node: Node) -> Double?
  func apply(action: Action, in node: Node) -> Node
  var actionSelectors: [Node: ActionSelector] { get set }
  func availableActions(at node: Node) -> [Action]
}

public protocol MonteCarloTreeSearchActionSelector {
  associatedtype Action: Hashable
  func select() -> Action
  func actionsDistribution() -> [Action: Double]
  mutating func update(action: Action, value: Double)
  init(actions: [Action])
}

public struct UCTActionSelector<Action: Hashable>: MonteCarloTreeSearchActionSelector {
  private var visits: [Action: Int]
  private var values: [Action: Double]
  private var totalVisits: Int
  
  public func select() -> Action {
    var ucbScores = [(Action, Double)]()
    var maxUcbScore = 0.0
    for action in values.keys {
      let ucbScore = values[action]! +
      (AIKitConfig.uctExplorationParameterC * sqrt(log(Double(totalVisits)) / Double(visits[action]!)))
      ucbScores.append((action, ucbScore))
      maxUcbScore = max(maxUcbScore, ucbScore)
    }
    var maxActions = [Action]()
    for (action, ucbScore) in ucbScores where ucbScore >= maxUcbScore - AIKitConfig.defaultEpsilon {
      maxActions.append(action)
    }
    return maxActions.randomElement()!
  }
  
  public func actionsDistribution() -> [Action : Double] {
    visits.mapValues { Double($0) / Double(totalVisits) }
  }
  public mutating func update(action: Action, value: Double) {
    totalVisits += 1
    visits[action, default: 0] += 1
    if visits[action] == 1 {
      values[action] = value
    } else {
      values[action]! += (value - values[action]!) / Double(visits[action]!)
    }
  }
  
  public init(actions: [Action]) {
    self.visits = [:]
    self.values = [:]
    self.totalVisits = 0
  }
}

extension MonteCarloTreeSearch {
  public mutating func samplePlan(numberOfPreTreeTraversals: Int) -> (reward: Double, path: [Action])? {
    for _ in 0..<numberOfPreTreeTraversals {
      _ = runIteration()
    }
    return samplePlan()
  }
  public func samplePlan() -> (reward: Double, path: [Action])? {
    var currentState = root
    var path = [Action]()
    while !isTerminal(currentState) {
      let actionsProbs = availableActionsPlayDistribution(in: currentState)
      if actionsProbs.isEmpty { return nil } // Tree was not explored till any terminal node.
      let randomAction = RandomUtils.sampleKey(probabilities: actionsProbs)
      path.append(randomAction)
      currentState = apply(action: randomAction, in: currentState)
    }
    return (reward: reward(currentState)!, path: path)
  }
  public func availableActionsPlayDistribution(in node: Node) -> [Action: Double] {
    actionSelectors[node]!.actionsDistribution()
  }
  func runRandomSimulation(from node: Node) -> Double {
    var currentNode = node
    while !isTerminal(currentNode) {
      let selectedAction = availableActions(at: currentNode).randomElement()!
      currentNode = apply(action: selectedAction, in: currentNode)
    }
    return reward(currentNode)!
  }
  
  mutating func runIteration() -> Double {
    runIteration(from: root)
  }
  mutating func runIteration(from node: Node) -> Double {
    if isTerminal(node) {
      return reward(node)!
    }
    let subTreeValue: Double
    var nodeCurrentSelector: ActionSelector
    let selectedAction: Action
    if let nodeActionSelector = actionSelectors[node] {
      selectedAction = nodeActionSelector.select()
      nodeCurrentSelector = nodeActionSelector
      let nextNode = apply(action: selectedAction, in: node)
      subTreeValue = runIteration(from: nextNode)
    } else {
      let availableActions = availableActions(at: node)
      nodeCurrentSelector = ActionSelector(actions: availableActions)
      selectedAction = availableActions.randomElement()!
      let nextNode = apply(action: selectedAction, in: node)
      subTreeValue = runRandomSimulation(from: nextNode)
    }
    nodeCurrentSelector.update(action: selectedAction, value: subTreeValue)
    actionSelectors[node] = nodeCurrentSelector
    return subTreeValue
  }
}
