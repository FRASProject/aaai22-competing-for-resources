//
//  File.swift
//  
//
//  Created by Pavel Rytir on 3/5/21.
//

import Foundation
import MathLib

extension TemporalProblemFDRCoding {
  
  /// Algorithm 1a from Planning in Adversarial Domains - SoCS 2020
  /// - Returns: Action times
  public func findEarliestActionTimes() -> [Int: Int] {
    let initState = FDRStateRelaxed(FDRState(variablesValues: initState))
    var factTimes = [Tuple<Int, Int>: Int]()
    for (fact, value) in self.initState.enumerated() {
      if normalVariablesIndexSet.contains(fact) && variables[fact].count - 1 != value {
        factTimes[Tuple(fact, value)] = 0
      }
    }
    return findEarliestActionTimes(
      initState: initState,
      factTimes: factTimes,
      actionTimes: [Int: Int](),
      oSet: Set<Int>()).actionTimes
  }
  /// Algorithm 2 from Planning in Adversarial Domains - SoCS 2020
  /// - Parameter actionTimes: Current actions times
  /// - Returns: Action times
  public func findEarliestActionTimes(
    withActionTimes actionTimes: [Int: Int]) -> [Int: Int]
  {
    let initState = FDRStateRelaxed(FDRState(variablesValues: initState))
    var newState = initState
    let actionsVariables = Set(actionTimes.flatMap {
      operators[$0.key].allPreconditions.map {$0.variable} + operators[$0.key].effects.map {$0.variable}
    })
    var factTimes = [Tuple<Int, Int>: Int]()
    for (variable, value) in self.initState.enumerated() {
      if normalVariablesIndexSet.contains(variable) &&
          variables[variable].count - 1 != value &&
          !actionsVariables.contains(variable)
      {
        factTimes[Tuple(variable, value)] = 0
      }
    }
    for actionVariable in actionsVariables.sorted() {
      var maxTime = 0
      var maximizingVarValue: Int? = nil
      
      for (actionId, actionTime) in actionTimes.sorted(by: { $0.key < $1.key}) {
        let actionDuration = operators[actionId].duration(in: initState)
        for fact in
          operators[actionId].preconditionAtStart +
          operators[actionId].effectsAtStart.map({(variable: $0.variable, value: $0.newValue)})
        where fact.variable == actionVariable
        {
          if maxTime <= actionTime {
            maximizingVarValue = fact.value
            maxTime = actionTime
          }
        }
        for fact in operators[actionId].allPreconditionAtEnd +
              operators[actionId].preconditionOverAll +
          operators[actionId].effectsAtEnd.map({(variable: $0.variable, value: $0.newValue)})
        where fact.variable == actionVariable
        {
          if maxTime <= actionTime + actionDuration {
            maxTime = actionTime + actionDuration
            maximizingVarValue = fact.value
          }
        }
      }
      newState.replaceVariableValueBy(fact: (variable: actionVariable, value: maximizingVarValue!))
      factTimes[Tuple(actionVariable, maximizingVarValue!)] = maxTime
    }
    return findEarliestActionTimes(
      initState: newState,
      factTimes: factTimes,
      actionTimes: actionTimes,
      oSet: Set(actionTimes.keys)).actionTimes
  }
  /// Algorithm 1b from Planning in Adversarial Domains - SoCS 2020. The set A parameters are self.operators
  /// - Parameters:
  ///   - initState: F set
  ///   - factTimes: time
  ///   - actionTimes: time
  ///   - oSet: O set
  /// - Returns: 
  public func findEarliestActionTimes(
    initState: FDRStateRelaxed,
    factTimes: [Tuple<Int, Int>: Int],
    actionTimes: [Int: Int],
    oSet: Set<Int>) -> (
      state: FDRStateRelaxed,
      actionTimes: [Int: Int],
      factTimes: [Tuple<Int, Int>: Int])
  {
    var currentState = initState
    var factTimes = factTimes
    var actionTimes = actionTimes
    var oSet = oSet
    
    while true {
      var applicableOperatorsIndices = [Int]()
      for i in operators.indices where !oSet.contains(i) {
        if operators[i].isApplicableAtStart(in: currentState) {
          applicableOperatorsIndices.append(i)
        }
      }
      
      //      let applicableOperatorsIndices = operators.enumerated().filter {
      //        $0.1.isApplicableAtStart(in: currentState)}.map {$0.0}.filter {!oSet.contains($0)}
      if applicableOperatorsIndices.isEmpty {
        break
      }
      for applicableOperatorIndex in applicableOperatorsIndices {
        let factAtStartMaxTime = operators[applicableOperatorIndex].allPreconditionAtStart.max(by: {
          factTimes[Tuple($0.variable, $0.value)]! < factTimes[Tuple($1.variable, $1.value)]!
        }).map {factTimes[Tuple($0.variable, $0.value)]!} ?? 0
        
        //          .map {
//          factTimes[Tuple($0.variable, $0.value)]!}.max() ?? 0
        let factOverAllMaxTime = operators[applicableOperatorIndex].preconditionOverAll.max(by: {
          factTimes[Tuple($0.variable, $0.value)]! < factTimes[Tuple($1.variable, $1.value)]!
        }).map {factTimes[Tuple($0.variable, $0.value)]!} ?? 0
          
          
//          .map {
//          factTimes[Tuple($0.variable, $0.value)]!}.max() ?? 0
        
        actionTimes[applicableOperatorIndex] = max(factAtStartMaxTime, factOverAllMaxTime)
      }
      let actionTimeMin = applicableOperatorsIndices.map {actionTimes[$0]!}.min()!
      let minActionsIndices = applicableOperatorsIndices.filter {actionTimes[$0]! == actionTimeMin}
      oSet = oSet.union(minActionsIndices)
      for minActionIndex in minActionsIndices {
        for effect in operators[minActionIndex].effectsAtStart {
          let fact = Tuple(effect.variable, effect.newValue)
          let actionStartTime = actionTimes[minActionIndex]!
          factTimes[fact] = min(factTimes[fact] ?? Int.max, actionStartTime)
          currentState.variablesValues[fact.first].insert(fact.second)
        }
        for effect in operators[minActionIndex].effectsAtEnd {
          let fact = Tuple(effect.variable, effect.newValue)
          let actionFinishTime = actionTimes[minActionIndex]! +
            operators[minActionIndex].duration(in: currentState)
          factTimes[fact] = min(factTimes[fact] ?? Int.max, actionFinishTime)
          currentState.variablesValues[fact.first].insert(fact.second)
        }
      }
    }
    return (state:currentState, actionTimes: actionTimes, factTimes: factTimes)
  }
  
  /// estimateTimeAsAdversary
  /// - Parameter clusters: Index is goal number. The sets contain adversary actions from the
  /// view of the opponent player.
  /// - Returns: Estimated (opponent adversary) action times.
  public func estimateTimeAsAdversary(clusters: [Set<Int>]) -> [Int: Int] {
    var sa = Set<Int>()
    var time = [Int: Int]()
    let initState = FDRStateRelaxed(FDRState(variablesValues: initState))
    var currentState = initState
    var factTimes = [Tuple<Int, Int>: Int]()
    for (fact, value) in self.initState.enumerated() {
      if normalVariablesIndexSet.contains(fact) && variables[fact].count - 1 != value {
        factTimes[Tuple(fact, value)] = 0
      }
    }
    var clustersToFinish = Set(clusters.indices)
    
    while !clustersToFinish.isEmpty {
      let (newF, timePrime, factTimePrime) = findEarliestActionTimes(
        initState: currentState,
        factTimes: factTimes,
        actionTimes: time,
        oSet: sa)
      currentState = newF
      let candidateActions = Set(clustersToFinish.flatMap {clusters[$0]}.filter {timePrime[$0] != nil})
      if candidateActions.isEmpty { break }
      let candidateActionsTime = timePrime.filter {candidateActions.contains($0.key)}
      let selectedAction = sample(time: candidateActionsTime)
      sa.insert(selectedAction)
      time[selectedAction] = candidateActionsTime[selectedAction]
      factTimes = updateFactTime(
        actionId: selectedAction,
        state: currentState,
        actionTime: time[selectedAction]!,
        factTimes: factTimePrime)
      let selectedActionClusters = clustersToFinish.filter {clusters[$0].contains(selectedAction)}
      clustersToFinish.subtract(selectedActionClusters)
    }
    let result = time.filter {sa.contains($0.key)}
    return result
  }
  
  private func updateFactTime(
    actionId: Int,
    state: FDRStateRelaxed,
    actionTime: Int,
    factTimes: [Tuple<Int, Int>: Int]) -> [Tuple<Int, Int>: Int]
  {
    let action = operators[actionId]
    var newFactTimes = factTimes
    for fact in action.allPreconditionAtStart + action.effectsAtStart.map({(variable: $0.variable, value: $0.newValue)})
    {
      //newState.replaceVariableValueBy(fact: fact)
      newFactTimes[Tuple(fact.variable, fact.value)] = actionTime
    }
    for fact in action.allPreconditionAtEnd + action.effectsAtEnd.map({(variable: $0.variable, value: $0.newValue)})
    {
      newFactTimes[Tuple(fact.variable, fact.value)] = actionTime + action.duration(in: state)
      //newState.replaceVariableValueBy(fact: fact)
    }
    return newFactTimes
  }
  
  private func sample(time: [Int: Int]) -> Int {
    precondition(!time.isEmpty)
    if time.count == 1 { return time.first!.key }
    let probs = Array(calculateActionProbabilities(time: time))
    let rnd = Double.random(in: 0..<1)
    var acc = 0.0
    for (actionId, p) in probs {
      acc += p
      if rnd < acc { return actionId }
    }
    return probs.last!.key
  }
  
  private func calculateActionProbabilities(time: [Int: Int]) -> [Int: Double] {
    let sum = Double(time.values.sum)
    let divisor = Double(time.count - 1)
    let probs = time.mapValues {
      (1.0 - (Double($0) / sum)) / divisor
    }
    return probs
  }
  
}

