//
//  PlanningProblem.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/12/20.
//

import Foundation

public protocol PlanningProblem {
  associatedtype State: PlanningState
  associatedtype Action: PlanActionProtocol
  
  var initState: State { get }
  func availableActions(at state: State) -> [Action]
  func apply(action: Action, on state: State) -> State
  func isFinal(_ state: State) -> Bool
  func cost(of action: Action) -> Double
}
