//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/14/21.
//

import Foundation

public struct PDDLCache : Codable {
  let problemPddl: String
  let domainPddl: String
  let plannerOutput: String
  let computationDuration: TimeInterval
  let plannerLog: String
  let plannerErrorOutput: String
  
  public static func loadPlannerOutputFromCache(
    pddl: String,
    domainPDDL: String,
    planCache: URL) -> (String, TimeInterval, String)?
  {
    // TODO: Implement multiplans cache.
    let decoder = JSONDecoder()
    if let data = try? Data(contentsOf: planCache),
      let pddlCache = try? decoder.decode(PDDLCache.self, from: data),
      pddl == pddlCache.problemPddl, domainPDDL == pddlCache.domainPddl {
      return (pddlCache.plannerOutput, pddlCache.computationDuration, pddlCache.plannerLog)
    } else {
      return nil
    }
  }

  public static func savePlannerOutputToCache(
    pddl: String,
    domainPDDL: String,
    plannerOutput: String,
    computationDuration: TimeInterval,
    plannerLog: String,
    plannerErrorOutput: String,
    planCache: URL) throws
  {
    let cacheDir = planCache.deletingLastPathComponent()
    try? FileManager.default.createDirectory(at: cacheDir, withIntermediateDirectories: true)
    let newCache = PDDLCache(
      problemPddl: pddl,
      domainPddl: domainPDDL,
      plannerOutput: plannerOutput,
      computationDuration: computationDuration,
      plannerLog: plannerLog,
      plannerErrorOutput: plannerErrorOutput)
    let encoder = JSONEncoder()
    let data = try encoder.encode(newCache)
    try data.write(to: planCache)
    try pddl.write(
      to: URL(fileURLWithPath: planCache.path.appending(".problem.pddl")),
      atomically: true,
      encoding: .utf8)
    try domainPDDL.write(
      to: URL(fileURLWithPath: planCache.path.appending(".domain.pddl")),
      atomically: true,
      encoding: .utf8)
    try plannerOutput.write(
      to: URL(fileURLWithPath: planCache.path.appending(".plan.sol")),
      atomically: true,
      encoding: .utf8)
    try plannerLog.write(
      to: URL(fileURLWithPath: planCache.path.appending(".planner.log")),
      atomically: true,
      encoding: .utf8)
    try plannerErrorOutput.write(
      to: URL(fileURLWithPath: planCache.path.appending(".plannerErr.log")),
      atomically: true,
      encoding: .utf8)
  }
}
