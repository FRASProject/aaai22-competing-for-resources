//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/14/21.
//

import Foundation

public struct PDDLGroundingFDRCache: Codable {
  let problemPddl: String
  let domainPddl: String
  let sasOutput: String
  let variables: String?
  let groups: String?
  let preprocessorOutput: String?
  let computationDuration: Double
  public func saveToCache(_ cacheFile: URL) throws {
    let cacheDir = cacheFile.deletingLastPathComponent()
    try? FileManager.default.createDirectory(at: cacheDir, withIntermediateDirectories: true)
    let encoder = JSONEncoder()
    let data = try encoder.encode(self)
    try data.write(to: cacheFile)
    try problemPddl.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".problem.pddl")),
      atomically: true,
      encoding: .utf8)
    try domainPddl.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".domain.pddl")),
      atomically: true,
      encoding: .utf8)
    try sasOutput.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".output.sas")),
      atomically: true,
      encoding: .utf8)
    try variables?.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".variables.groups")),
      atomically: true,
      encoding: .utf8)
    try groups?.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".all.groups")),
      atomically: true,
      encoding: .utf8)
    try preprocessorOutput?.write(
      to: URL(fileURLWithPath: cacheFile.path.appending(".output")),
      atomically: true,
      encoding: .utf8)
  }
  public static func loadFromCache(_ cacheFile: URL) -> PDDLGroundingFDRCache? {
    try? JSONDecoder().decode(PDDLGroundingFDRCache.self, from: Data(contentsOf: cacheFile))
  }
}
