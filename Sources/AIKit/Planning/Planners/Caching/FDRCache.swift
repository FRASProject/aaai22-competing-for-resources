//
//  planCaching.swift
//  Planner
//
//  Created by Pavel Rytir on 6/16/18.
//

import Foundation

public struct FDRCache : Codable {
  let representationFDR: String
  let plannerOutput: String
  let computationDuration: TimeInterval
  let plannerLog: String
  let plannerErrorOutput: String
  
  public static func savePlannerOutputToCache(
    representationFDR: String,
    plannerOutput: String,
    computationDuration: TimeInterval,
    plannerLog: String,
    plannerErrorOutput: String,
    planCache: URL) throws
  {
    let cacheDir = planCache.deletingLastPathComponent()
    try? FileManager.default.createDirectory(at: cacheDir, withIntermediateDirectories: true)
    let newCache = FDRCache(
      representationFDR: representationFDR,
      plannerOutput: plannerOutput,
      computationDuration: computationDuration,
      plannerLog: plannerLog,
      plannerErrorOutput: plannerErrorOutput)
    let encoder = JSONEncoder()
    let data = try encoder.encode(newCache)
    try data.write(to: planCache)
    try representationFDR.write(
      to: URL(fileURLWithPath: planCache.path.appending(".output.sas")),
      atomically: true,
      encoding: .utf8)
    try plannerOutput.write(
      to: URL(fileURLWithPath: planCache.path.appending(".plan.sol")),
      atomically: true,
      encoding: .utf8)
    try plannerLog.write(
      to: URL(fileURLWithPath: planCache.path.appending(".planner.log")),
      atomically: true,
      encoding: .utf8)
    try plannerErrorOutput.write(
      to: URL(fileURLWithPath: planCache.path.appending(".plannerErr.log")),
      atomically: true,
      encoding: .utf8)
  }

  public static func loadPlannerOutputFromCache(
    representationFDR: String,
    planCache: URL) -> (String, TimeInterval, String)?
  {
    let decoder = JSONDecoder()
    if let data = try? Data(contentsOf: planCache),
      let pddlCache = try? decoder.decode(FDRCache.self, from: data),
       representationFDR == pddlCache.representationFDR
    {
      return (pddlCache.plannerOutput, pddlCache.computationDuration, pddlCache.plannerLog)
    } else {
      return nil
    }
  }
  
}
