//
//  PlanningHeuristic.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/12/20.
//

import Foundation

public protocol PlanningHeuristic {
  associatedtype State: PlanningState
  
  func estimate(_ state: State) -> Double
}
