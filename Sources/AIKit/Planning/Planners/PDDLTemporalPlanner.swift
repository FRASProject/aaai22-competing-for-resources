//
//  File.swift
//  
//
//  Created by Pavel Rytir on 11/14/20.
//

import Foundation

public protocol PDDLTemporalPlanner: PDDLRawPlanner {
  func start(problem: PDDLPlanningProblem, planCache: URL?) throws
  func getLastPlan() -> ([TemporalPlan], [Double], String, String, [Double])
}

extension PDDLTemporalPlanner {
  
  public func start(problem: PDDLPlanningProblem, planCache: URL?) throws {
    try start(problemPDDL: problem.problem.text, domainPDDL: problem.domain.text, planCache: planCache)
  }
  private func parse(_ data: ([String], String, String, [Double])) -> (
    [TemporalPlan],
    [Double],
    String,
    String,
    [Double])
  {
    let parsedPlans = data.0.map {TemporalPlan(raw: $0) }
    let costs = data.0.map {TemporalPlan.parseCost(raw: $0) ?? 0.0 }
    return (parsedPlans, costs, data.1, data.2, data.3)
  }
  public func getLastPlan() -> (
    [TemporalPlan],
    [Double],
    String,
    String,
    [Double]) {
      parse(getLastRawPlan())
    }
  public func findFirst(problem: PDDLPlanningProblem, planCache: URL?) throws ->
  ([TemporalPlan], [Double], String, String, [Double])
  {
    try start(problem: problem, planCache: planCache)
    if try waitForNextPlan() {
      let plan = getLastPlan()
      try reset()
      return plan
    } else {
      let (_, log, errorLog, duration) = getLastRawPlan()
      try reset()
      return ([], [], log, errorLog, duration)
    }
  }
  public func findFirst(
    problemPDDL: String,
    domainPDDL: String,
    planCache: URL?) throws ->
  ([TemporalPlan], [Double], String, String, [Double])
  {
    try parse(findFirst(
      problemPDDL: problemPDDL,
      domainPDDL: domainPDDL,
      planCache: planCache))
  }
  
  public func findBest(problemPDDL: String, domainPDDL: String, planCache: URL?) throws ->
  ([TemporalPlan], [Double], String, String, [Double])
  {
    try parse(findBest(
      problemPDDL: problemPDDL,
      domainPDDL: domainPDDL,
      planCache: planCache))
  }
}
