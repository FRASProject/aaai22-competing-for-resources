//
//  errors.swift
//  Planner
//
//  Created by Pavel Rytir on 6/15/18.
//

import Foundation

public struct PlannerError : PDDLPlannerError {
    public init(message: String) {
        self.message = message
        self.errorCode = nil
    }
    public init(message: String, errorCode: Int) {
        self.message = message
        self.errorCode = errorCode
    }
    public let message: String
    public let errorCode: Int?
}
