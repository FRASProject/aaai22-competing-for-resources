//
//  CPT5Planner.swift
//  AIKit
//
//  Created by Pavel Rytir on 9/15/20.
//

import Foundation

open class CPT5Planner: PDDLTemporalPlanner {
  public enum State {
    case iddle, running, finished, failed
  }
  public let plannerExecutablePath: String
  public let mode: PlannerType = .temporal
  public private(set) var computationStart: Date?
  public private(set) var state: State
  public private(set) var planCache: URL?
  public private(set) var lastPlan: String?
  public private(set) var lastLog: String?
  public private(set) var lastErrorLog: String?
  public private(set) var computationDuration: Double?
  public var verbose: Bool
  public var maxTime: Int
  private var tempDirURL: URL?
  private var problemPDDL: String?
  private var domainPDDL: String?
  private var proc: Process!
  private var standardOutput: Pipe!
  private var standardError: Pipe!
  
  
  private var waitForNextWillReturnCache: Bool
  
  public init(plannerExecutablePath: String, maxTime: Int, verbose: Bool = false) {
    self.verbose = verbose
    self.maxTime = maxTime
    self.plannerExecutablePath = plannerExecutablePath
    self.state = .iddle
    self.waitForNextWillReturnCache = false
  }
  
  public func start(problemPDDL: String, domainPDDL: String, planCache: URL?) throws {
    guard state != .running else {
      fatalError("Planner already started!")
    }
    self.problemPDDL = problemPDDL
    self.domainPDDL = domainPDDL
    if let planCache = planCache,
       let (plannerOutput, duration, log) = PDDLCache.loadPlannerOutputFromCache(
        pddl: problemPDDL,
        domainPDDL: domainPDDL,
        planCache: planCache)
    {
      lastPlan = plannerOutput
      state = .finished
      computationDuration = duration
      lastLog = log
      waitForNextWillReturnCache = true
      return
    }
    self.planCache = planCache
    let temporaryDirectory = "tmp_" + UUID().uuidString
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    self.tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try problemPDDL.write(
      to: self.tempDirURL!.appendingPathComponent("problem.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    try domainPDDL.write(
      to: self.tempDirURL!.appendingPathComponent("domain.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    
    computationStart = Date()
    proc = Process()
    proc.currentDirectoryURL = tempDirURL
    proc.executableURL = URL(fileURLWithPath: plannerExecutablePath)
    proc.arguments = [
      "-o",
      "domain.pddl",
      "-f",
      "problem.pddl",
      "-out",
      "plan.SOL",
//      ">",
//      "plannerFRASLog.txt",
//      "2>",
//      "plannerFRASLogErr.txt"
    ]
    
    print("CPT5 command: \(plannerExecutablePath) \(proc.arguments!.joined(separator: " ")) in dir: \(temporaryDirectory)")
    
    
    if !verbose {
      proc.standardOutput = FileHandle.nullDevice
    }
    
//    standardOutput = Pipe()
//    standardError = Pipe()
//    proc.standardOutput = standardOutput
//    proc.standardError = standardError
    
    do {
      try proc.run()
    } catch {
      self.state = .failed
      throw error
    }
    self.state = .running
  }
  
  public func reset() throws {
    switch self.state {
    case .iddle:
      return
    case .running:
      if proc.isRunning {
        try self.terminateProcess()
      }
      fallthrough
    case .finished:
      if let planCache = planCache, lastPlan != nil {
        try PDDLCache.savePlannerOutputToCache(
          pddl: problemPDDL!,
          domainPDDL: domainPDDL!,
          plannerOutput: lastPlan!,
          computationDuration: computationDuration!,
          plannerLog: lastLog!,
          plannerErrorOutput: lastErrorLog!,
          planCache: planCache)
      }
      fallthrough
    case .failed:
      self.problemPDDL = nil
      self.domainPDDL = nil
      if let tempDirUrl = self.tempDirURL {
        try? FileManager.default.removeItem(at: tempDirUrl)
      }
      self.tempDirURL = nil
      self.state = .iddle
      self.lastPlan = nil
      self.lastLog = nil
      self.computationDuration = nil
      self.planCache = nil
    }
  }
  
  public func waitForNextPlan() throws -> Bool {
    if waitForNextWillReturnCache {
      self.waitForNextWillReturnCache = false
      return true
    }
    
    guard self.state != .iddle else {
      fatalError("Invalid planner state")
    }
    
    if self.state != .running {
      return false
    }
    while proc.isRunning && !self.isAfterDeadline {
      Thread.sleep(forTimeInterval: 0.01)
    }
    if !proc.isRunning && proc.terminationStatus != 0 {
      self.state = .failed
      throw Error(
        message: "Planner returned non-zero code: \(proc.terminationStatus)",
        errorCode: Int(proc.terminationStatus))
    }
    
    if proc.isRunning && self.isAfterDeadline {
      try self.terminateProcess()
    }
    self.state = .finished
    
    let solution = try checkForSolution()
    if solution != nil {
      self.computationDuration = -computationStart!.timeIntervalSinceNow
      self.lastPlan = solution
      (self.lastLog,self.lastErrorLog) = loadLogs()
      //try? FileManager.default.removeItem(at: self.tempDirURL!)
      return true
    } else {
      return false
    }
  }
  
  func checkForSolution() throws -> String? {
    print("cpt 5 checking for solution") //TODO: delete
    
//    let handle = standardOutput.fileHandleForReading
//    let data = handle.readDataToEndOfFile()
//    print(String(data: data, encoding: .utf8)!)
//
//    let handle2 = standardError.fileHandleForReading
//    let data2 = handle2.readDataToEndOfFile()
//    print(String(data: data2, encoding: .utf8)!)
    
    print("End of stdout and stderr")
    
    if let output = try? String(
      contentsOf: self.tempDirURL!.appendingPathComponent(
        "plan.SOL"),
      encoding: String.Encoding.utf8),
      !output.starts(with: "Not found")
    {
      return output
    }
    return nil
  }
  
  public var computationDeadline: Double {
    return Double(maxTime)
  }
  
  private var isAfterDeadline: Bool {
    return -computationStart!.timeIntervalSinceNow > computationDeadline
  }
  
  private func terminateProcess() throws {
    fatalError("Not implemented")
  }
  
  public func getLastRawPlan() -> (plan: [String],
                                   log: String,
                                   errorLog: String,
                                   duration: [Double])
  {
    if let plan = self.lastPlan {
      return ([plan], "", "", [computationDuration!])
    } else {
      let elapsedTime = -self.computationStart!.timeIntervalSinceNow
      let (log, errorLog) = loadLogs()
      return ([], log, errorLog, [elapsedTime])
    }
  }
  
//  public func getLastPlan() -> (TemporalPlan?, Double?, String, String, Double) {
//    if let plan = self.lastPlan {
//      let (parsedPlan, cost) = PlanParser.parseTemporalPlannerOutput(plannerOutput: plan)
//      return (parsedPlan, cost, lastLog!, lastErrorLog!, computationDuration!)
//    } else {
//      let elapsedTime = -self.computationStart!.timeIntervalSinceNow
//      let (log, errorLog) = loadLogs()
//      return (nil, nil, log, errorLog, elapsedTime)
//    }
//  }
  
  private func loadLogs() -> (String, String) {
    let log = (try? String(
      contentsOf: self.tempDirURL!.appendingPathComponent("plannerFRASLog.txt"),
      encoding: String.Encoding.utf8)) ??
    "Logs not found - todo implement for docker."
    let plannerErrorOutput = (try? String(
      contentsOf: self.tempDirURL!.appendingPathComponent("plannerFRASLogErr.txt"))
      ) ?? "No log"
    return (log,plannerErrorOutput)
  }
  
  public var shortDescription: String {
    return "CPT5 temporal optimal"
  }
  
  public struct Error : PDDLPlannerError {
      public init(message: String) {
          self.message = message
          self.errorCode = nil
      }
      public init(message: String, errorCode: Int) {
          self.message = message
          self.errorCode = errorCode
      }
      public let message: String
      public let errorCode: Int?
  }
}
