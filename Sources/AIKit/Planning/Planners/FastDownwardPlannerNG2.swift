//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/4/22.
//

import Foundation

//public actor FastDownwardPlannerNG2: FDRPlanner {
//  public private(set) var plannerStart: Date?
//  public private(set) var translatorStart: Date?
//  public var computationStart: Date? {
//    translatorStart ?? plannerStart
//  }
//  public private(set) var state: FastDownwardPlannerNG.State
//  public private(set) var lastPlan: String?
//  public private(set) var lastLog: String?
//  public private(set) var lastErrorLog: String?
//  public private(set) var computationDuration: Double?
//  public private(set) var translationDuration: Double?
//  public private(set) var planCache: URL?
//  //public let plannerConfig: Config
//  public let mode: PlannerType = .classical
//  public var verbose: Bool
//  public private(set) var anytime: Bool
//  public var maxTime: Int?
//  //public private(set) var searchStringType: String
//  public private(set) var searchParameters: [String]
//  private var problemPDDL: String?
//  private var domainPDDL: String?
//  private var representationFDR: String?
//  private var terminatePlannerSignal: Bool
//  private var lastPlanRead: Bool
//  public private(set) var plans: [String]
//  public private(set) var plansComputationDurations: [Double]
//  private var waitForNextWillReturnCache: Bool
//  static public let astarPotentParameters = ["--search", "astar(diverse_potentials(num_samples=1000, max_num_heuristics=infinity, max_potential=1e8, lpsolver=CPLEX, transform=no_transform(), cache_estimates=true, random_seed=-1))"]
//  
//  /// Returns an initialized FastDownwardPlanner class.
//  /// - Parameters:
//  ///   - searchString: FastDownward's search string or allias name.
//  ///   - searchStringType: Specify if searchString is a search string or allias name.
//  ///   - plannerConfig: Config that contains path to FastDownward planner.
//  ///   - anytime: Anytime planner returns plans during the compution.
//  ///              Not anytime planner returns plan after it finishes.
//  ///   - maxTime: Maximum computation time. After this time the planner is terminated.
//  ///   - verbose: Verbose.
//  public init(
//    searchParameters: [String],
//    anytime: Bool,
//    maxTime: Int,
//    verbose : Bool = false)
//  {
//    self.verbose = verbose
//    self.anytime = anytime
//    self.maxTime = maxTime
//    self.searchParameters = ["downward"] + searchParameters
//    self.plannerStart = Date()
//    self.plans = []
//    self.plansComputationDurations = []
//    self.state = .iddle
//    self.lastPlan = nil
//    self.computationDuration = nil
//    self.planCache = nil
//    self.lastLog = "NA"
//    self.lastErrorLog = "NA"
//    self.waitForNextWillReturnCache = false
//    self.terminatePlannerSignal = false
//    self.lastPlanRead = false
//  }
//  
//  public var computationDeadline: Double? {
//    guard let maxTime = maxTime else {
//      return nil
//    }
//    return Double(maxTime)
//  }
//  
//  private var isAfterDeadline: Bool {
//    guard let computationDeadline = computationDeadline else {
//      return false
//    }
//    return -plannerStart!.timeIntervalSinceNow > computationDeadline
//  }
//  
//  /// Starts FastDownward planner. After the start the function returns imeadiately.
//  /// - Parameters:
//  ///   - problemPDDL: Problem PDDL
//  ///   - domainPDDL: Domain PDDL
//  ///   - planCache: URL of file where to store computed plan. If there is a cache that
//  ///                match problem and domain PDDLs. The planner is not started and
//  ///                the plan is loaded from the cache.
//  public func start(problemPDDL: String, domainPDDL: String, planCache: URL?) throws {
//    guard state != .running && state != .translating else {
//      fatalError("Planner already started!")
//    }
//    self.problemPDDL = problemPDDL
//    self.domainPDDL = domainPDDL
//    if let planCache = planCache,
//       let (plannerOutput, duration, log) = PDDLCache.loadPlannerOutputFromCache(
//        pddl: problemPDDL,
//        domainPDDL: domainPDDL,
//        planCache: planCache)
//    {
//      lastPlan = plannerOutput
//      state = .finished
//      computationDuration = duration
//      lastLog = log
//      lastErrorLog = "plan loaded from cache - no error log implemented - todo"
//      waitForNextWillReturnCache = true
//      return
//    }
//    self.planCache = planCache
//    self.state = .translating
//    if self.verbose {
//      print("Started translation (FastDownwardPlannerNG.swift)")
//    }
//    
//    Task.detached(priority: .userInitiated) {
//      let (representationFDR, computationDuration) = try await PythonManager.shared.fastdownward_translate(
//        domainPddl: domainPDDL,
//        problemPddl: problemPDDL,
//        verbose: self.verbose)
//    }
//    
//    DispatchQueue.global(qos: .userInitiated).async {
//      self.semaphore.wait()
//      self.translatorStart = Date()
//      self.semaphore.signal()
//      let representationFDR = try! PythonInteroperabilityManager.defaultManager.fastdownward_translate(
//        domainPddl: domainPDDL,
//        problemPddl: problemPDDL,
//        verbose: self.verbose)
//      self.semaphore.wait()
//      self.translationDuration = -self.translatorStart!.timeIntervalSinceNow
//      self.representationFDR = representationFDR
//      self.semaphore.signal()
//      self.start(representationFDR: representationFDR, planCache: planCache)
//    }
//  }
//  
//  public func start(representationFDR: String, planCache: URL?) {
//    self.semaphore.wait()
//    guard state != .running else {
//      fatalError("Planner already started!")
//    }
//    self.planCache = planCache
//    self.state = .running
//    self.representationFDR = representationFDR
//    self.semaphore.signal()
//    if self.verbose {
//      print("started (FastDownwardPlanner.swift)")
//    }
//    
//    DispatchQueue.global(qos: .userInitiated).async {
//      self.semaphore.wait()
//      self.plannerStart = Date()
//      self.semaphore.signal()
////      let sas_representation = try! PythonInteroperabilityManager.defaultManager.fastdownward_translate(
////        domainPddl: domainPDDL,
////        problemPddl: problemPDDL,
////        verbose: self.verbose)
//      // Crazy pointers stuff: https://forums.swift.org/t/swift-function-as-a-callback-to-a-c-function/37409/11
//      let selfWeak = Unmanaged.passRetained(Weak(self))
//      let selfPointer = selfWeak.toOpaque()
//      var cSearchParameters = self.searchParameters.map { strdup($0) }
//      let planCString = fastdownward_planner(
//        representationFDR,
//        &cSearchParameters,
//        Int32(cSearchParameters.count),
//        0,
//        selfPointer,
//        { context, planCString in
//          guard let context = context else { fatalError() }
//          let selfPointer = Unmanaged<Weak<FastDownwardPlannerNG>>.fromOpaque(context)
//          let selfWeakContext = selfPointer.takeUnretainedValue()
//          guard let selfContext = selfWeakContext.value else { fatalError() }
//          let plan = planCString.map {String(cString: $0)}
//          planCString?.deallocate()
//          //print(plan ?? "downward planner failure")
//          selfContext.semaphore.wait()
//          selfContext.computationDuration = -selfContext.plannerStart!.timeIntervalSinceNow
//          if let plan = plan {
//            if plan != "Nothing was found" {
//              selfContext.lastPlan = plan
//              selfContext.lastPlanRead = false
//              selfContext.plans.append(plan)
//              selfContext.plansComputationDurations.append(-selfContext.computationStart!.timeIntervalSinceNow)
//            }
//          } else {
//            selfContext.state = .failed
//          }
//          selfContext.semaphore.signal()
//        },
//        { context in
//          guard let context = context else { fatalError() }
//          let selfPointer = Unmanaged<Weak<FastDownwardPlannerNG>>.fromOpaque(context)
//          let selfWeakContext = selfPointer.takeUnretainedValue()
//          guard let selfContext = selfWeakContext.value else { fatalError() }
//          selfContext.semaphore.wait()
//          let terminate = selfContext.terminatePlannerSignal
//          selfContext.semaphore.signal()
//          return terminate ? 1 : 0
//        })
//      for ptr in cSearchParameters { free(ptr!) }
//      _ = selfWeak.takeRetainedValue()
//      
//      let plan = planCString.map {String(cString: $0)}
//      planCString?.deallocate()
//      //print(plan ?? "downward planner failure")
//      self.semaphore.wait()
//      self.computationDuration = -self.plannerStart!.timeIntervalSinceNow
//      if let plan = plan {
//        if plan != "Nothing was found" {
//          self.lastPlan = plan
//          self.plans.append(plan)
//          self.plansComputationDurations.append(-self.computationStart!.timeIntervalSinceNow)
//          self.lastPlanRead = false
//        }
//        self.state = .finished
//      } else {
//        self.state = .failed
//      }
//      self.semaphore.signal()
//    }
//  }
//  
//  private func terminatePlanner() {
//    self.semaphore.wait()
//    self.terminatePlannerSignal = true
//    var currentState = self.state
//    self.semaphore.signal()
//    while currentState == .running {
//      Thread.sleep(forTimeInterval: 1)
//      self.semaphore.wait()
//      currentState = self.state
//      self.semaphore.signal()
//    }
//  }
//  
//  /// Saves the computed plan into cache, if the cache was set, and reset the planner and prepares it for a new computation.
//  public func reset() throws {
//    if verbose {
//      print("reset (FastDownwardPlannerNG.swift)")
//    }
//    self.semaphore.wait()
//    let currentState = self.state
//    self.semaphore.signal()
//    switch currentState {
//    case .iddle:
//      return
//    case .running, .translating:
//      terminatePlanner()
//      fallthrough
//    case .finished:
//      if let planCache = planCache,
//         lastPlan != nil,
//         let problemPDDL = problemPDDL,
//         let domainPDDL = domainPDDL
//      {
//        try PDDLCache.savePlannerOutputToCache(
//          pddl: problemPDDL,
//          domainPDDL: domainPDDL,
//          plannerOutput: lastPlan!,
//          computationDuration: computationDuration!,
//          plannerLog: lastLog!,
//          plannerErrorOutput: lastErrorLog!,
//          planCache: planCache)
//      }
//      fallthrough
//    case .failed:
//      self.problemPDDL = nil
//      self.domainPDDL = nil
//      self.plans = []
//      self.plansComputationDurations = []
//      self.state = .iddle
//      self.lastPlan = nil
//      self.lastLog = "NA"
//      self.lastErrorLog = "NA"
//      self.waitForNextWillReturnCache = false
//      self.computationDuration = nil
//      self.planCache = nil
//      self.terminatePlannerSignal = false
//      self.lastPlanRead = false
//    }
//  }
//  
//  
//  /// Wait until the planner finds a new plan.
//  /// - Returns: True if new plan is available. False if the planner terminated and did not find a plan.
//  public func waitForNextPlan() throws -> Bool {
//    if waitForNextWillReturnCache {
//      self.waitForNextWillReturnCache = false
//      return true
//    }
//    self.semaphore.wait()
//    guard self.state != .iddle else {
//      fatalError("Invalid planner state")
//    }
//    if self.state != .running && self.state != .translating {
//      self.semaphore.signal()
//      return false
//    }
//    
//    if !lastPlanRead && lastPlan != nil {
//      self.lastPlanRead = true
//      self.semaphore.signal()
//      return true
//    } else {
//      self.semaphore.signal()
//      while true {
//        Thread.sleep(forTimeInterval: 1)
//        let breakLoop: Bool
//        semaphore.wait()
//        breakLoop = !self.lastPlanRead && lastPlan != nil
//        semaphore.signal()
//        if breakLoop {
//          self.lastPlanRead = true
//          return true
//        }
//        semaphore.wait()
//        let afterDeadline = self.isAfterDeadline
//        semaphore.signal()
//        if afterDeadline {
//          terminatePlanner()
//          return false
//        }
//      }
//    }
//  }
//  
//  
//  /// Returns the last found plan. It return nil if planner have not found any plan.
//  public func getLastRawPlan() -> (
//    plan: [String],
//    log: String,
//    errorLog: String,
//    duration: [Double]) {
//      semaphore.wait()
//      //if let plan = self.lastPlan {
//      if !plans.isEmpty {
//        let log = lastLog!
//        let errorLog = lastErrorLog!
//        //let computationDurationRet = computationDuration!
//        semaphore.signal()
//        return (plans, log, errorLog, plansComputationDurations)
//      } else {
//        let elapsedTime = -self.computationStart!.timeIntervalSinceNow
//        let (log, errorLog) = ("NA", "NA")
//        semaphore.signal()
//        return ([], log, errorLog, [elapsedTime])
//      }
//    }
//  
//  
//  public var shortDescription: String {
//    return "FastDownwardNG \(searchParameters.dropFirst()) Timeout \(maxTime ?? -1)"
//  }
//  
//  /// An error thrown by FastDownward planner.
//  public struct Error : PDDLPlannerError {
//      public init(message: String) {
//          self.message = message
//          self.errorCode = nil
//      }
//      public init(message: String, errorCode: Int) {
//          self.message = message
//          self.errorCode = errorCode
//      }
//      public let message: String
//      public let errorCode: Int?
//  }
//}
//
//
//
