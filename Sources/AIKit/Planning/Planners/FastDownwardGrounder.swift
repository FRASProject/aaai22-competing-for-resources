//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/17/20.
//

import Foundation

public final class FastDownwardGrounder {
  private static let python2 = "/usr/bin/python2"
  private static let python3 = "/usr/bin/python3"
  private let executablePath: String
  private let executablePreprocesingPath: String?
  private let temporal: Bool
  public var verbose: Bool
  private var fdrCache: URL?
  public init(
    temporal: Bool,
    runPreprocessing: Bool = false,
    verbose: Bool = false,
    configKey: String? = nil,
    configKeyPreprocessing: String? = nil
  ) {
    self.temporal = temporal
    self.verbose = verbose
    if let configKey = configKey {
      self.executablePath = ExperimentManager.defaultManager.systemConfig[configKey]!.string!
    } else if temporal {
      self.executablePath = ExperimentManager.defaultManager.systemConfig[
        "TemporalFastDownwardGrounderPath"]!.string!
    } else {
      self.executablePath = ExperimentManager.defaultManager.systemConfig[
        "FastDownwardGrounderPath"]!.string!
    }
    if runPreprocessing {
      if let configKeyPreprocessing = configKeyPreprocessing {
        self.executablePreprocesingPath = ExperimentManager.defaultManager.systemConfig[configKeyPreprocessing]!.string!
      } else if temporal {
        self.executablePreprocesingPath = ExperimentManager.defaultManager.systemConfig[
          "TemporalFastDownwardPreprocessorPath"]!.string!
      } else {
        self.executablePreprocesingPath = ExperimentManager.defaultManager.systemConfig[
          "FastDownwardPreprocessorPath"]!.string!
      }
    } else {
      executablePreprocesingPath = nil
    }
  }
  public func ground(
    problemPDDL: String,
    domainPDDL: String,
    fdrCache: URL? = nil,
    maxTime: Int? = nil) throws -> (
      sas: String,
      variables: String?,
      groups: String?,
      preprocessorOutput: String?,
      computationDuration: Double
    )?
  {
#if os(Linux)
    // On linux, for some reason, spawning more than one process in parallel causes strange bugs.
    ExperimentManager.defaultManager.semaphore1.wait()
    defer {
      ExperimentManager.defaultManager.semaphore1.signal()
    }
#endif
    self.fdrCache = fdrCache
    if let fdrCache = fdrCache,
       let cache = PDDLGroundingFDRCache.loadFromCache(fdrCache)
    {
      return (
        cache.sasOutput,
        cache.variables,
        cache.groups,
        cache.preprocessorOutput,
        cache.computationDuration)
    }
    let temporaryDirectory = "tmp_" + UUID().uuidString
    if verbose {
      print("Starting grounder. Temp dir :\(temporaryDirectory)")
    }
    try FileManager.default.createDirectory(
      atPath: temporaryDirectory,
      withIntermediateDirectories: false)
    let tempDirURL = URL(fileURLWithPath: temporaryDirectory, isDirectory: true)
    try problemPDDL.write(
      to: tempDirURL.appendingPathComponent("problem.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    try domainPDDL.write(
      to: tempDirURL.appendingPathComponent("domain.pddl"),
      atomically: true,
      encoding: String.Encoding.utf8)
    let computationStart = Date()
    let proc = Process()
    proc.executableURL = URL(
      fileURLWithPath: temporal ? FastDownwardGrounder.python2 : FastDownwardGrounder.python3)
    proc.arguments = [executablePath, "domain.pddl", "problem.pddl"]
    proc.currentDirectoryURL = tempDirURL
    let stdout = Pipe()
    proc.standardOutput = stdout
    try proc.run()
    while proc.isRunning && (
            maxTime == nil || -computationStart.timeIntervalSinceNow < Double(maxTime!)) {
      Thread.sleep(forTimeInterval: 1)
    }
    
    if proc.isRunning {
      proc.terminate()
      proc.waitUntilExit()
      try FileManager.default.removeItem(at: tempDirURL)
      return nil
    }
    let computationDuration = -computationStart.timeIntervalSinceNow
    
    if proc.terminationStatus != 0 {
      throw AIKitError(message: "Grounder returned non-zero code: \(proc.terminationStatus)")
    }
    
    if let executablePreprocesingPath = executablePreprocesingPath {
      let sasBefore = try String(contentsOf: tempDirURL.appendingPathComponent("output.sas"))
      let proc = Process()
      proc.executableURL = URL(fileURLWithPath: executablePreprocesingPath)
      proc.currentDirectoryURL = tempDirURL
      let stdout = Pipe()
      let stdin = Pipe()
      proc.standardOutput = stdout
      proc.standardInput = stdin
      try proc.run()
      let writeHandle = stdin.fileHandleForWriting
      let data = sasBefore.data(using: .utf8)!
      writeHandle.write(data)
      writeHandle.closeFile()
      proc.waitUntilExit()
      if proc.terminationStatus != 0 {
        throw AIKitError(message: "Preprocesser returned non-zero code: \(proc.terminationStatus)")
      }
    }
    
    let sas = try String(contentsOf: tempDirURL.appendingPathComponent("output.sas"))
    let variables = try? String(contentsOf: tempDirURL.appendingPathComponent("variables.groups"))
    let groups = try? String(contentsOf: tempDirURL.appendingPathComponent("all.groups"))
    let preprocessorOutput = try? String(contentsOf: tempDirURL.appendingPathComponent("output"))
    
    try FileManager.default.removeItem(at: tempDirURL)
    if verbose {
      print("Grounder finished succesfuly.")
    }
    if let fdrCache = fdrCache {
      try PDDLGroundingFDRCache(
        problemPddl: problemPDDL,
        domainPddl: domainPDDL,
        sasOutput: sas,
        variables: variables,
        groups: groups,
        preprocessorOutput: preprocessorOutput,
        computationDuration: computationDuration).saveToCache(fdrCache)
    }
    return (
      sas: sas,
      variables: variables,
      groups: groups,
      preprocessorOutput: preprocessorOutput,
      computationDuration: computationDuration)
  }
}

extension FastDownwardGrounder {
  public func ground(
    _ problem: PDDLPlanningProblem,
    fdrCache: URL? = nil,
    maxTime: Int? = nil) throws -> (
      sas: String,
      variables: String?,
      groups: String?,
      preprocessorOutput: String?,
      computationDuration: Double
    )?
  {
    return try ground(
      problemPDDL: problem.problem.text,
      domainPDDL: problem.domain.text,
      fdrCache: fdrCache,
      maxTime: maxTime)
  }
}
