//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/4/22.
//

import Foundation

public final class ClassicalToTemporalAdapter: PDDLTemporalPlanner {
  public let classicalPlanner: PDDLPlanner
  private var durationOfUnitTimestamp: Int?
  private var unitsParameters: [String]?
  public init(classicalPlanner: PDDLPlanner) {
    self.classicalPlanner = classicalPlanner
    self.durationOfUnitTimestamp = nil
  }
  
  public func start(problemPDDL: String, domainPDDL: String, planCache: URL?) throws {
    let problem = try PDDLPlanningProblem(domain: domainPDDL, problem: problemPDDL)
    try start(problem: problem, planCache: planCache)
  }
  
  public func start(problem: PDDLPlanningProblem, planCache: URL?) throws {
    let classicalProblem = problem.convertingToClassicalPlanningProblem(
      removeDurationFunctions: true)
    self.unitsParameters = classicalProblem.problem.units
    self.durationOfUnitTimestamp = classicalProblem.durationOfUnitTimestamp
    try classicalPlanner.start(problem: classicalProblem, planCache: planCache)
  }
  
  public func reset() throws {
    try classicalPlanner.reset()
  }
  
  public func waitForNextPlan() throws -> Bool {
    try classicalPlanner.waitForNextPlan()
  }
  
  public var shortDescription: String {
    "Temporal adapter of " + classicalPlanner.shortDescription
  }
  
  public func getLastPlan() -> ([TemporalPlan], [Double], String, String, [Double]) {
    let (plans, costs, log, errorLog, durations) = classicalPlanner.getLastPlan()
    let temporalPlans = plans.map {
      TemporalPlan(
        classicalPlan: $0.parsed!,
        durationOfUnitTimestamp: self.durationOfUnitTimestamp!,
        units: self.unitsParameters!,
        removingNoopActions: true)
    }
    return (temporalPlans, costs, log, errorLog, durations)
  }
  
  public func getLastRawPlan() -> (plan: [String], log: String, errorLog: String, duration: [Double]) {
    let (plans, costs, log, errorLog, durations) = getLastPlan()
    let rawPlans = zip(plans, costs).map {
      "\($0.0.text)\n; cost = \($0.1) (general cost)"
    }
    return (rawPlans, log, errorLog, durations)
  }
}
