//
//  PDDLPlannerError.swift
//  Planner
//
//  Created by Pavel Rytir on 1/10/20.
//

import Foundation

/// Error that can be throws by planners in Planner framework.
public protocol PDDLPlannerError : Error {}
