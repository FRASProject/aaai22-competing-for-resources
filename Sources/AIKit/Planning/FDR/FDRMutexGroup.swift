//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

public struct FDRMutexGroup {
  public let extraLines: [String] // Mainly for symk planner.
  public let facts: [FDRFact]
}

extension FDRMutexGroup {
  public var text: String {
    var result = [String]()
    result.append("begin_mutex_group")
    result.append(contentsOf: extraLines)
    result.append("\(facts.count)")
    result.append(contentsOf: facts.map {"\($0.variable) \($0.value)"})
    result.append("end_mutex_group")
    return result.joined(separator: "\n")
  }
}
