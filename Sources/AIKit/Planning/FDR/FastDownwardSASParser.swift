//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

public enum FastDownwardSASParser {
  public static func parseSAS(_ input: String, operatorCostMultiplier: Int = 1) -> FDRCoding {
    // Check file format version
    let versionString = input[
      input.range(of: "begin_version")!.upperBound..<input.range(
        of: "end_version")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    precondition(versionString == "3", "Wrong file format version")
    // Metric
    let metricString = input[
      input.range(of: "begin_metric")!.upperBound..<input.range(
        of: "end_metric")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    let metric = Int(metricString)!
    //Variables
    let variablesLines = input.components(separatedBy: "begin_variable").compactMap { s -> Substring? in
      if let range = s.range(of: "end_variable") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
    let variables = variablesLines.map { lines -> FDRVariable in
      let name = lines[0]
      let axiomLayer = Int(lines[1])!
      let numberOfValues = Int(lines[2])!
      let values = lines[3..<(3 + numberOfValues)]
      return FDRVariable(name: name, axiomLayer: axiomLayer, values: Array(values))
    }
    // Mutex groups
    let mutexLines = input.components(separatedBy: "begin_mutex_group").compactMap { s -> Substring? in
      if let range = s.range(of: "end_mutex_group") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
    let mutexGroups = mutexLines.map { lines -> FDRMutexGroup in
      var offset = 0
      var extraLines = [String]()
      while Int(lines[offset]) == nil {
        extraLines.append(lines[offset])
        offset += 1
      }
      let lines = Array(lines.dropFirst(offset))
      let numberOfFacts = Int(lines[0])!
      let facts = lines[1...numberOfFacts].map { s -> FDRFact in
        let line = s.components(separatedBy: " ")
        return FDRFact(variable: Int(line[0])!, value: Int(line[1])!)
      }
      return FDRMutexGroup(extraLines: extraLines, facts: facts)
    }
    // Init state
    let initStateString = input[
      input.range(of: "begin_state")!.upperBound..<input.range(
        of: "end_state")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    let initState = initStateString.components(separatedBy: .newlines).map {Int($0)!}
    // Goal
    let goalString = input[
      input.range(of: "begin_goal")!.upperBound..<input.range(
        of: "end_goal")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    let goal = goalString.components(separatedBy: .newlines).dropFirst().map {
      line -> (variable: Int, value: Int) in
      let variableValue = line.components(separatedBy: .whitespaces)
      precondition(variableValue.count == 2)
      return (variable: Int(variableValue[0])!, value: Int(variableValue[1])!)
    }
    // Operators
    let operatorsStrings = input.components(separatedBy: "begin_operator").compactMap { s -> Substring? in
      if let range = s.range(of: "end_operator") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
    let operators = operatorsStrings.map { op -> FDROperator in
      let action = op.first!.components(separatedBy: .whitespaces)
      let preconditions = TemporalFastDownwardParser.parsePreconditions(lines: op, lineNumber: 1)
      let effectsFirstLine = preconditions.count + 2
      let effects = parseEffects(lines: op, lineNumber: effectsFirstLine)
      let cost = Int(op.last!)! * operatorCostMultiplier
      return FDROperator(
        name: action.first!,
        parameters: Array(action.dropFirst()),
        preconditions: preconditions,
        effects: effects,
        cost: cost)
    }
    // Axioms
    let axiomsStrings = input.components(separatedBy: " begin_rule").compactMap { s -> Substring? in
      if let range = s.range(of: "end_rule") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
    
    precondition(axiomsStrings.isEmpty, "Finish FDR parser implementation.")
    
    // Preprocessing
    let preprocessingString = input.range(of: "begin_SG").map { input[
      $0.lowerBound..<input.range(
        of: "end_CG")!.upperBound].trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    return FDRCoding(
      metric: metric,
      variables: Dictionary(
        uniqueKeysWithValues: variables.enumerated().map {($0.offset, $0.element)}),
      mutexGroups: mutexGroups,
      initState: Dictionary(
        uniqueKeysWithValues: initState.enumerated().map {($0.offset, $0.element)}),
      goal: Dictionary(uniqueKeysWithValues: goal),
      operators: Dictionary(
        uniqueKeysWithValues: operators.enumerated().map {($0.offset, $0.element)}),
      preprocessingString: preprocessingString)
  }
  private static func parseEffects(lines: [String], lineNumber: Int) ->
  [FDRConditionEffect]
  {
    let numberOfEffects = Int(lines[lineNumber])!
    var effects = [FDRConditionEffect]()
    for i in 0..<numberOfEffects {
      let line = lines[lineNumber + 1 + i].components(separatedBy: .whitespaces)
      let numberOfPreconditions = Int(line[0])!
      var preconditions = [FDRFact]()
      for i in 0..<numberOfPreconditions {
        preconditions.append(FDRFact(variable: Int(line[i + 1])!, value: Int(line[i + 2])!))
      }
      let offset = numberOfPreconditions * 2
      effects.append(FDRConditionEffect(
        variable: Int(line[1 + offset])!,
        oldValueCondition: Int(line[2 + offset])!,
        newValue: Int(line[3 + offset])!, preconditions: preconditions))
    }
    return effects
  }
}
