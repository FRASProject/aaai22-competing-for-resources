//
//  File.swift
//  
//
//  Created by Pavel Rytir on 10/27/21.
//

import Foundation

extension FDRCoding {
  public func projecting(on projectionVariables: Set<Int>) -> FDRCoding? {
    precondition(Set(self.variables.keys).isSuperset(of: projectionVariables))
    let variablesToKeep = self.variables.filter {
      projectionVariables.contains($0.key)
    }
    let variablesIndexTranslation = Dictionary(
      uniqueKeysWithValues: variablesToKeep.sorted(by: {$0.key < $1.key}).enumerated().map {
        ($0.element.key, $0.offset)})
    if variablesToKeep.isEmpty { return nil }
    let newVariables = Dictionary(
      uniqueKeysWithValues: variablesToKeep.map {
        (variablesIndexTranslation[$0.key]!, $0.value) })
    
    let newMutexGroups = mutexGroups.compactMap {$0.projecting(on: variablesIndexTranslation)}
    let newInitState = Dictionary(uniqueKeysWithValues: initState.compactMap { (k,v) in
      variablesIndexTranslation[k].map {($0, v)}})
    let newGoal = Dictionary(uniqueKeysWithValues: goal.compactMap { (k,v) in
      variablesIndexTranslation[k].map {($0, v)}})
    if newGoal.isEmpty { return nil }
    let newOperators = operators.compactMapValues { $0.projecting(on: variablesIndexTranslation) }
    if newOperators.isEmpty { return nil }
    return FDRCoding(
      metric: metric,
      variables: newVariables,
      mutexGroups: newMutexGroups,
      initState: newInitState,
      goal: newGoal,
      operators: newOperators,
      preprocessingString: preprocessingString)
  }
}

extension FDRMutexGroup {
  public func projecting(on variables: [Int: Int]) -> FDRMutexGroup? {
    let newfacts = facts.compactMap {$0.translate(variables)}
    if newfacts.isEmpty {
      return nil
    } else {
      return FDRMutexGroup(extraLines: extraLines, facts: newfacts)
    }
  }
}

extension FDRConditionEffect {
  public func projecting(on variables: [Int: Int]) -> FDRConditionEffect? {
    guard let newVariable = variables[variable] else { return nil }
    let newPreconditions = preconditions.compactMap {
      $0.translate(variables)
    }
    return FDRConditionEffect(variable: newVariable, oldValueCondition: oldValueCondition, newValue: newValue, preconditions: newPreconditions)
  }
}

extension FDROperator {
  public func projecting(on variables: [Int: Int]) -> FDROperator? {
    let newPreconditions = preconditions.compactMap { (k,v) in
      variables[k].map {($0, v)}}
    let newEffects = effects.compactMap { $0.projecting(on: variables) }
    if newEffects.isEmpty {
      return nil
    } else {
      return FDROperator(name: name, parameters: parameters, preconditions: newPreconditions, effects: newEffects, cost: cost)
    }
  }
}
