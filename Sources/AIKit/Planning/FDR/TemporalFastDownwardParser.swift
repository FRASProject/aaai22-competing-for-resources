//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/26/20.
//

import Foundation
import MathLib

public enum TemporalFastDownwardParser {
  public static func parseSAS(_ input: String) -> [PlanAction] {
    let operators = input.components(separatedBy: "begin_operator").compactMap { s -> Substring? in
      if let range = s.range(of: "end_operator") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
      
    let result = operators.map { op -> PlanAction in
      let action = op.first!.components(separatedBy: .whitespaces)
      return PlanAction(
        name: action.first!,
        parameters: Array(action.dropFirst()))
    }
    return result
  }
  
  public static func parseSASNew(_ tfdOutput: (sas: String, variables: String, groups: String)) ->
  TemporalProblemFDRCoding
  {
    return parseSASNew(inputSas: tfdOutput.sas, inputVariables: tfdOutput.variables)
  }
  
  public static func parseSASNew(inputSas: String, inputVariables: String) -> TemporalProblemFDRCoding
  {
    let varRanges = inputVariables.findAllOccurenciesOf(regex: #"var\d+"#)
    var varStrings = [String]()
    for i in varRanges.indices {
      if i == varRanges.count - 1 {
        varStrings.append(
          String(
            inputVariables[varRanges[i].upperBound..<inputVariables.endIndex].trimmingCharacters(
              in: .whitespacesAndNewlines)))
      } else {
        varStrings.append(
          String(
            inputVariables[varRanges[i].upperBound..<varRanges[i + 1].lowerBound].trimmingCharacters(
              in: .whitespacesAndNewlines)))
      }
    }
    let variables = varStrings.map {
      Dictionary<Int, String>(uniqueKeysWithValues: $0.components(separatedBy: .newlines).map {
        let numberName = $0.components(separatedBy: ":")
        precondition(numberName.count == 2)
        let number = Int(numberName[0].trimmingCharacters(in: .whitespaces))!
        let name = numberName[1].trimmingCharacters(in: .whitespaces)
        return (number, name)
      })
    }
    let initStateString = inputSas[
      inputSas.range(of: "begin_state")!.upperBound..<inputSas.range(
        of: "end_state")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    let initState = initStateString.components(separatedBy: .newlines).map {
      line -> Int in
      if let n = Int(line) {
        return n
      } else {
        let d = Double(line)!
        if d == d.rounded() {
          return Int(d)
        } else {
          fatalError("Non integer number in SAS file. Improve parsing.")
        }
      }
    }
    
    let operators = inputSas.components(separatedBy: "begin_operator").compactMap { s -> Substring? in
      if let range = s.range(of: "end_operator") {
        return s[s.startIndex..<range.lowerBound]
      }
      return nil
    }.map {
      $0.trimmingCharacters(in: .whitespacesAndNewlines)
    }.map { $0.components(separatedBy: .newlines) }
      
    let result = operators.map { op -> TemporalFDROperator in
      let action = op.first!.components(separatedBy: .whitespaces)
      let durationVariableIndex = Int(op[1].dropFirst(2))!
      
      let preconditionsAtStart =  parsePreconditions(lines: op, lineNumber: 2)
      let preconditionsOverAll =  parsePreconditions(lines: op, lineNumber: 2 + preconditionsAtStart.count + 1)
      let preconditionsAtEnd =  parsePreconditions(
        lines: op,
        lineNumber: 2 + preconditionsAtStart.count + 1 + preconditionsOverAll.count + 1)
      let numberOfEffectsAtStartLineNumber = 2 + preconditionsAtStart.count + 1 +
        preconditionsOverAll.count + 1 + preconditionsAtEnd.count + 1
      
      let effectsAtStart = parseEffects(lines: op, lineNumber: numberOfEffectsAtStartLineNumber)
      let effectsAtEnd = parseEffects(
        lines: op,
        lineNumber: numberOfEffectsAtStartLineNumber + effectsAtStart.count + 1)
      
      return TemporalFDROperator(
        name: action.first!,
        parameters: Array(action.dropFirst()),
        durationVariableIndex: durationVariableIndex,
        preconditionAtStart: preconditionsAtStart,
        preconditionOverAll: preconditionsOverAll,
        preconditionAtEnd: preconditionsAtEnd,
        effectsAtStart: effectsAtStart,
        effectsAtEnd: effectsAtEnd
        )
    }
    
    let goalString = inputSas[
      inputSas.range(of: "begin_goal")!.upperBound..<inputSas.range(
        of: "end_goal")!.lowerBound].trimmingCharacters(in: .whitespacesAndNewlines)
    let goal = goalString.components(separatedBy: .newlines).dropFirst().map {
      line -> (variable: Int, value: Int) in
      let variableValue = line.components(separatedBy: .whitespaces)
      precondition(variableValue.count == 2)
      return (variable: Int(variableValue[0])!, value: Int(variableValue[1])!)
    }
    
    return TemporalProblemFDRCoding(
      variables: variables,
      operators: result,
      initState: initState,
      goal: goal)
  }
  
  private static func parseEffects(lines: [String], lineNumber: Int) ->
  [TemporalFDRConditionEffect]
  {
    let numberOfEffects = Int(lines[lineNumber])!
    var effects = [TemporalFDRConditionEffect]()
    for i in 0..<numberOfEffects {
      let line = lines[lineNumber + 1 + i].components(separatedBy: .whitespaces)
      precondition(
        line[0] == "0" && line[1] == "0",
        "finish effect fdr parser")
      
      let offset = Int(line[2])! * 2
      if offset != 0 {
        print("Warning: Incomplete conditional effect parsing")
      }
      effects.append(TemporalFDRConditionEffect(
        variable: Int(line[3 + offset])!,
        oldValueCondition: Int(line[4 + offset])!,
        newValue: Int(line[5 + offset])!))
    }
    return effects
  }
  
  static func parsePreconditions(lines: [String], lineNumber: Int) -> [(variable: Int, value: Int)] {
    let numberOfPreconditions = Int(lines[lineNumber])!
    var preconditions = [(variable: Int, value: Int)]()
    for i in 0..<numberOfPreconditions {
      let line = lines[lineNumber + 1 + i].components(separatedBy: .whitespaces)
      precondition(line.count == 2)
      let variableIdx = Int(line[0])!
      let value = Int(line[1])!
      preconditions.append((variable: variableIdx, value: value))
    }
    return preconditions
  }
}
