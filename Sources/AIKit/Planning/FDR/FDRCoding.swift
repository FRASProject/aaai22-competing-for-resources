//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/28/20.
//

import Foundation

public struct FDRCoding {
  public let metric: Int
  public let variables: [Int: FDRVariable]
  public let mutexGroups: [FDRMutexGroup]
  public let initState: [Int: Int]
  public let goal: [Int: Int]
  public let operators: [Int: FDROperator]
  public let operatorNameIndex: [PlanAction: Int]
  public let axioms: [String] = []
  public let preprocessingString: String?
  public init(
    metric: Int,
    variables: [Int: FDRVariable],
    mutexGroups: [FDRMutexGroup],
    initState: [Int: Int],
    goal: [Int: Int],
    operators: [Int: FDROperator],
    preprocessingString: String?)
  {
    self.metric = metric
    self.variables = variables
    self.mutexGroups = mutexGroups
    self.initState = initState
    self.goal = goal
    self.operators = operators
    self.preprocessingString = preprocessingString
    self.operatorNameIndex = Dictionary(
      uniqueKeysWithValues: operators.map {
        (PlanAction(name: $0.value.name, parameters: $0.value.parameters), $0.key)})
  }
}

extension FDRCoding {
  public func operatorIdxTranslationTableTo(other: FDRCoding) -> [Int: Int] {
    if operators.count != other.operators.count {
      print("Warning: some operators are not present in the other FDR representation.")
      //print(Set(operators.map {$0.value.planAction}).subtracting(other.operators.map {$0.value.planAction}))
    }
    //precondition(operators.count == other.operators.count)
    return Dictionary(uniqueKeysWithValues: operators.compactMap {
      guard let otherOperatorIndex = other.operatorNameIndex[$0.value.planAction] else {
        return nil
      }
      return ($0.key, otherOperatorIndex)
    })
  }
  
  
  public var text: String {
    var result = [String]()
    result.append("begin_version\n3\nend_version")
    result.append("begin_metric\n\(metric)\nend_metric")
    result.append("\(variables.count)")
    result.append(contentsOf: variables.sorted(by: {$0.key < $1.key}).map {$0.value.text})
    result.append("\(mutexGroups.count)")
    result.append(contentsOf: mutexGroups.map {$0.text})
    result.append("begin_state")
    result.append(contentsOf: initState.sorted(by: {$0.key < $1.key}).map {"\($0.value)"})
    result.append("end_state")
    result.append("begin_goal")
    result.append("\(goal.count)")
    result.append(contentsOf: goal.sorted(by: {$0.key < $1.key}).map {"\($0.key) \($0.value)"})
    result.append("end_goal")
    result.append("\(operators.count)")
    result.append(contentsOf: operators.sorted(by: { $0.key < $1.key }).map {$0.value.text})
    result.append("0")
    // Axioms todo:
    if let preprocessingString = preprocessingString {
      result.append(preprocessingString)
    }
    return result.joined(separator: "\n") + "\n"
  }
}

extension FDRCoding {
  public func mutliplyingCostFunctionBy(factors: [Int: Double]) -> FDRCoding {
    let newOperators = Dictionary(uniqueKeysWithValues: operators.map {
      ($0.key, $0.value.multiplyingCostBy(factor: factors[$0.key] ?? 1))
    })
    return FDRCoding(
      metric: 1,
      variables: variables,
      mutexGroups: mutexGroups,
      initState: initState,
      goal: goal,
      operators: newOperators,
      preprocessingString: preprocessingString)
  }
  public func increasingCostFunctionBy(increments: [Int: Int]) -> FDRCoding {
    let newOperators = Dictionary(uniqueKeysWithValues: operators.map {
      ($0.key, $0.value.increasingCostBy(increment: increments[$0.key] ?? 0))
    })
    return FDRCoding(
      metric: 1,
      variables: variables,
      mutexGroups: mutexGroups,
      initState: initState,
      goal: goal,
      operators: newOperators,
      preprocessingString: preprocessingString)
  }
  public func settingCostFunction(_ costFunction: [Int: Int], zeroElsewhere: Bool) -> FDRCoding {
    let newOperators = Dictionary(uniqueKeysWithValues: operators.map {
      ($0.key, $0.value.settingCost(to: costFunction[$0.key] ?? (zeroElsewhere ? 0 : $0.value.cost)))
    })
    return FDRCoding(
      metric: 1,
      variables: variables,
      mutexGroups: mutexGroups,
      initState: initState,
      goal: goal,
      operators: newOperators,
      preprocessingString: preprocessingString)
  }
}
