//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/30/20.
//

import Foundation
import Algorithms
import MathLib

public struct FDRState {
  public var variablesValues: [Int]
  public init(variablesValues: [Int]) {
    self.variablesValues = variablesValues
  }
}

public struct FDRStateRelaxed {
  public var variablesValues: [Set<Int>]
  public init(numberOfVariables: Int) {
    self.variablesValues = Array(repeating: Set(), count: numberOfVariables)
  }
  public init(_ state: FDRState) {
    self.variablesValues = state.variablesValues.map {Set([$0])}
  }
  public mutating func insert(fact: (variable: Int, value: Int)) {
    variablesValues[fact.variable].insert(fact.value)
  }
  public mutating func replaceVariableValueBy(fact: (variable: Int, value: Int)) {
    precondition(variablesValues[fact.variable].count < 2)
    variablesValues[fact.variable] = Set([fact.value])
  }
}

public struct TemporalProblemFDRCoding {
  public let variables: [[Int:String]]
  public let normalVariablesIndexSet: Set<Int>
  public let derivedVariablesIndexSet: Set<Int>
//  public var derivedVariables: [[Int: String]] {
//    variables.filter {$0.keys.contains(-2)}
//  }
  public var normalVariables: [[Int: String]] {
    variables.filter {!$0.keys.contains(-2)}
  }
  public let operatorIndex: [PlanAction: Int]
  public let operators: [TemporalFDROperator]
  public let initState: [Int]
  public let goal: [(variable: Int, value: Int)]
  public init(
    variables: [[Int:String]],
    operators: [TemporalFDROperator],
    initState: [Int],
    goal: [(variable: Int, value: Int)])
  {
    self.variables = variables
    self.operators = operators
    self.operatorIndex = Dictionary(
      operators.enumerated().map {
        (PlanAction(name: $0.element.name, parameters: $0.element.parameters), $0.offset)
      },
      uniquingKeysWith: { a, _ in a })
    self.initState = initState
    self.goal = goal
    self.normalVariablesIndexSet = Set(
      variables.enumerated().filter {!$0.element.keys.contains(-2)}.map {$0.offset})
    self.derivedVariablesIndexSet = Set(
      variables.enumerated().filter {$0.element.keys.contains(-2)}.map {$0.offset})
  }
}

public struct TemporalFDRConditionEffect {
  public let variable: Int
  public let oldValueCondition: Int
  public let newValue: Int
}

public struct TemporalFDROperator {
  public let name: String
  public let parameters: [String]
  public let durationVariableIndex: Int
  public let preconditionAtStart: [(variable: Int, value: Int)]
  public let preconditionOverAll: [(variable: Int, value: Int)]
  public let preconditionAtEnd: [(variable: Int, value: Int)]
  public let effectsAtStart: [TemporalFDRConditionEffect]
  public let effectsAtEnd: [TemporalFDRConditionEffect]
  public let preconditionEffectAtStart: [(variable: Int, value: Int)]
  public let preconditionEffectAtEnd: [(variable: Int, value: Int)]
  public init(
    name: String,
    parameters: [String],
    durationVariableIndex: Int,
    preconditionAtStart: [(variable: Int, value: Int)],
    preconditionOverAll: [(variable: Int, value: Int)],
    preconditionAtEnd: [(variable: Int, value: Int)],
    effectsAtStart: [TemporalFDRConditionEffect],
    effectsAtEnd: [TemporalFDRConditionEffect])
  {
    self.name = name
    self.parameters = parameters
    self.durationVariableIndex = durationVariableIndex
    self.preconditionAtStart = preconditionAtStart
    self.preconditionOverAll = preconditionOverAll
    self.preconditionAtEnd = preconditionAtEnd
    self.effectsAtStart = effectsAtStart
    self.effectsAtEnd = effectsAtEnd
    self.preconditionEffectAtStart = effectsAtStart.map { (variable: $0.variable, value: $0.oldValueCondition) }
    self.preconditionEffectAtEnd = effectsAtEnd.map { (variable: $0.variable, value: $0.oldValueCondition) }
  }
  
  public var effects: Chain2Sequence<[TemporalFDRConditionEffect], [TemporalFDRConditionEffect]> {
    chain(effectsAtStart, effectsAtEnd)
  }
  public var allPreconditions: Chain2Sequence<Chain2Sequence<Chain2Sequence<[(variable: Int, value: Int)], [(variable: Int, value: Int)]>, Chain2Sequence<[(variable: Int, value: Int)], [(variable: Int, value: Int)]>>, [(variable: Int, value: Int)]> {
    chain(chain(allPreconditionAtStart, allPreconditionAtEnd), preconditionOverAll)
  }
//  public var preconditionEffectAtStart: [(variable: Int, value: Int)] {
//    effectsAtStart.map { (variable: $0.variable, value: $0.oldValueCondition) }
//  }
//  public var preconditionEffectAtEnd: [(variable: Int, value: Int)] {
//    effectsAtEnd.map { (variable: $0.variable, value: $0.oldValueCondition) }
//  }
  public var allPreconditionAtStart: Chain2Sequence<[(variable: Int, value: Int)], [(variable: Int, value: Int)]>
  {
    chain(preconditionAtStart, preconditionEffectAtStart)
  }
  public var allPreconditionAtEnd: Chain2Sequence<[(variable: Int, value: Int)], [(variable: Int, value: Int)]>
  {
    chain(preconditionAtEnd, preconditionEffectAtEnd)
  }
  public var allPreconditionsExclusivelyAtEnd: [(variable: Int, value: Int)] {
    var allPreconditionAtEndSet = Set(allPreconditionAtEnd.map {Tuple($0.variable, $0.value)})
    allPreconditionAtEndSet.subtract(allPreconditionAtStart.map {Tuple($0.variable, $0.value)})
    allPreconditionAtEndSet.subtract(preconditionOverAll.map {Tuple($0.variable, $0.value)})
    return allPreconditionAtEndSet.map {(variable: $0.first, value: $0.second)}.sorted(by: {
      $0.variable < $0.variable || ($0.variable == $1.variable && $0.value < $1.value)
    })
  }
  public var asPlanAction: PlanAction {
    PlanAction(name: name, parameters: parameters)
  }
}

extension TemporalFDROperator {
  public var variables: Set<Int> {
    Set(allPreconditions.map {$0.variable} + effects.map {$0.variable})
  }
  public var allPreconditionsVariables: Set<Int> {
    Set(allPreconditions.map {$0.variable})
  }
  public func duration(in state: FDRState) -> Int {
    state.variablesValues[durationVariableIndex]
  }
  public func duration(in state: FDRStateRelaxed) -> Int {
    precondition(state.variablesValues[durationVariableIndex].count == 1)
    return state.variablesValues[durationVariableIndex].first!
  }
  private func isSatisfied(
    condition: [(variable: Int, value: Int)],
    in state: FDRStateRelaxed) -> Bool
  {
    for (variable, value) in condition {
      let stateVariableValues = state.variablesValues[variable]
      if !stateVariableValues.contains(value) {
        return false
      }
    }
    return true
  }
  private func isSatisfied(condition: [(variable: Int, value: Int)], in state: FDRState) -> Bool {
    for (variable, value) in condition {
      let stateVariableValue = state.variablesValues[variable]
      if value != stateVariableValue {
        return false
      }
    }
    return true
  }
  public func isApplicableAtStart(in state: FDRState) -> Bool {
    isSatisfied(condition: preconditionOverAll, in: state) &&
    isSatisfied(condition: preconditionAtStart, in: state) &&
    isSatisfied(condition: preconditionEffectAtStart, in: state)
  }
  public func isApplicableAtStart(in state: FDRStateRelaxed) -> Bool {
    isSatisfied(condition: preconditionOverAll, in: state) &&
    isSatisfied(condition: preconditionAtStart, in: state) &&
    isSatisfied(condition: preconditionEffectAtStart, in: state)
  }
  public func isApplicableAtEnd(in state: FDRState) -> Bool {
    isSatisfied(condition: preconditionOverAll, in: state) &&
    isSatisfied(condition: preconditionAtEnd, in: state) &&
    isSatisfied(condition: preconditionEffectAtEnd, in: state)
  }
  public func isApplicableAtEnd(in state: FDRStateRelaxed) -> Bool {
    isSatisfied(condition: preconditionOverAll, in: state) &&
    isSatisfied(condition: preconditionAtEnd, in: state) &&
    isSatisfied(condition: preconditionEffectAtEnd, in: state)
  }
}


extension TemporalProblemFDRCoding {
  public var instances: [PlanAction] {
    operators.map {PlanAction(name: $0.name, parameters: $0.parameters)}
  }
  public func action1InfluencesAction2(action1: Int, action2: Int) -> Bool {
    let op1 = operators[action1]
    let op2 = operators[action2]
    return !op1.variables.intersection(op2.allPreconditionsVariables).isEmpty
  }
}
