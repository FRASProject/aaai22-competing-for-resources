//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/18/21.
//

import Foundation
import Algorithms

public struct TemporalPlan: Hashable, Codable {
  public let actions: [PlanTemporalAction]
  public init(actions: [PlanTemporalAction]) {
    self.actions = actions
  }
  public init() {
    self.actions = []
  }
}

extension TemporalPlan {
  
  public func extractDeadlines(
    actionCriticalFacts: [PlanAction: [PDDLNameLiteral]],
    unitsParameters: Set<String>) -> [PDDLNameLiteral: Int]
  {
    var result = [PDDLNameLiteral: Int]()
    for action in self.actions {
      let planAction = PlanAction(action).removing(parameters: unitsParameters)
      for criticalFact in actionCriticalFacts[planAction] ?? []
      {
        result[criticalFact] = min(action.start, result[criticalFact] ?? action.start)
      }
    }
    return result
  }
  
  public func extractTimes(
    of criticalActions: Set<PlanAction>,
    unitsParameters: Set<String>) -> [PlanAction: Int]
  {
    var result = [PlanAction: Int]()
    for action in self.actions {
      let actionNoUnits = PlanAction(action).removing(parameters: unitsParameters)
      if criticalActions.contains(actionNoUnits)
      {
        result[actionNoUnits] = min(action.start, result[actionNoUnits] ?? action.start)
      }
    }
    return result
  }
  
  public func cost(
    function: [PDDLNameLiteral: [Int]],
    actionCriticalFacts: [PlanAction: [PDDLNameLiteral]],
    unitsParameters: Set<String>,
    missingGoalPenalty: [PDDLNameLiteral: Int]) -> Int
  {
    let planDeadlines = extractDeadlines(
      actionCriticalFacts: actionCriticalFacts,
      unitsParameters: unitsParameters)
    
    let cost = function.map { (criticalFact, costFunction) -> Int in
      if let deadline = planDeadlines[criticalFact] {
        if deadline < costFunction.count {
          return costFunction[deadline]
        } else {
          return costFunction.last ?? 0
        }
      } else {
        let missingGoalCost = missingGoalPenalty[criticalFact] ?? 0
        return missingGoalCost
      }
    }.reduce(0, +)
    return cost
  }
  
  public func costByNumberOfCriticalFactsPresent(
    actionCriticalFacts: [PlanAction: [PDDLNameLiteral]],
    unitsParameters: Set<String>) -> Int
  {
    let allCriticalFacts = Set(actionCriticalFacts.flatMap {$0.value})
    let planDeadlines = extractDeadlines(
      actionCriticalFacts: actionCriticalFacts,
      unitsParameters: unitsParameters)
    return allCriticalFacts.count - planDeadlines.keys.count
  }
  
  public var text: String {
    actions.map {$0.text}.joined(separator: "\n")
  }
  
  public var textNicely: String {
    let rows = actions.map {(String($0.start), $0.name, $0.parameters.joined(separator: " "), String($0.duration))}
    let collumnWidths = rows.reduce((0,0,0,0)) {
      (max($0.0, $1.0.count),
       max($0.1, $1.1.count),
       max($0.2, $1.2.count),
       max($0.3, $1.3.count))
    }
    let textRows = rows.map {
      $0.0.padding(toLength: collumnWidths.0, withPad: " ", startingAt: 0) + " " +
      $0.1.padding(toLength: collumnWidths.1, withPad: " ", startingAt: 0) + " " +
      $0.2.padding(toLength: collumnWidths.2, withPad: " ", startingAt: 0) + " " +
      $0.3.padding(toLength: collumnWidths.3, withPad: " ", startingAt: 0)
    }
    return textRows.joined(separator: "\n")
  }
  
  /// Merges individual unit's plans into one cooperative plan.
  /// First it identifies cooperative actions by comparing plans in individual plans and action in full domain.
  /// Actions that have more than one unit parameters are cooperative.
  /// Then it performs simulation of the individual plans. When it sees a cooperative actions, it tries to group individual plans
  /// action into cooperative.
  /// - Parameters:
  ///   - plans: Units' individial plans
  ///   - units: Units objects names
  ///   - domain: Full pddl domain
  /// - Returns: (Merged plan, not executable cooperative actions.)
  public static func mergingOld(
    plans: [TemporalPlan],
    units: Set<String>,
    in domain: PDDLDomain) -> (
      plan: TemporalPlan,
      removedActions: [PlanTemporalAction])
  {
    var normalActions = [PlanTemporalAction]()
    var notExecutableActions = [PlanTemporalAction]()
    var cooperativeActions = [PlanAction: [PlanTemporalAction]]()
    for plan in plans {
      for action in plan.actions {
        let domainAction = domain.getDurativeAction(by: action.name)!
        if domainAction.unitsParamaters.count <= 1 {
          normalActions.append(action)
        } else {
          let nonUnitParameters = action.parameters.filter { !units.contains($0)}
          cooperativeActions[
            PlanAction(name: action.name, parameters: nonUnitParameters),
            default: []].append(action)
        }
      }
    }
    for (actionWithoutUnits, actions) in cooperativeActions {
      let domainAction = domain.getDurativeAction(by: actionWithoutUnits.name)!
      let unitsParameters = domainAction.unitsParamaters
      let groupedActions = Dictionary(grouping: actions, by: {$0.start}).filter {
        $0.value.count >= unitsParameters.count
      }
      //TODO: Taking first group. Is it the best choice?
      if let group = groupedActions.first?.value {
        let units = group.flatMap { $0.parameters.filter {units.contains($0)} }
        precondition(unitsParameters.count == units.count, "Weird number of units.")
        let newParameters = PlanMerger.addUnitsParameter(
          unitsParametersOffsets: unitsParameters.map {$0.offset},
          unitParameters: units,
          nonUnitsParameters: actionWithoutUnits.parameters)
        normalActions.append(
          PlanTemporalAction(
            start: group.first!.start,
            name: actionWithoutUnits.name,
            parameters: newParameters,
            duration: group.first!.duration))
      } else {
        notExecutableActions.append(contentsOf: actions)
      }
    }
    normalActions.sort(by: {$0.start < $1.start})
    notExecutableActions.sort(by: {$0.start < $1.start})
    return (plan: TemporalPlan(actions: normalActions), removedActions: notExecutableActions)
  }
  
  public func removingParametersWith(prefixes: [String]) -> TemporalPlan {
    var newActions = [PlanTemporalAction]()
    for action in actions {
      newActions.append(
        PlanTemporalAction(
          start: action.start,
          name: action.name,
          parameters: action.parameters.filter {
            parameter in
            return prefixes.allSatisfy {
              parameter.findAllOccurenciesOf(regex: "^\($0)[0-9]+").isEmpty
            }},
          duration: action.duration))
    }
    return TemporalPlan(actions: newActions)
  }
  
  public func removingSuffixFromActionNames() -> TemporalPlan {
    let newActions =  actions.map {
      PlanTemporalAction(
        start: $0.start,
        name: $0.originalName,
        parameters: $0.parameters,
        duration: $0.duration)
    }
    return TemporalPlan(actions: newActions)
  }
  
  public init(classicalPlan: ClassicalPlan, durationOfUnitTimestamp: Int, removingNoopActions: Bool) {
    var newActions = [PlanTemporalAction]()
    for action in classicalPlan.actions {
      if removingNoopActions && action.name == PDDLAction.noopActionName {
        continue
      }
      let timestamps = action.parameters.enumerated().filter {
        !$0.element.findAllOccurenciesOf(regex: "^t[0-9]+").isEmpty
      }
      precondition(timestamps.count >= 3)
      precondition(timestamps.map {$0.offset}.adjacentPairs().allSatisfy {$0.0 + 1 == $0.1})
      let timestampsIndices = Set(timestamps.map {$0.offset})
      let start = Int(timestamps.first!.element.dropFirst())!
      let duration = Int(timestamps.last!.element.dropFirst())!
      let end = Int(timestamps[timestamps.count - 2].element.dropFirst())!
      precondition(start + duration == end)
      let newParameters = action.parameters.enumerated().filter {
        !timestampsIndices.contains($0.offset)}.map {$0.element}
      newActions.append(PlanTemporalAction(
        start: start * durationOfUnitTimestamp,
        name: action.name,
        parameters: newParameters,
        duration: duration * durationOfUnitTimestamp))
    }
    self.actions = newActions
  }
  
  public init(
    classicalPlan: ClassicalPlan,
    durationOfUnitTimestamp: Int,
    units: [String],
    removingNoopActions: Bool
  )
  {
    var newActions = [PlanTemporalAction]()
    var unitsTime = Dictionary(uniqueKeysWithValues: units.map {($0, 0)})
    for action in classicalPlan.actions {
      if removingNoopActions && action.name == PDDLAction.noopActionName {
        continue
      }
      let actionUnits = action.parameters.filter {unitsTime[$0] != nil}
      let numberOfTimestampParameters = actionUnits.count + 2
      let timestamps: Array = action.parameters.suffix(numberOfTimestampParameters)
      precondition(timestamps.count == numberOfTimestampParameters)
      let start = Int(timestamps[0].dropFirst())! * durationOfUnitTimestamp
      let realStartTime = max(actionUnits.map {unitsTime[$0]!}.max() ?? 0, start)
      let duration = Int(timestamps[actionUnits.count + 1].dropFirst())! * durationOfUnitTimestamp
      let realEndTime = realStartTime + duration
      for unit in actionUnits {
        unitsTime[unit] = realEndTime
      }
      newActions.append(PlanTemporalAction(
                        start: realStartTime,
                        name: action.name,
                        parameters: action.parameters.dropLast(numberOfTimestampParameters),
                        duration: duration))
    }
    self.actions = newActions
  }
  public init(raw: String) {
    var actions = [PlanTemporalAction]()
    for line in raw.components(separatedBy: "\n") where !line.isEmpty {
      if !line.starts(with: ";")  {
        let start: Int
        let duration: Int
        if line.contains(":") && line.contains("[") {
          let startPart = line[..<line.firstIndex(of: ":")!].trimmingCharacters(in: .whitespaces)
          start = Int(exactly: Double(startPart)!)!
          let durationPart = line[line.index(after: line.firstIndex(
            of: "[")!)..<line.firstIndex(of: "]")!]
          duration = Int(exactly: Double(durationPart)!)!
        } else {
          fatalError("Plan parsing error")
        }
        
        let actionPart = line[line.index(after: line.firstIndex(of: "(")!)..<line.lastIndex(of: ")")!]
          .trimmingCharacters(in: .whitespaces)
        let actionComponents = actionPart.components(separatedBy: " ")
        let actionName = actionComponents[0] //.uppercased()
        let actionArguments = Array(actionComponents[1...].map { $0.lowercased() })
        let pddlCommand = PlanTemporalAction(
          start: start,
          name: actionName,
          parameters: actionArguments,
          duration: duration)
        actions.append(pddlCommand)
      }
    }
    self.actions = actions
  }
  public static func parseCost(raw: String) -> Double? {
    var cost: Double? = nil
    for line in raw.components(separatedBy: "\n") where line.contains("cost") {
      if let range = line.range(of: #"-?\d+\.?\d*"#, options: .regularExpression) {
        cost = Double(line[range])!
      }
    }
    return cost
  }
}


public struct TemporalPlannerOutput {
  public let raw: String
  public let rawWithoutComments: String
  public let parsed: TemporalPlan
  public let cost: Double?
  public init<A: PlanActionProtocol>(actions: [A], cost: Double?) {
    self.cost = cost
    fatalError("TODO: dediscretize")
  }
  public init(raw: String) {
    let cost = TemporalPlan.parseCost(raw: raw)
    self.raw = raw
    self.rawWithoutComments = PlanParser.removeComments(raw)
    self.parsed = TemporalPlan(raw: raw)
    self.cost = cost
  }
}

