//
//  PDDLPlan.swift
//  FRASLib
//
//  Created by Pavel Rytir on 7/4/20.
//

import Foundation

public struct ClassicalPlan: Hashable, Codable {
  public let actions: [PlanAction]
}

extension ClassicalPlan: PureStrategy {}

extension ClassicalPlan {
  public func filterOutFailingGoalsActions() -> ClassicalPlan {
    ClassicalPlan(
      actions: actions.filter { !$0.name.starts(with: PDDLAction.failingGoalActionPrefix) })
  }
  public func filterOutNoOpActions() -> ClassicalPlan {
    ClassicalPlan(actions: actions.filter { $0.name != PDDLDomain.noOpActionName})
  }
}

public struct PlannerClassicalOutput {
  public init(_ raw: String) {
    self.raw = raw
    self.rawWithoutComments = PlanParser.removeComments(raw)
    self.parsed = nil
    self.cost = nil
  }
  public init(raw: String, parsed: ClassicalPlan, cost: Double?) {
    self.raw = raw
    self.parsed = parsed
    self.cost = cost
    self.rawWithoutComments = PlanParser.removeComments(raw)
  }
  public let raw: String
  public let rawWithoutComments: String
  public let parsed: ClassicalPlan?
  public let cost: Double?
}
