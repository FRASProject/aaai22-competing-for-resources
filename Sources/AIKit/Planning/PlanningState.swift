//
//  PlanningState.swift
//  AIKit
//
//  Created by Pavel Rytir on 7/12/20.
//

import Foundation

public protocol PlanningState: Hashable, Codable {}

