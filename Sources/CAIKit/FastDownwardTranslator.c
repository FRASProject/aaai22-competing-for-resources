//
//  File.c
//  
//
//  Created by Pavel Rytir on 9/15/21.
//

#include "FastDownwardTranslator.h"
#include <Python.h>

const char* fastdownward_translate(const char* domainPddl, const char* problemPddl, int verbose) {
  // This function is leaking somewhere.
  PyObject *pName, *pModule, *pFunc;
  PyObject *pArgs, *pValue, *repr;
  char* fValue = NULL;
  //Py_Initialize();
  pName = PyUnicode_FromString("translate2"); //calling our module
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);
  if (pModule != NULL) {
    pFunc = PyObject_GetAttrString(pModule, "main2"); //calling our function
    if (pFunc && PyCallable_Check(pFunc)) {
      pArgs = PyTuple_Pack(3, PyUnicode_DecodeFSDefault(domainPddl), PyUnicode_DecodeFSDefault(problemPddl),
                           PyLong_FromLong(verbose)); //our arguments
      pValue = PyObject_CallObject(pFunc, pArgs); //executing the function and returning value from python
      repr = PyObject_Str(pValue);
      PyObject *str = PyUnicode_AsEncodedString(repr, "utf-8", "strict");
      char* decoded = PyBytes_AsString(str);
      fValue = strdup(decoded); //need to test parsing of outcome argument
      Py_XDECREF(pValue);
      Py_XDECREF(repr);
      Py_XDECREF(str);
      Py_DECREF(pArgs);
    }
    else {
      printf("FUNCTION DOENSN`T EXIST OT NOT CALLABLE ");
    }
    Py_XDECREF(pFunc);
  }
  else {
    PyErr_Print();
    printf("MODULE NOT FOUND ERROR ");
  }
  Py_XDECREF(pModule);
  //Py_Finalize();
  return fValue;
}
