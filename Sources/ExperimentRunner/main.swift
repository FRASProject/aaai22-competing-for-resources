//
//  main.swift
//  UAVFRAS
//
//  Created by Pavel Rytir on 9/15/20.
//

import Foundation
import AIKit
import ArgumentParser
//import Yams


struct ExperimentRunner: ParsableCommand {
  
  @Option(help: "A filename with general FRAS framework configuration.")
  var frasconfig: String
  
  @Option(help: "A filename with experiment configuration.")
  var experimentconfig: String
  
  @Option(help: "A filename with experiment configuration.")
  var fastdownwardaliases: String?
  
  @Option(help: "A name of experiment to run.")
  var experiment: String = "PDDL_DO_Sampling_Baseline"
  
  @Option(help: "Number of task to run simultaneously")
  var ntasks: Int = 1
  
  @Option(help: "Analytics engine")
  var analytics: String?
  
  mutating func run() throws {

    let frasConfig = try AIKitData(
      withJSON: Data(contentsOf: URL(fileURLWithPath: frasconfig)))
    let fastDownwardAliases = try fastdownwardaliases.map {
      try JSONDecoder().decode(
        Dictionary<String,[String]>.self,
        from: Data(contentsOf: URL(fileURLWithPath: $0)))
    } ?? [:]
    let config = try AIKitData(
      withJSON: Data(contentsOf: URL(fileURLWithPath: experimentconfig)))
    ExperimentManager.initDefault(
      experimentConfig: config,
      systemConfig: frasConfig.dictionary!,
      fastDownwardAliases: fastDownwardAliases,
      maxNumberOfConcurrentTasks: ntasks,
      currentWorkingDirectory: FileManager.default.currentDirectoryPath,
      analytics: analytics)
    
    print("Starting experiment \(experiment)")
    print("Running \(ntasks) simultaneously")
    
    
    
    //try Yams.dump(object: config).write(to: URL(fileURLWithPath: "experiment_config.yaml"), atomically: false, encoding: .utf8)
    
    switch experiment {
//    case "PDDL_DO_pruning_ordering_simanneal":
//      // Experiments from Planning in Adversarial Domains - SoCS 2020
//      let e = CombinatorExperiment<PDDLDoubleOracleAndExploitability.Result, PDDLDoubleOracleAndExploitability>(configKeys: ["games"])
//      try e.run()
    case "PDDL_DO_Sampling_Baseline":
      // Experiments from Estimating Opponent Strategy in Adversarial Domains - AAAI 2022
      // Estimating Opponent Strategy in Adversarial Domains - SoCS 2021
      let e = CombinatorExperiment<SamplingAdversarialPlanningExperiment>(configKeys: ["games"])
      try e.run()
    case "PDDL_Double_Oracle":
      let e = CombinatorExperiment<PDDLDoubleOracleExperiment>(configKeys: ["games", "DoubleOracle"])
      try e.run()
    case "PlanningGames":
      // Some experiments from Optimal Mixed Strategies for Cost-Adversarial Planning Games - ICAPS 2022
      let e = CombinatorExperiment<PlanningGameDOSymkExperiment>(configKeys: ["games"])
      try e.run()
    case "PlanningGamesAbstractions":
      let e = CombinatorExperiment<PlanningGamesExperiment>(configKeys: ["games"])
      try e.run()
    case "DistributedAdversarialPlanning", "PDDL_DO_Double_Pruning", "SerialBestResponseDO", "PDDL_DO_pruning_ordering_simanneal":
      // Experiments from IJCAI 2022
      // Experiments from Planning in Adversarial Domains - SoCS 2020
      let e = MultiGameDoubleOracleExploitabilityExperiment()
      try e.run()
    default:
      fatalError("Invalid experiment name")
    }
  }
}

ExperimentRunner.main()
