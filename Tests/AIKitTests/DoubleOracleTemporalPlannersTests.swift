//
//  DoubleOracleTemporalPlannersTests.swift
//  FRASTests
//
//  Created by Pavel Rytir on 10/09/2018.
//

import XCTest
@testable import AIKit


//final class DoubleOracleTemporalPlannersTests: XCTestCase {
//    func testLPGInitLPGBestResponse3UAVNoCommDelaunayAbstraction0() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, numUAVP2: i, numResources: i, singleControlNode : false, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .noCommunication)
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = DelaunayAbstraction(numberOfSubdivisions: 0)
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 30, maxTime: 20, verbose: verbose, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .noCommunication)
//        let bestResponsePlanner = TemporalBestResponsePlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: true, mode: .noCommunication, verbose: true)
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: bestResponsePlanner, bestResponsePlannerP2: bestResponsePlanner, randomSeed: 346346, epsilon: FRASLibConfig.defaultEpsilon)
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        //print(stats)
//    }
//    
//    func testLPGInitLPGBestResponse3UAVNoCommCompletePOIAbstraction0() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, numUAVP2: i, numResources: i, singleControlNode : false, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .noCommunication)
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = CompletePOIAbstraction(numberOfSubdivisions: 0)
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 30, maxTime: 20, verbose: verbose, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .noCommunication)
//        let bestResponsePlanner = TemporalBestResponsePlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: true, mode: .noCommunication, verbose: true)
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: bestResponsePlanner, bestResponsePlannerP2: bestResponsePlanner, randomSeed: 346346, epsilon: FRASLibConfig.defaultEpsilon)
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        //print(stats)
//    }
//    
//    func testLPGInitLPGBestResponse3UAVNoCommDelaunayAbstraction4SoftDeadlines() {
//        // This takes around 3 minutes.
//        let i = 3
//        let verbose = false
//        let gridX = 3 * i + (i % 2 == 0 ? 1 : 0)
//        let gridY = 2 * i + (i % 2 == 0 ? 0 : 1)
//        let gridZ = 1
//        let communicationRange = ceil(sqrt(Double(gridX) / 2 + Double(gridY) / 2 + Double(i) / 2))
//        let gridGameConfig = FRASGridGameGeneratorConfig(x: gridX, y: gridY, z: gridZ, numUAVP1: i, numUAVP2: i, numResources: i, singleControlNode : false, spreadResources: false, length: Double(10 * (gridX - 1)), width: Double(10 * (gridY - 1)), height: Double(1 * gridZ), communicationRange: communicationRange, communicationMode: .noCommunication)
//        let game = generateSimpleGridGame(from: gridGameConfig)
//        
//        let abstraction = DelaunayAbstraction(numberOfSubdivisions: 4)
//        
//        let lpgConfig = LPGPlanner.LPGConfig(numberOfIterations: 30, maxTime: 30, verbose: verbose, plannerLPGExecutablePath: LPGPlanner.LPGConfig.defaultConfig.plannerLPGExecutablePath)
//        
//        let initPlanner = TemporalPlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction,  mode: .noCommunication)
//        let bestResponsePlanner = TemporalBestResponsePlanner(planner: LPGPlanner(plannerConfig: lpgConfig, verbose: verbose), abstraction: abstraction, hardDeadlines: false, mode: .noCommunication, verbose: true)
//        
//        let doubleOracleConfiguration = DoubleOracleConfiguration(numberOfInitialPlans: 1, maximumNumberOfAttemptsToImproveEquilibriumApprox: 2, initialPlansPlannerP1: initPlanner, initialPlansPlannerP2: initPlanner, bestResponsePlannerP1: bestResponsePlanner, bestResponsePlannerP2: bestResponsePlanner, randomSeed: 346346, epsilon: FRASLibConfig.defaultEpsilon)
//        
//        var doubleOracle = try! DoubleOracleAlgorithm(game: game, configuration: doubleOracleConfiguration, plansCacheDir: nil, verbose: verbose)
//        
//        let equilibriumReached = try! doubleOracle.run(numberOfIterations: 20)
//        
//        XCTAssertTrue(equilibriumReached)
//        let stats = doubleOracle.log.map { $0.statItem }
//        print(stats)
//        
//        XCTAssert(abs(stats.last!.player1EqVal - 1.5) < 0.25)
//        XCTAssert(abs(stats.last!.player2EqVal - 1.5) < 0.25)
//        
//        
//    }
//    
//    static var allTests = [
//        ("testLPGInitLPGBestResponse3UAVNoCommDelaunayAbstraction0", testLPGInitLPGBestResponse3UAVNoCommDelaunayAbstraction0),
//        ]
//
//}
